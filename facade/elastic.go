package facade

import (
	"github.com/olivere/elastic/v7"
)

type ElasticHandler interface {
	Debug(debug bool)
	Host() string
	Port() int
	Client() *elastic.Client
	AdvancedSearch(indexName string, includeField []string, excludeFields []string, storedFields []string, sort map[string]string, from int64, size int64, query string) (int64, []interface{})
	Search(indexName string, source []string, fields []string, sort []map[string]string, from int64, size int64, query string) (int64, []interface{})
	//GetDocumentId(indexName string, source []string, fields map[string]interface{}) (bool, string)
	//GetByField(indexName string, source []string, fields map[string]interface{}) []interface{}
	DeleteById(indexName, id string) bool
	GetDetailById(indexName string, includeFields []string, excludeFields []string, objId string) []interface{}
	GetDetail(indexName string, includeFields []string, excludeFields []string, fields map[string]interface{}) (interface{}, error)
	GetId(indexName string, fields []map[string]interface{}) (string, error)
	Upsert(indexName string, id string, document interface{}) (string, error)
	Update(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error)
	Add(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error)
	ReplaceWithKey(key string, value interface{}, indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error)
	Insert(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error)
	DisableIndex(indexName string)
	EnableIndex(indexName string)
	SetMaxResult(indexName string, max int64)
	ScriptUpdate(indexName string, _id string, script string, params map[string]interface{}) (bool, error)
	SearchMatch(dataContext *DataContext, excludeFields []string, includeFields []string, sort map[string]string, query elastic.Query) ([]interface{}, int64, error)
	FindDetail(indexName string, key string, value interface{}) ([]interface{}, error)
	FindDetailMultiTerm(indexName string, term []map[string]interface{}) ([]interface{}, error)
	PrepareTerm(term []map[string]interface{}, field string, value interface{}) []map[string]interface{}
	PageToOffset(page int64, limit int64) (int64, int64)
	Aggregate(indexName string, term []map[string]interface{}, field string, script string) ([]interface{}, error)
	UpdateByScript(scriptName string, indexName string, _id string, params map[string]interface{}) error
	PrintSource(src interface{}, err error)
}
