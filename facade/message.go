package facade

type ErrorMessage struct {
	Code    string `structs:"error_code" json:"error_code" mapstructure:"error_message"`
	Message string `structs:"error_message" json:"error_message" mapstructure:"error_message"`
}

const (
	LIC_GLR0001 = "Hostname is empty."
	LIC_GLR0002 = "Can't extract network information."
	LIC_GLR0003 = "Private key store problem or invalid."
	LIC_GLR0004 = "License service failure."
	LIC_GLR0005 = "Invalid crypted text."
	LIC_GLI0001 = "Public key store problem or invalid"
	LIC_GLI0002 = "License request is invalid"
	LIC_GLI0003 = "License verify failure."
	LIC_GLI0004 = "Invalid license signature."
	LIC_GLI0005 = "Unmarshal license request failure."
	LIC_GLI0006 = "Marshal license information failure."
	LIC_GLI0007 = "Private key store problem or invalid."
	LIC_GLI0008 = "License service failure."
	LIC_GLR0009 = "Invalid crypted text."
	LIC_ACL0001 = "Application identifier is not match."
	LIC_ACL0002 = "License is already activated."
	LIC_ACL0003 = "License host credential is invalid."
	LIC_ACL0004 = "License was expired."
	LIC_ACL0005 = "License activate failure."
	LIC_VHC0001 = "Hostname is not match in activation."
	LIC_VHC0002 = "Can't extract network information."
	LIC_VHC0003 = "Hardware MAC address is not match."
	LIC_VHC0004 = "Psysical hardware MAC address is not match."
	LIC_VAL0001 = "License file not found or invalid."
	LIC_VAL0002 = "Application identifier is not match."
	LIC_VAL0003 = "License is already activated."
	LIC_VAL0004 = "License host credential is invalid."
	LIC_VAL0005 = "License was expired."
)
