package facade

import (
	"time"
)

// Cache handler is the caching data by using Redis service.
// Cache is a Redis client automatically representing a pool of zero or more
// underlying connections. It's safe for concurrent use by multiple
// goroutines.
type CacheHandler interface {

	// Find keys by patterns Ex. mydata*
	Keys(keyPattern string) ([]string, error)

	IsExist(key string) bool

	// Delete key from cache.
	Del(key string) error

	// Get value from key.
	// Ex. Get("xxx", true) means when found the data from key it's will remove the entry.
	//     Get("yyy", false) means when found the data from key the entry still live in the cache.
	Get(key string, autoRemove bool) (string, error)

	// Get value from key.
	// Ex. Get("xxx", true) means when found the data from key it's will remove the entry.
	//     Get("yyy", false) means when found the data from key the entry still live in the cache.
	// Return json parser handler.
	GetJson(key string, autoRemove bool) (JsonHandler, error)

	// Set cache value to key.
	// Ex. Set("xxx", "{"a": "11"}", 60*time.Second) means setting new cache value from key with the expiration 60 seconds.
	//	   The duration can be use as Second, Minute, Hour
	Set(key string, value string, expiration time.Duration) error

	// Set cache value to key.
	// Ex. Set("xxx", model, 60*time.Second) means setting new cache value from key with the expiration 60 seconds.
	//	   The duration can be use as Second, Minute, Hour
	// Value must be map[string]interface{} or struct model.
	SetJson(key string, value interface{}, expiration time.Duration) error
}
