package facade

import (
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/context"
)

type AppFilter interface {
	JsonFilter() func(ctx *context.Context)
	CredentialFilter() func(ctx *context.Context)
	VersionFilter(versionList []string) func(ctx *context.Context)
	BotFilter() func(ctx *context.Context)
	AppKeyCacheFilter(beegoCache cache.Cache) func(ctx *context.Context)
	AppKeyFilter(authKey string) func(ctx *context.Context)
	LicenseFilter(authKey string) func(ctx *context.Context)
}
