package passlib

import "fmt"

// Indicates that password verification failed because the provided password
// does not match the provided hash.
var ErrInvalidPassword = fmt.Errorf("invalid password")

// Indicates that password verification is not possible because the hashing
// scheme used by the hash provided is not supported.
var ErrUnsupportedScheme = fmt.Errorf("unsupported scheme")
