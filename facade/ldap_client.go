package facade

import (
	"crypto/tls"
)

type LDAPConfig struct {
	Attributes         []string
	Base               string
	BindDN             string
	BindPassword       string
	GroupFilter        string // e.g. "(memberUid=%s)"
	Host               string
	ServerName         string
	UserFilter         string // e.g. "(uid=%s)"
	Port               int
	InsecureSkipVerify bool
	UseSSL             bool
	SkipTLS            bool
	TrustCertFilePath  string
	ClientCertificates []tls.Certificate // Adding client certificates
}
type LDAPClient interface {
	// Set configuration.
	Config(config *LDAPConfig)

	Connect() error
	// Close closes the ldap backend connection.
	Close()
	// Authenticate authenticates the user against the ldap backend.
	Authenticate(username, password string) (bool, map[string]string, error)
	// GetGroupsOfUser returns the group for a user.
	GetGroupsOfUser(username string) ([]string, error)
}
