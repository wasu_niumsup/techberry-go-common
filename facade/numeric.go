package facade

type NumericHandler interface {
	NumDecPlaces(v float64) int
	ToDecimal64(num float64, precision int) float64
	ToDecimal(num float32, precision int) float32
	Round(num float64) int
	Int(i int, defaultValue int) int
	Int64(i int64, defaultValue int64) int64
	ForceInt64(v interface{}, defaultValue int64) int64
	ForceInt(v interface{}, defaultValue int) int
}
