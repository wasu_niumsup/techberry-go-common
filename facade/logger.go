package facade

import (
	"fmt"
	"time"
)

const (
	L_CONSOLE = iota
	L_FILE
	L_ELASTIC
	L_KAFKA
)

type Logger interface {
	ReloadLevel(level int)
	Debug(v ...interface{})
	DebugFormat(format string, v ...interface{})
	Info(v ...interface{})
	InfoFormat(format string, v ...interface{})
}

type LogModuleInfo struct {
	ModuleTitle   string
	ModuleVersion string
	ModuleContact string
}

type LogEvent interface {
	Kafka(bootstrapServers string, topic string)
	SetModuleInfo(info *LogModuleInfo) LogEvent
	SetXTrace(flag bool)
	SetOutput(outputType int)
	Info() LogEvent
	Debug() LogEvent
	Error() LogEvent
	Trace() LogEvent
	Msgf(format string, v ...interface{})
	Msg(msg string)
	// Str adds the field key with val as a string to the logger context.
	Str(key, val string) LogEvent

	// Strs adds the field key with val as a string to the logger context.
	Strs(key string, vals []string) LogEvent

	// Stringer adds the field key with val.String() (or null if val is nil) to the logger context.
	Stringer(key string, val fmt.Stringer) LogEvent

	// Bytes adds the field key with val as a []byte to the logger context.
	Bytes(key string, val []byte) LogEvent

	// Hex adds the field key with val as a hex string to the logger context.
	Hex(key string, val []byte) LogEvent

	// RawJSON adds already encoded JSON to context.
	//
	// No sanity check is performed on b; it must not contain carriage returns and
	// be valid JSON.
	RawJSON(key string, b []byte) LogEvent

	AnErr(key string, err error) LogEvent

	// Errs adds the field key with errs as an array of serialized errors to the
	// logger context.
	Errs(key string, errs []error) LogEvent

	// Err adds the field "error" with serialized err to the logger context.
	Err(err error) LogEvent

	// Bool adds the field key with val as a bool to the logger context.
	Bool(key string, b bool) LogEvent

	// Bools adds the field key with val as a []bool to the logger context.
	Bools(key string, b []bool) LogEvent

	// Int adds the field key with i as a int to the logger context.
	Int(key string, i int) LogEvent

	// Ints adds the field key with i as a []int to the logger context.
	Ints(key string, i []int) LogEvent

	// Int8 adds the field key with i as a int8 to the logger context.
	Int8(key string, i int8) LogEvent

	// Ints8 adds the field key with i as a []int8 to the logger context.
	Ints8(key string, i []int8) LogEvent

	// Int16 adds the field key with i as a int16 to the logger context.
	Int16(key string, i int16) LogEvent

	// Ints16 adds the field key with i as a []int16 to the logger context.
	Ints16(key string, i []int16) LogEvent

	// Int32 adds the field key with i as a int32 to the logger context.
	Int32(key string, i int32) LogEvent

	// Ints32 adds the field key with i as a []int32 to the logger context.
	Ints32(key string, i []int32) LogEvent

	// Int64 adds the field key with i as a int64 to the logger context.
	Int64(key string, i int64) LogEvent

	// Ints64 adds the field key with i as a []int64 to the logger context.
	Ints64(key string, i []int64) LogEvent

	// Uint adds the field key with i as a uint to the logger context.
	Uint(key string, i uint) LogEvent

	// Uints adds the field key with i as a []uint to the logger context.
	Uints(key string, i []uint) LogEvent

	// Uint8 adds the field key with i as a uint8 to the logger context.
	Uint8(key string, i uint8) LogEvent

	// Uints8 adds the field key with i as a []uint8 to the logger context.
	Uints8(key string, i []uint8) LogEvent

	// Uint16 adds the field key with i as a uint16 to the logger context.
	Uint16(key string, i uint16) LogEvent

	// Uints16 adds the field key with i as a []uint16 to the logger context.
	Uints16(key string, i []uint16) LogEvent

	// Uint32 adds the field key with i as a uint32 to the logger context.
	Uint32(key string, i uint32) LogEvent

	// Uints32 adds the field key with i as a []uint32 to the logger context.
	Uints32(key string, i []uint32) LogEvent

	// Uint64 adds the field key with i as a uint64 to the logger context.
	Uint64(key string, i uint64) LogEvent

	// Uints64 adds the field key with i as a []uint64 to the logger context.
	Uints64(key string, i []uint64) LogEvent

	// Float32 adds the field key with f as a float32 to the logger context.
	Float32(key string, f float32) LogEvent

	// Floats32 adds the field key with f as a []float32 to the logger context.
	Floats32(key string, f []float32) LogEvent

	// Float64 adds the field key with f as a float64 to the logger context.
	Float64(key string, f float64) LogEvent

	// Floats64 adds the field key with f as a []float64 to the logger context.
	Floats64(key string, f []float64) LogEvent

	// Timestamp adds the current local time as UNIX timestamp to the logger context with the "time" key.
	// To customize the key name, change zerolog.TimestampFieldName.
	//
	// NOTE: It won't dedupe the "time" key if the *Context has one already.
	Timestamp() LogEvent

	// Time adds the field key with t formated as string using zerolog.TimeFieldFormat.
	Time(key string, t time.Time) LogEvent

	// Times adds the field key with t formated as string using zerolog.TimeFieldFormat.
	Times(key string, t []time.Time) LogEvent

	// Dur adds the fields key with d divided by unit and stored as a float.
	Dur(key string, d time.Duration) LogEvent

	// Durs adds the fields key with d divided by unit and stored as a float.
	Durs(key string, d []time.Duration) LogEvent

	// Interface adds the field key with obj marshaled using reflection.
	Interface(key string, i interface{}) LogEvent

	MsgError(err error)
	OnDemandTrace(format string, v ...interface{})
}
