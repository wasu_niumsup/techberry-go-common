package facade

import "time"

type MapHandler interface {
	Time(inputMap map[string]interface{}, keyPath string, defaultValue time.Time) time.Time
	String(inputMap map[string]interface{}, key string, defaultValue string) string
	Int64(inputMap map[string]interface{}, key string, defaultValue int64) int64
	Int(inputMap map[string]interface{}, key string, defaultValue int) int
	Float(inputMap map[string]interface{}, key string, defaultValue float64) float64
	Bool(inputMap map[string]interface{}, key string, defaultValue bool) bool
	GetArray(inputMap map[string]interface{}, keyPath string) []interface{}

	// GetByPath get value from a map[string]interface{}. eg "top" "top.sub"
	GetByPath(key string, mp map[string]interface{}) (val interface{}, ok bool)

	// Keys get all keys of the given map.
	Keys(mp interface{}) (keys []string)

	// Values get all values from the given map.
	Values(mp interface{}) (values []interface{})

	// LowerKeys convert keys to lower case.
	LowerKeys(f interface{}) interface{}

	// To marshal model into byte then unmarshal into map.
	ToMap(v interface{}) map[string]interface{}

	// To unmarshal JSON into an interface value, Unmarshal stores one of these in the interface value:
	FromJson(data []byte, v interface{}) error

	// Convert map interface value into json byte value.
	ToJson(v interface{}) ([]byte, error)

	// Binding struct from map.
	ToStruct(data interface{}, result interface{}) error

	// Fill struct from map.
	FillStruct(data map[string]interface{}, result interface{})
}
