package facade

type DFQueryResult struct {
	Hits struct {
		Total int64                    `structs:"total" json:"total" bson:"total"`
		Data  []map[string]interface{} `structs:"data" json:"data" bson:"data"`
	} `structs:"hits" json:"hits" bson:"hits"`
	Latencies struct {
		Prepare float64 `structs:"prepare" json:"prepare" bson:"prepare"`
		Execute float64 `structs:"execute" json:"execute" bson:"execute"`
	} `structs:"latencies" json:"latencies" bson:"latencies"`
}

type DFResult struct {
	Status      bool        `structs:"total" json:"total" bson:"total"`
	RowAffected interface{} `structs:"rowAffected" json:"rowAffected" bson:"rowAffected"`
	Took        float64     `structs:"took" json:"took" bson:"took"`
}

type DataFactory interface {
	SetBasicAuth(username string, passwd string) DataFactory
	SetApiKey(key string, value string) DataFactory
	ExecuteQuery(group string, name string, data interface{}) (DFQueryResult, error)
	ExecuteUpdate(group string, action string, id string, data interface{}) (DFResult, error)
	SelectOne(group string, mapper_id string, params interface{}) (map[string]interface{}, error)
	SelectMany(group string, mapper_id string, params interface{}) ([]map[string]interface{}, error)
}
