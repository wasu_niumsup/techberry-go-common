package facade

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type TimeHandler interface {
	SetLocation(loc string)
	CurrentTimeMillis() int64
	DayDiffFromNow(year int, month int, day int, hour int, min int, sec int) int
	TimeMillisFromDate(date time.Time) int64
	DateTimeMillis(year int, month int, day int, hour int, min int, sec int) time.Time
	TimeMillis(year int, month int, day int, hour int, min int, sec int) int64
	TimeMillisFromNow(year int, month int, day int) (int64, int64)
	Now() string
	CurrentDateTime(layout string) time.Time
	ParseDateTime(layout string, value string) (time.Time, error)
	ParseTimestamp(data string) (time.Time, error)
	MonthIndex(month time.Month) int
}

// Timestamp is a time but it serializes to ISO8601 format with millis
type LocalDateTime struct {
	time.Time
}

// ISO8601 format to millis instead of to nanos
const RFC3339Millis = "2006-01-02T15:04:05.000Z"

// ParseTimestamp parses a string that represents an ISO8601 time or a unix epoch
func ParseTimestamp(data string) (LocalDateTime, error) {
	d := time.Now().UTC()
	if data != "now" {
		// fmt.Println("we should try to parse")
		dd, err := time.Parse(RFC3339Millis, data)
		if err != nil {
			dd, err = time.Parse(time.RFC3339, data)
			if err != nil {
				dd, err = time.Parse(time.RFC3339Nano, data)
				if err != nil {
					if data == "" {
						data = "0"
					}
					t, err := strconv.ParseInt(data, 10, 64)
					if err != nil {
						return LocalDateTime{}, err
					}
					dd = time.Unix(0, t*int64(time.Millisecond))
				}
			}
		}
		d = dd
	}
	return LocalDateTime{Time: d.UTC()}, nil
}

// UnmarshalJSON implements the json unmarshaller interface
func (t *LocalDateTime) UnmarshalJSON(data []byte) error {
	var value interface{}
	json.Unmarshal(data, &value)

	switch value.(type) {
	case string:
		v := value.(string)
		if v == "" {
			return nil
		}
		d, err := ParseTimestamp(v)
		if err != nil {
			return err
		}
		*t = d
	case float64:
		*t = LocalDateTime{time.Unix(0, int64(value.(float64))*int64(time.Millisecond)).UTC()}
	default:
		return fmt.Errorf("Couldn't convert json from (%T) %s to a time.Time", value, data)
	}
	return nil
}
