package facade

import (
	"context"

	"go.opencensus.io/trace"
)

type MediationContext interface {
	Logger() Logger
	Data() interface{}
	SetData(data interface{})
	SetTraceContext(TraceContext)
	TraceContext() TraceContext
	RootModel() *TraceModel
	ParentModel() *TraceModel
	LastModel() *TraceModel
	RootCtx() context.Context
	ParentCtx() context.Context
	LastCtx() context.Context
	StartSpan(name string, o ...trace.StartOption) Span
	Span(ctx context.Context, name string, o ...trace.StartOption) Span
}
