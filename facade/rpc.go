package facade

type JsonRpcRequest struct {
	AppKey  string      `json:"appKey"`
	JsonRpc string      `json:"jsonrpc"`
	Id      int         `json:"id"`
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`
}

type JsonRpcResponse struct {
	JsonRpc      string      `json:"jsonrpc"`
	Id           int         `json:"id"`
	Result       interface{} `json:"result"`
	ErrorContext struct {
		Code    string      `json:"code"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	} `json:"error"`
}
