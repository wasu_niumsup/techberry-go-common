package facade

type criterion []interface{}

/*
Criteria is a set of criterion, each criterion is a triple (field_name, operator, value).
It allow you to search models.
see documentation: https://www.odoo.com/documentation/13.0/reference/orm.html#reference-orm-domains
*/
type Criteria []*criterion

// NewCriteria creates a new *Criteria.
func NewCriteria() *Criteria {
	return &Criteria{}
}

func newCriterion(field, operator string, value interface{}) *criterion {
	c := criterion(newTuple(field, operator, value))
	return &c
}

// Add a new criterion to a *Criteria.
func (c *Criteria) Add(field, operator string, value interface{}) *Criteria {
	*c = append(*c, newCriterion(field, operator, value))
	return c
}

func newTuple(values ...interface{}) []interface{} {
	t := make([]interface{}, len(values))
	for i, v := range values {
		t[i] = v
	}
	return t
}

// Options allow you to filter search results.
type Options map[string]interface{}

// NewOptions creates a new *Options
func NewOptions() *Options {
	opt := Options(make(map[string]interface{}))
	return &opt
}

// Offset adds the offset options.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#pagination
func (o *Options) Offset(offset int) *Options {
	return o.Add("offset", offset)
}

// Limit adds the limit options.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#pagination
func (o *Options) Limit(limit int) *Options {
	return o.Add("limit", limit)
}

// FetchFields allow you to precise the model fields you want odoo to return.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#search-and-read
func (o *Options) FetchFields(fields ...string) *Options {
	ff := []string{}
	for _, f := range fields {
		ff = append(ff, f)
	}
	return o.Add("fields", ff)
}

// AllFields is useful for FieldsGet function. It represents the fields to document
// you want odoo to return.
// https://www.odoo.com/documentation/13.0/reference/orm.html#fields-views
func (o *Options) AllFields(fields ...string) *Options {
	ff := []string{}
	for _, f := range fields {
		ff = append(ff, f)
	}
	return o.Add("allfields", ff)
}

// Attributes is useful for FieldsGet function. It represents the attributes to document
// you want odoo to return.
// https://www.odoo.com/documentation/13.0/reference/orm.html#fields-views
func (o *Options) Attributes(attributes ...string) *Options {
	aa := []string{}
	for _, a := range attributes {
		aa = append(aa, a)
	}

	return o.Add("attributes", aa)
}

// Add on option by providing option name and value.
func (o *Options) Add(opt string, v interface{}) *Options {
	(*o)[opt] = v
	return o
}

type OdooXmlRpcClient interface {
	Create(model string, values interface{}) (int64, error)
	Update(model string, ids []int64, values interface{}) error
	Delete(model string, ids []int64) error
	SearchRead(model string, criteria *Criteria, options *Options, elem interface{}) error
	Read(model string, ids []int64, options *Options, elem interface{}) error
	Count(model string, criteria *Criteria, options *Options) (int64, error)
	Search(model string, criteria *Criteria, options *Options) ([]int64, error)
	FieldsGet(model string, options *Options) (map[string]interface{}, error)
	ExecuteKw(method, model string, args []interface{}, options *Options) (interface{}, error)
}
