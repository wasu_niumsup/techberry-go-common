package facade

type AesCryptoHandler interface {
	PaddingEncrypt(key []byte, text string) (string, error)
	PaddingDecrypt(key []byte, text string) (string, error)
	SimpleEncrypt(text string) string
	Encrypt(key []byte, text string) string
	SimpleDecrypt(cryptoText string) string
	Decrypt(key []byte, cryptoText string) string
}
