package facade

import (
	"context"
	"io"
	"net/http"
	"net/url"
	"time"
)

type HttpClient interface {
	SetHostURL(host_url string) HttpClient
	SetHeaders(map[string]string) HttpClient
	SetHeader(key string, value string) HttpClient
	SetAuthHeader(token string) HttpClient
	SetBasicAuth(username string, password string) HttpClient
	SetJsonType() HttpClient
	SetFormType() HttpClient
	SetPlainTextType() HttpClient
	SetTimeout(timeout time.Duration) HttpClient
	DisableSecurityCheck() HttpClient
	SetBody(body interface{}) HttpClient
	HookRequest(m func(HttpClient, Request) error) HttpClient
	HookResponse(m func(HttpClient, Response) error) HttpClient
	PostUri(uri string) (Response, error)
	Post(endpoint string) (Response, error)
	GetUri(uri string) (Response, error)
	Get(endpoint string) (Response, error)
	PutUri(uri string) (Response, error)
	Put(endpoint string) (Response, error)
	PatchUri(uri string) (Response, error)
	Patch(endpoint string) (Response, error)
	DeleteUri(uri string) (Response, error)
	Delete(endpoint string) (Response, error)
}

type MultipartField struct {
	Param       string
	FileName    string
	ContentType string
	io.Reader
}

type Request interface {
	// Context method returns the Context if its already set in request
	// otherwise it creates new one using `context.Background()`.
	Context() context.Context

	// SetContext method sets the context.Context for current Request. It allows
	// to interrupt the request execution if ctx.Done() channel is closed.
	// See https://blog.golang.org/context article and the "context" package
	// documentation.
	SetContext(ctx context.Context) Request

	// SetHeader method is to set a single header field and its value in the current request.
	//
	// For Example: To set `Content-Type` and `Accept` as `application/json`.
	// 		client.
	//			SetHeader("Content-Type", "application/json").
	//			SetHeader("Accept", "application/json")
	//
	// Also you can override header value, which was set at client instance level.
	SetHeader(header, value string) Request

	// SetHeaders method sets multiple headers field and its values at one go in the current request.
	//
	// For Example: To set `Content-Type` and `Accept` as `application/json`
	//
	// 		client.
	//			SetHeaders(map[string]string{
	//				"Content-Type": "application/json",
	//				"Accept": "application/json",
	//			})
	// Also you can override header value, which was set at client instance level.
	SetHeaders(headers map[string]string) Request

	// SetQueryParam method sets single parameter and its value in the current request.
	// It will be formed as query string for the request.
	//
	// For Example: `search=kitchen%20papers&size=large` in the URL after `?` mark.
	// 		client.
	//			SetQueryParam("search", "kitchen papers").
	//			SetQueryParam("size", "large")
	// Also you can override query params value, which was set at client instance level.
	SetQueryParam(param, value string) Request

	// SetQueryParams method sets multiple parameters and its values at one go in the current request.
	// It will be formed as query string for the request.
	//
	// For Example: `search=kitchen%20papers&size=large` in the URL after `?` mark.
	// 		client.
	//			SetQueryParams(map[string]string{
	//				"search": "kitchen papers",
	//				"size": "large",
	//			})
	// Also you can override query params value, which was set at client instance level.
	SetQueryParams(params map[string]string) Request

	// SetQueryParamsFromValues method appends multiple parameters with multi-value
	// (`url.Values`) at one go in the current request. It will be formed as
	// query string for the request.
	//
	// For Example: `status=pending&status=approved&status=open` in the URL after `?` mark.
	// 		client.
	//			SetQueryParamsFromValues(url.Values{
	//				"status": []string{"pending", "approved", "open"},
	//			})
	// Also you can override query params value, which was set at client instance level.
	SetQueryParamsFromValues(params url.Values) Request

	// SetQueryString method provides ability to use string as an input to set URL query string for the request.
	//
	// Using String as an input
	// 		client.
	//			SetQueryString("productId=232&template=fresh-sample&cat=resty&source=google&kw=buy a lot more")
	SetQueryString(query string) Request

	// SetFormData method sets Form parameters and their values in the current request.
	// It's applicable only HTTP method `POST` and `PUT` and requests content type would be set as
	// `application/x-www-form-urlencoded`.
	// 		client.
	// 			SetFormData(map[string]string{
	//				"access_token": "BC594900-518B-4F7E-AC75-BD37F019E08F",
	//				"user_id": "3455454545",
	//			})
	// Also you can override form data value, which was set at client instance level.
	SetFormData(data map[string]string) Request

	// SetFormDataFromValues method appends multiple form parameters with multi-value
	// (`url.Values`) at one go in the current request.
	// 		client.
	//			SetFormDataFromValues(url.Values{
	//				"search_criteria": []string{"book", "glass", "pencil"},
	//			})
	// Also you can override form data value, which was set at client instance level.
	SetFormDataFromValues(data url.Values) Request

	// SetBody method sets the request body for the request. It supports various realtime needs as easy.
	// We can say its quite handy or powerful. Supported request body data types is `string`,
	// `[]byte`, `struct`, `map`, `slice` and `io.Reader`. Body value can be pointer or non-pointer.
	// Automatic marshalling for JSON and XML content type, if it is `struct`, `map`, or `slice`.
	//
	// Note: `io.Reader` is processed as bufferless mode while sending request.
	//
	// For Example: Struct as a body input, based on content type, it will be marshalled.
	//		client.
	//			SetBody(User{
	//				Username: "jeeva@myjeeva.com",
	//				Password: "welcome2resty",
	//			})
	//
	// Map as a body input, based on content type, it will be marshalled.
	//		client.
	//			SetBody(map[string]interface{}{
	//				"username": "jeeva@myjeeva.com",
	//				"password": "welcome2resty",
	//				"address": &Address{
	//					Address1: "1111 This is my street",
	//					Address2: "Apt 201",
	//					City: "My City",
	//					State: "My State",
	//					ZipCode: 00000,
	//				},
	//			})
	//
	// String as a body input. Suitable for any need as a string input.
	//		client.
	//			SetBody(`{
	//				"username": "jeeva@getrightcare.com",
	//				"password": "admin"
	//			}`)
	//
	// []byte as a body input. Suitable for raw request such as file upload, serialize & deserialize, etc.
	// 		client.
	//			SetBody([]byte("This is my raw request, sent as-is"))
	SetBody(body interface{}) Request

	// SetResult method is to register the response `Result` object for automatic unmarshalling for the request,
	// if response status code is between 200 and 299 and content type either JSON or XML.
	//
	// Note: Result object can be pointer or non-pointer.
	//		client.SetResult(&AuthToken{})
	//		// OR
	//		client.SetResult(AuthToken{})
	//
	// Accessing a result value from response instance.
	//		response.Result().(*AuthToken)
	SetResult(res interface{}) Request

	// SetError method is to register the request `Error` object for automatic unmarshalling for the request,
	// if response status code is greater than 399 and content type either JSON or XML.
	//
	// Note: Error object can be pointer or non-pointer.
	// 		client.SetError(&AuthError{})
	//		// OR
	//		client.SetError(AuthError{})
	//
	// Accessing a error value from response instance.
	//		response.Error().(*AuthError)
	SetError(err interface{}) Request

	// SetFile method is to set single file field name and its path for multipart upload.
	//	client.
	//		SetFile("my_file", "/Users/jeeva/Gas Bill - Sep.pdf")
	SetFile(param, filePath string) Request

	// SetFiles method is to set multiple file field name and its path for multipart upload.
	//	client.
	//		SetFiles(map[string]string{
	//				"my_file1": "/Users/jeeva/Gas Bill - Sep.pdf",
	//				"my_file2": "/Users/jeeva/Electricity Bill - Sep.pdf",
	//				"my_file3": "/Users/jeeva/Water Bill - Sep.pdf",
	//			})
	SetFiles(files map[string]string) Request

	// SetFileReader method is to set single file using io.Reader for multipart upload.
	//	client.
	//		SetFileReader("profile_img", "my-profile-img.png", bytes.NewReader(profileImgBytes)).
	//		SetFileReader("notes", "user-notes.txt", bytes.NewReader(notesBytes))
	SetFileReader(param, fileName string, reader io.Reader) Request

	// SetMultipartField method is to set custom data using io.Reader for multipart upload.
	SetMultipartField(param, fileName, contentType string, reader io.Reader) Request

	// SetMultipartFields method is to set multiple data fields using io.Reader for multipart upload.
	//
	// For Example:
	// 	client.SetMultipartFields(
	// 		&resty.MultipartField{
	//			Param:       "uploadManifest1",
	//			FileName:    "upload-file-1.json",
	//			ContentType: "application/json",
	//			Reader:      strings.NewReader(`{"input": {"name": "Uploaded document 1", "_filename" : ["file1.txt"]}}`),
	//		},
	//		&resty.MultipartField{
	//			Param:       "uploadManifest2",
	//			FileName:    "upload-file-2.json",
	//			ContentType: "application/json",
	//			Reader:      strings.NewReader(`{"input": {"name": "Uploaded document 2", "_filename" : ["file2.txt"]}}`),
	//		})
	//
	// If you have slice already, then simply call-
	// 	client.SetMultipartFields(fields...)
	//SetMultipartFields(fields ...*MultipartField) Request

	// SetContentLength method sets the HTTP header `Content-Length` value for current request.
	// By default Resty won't set `Content-Length`. Also you have an option to enable for every
	// request.
	//
	// See `Client.SetContentLength`
	// 		client.SetContentLength(true)
	SetContentLength(l bool) Request

	// SetBasicAuth method sets the basic authentication header in the current HTTP request.
	//
	// For Example:
	//		Authorization: Basic <base64-encoded-value>
	//
	// To set the header for username "go-resty" and password "welcome"
	// 		client.SetBasicAuth("go-resty", "welcome")
	//
	// This method overrides the credentials set by method `Client.SetBasicAuth`.
	SetBasicAuth(username, password string) Request

	// SetAuthToken method sets bearer auth token header in the current HTTP request. Header example:
	// 		Authorization: Bearer <auth-token-value-comes-here>
	//
	// For Example: To set auth token BC594900518B4F7EAC75BD37F019E08FBC594900518B4F7EAC75BD37F019E08F
	//
	// 		client.SetAuthToken("BC594900518B4F7EAC75BD37F019E08FBC594900518B4F7EAC75BD37F019E08F")
	//
	// This method overrides the Auth token set by method `Client.SetAuthToken`.
	SetAuthToken(token string) Request

	// SetOutput method sets the output file for current HTTP request. Current HTTP response will be
	// saved into given file. It is similar to `curl -o` flag. Absolute path or relative path can be used.
	// If is it relative path then output file goes under the output directory, as mentioned
	// in the `Client.SetOutputDirectory`.
	// 		client.
	// 			SetOutput("/Users/jeeva/Downloads/ReplyWithHeader-v5.1-beta.zip").
	// 			Get("http://bit.ly/1LouEKr")
	//
	// Note: In this scenario `Response.Body` might be nil.
	SetOutput(file string) Request

	// SetDoNotParseResponse method instructs `Resty` not to parse the response body automatically.
	// Resty exposes the raw response body as `io.ReadCloser`. Also do not forget to close the body,
	// otherwise you might get into connection leaks, no connection reuse.
	//
	// Note: Response middlewares are not applicable, if you use this option. Basically you have
	// taken over the control of response parsing from `Resty`.
	SetDoNotParseResponse(parse bool) Request

	// SetPathParams method sets multiple URL path key-value pairs at one go in the
	// Resty current request instance.
	// 		client.SetPathParams(map[string]string{
	// 		   "userId": "sample@sample.com",
	// 		   "subAccountId": "100002",
	// 		})
	//
	// 		Result:
	// 		   URL - /v1/users/{userId}/{subAccountId}/details
	// 		   Composed URL - /v1/users/sample@sample.com/100002/details
	// It replace the value of the key while composing request URL. Also you can
	// override Path Params value, which was set at client instance level.
	SetPathParams(params map[string]string) Request

	// ExpectContentType method allows to provide fallback `Content-Type` for automatic unmarshalling
	// when `Content-Type` response header is unavailable.
	ExpectContentType(contentType string) Request

	// SetJSONEscapeHTML method is to enable/disable the HTML escape on JSON marshal.
	//
	// Note: This option only applicable to standard JSON Marshaller.
	SetJSONEscapeHTML(b bool) Request

	// SetCookie method appends a single cookie in the current request instance.
	// 		client.SetCookie(&http.Cookie{
	// 					Name:"go-resty",
	// 					Value:"This is cookie value",
	// 				})
	//
	// Note: Method appends the Cookie value into existing Cookie if already existing.
	//
	// Since v2.1.0
	SetCookie(hc *http.Cookie) Request

	// SetCookies method sets an array of cookies in the current request instance.
	// 		cookies := []*http.Cookie{
	// 			&http.Cookie{
	// 				Name:"go-resty-1",
	// 				Value:"This is cookie 1 value",
	// 			},
	// 			&http.Cookie{
	// 				Name:"go-resty-2",
	// 				Value:"This is cookie 2 value",
	// 			},
	// 		}
	//
	//		// Setting a cookies into resty's current request
	// 		client.SetCookies(cookies)
	//
	// Note: Method appends the Cookie value into existing Cookie if already existing.
	//
	// Since v2.1.0
	SetCookies(rs []*http.Cookie) Request

	// Get method does GET HTTP request. It's defined in section 4.3.1 of RFC7231.
	Get(url string) (Response, error)

	// Head method does HEAD HTTP request. It's defined in section 4.3.2 of RFC7231.
	Head(url string) (Response, error)

	// Post method does POST HTTP request. It's defined in section 4.3.3 of RFC7231.
	Post(url string) (Response, error)

	// Put method does PUT HTTP request. It's defined in section 4.3.4 of RFC7231.
	Put(url string) (Response, error)

	// Delete method does DELETE HTTP request. It's defined in section 4.3.5 of RFC7231.
	Delete(url string) (Response, error)

	// Options method does OPTIONS HTTP request. It's defined in section 4.3.7 of RFC7231.
	Options(url string) (Response, error)

	// Patch method does PATCH HTTP request. It's defined in section 2 of RFC5789.
	Patch(url string) (Response, error)

	// Execute method performs the HTTP request with given HTTP method and URL
	// for current `Request`.
	// 		resp, err := client.Execute(resty.GET, "http://httpbin.org/get")
	Execute(method, url string) (Response, error)
}

type Response interface {
	// Body method returns HTTP response as []byte array for the executed request.
	//
	// Note: `Response.Body` might be nil, if `Request.SetOutput` is used.
	Body() []byte

	// Status method returns the HTTP status string for the executed request.
	//	Example: 200 OK
	Status() string

	// StatusCode method returns the HTTP status code for the executed request.
	//	Example: 200
	StatusCode() int

	// Json method returns the Json handler for handling the json using get/set from path.
	Json() JsonHandler

	// Data method returns the json object.
	Data() interface{}

	// Result method returns the response value as an object if it has one
	Result() interface{}

	// Error method returns the error object if it has one
	Error() interface{}

	// Header method returns the response headers
	Header() http.Header

	// Cookies method to access all the response cookies
	Cookies() []*http.Cookie

	// String method returns the body of the server response as String.
	String() string

	// Time method returns the time of HTTP response time that from request we sent and received a request.
	//
	// See `Response.ReceivedAt` to know when client recevied response and see `Response.Request.Time` to know
	// when client sent a request.
	Time() time.Duration

	// ReceivedAt method returns when response got recevied from server for the request.
	ReceivedAt() time.Time

	// Size method returns the HTTP response size in bytes. Ya, you can relay on HTTP `Content-Length` header,
	// however it won't be good for chucked transfer/compressed response. Since Resty calculates response size
	// at the client end. You will get actual size of the http response.
	Size() int64

	// RawBody method exposes the HTTP raw response body. Use this method in-conjunction with `SetDoNotParseResponse`
	// option otherwise you get an error as `read err: http: read on closed response body`.
	//
	// Do not forget to close the body, otherwise you might get into connection leaks, no connection reuse.
	// Basically you have taken over the control of response parsing from `Resty`.
	RawBody() io.ReadCloser

	// IsSuccess method returns true if HTTP status `code >= 200 and <= 299` otherwise false.
	IsSuccess() bool

	// IsError method returns true if HTTP status `code >= 400` otherwise false.
	IsError() bool
}
