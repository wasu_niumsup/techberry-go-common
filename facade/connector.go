package facade

import (
	"github.com/astaxie/beego/cache"
)

type Connector interface {
	GetOdooXmlRpcConnection(username string, passwd string, db string, url string) OdooXmlRpcClient

	HttpClient() HttpClient
	BeegoCache() cache.Cache
	// Get RPC cache connector.
	RpcCache() RpcCacheConnector
	GetCacheRpc(username string, token_id string) OdooCredential
	// Get RPC connector.
	GetRpcConnection(host string, port int, timeout int) RpcConnector
	InitRpcConnector(host string, port int, timeout int) Connector
	Rpc() RpcConnector

	Csv(csvInputFile string, isFirstRowHeader bool) CsvHandler
	LDAP(config *LDAPConfig) LDAPClient
	IsValidSession(username string, token_id string) bool
	InvalidateSession(token_id string)

	Mqtt(key string, clientId string) MqttClient
	InitMqttConnector(name string, url string, username string, password string) Connector
	GetMqttConnection(url string, username string, password string, clientId string) MqttClient

	Kafka() Kafka
	InitKafka(broker string) Connector
	GetKafkaConnection(broker string) Kafka

	Redis(db int) CacheHandler
	InitRedisConnector(host string, port int, db int, poolSize int) Connector
	GetRedisConnection(host string, port int, db int, poolSize int) CacheHandler

	Orm(key string) OrmHandler
	InitOrmConnector(key string, driverName string, dbType string, connstring string, timeout int, maxIdleConn int, maxOpenConn int) Connector
	GetOrmConnection(driverName string, dbType string, connstring string, timeout int, maxIdleConn int, maxOpenConn int) OrmHandler

	Elastic(string) ElasticHandler
	InitElasticConnector(name string, isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) Connector
	GetElasticConnection(isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) ElasticHandler

	InitMongo(name string, url string) Connector
	Mongo(name string, db string) CRUDDocumentStore
	GetMongoConnection(name string, url string, db string) CRUDDocumentStore
}
