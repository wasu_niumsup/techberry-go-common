package facade

type AppContext interface {
	AcceptResponse(data interface{})
	Response(status int, data interface{})
	GetAuthResponseMap(isSuccess bool, token string, cred string, message string, record_count int, data interface{}, errorMessage string) map[string]interface{}
	GetResponseMap(isSuccess bool, message string, record_count int, data interface{}, errorMessage string) map[string]interface{}
	ReplyCustom(version string, data interface{}, message Message, action interface{})
	ReplyWithMessage(err error, msg string, data interface{}, total int)
	Reply(err error, module string, functionName string, data interface{}, total int)
	ReplyAsync(id string)
	GetResponseDataMap() map[string]interface{}
	ResponseJson(res map[string]interface{})
	ResponseJsonStatus(status int, json string)
	GetJsonDataService() JsonHandler
	GetErrorMessage(errorCode string, errorDesc string) map[string]interface{}
	GetInputParam(name string) string
	ReplyMessage(replyUrl string, accessToken string, replyToken string, data []interface{})
	AuthCred() *AuthCredential
	JsonData() JsonHandler
	GetFile(index int) ([]byte, error)
	GetFileDescriptor(index int) UploadFile
}

type UploadFile struct {
	Name      string
	Part      int64
	Directory string
	FilePath  string
}

type RequestContext struct {
	Version string `structs:"version"`
	Service string `structs:"service"`
	Uri     string `structs:"uri"`
}

type AuthCredential struct {
	TokenId               string `structs:"token_id"`
	Credential            string `structs:"credential"`
	UserName              string `structs:"userName"`
	Uid                   int64  `structs:"uid"`
	CompanyId             int64  `structs:"company_id"`
	CurrentCompanyId      int64  `structs:"currentCompany_id"`
	CurrentOwnerCompanyId int64  `structs:"currentOwnerCompany_id"`
	Offset                int64  `structs:"offset"`
	Limit                 int64  `structs:"limit"`
	UserCode              string `structs:"userCode"`
}

type UserCredential struct {
	Uid      int64  `structs:"uid" json:"uid" bson:"uid"`
	UserName string `structs:"userName" json:"userName" bson:"userName"`
	UserCode string `structs:"userCode" json:"userCode" bson:"userCode"`
	TypeCode string `structs:"typeCode" json:"typeCode" bson:"typeCode"`
	Partner  struct {
		Code        string `structs:"code" json:"code" bson:"code"`
		Name        string `structs:"name" json:"name" bson:"name"`
		GroupName   string `structs:"groupName" json:"groupName" bson:"groupName"`
		Address     string `structs:"address" json:"address" bson:"address"`
		Salesperson struct {
			Id   int64  `structs:"id" json:"id" bson:"id"`
			Name string `structs:"name" json:"name" bson:"name"`
		} `structs:"salesperson" json:"salesperson" bson:"salesperson"`
	} `structs:"partner" json:"partner" bson:"partner"`
	Company struct {
		Id   int64  `structs:"id" json:"id" bson:"id"`
		Code string `structs:"code" json:"code" bson:"code"`
		Name string `structs:"name" json:"name" bson:"name"`
	} `structs:"company" json:"company" bson:"company"`
}

// Changed to structs prefix for lower case
// JSONResponse is the data standard format
type AuthJsonResponse struct {
	Success     bool        `structs:"success"`
	Data        interface{} `structs:"data"`
	Credential  string      `structs:"credential"`
	TokenId     string      `structs:"token_id"`
	Code        string      `structs:"code"`
	Message     string      `structs:"message"`
	RecordCount int         `structs:"recordCount"`
	Error       string      `structs:"error"`
}

type JsonCustomResponse struct {
	Version string      `structs:"version"`
	Data    interface{} `structs:"data"`
	Message Message     `structs:"message"`
	Action  interface{} `structs:"action"`
}

type Message struct {
	Description string `structs:"description"`
	Code        string `structs:"code"`
}

type JsonResponse struct {
	Success     bool        `structs:"success"`
	Data        interface{} `structs:"data"`
	Code        string      `structs:"code"`
	Message     string      `structs:"message"`
	RecordCount int         `structs:"recordCount"`
	Error       string      `structs:"error"`
}

type JsonResponseArray struct {
	Success     bool          `structs:"success"`
	Data        []interface{} `structs:"data"`
	Code        string        `structs:"code"`
	Message     string        `structs:"message"`
	RecordCount int           `structs:"recordCount"`
	Error       string        `structs:"error"`
}

type ErrorResponse struct {
	ErrorCode    int         `structs:"error_code"`
	ErrorMessage interface{} `structs:"error_message"`
}

// TextMessage type
type LineMessage struct {
	ReplyToken  string        `structs:"replyToken"`
	MessageData []interface{} `structs:"messages"`
}

// TextMessage type
type TextMessage struct {
	Type string `structs:"type"`
	Text string `structs:"text"`
}

type Properties interface {
	// Set new key and value into properties.
	Set(key, val string) error

	// Return string value from key.
	String(key string) string

	// Return array of string from key.
	Strings(key string) []string

	// Return integer from key.
	Int(key string) (int, error)

	// Return integer64 from key.
	Int64(key string) (int64, error)

	// Return boolean from key.
	Bool(key string) (bool, error)

	// Return float from key.
	Float(key string) (float64, error)

	// Return string value from key with default value when string is blank.
	DefaultString(key string, defaultVal string) string

	// Return array of string from key with default value when string is blank.
	DefaultStrings(key string, defaultVal []string) []string

	// Return integer from key with default value when is zero.
	DefaultInt(key string, defaultVal int) int

	// Return integer64 from key with default value when is zero.
	DefaultInt64(key string, defaultVal int64) int64

	// Return boolan from key with default value when is false.
	DefaultBool(key string, defaultVal bool) bool

	// Return float from key with default value when is zero.
	DefaultFloat(key string, defaultVal float64) float64
}
