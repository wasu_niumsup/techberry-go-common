package facade

type JsonHandler interface {
	IsValidJson() bool
	Delete(path string)
	SetBool(value bool, path string)
	SetInt(value int64, path string)
	SetFloat(value float64, path string)
	SetString(value string, path string)
	Set(value interface{}, path string)
	SetBlank(path string)
	SetBlankArray(path string)
	GetData() []byte
	GetDataStruct(path string) []byte
	Exists(path string) bool
	String(defaultValue string, path string) string
	Bool(defaultValue bool, path string) bool
	Int(defaultValue int64, path string) int64
	Float(defaultValue float64, path string) float64
	GetMany(path ...string) []interface{}
	GetArray(path string) (bool, []interface{})
	GetBlankJson() interface{}
	ToMap() map[string]interface{}
	DecodingJson() interface{}
	FromMap(inputMap map[string]interface{}) (interface{}, error)
	// Binding JSON into an interface value, Unmarshal stores one of these in the interface value:
	Binding(v interface{}) error
	Value(path string) interface{}
	IsArray(path string) bool
	IsObject(path string) bool
}
