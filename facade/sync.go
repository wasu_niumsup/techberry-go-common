package facade

import (
	"time"
)

const (
	SyncTargetType_Orm     = "orm"
	SyncTargetType_Elastic = "elastic"
	SyncTargetType_Mongodb = "mongodb"
	SyncTargetType_Kafka   = "kafka"
)

type SyncHandler interface {
	Debug(debug bool)
	GetConfig() *SyncConfig
	Source(v interface{}) SyncHandler
	Target(v interface{}) SyncHandler
	FetchSource(m func(adapter Adapter, config *SyncConfig, input interface{}) (int, []interface{})) SyncHandler
	UpdateJournalSource(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	BindDataModel(m func(adapter Adapter, config *SyncConfig, data interface{}, dataModel interface{}) interface{}) SyncHandler
	ClearTarget(m func(adapter Adapter, config *SyncConfig, input interface{}) error) SyncHandler
	DeleteTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	HookBeforeTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	HookAfterTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	ProcessTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	//PublishTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	UpdateJournalTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	SetInputData(input interface{})
	SetConnectors(source ConnectorContext, target []ConnectorContext, plugin []ConnectorContext)
	SetConfig(config *SyncConfig)
	//SourceCSVFileName(path string, firstRowIsHeader bool)
	//SourceCSVFilePath(path string, firstRowIsHeader bool)

	// Database (Source) -> Database (Target)
	// SourceDB()
	// TargetDB()
	// CDC is the source journal
	// 1. Clear all data from target database.
	// 2. Fetch all CDC data from source database
	// 3. Get fields data from source.
	// 4. Delete sync item from target database (CDC).
	// 5. If the create target is success, it will deleted from the journal
	Sync() (error, int, int, int)

	// DB -> Elastic, NoSQL
	/*
		SyncType1(fetchJournal func(OrmHandler, interface{}) (int, []interface{}),
			clearNoSQLData func(CRUDDocumentStore, interface{}) error,
			clearElasticData func(ElasticHandler, interface{}) error,
			bindingDatadataModel func(OrmHandler, interface{}, map[string]interface{}) interface{},
			upsertElastic func(ElasticHandler, string, string, interface{}, interface{}) (bool, error),
			upsertNoSQL func(CRUDDocumentStore, interface{}, interface{}) bool,
			updateJournal func(OrmHandler, interface{}, map[string]interface{}) bool)
	*/
	// Database (Source) -> DB (Target)

	//SyncType10(csvColumndataModel func() []CSVModel,
	//	getTargetDBColumn func() (string, []string)) bool

	// DB -> NoSQL
	// DB -> Elastic
	// NoSQL -> Elastic
	// NoSQL -> DB
	// Elastic -> NoSQL
	// Elastic -> DB
	// API -> NoSQL : 1, 2, 3
	// NoSQL -> API : 3, 2 ,1
	// API -> File System
	// API -> Zip
	// API -> FTP(s)

	//SetSourceTransport(connector interface{})
	//SetTargetTransport(connector interface{})

	// Export
	/*
		Export(fetchSource func(connector interface{}, input_data interface{}) (int, []interface{}),
			updateSourceJournal func(connector interface{}, input_data interface{}) (bool),
			deleteTarget func(connector interface{}, input_data interface{})
			createTarget func(connector interface{}, input_data interface{})
	*/
	// Import

	/*
		Sync(isCompare Bool,
			compareSourceWithTarget func(connector interface{}, input_data interface{}) err,
			fetchSource func(connector interface{}, input_data interface{}) (int, []interface{}),
			fetchJournal func(connector interface{}, input_data interface{}) (int, []interface{}),
			clearSource func(connector interface{}, input_data interface{}) error,
			clearTarget func(connector interface{}, input_data interface{}) error,
			deleteTarget func(connector interface{}, input_data interface{}, data map[string]interface{}) bool,
			createTarget func(connector interface{}, input_data interface{}, data map[string]interface{}) (int, error),
			deleteJournal func(connector interface{}, input_data interface{}, data map[string]interface{}) bool) (int, int)
	*/
}

type CSVdataModel struct {
	Index                int
	Heading              string
	ColumnType           string // string, float64, int64
	DefaultValue         interface{}
	ConvertBlank         bool
	ConvertSingleQuote   bool
	ConvertHyphen        bool
	ConvertComma         bool
	TrimWhiteSpace       bool
	NullValueTextToEmpty bool
}

type SyncConfig struct {
	CdcMode                 bool   `structs:"cdcMode" json:"cdcMode" bson:"cdcMode"`
	IsTrackingState         bool   `structs:"isTrackingState" json:"isTrackingState" bson:"isTrackingState"`
	ProcessId               string `structs:"processId" json:"processId" bson:"processId"`
	ForceRunningTask        bool   `structs:"forceRunningTask" json:"forceRunningTask" bson:"forceRunningTask"`
	TrackingColumn          string `structs:"trackingColumn" json:"trackingColumn" bson:"trackingColumn"`
	SqlLastTimeValue        time.Time
	CurrentTimeValue        time.Time
	SqlLastValue            int
	SqlStartTime            string `structs:"sqlStartTime" json:"sqlStartTime" bson:"sqlStartTime"`
	SqlEndTime              string `structs:"sqlEndTime" json:"sqlEndTime" bson:"sqlEndTime"`
	TrackingColumnType      int    `structs:"trackingColumnType" json:"trackingColumnType" bson:"trackingColumnType"` // 0 is numeric , 1 is timestamp
	UseColumnValue          bool   `structs:"useColumnValue" json:"useColumnValue" bson:"useColumnValue"`             // When set to false, :sql_last_value reflects the last time the query was executed. When set to true, uses the defined tracking_column value as the :sql_last_value.
	Topic                   string `structs:"topic" json:"topic" bson:"topic"`
	VfsPath                 string `structs:"vfsPath" json:"vfsPath" bson:"vfsPath" default:"/shared"`
	BulkSize                int    `structs:"bulkSize" json:"bulkSize" bson:"bulkSize" default:"1000"`
	NoSqlCollection         string `structs:"nosqlCollection" json:"nosqlCollection" bson:"nosqlCollection"`
	ElasticAutoDisableIndex bool   `structs:"elasticAutoDisableIndex" json:"elasticAutoDisableIndex" bson:"elasticAutoDisableIndex" default:"true"`
	ElasticIndex            string `structs:"elasticIndex" json:"elasticIndex" bson:"elasticIndex"`
	ElasticDocType          string `structs:"elasticDocType" json:"elasticDocType" bson:"elasticDocType"`
	DelayUpdateBulk         int64  `structs:"delayUpdateBulk" json:"delayUpdateBulk" bson:"delayUpdateBulk" default:"10"`
	SkippedOnError          bool   `structs:"skippedOnError" json:"skippedOnError" bson:"skippedOnError" default:"true"`
	CsvInputFile            string `structs:"csvInputFile" json:"csvInputFile" bson:"csvInputFile"`
	Debug                   bool   `structs:"debug" json:"debug" bson:"debug" default:"false"`
	Upsert                  bool   `structs:"upsert" json:"upsert" bson:"upsert"` // When set to false, :sql_last_value reflects the last time the query was executed. When set to true, uses the defined tracking_column value as the :sql_last_value.
}

type TimestampTask struct {
	Name           string
	StartTimestamp time.Time
	EndTimestamp   time.Time
}

type ConnectorContext struct {
	Id                string                 `structs:"id" json:"id" bson:"id"`
	Name              string                 `structs:"name" json:"name" bson:"name"`
	Type              string                 `structs:"type" json:"type" bson:"type"`
	DriverName        string                 `structs:"driver_name" json:"driver_name" bson:"driver_name"`
	Vendor            string                 `structs:"vendor" json:"vendor" bson:"vendor"`
	ConnString        string                 `structs:"connstring" json:"connstring" bson:"connstring"`
	MaxIdleConn       int                    `structs:"maxIdleConn" json:"maxIdleConn" bson:"maxIdleConn"`
	MaxOpenConn       int                    `structs:"maxOpenConn" json:"maxOpenConn" bson:"maxOpenConn"`
	Timeout           int                    `structs:"timeout" json:"timeout" bson:"timeout"`
	IsSecure          bool                   `structs:"isSecure" json:"isSecure" bson:"isSecure"`
	Host              string                 `structs:"host" json:"host" bson:"host"`
	Port              int                    `structs:"port" json:"port" bson:"port"`
	ApiKey            string                 `structs:"api_key" json:"api_key" bson:"api_key"`
	BasicAuthUser     string                 `structs:"basicAuthUser" json:"basicAuthUser" bson:"basicAuthUser"`
	BasicAuthPassword string                 `structs:"basicAuthPassword" json:"basicAuthPassword" bson:"basicAuthPassword"`
	ConnectionUrl     string                 `structs:"connection_url" json:"connection_url" bson:"connection_url"`
	Database          string                 `structs:"db" json:"db" bson:"db"`
	BootstrapServers  string                 `structs:"bootstrapServers" json:"bootstrapServers" bson:"bootstrapServers"`
	Skip              bool                   `structs:"skip" json:"skip" bson:"skip"`
	Username          string                 `structs:"username" json:"username" bson:"username"`
	Password          string                 `structs:"password" json:"password" bson:"password"`
	Url               string                 `structs:"url" json:"url" bson:"url"`
	CsvInputFile      string                 `structs:"csvInputFile" json:"csvInputFile" bson:"csvInputFile"`
	IsFirstRowHeader  bool                   `structs:"isFirstRowHeader" json:"isFirstRowHeader" bson:"isFirstRowHeader" default:"true"`
	DataSource        string                 `structs:"datasource" json:"datasource" bson:"datasource"`
	MapperId          string                 `structs:"mapper_id" json:"mapper_id" bson:"mapper_id"`
	CdcMode           bool                   `structs:"cdc_mode" json:"cdc_mode" bson:"cdc_mode" default:"false"`
	LastTimestamp     string                 `structs:"last_timestamp" json:"last_timestamp" bson:"last_timestamp"`
	CurrentTimestamp  string                 `structs:"current_timestamp" json:"current_timestamp" bson:"current_timestamp"`
	BulkSize          int                    `structs:"bulk_size" json:"bulk_size" bson:"bulk_size"`
	Params            map[string]interface{} `structs:"params" json:"params" bson:"params"`
}
