package facade

type CsvHandler interface {
	HeaderLine() []string
	OpenFile(csvInputFile string) error
	Open() error
	Read() ([]string, error)
	ReadModel(model interface{}) (interface{}, error)
}
