package facade

type Adapter interface {
	SetPlugin(Plugin)
	Plugin() Plugin
	SetCsv(CsvHandler)
	Csv() CsvHandler
	SetElastic(ElasticHandler)
	Elastic() ElasticHandler
	SetNoSql(CRUDDocumentStore)
	NoSql() CRUDDocumentStore
	SetOrm(OrmHandler)
	Orm() OrmHandler
	SetKafka(Kafka)
	Kafka(singleton bool) Kafka
	SetRpc(RpcConnector)
	Rpc() RpcConnector
	SetOdoo(OdooXmlRpcClient)
	Odoo() OdooXmlRpcClient
	SetDataFactory(DataFactory)
	DataFactory() DataFactory
}

type Plugin interface {
	SetOrm(string, OrmHandler)
	Orm(string) OrmHandler
	SetElastic(string, ElasticHandler)
	Elastic(string) ElasticHandler
	SetDataFactory(string, DataFactory)
	DataFactory(string) DataFactory
}
