package facade

type Kafka interface {
	BootstrapServers() string
	AddHeader(key string, value []byte)
	Publish(topic string, message []byte)
}
