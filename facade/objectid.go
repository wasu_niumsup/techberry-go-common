package facade

import "time"

type ObjectID interface {
	// Timestamp extracts the time part of the ObjectId.
	Timestamp() time.Time

	// Hex returns the hex encoding of the ObjectID as a string.
	Hex() string

	String() string

	// IsZero returns true if id is the empty ObjectID.
	IsZero() bool

	// ObjectIDFromHex creates a new ObjectID from a hex string. It returns an error if the hex string is not a
	// valid ObjectID.
	ObjectIDFromHex(s string) (ObjectID, error)

	// MarshalJSON returns the ObjectID as a string
	MarshalJSON() ([]byte, error)

	// UnmarshalJSON populates the byte slice with the ObjectID. If the byte slice is 64 bytes long, it
	// will be populated with the hex representation of the ObjectID. If the byte slice is twelve bytes
	// long, it will be populated with the BSON representation of the ObjectID. Otherwise, it will
	// return an error.
	UnmarshalJSON(b []byte) error
}
