package facade

type Handler interface {
	Quote(singleton bool) Quote
	String(singleton bool) StringHandler
	Map(singleton bool) MapHandler
	Time(singleton bool) TimeHandler
	Struct(singleton bool) StructHandler
	StructMerge(singleton bool) StructMerge
	File(singleton bool) FileHandler
	AesCrypto(singleton bool) AesCryptoHandler
	Json(data []byte) JsonHandler
	JsonFromStruct(v interface{}) JsonHandler
	Statement(singleton bool) StatementHandler
	Numeric(singleton bool) NumericHandler
	HttpClient(singleton bool) HttpClient
	QueryDSL(singleton bool) QueryDSL
	SyncModel(logger LogEvent) SyncHandler
	NoSqlModel(store CRUDDocumentStore) NoSqlHandler
	NoSqlFromAdapter(adapter Adapter) NoSqlHandler
	AsynTask(cache CacheHandler) AsyncTask
	ObjectID() ObjectID
}
