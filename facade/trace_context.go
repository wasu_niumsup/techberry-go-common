package facade

import (
	"context"

	"go.opencensus.io/trace"
)

const (
	TraceStatusCodeOK                 = 0
	TraceStatusCodeCancelled          = 1
	TraceStatusCodeUnknown            = 2
	TraceStatusCodeInvalidArgument    = 3
	TraceStatusCodeDeadlineExceeded   = 4
	TraceStatusCodeNotFound           = 5
	TraceStatusCodeAlreadyExists      = 6
	TraceStatusCodePermissionDenied   = 7
	TraceStatusCodeResourceExhausted  = 8
	TraceStatusCodeFailedPrecondition = 9
	TraceStatusCodeAborted            = 10
	TraceStatusCodeOutOfRange         = 11
	TraceStatusCodeUnimplemented      = 12
	TraceStatusCodeInternal           = 13
	TraceStatusCodeUnavailable        = 14
	TraceStatusCodeDataLoss           = 15
	TraceStatusCodeUnauthenticated    = 16
)

type TraceModel struct {
	Context      context.Context
	TraceId      string
	ParentSpanId string
	SpanId       string
	Sampled      int
	Debug        bool
}

type Span interface {
	SetTagMap(m map[string]interface{}) Span
	SetTag(key string, value interface{}) Span
	SetStatus(code int32, message string) Span
	AddTag() Span
	Finish()
}

type TagAttribute struct {
	key   string
	value interface{}
}

type TraceContext interface {
	RootModel() *TraceModel
	ParentModel() *TraceModel
	LastModel() *TraceModel
	RootCtx() context.Context
	ParentCtx() context.Context
	LastCtx() context.Context
	StartSpan(name string, o ...trace.StartOption) Span
	Span(ctx context.Context, name string, o ...trace.StartOption) Span
}
