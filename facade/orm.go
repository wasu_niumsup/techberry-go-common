package facade

type OrmHandler interface {
	BeginTx() error
	CommitTx()
	RollbackTx()
	NamedInsert(pstmt string, args interface{}) (int, error)
	Insert(pstmt string, args ...interface{}) (int, error)
	NamedInsertTx(pstmt string, args interface{}) (int, error)
	InsertTx(pstmt string, args ...interface{}) (int, error)
	FetchAll(pstmt string, args ...interface{}) (int, []interface{})
	FetchAllIds(pstmt string, args ...interface{}) (int, []interface{})
	FetchOne(pstmt string, args ...interface{}) (map[string]interface{}, error)
	FetchId(pstmt string, args ...interface{}) int64

	Update(pstmt string, args ...interface{}) (err error)
	UpdateTx(pstmt string, args ...interface{}) (err error)
	Delete(pstmt string, args ...interface{}) (err error)
	DeleteTx(pstmt string, args ...interface{}) (err error)
	GetDriverName() string
	Close()
}
