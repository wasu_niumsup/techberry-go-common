package facade

type DataBus interface {
	Delete(path string)
	SetBool(value bool, path string)
	SetInt(value int64, path string)
	SetFloat(value float64, path string)
	SetString(value string, path string)
	Set(value interface{}, path string)
	SetBlank(path string)
	SetBlankArray(path string)
	GetData() []byte
	GetDataStruct(path string) []byte
	Exists(path string) bool
	String(defaultValue string, path string) string
	Bool(defaultValue bool, path string) bool
	Int(defaultValue int64, path string) int64
	UInt(defaultValue uint64, path string) uint64
	Float(defaultValue float64, path string) float64
	GetMany(path ...string) []interface{}
	GetArray(path string) (bool, []interface{})
	GetBlankJson() interface{}
	ToMap() map[string]interface{}
	ToJsonString() string
	Request() map[string]interface{}
	RequestBinding(v interface{})
}
