Pre-requisite packages for facade:

go get github.com/astaxie/beego
go get github.com/jmoiron/sqlx
go get github.com/olivere/elastic/v7
go get go.mongodb.org/mongo-driver
go get go.opencensus.io/trace