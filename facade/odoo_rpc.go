package facade

import (
	"time"
)

type RpcConnector interface {
	// Authentication to Odoo by specific (db, username, password)
	// Return jsonByteString, encryption for credential, token,
	Login(db string, username string, password string) (interface{}, *OdooCredential, error)
	Execute_KW(model string, method string, args interface{}, kwargs map[string]interface{}) ([]byte, error)
}

type RpcCacheConnector interface {
	Save(key string, val interface{}, timeout time.Duration)
	Load(key string) (interface{}, error)
	Remove(key string)
}

type OdooCredential struct {
	ClassName   string `json:"className"`
	Host        string `json:"host"`
	Protocol    string `json:"protocol"`
	Port        int    `json:"port"`
	Timeout     int    `json:"timeout"`
	User        string `json:"user"`
	Passwd      string `json:"passwd"`
	Database    string `json:"database"`
	UID         int64  `json:"uid"`
	UserContext struct {
		Lang string `json:"lang"`
		TZ   string `json:"tz"`
		UID  int64  `json:"uid"`
	} `json:"user_context"`
}
