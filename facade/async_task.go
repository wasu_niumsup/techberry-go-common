package facade

type EventState struct {
	Name     string      `structs:"name" json:"name" bson:"name" mapstructure:"name"`
	Category string      `structs:"category" json:"category" bson:"category" mapstructure:"category"`
	Status   string      `structs:"status" json:"status" bson:"status" mapstructure:"status"`
	Error    string      `structs:"error" json:"error" bson:"error" mapstructure:"error"`
	Data     interface{} `structs:"data" json:"data" bson:"data" mapstructure:"data"`
}

type AsyncTask interface {
	Start(name string, category string, uuid string)
	Fail(uuid string)
	Success(uuid string, data interface{})
	GetState(uuid string) (string, error)
}
