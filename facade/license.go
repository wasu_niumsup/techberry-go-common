package facade

type LicenseInfo struct {
	A1  interface{} `structs:"a1" json:"a1" bson:"a1"`
	A2  interface{} `structs:"a2" json:"a2" bson:"a2"`
	A3  interface{} `structs:"a3" json:"a3" bson:"a3"`
	A4  interface{} `structs:"a4" json:"a4" bson:"a4"`
	A5  interface{} `structs:"a5" json:"a5" bson:"a5"`
	A6  interface{} `structs:"a6" json:"a6" bson:"a6"`
	A7  interface{} `structs:"a7" json:"a7" bson:"a7"`
	A8  interface{} `structs:"a8" json:"a8" bson:"a8"`
	A9  interface{} `structs:"a9" json:"a9" bson:"a9"`
	A10 interface{} `structs:"a10" json:"a10" bson:"a10"`
	A11 interface{} `structs:"a11" json:"a11" bson:"a11"`
	A12 interface{} `structs:"a12" json:"a12" bson:"a12"`
	A13 interface{} `structs:"a13" json:"a13" bson:"a13"`
	A14 interface{} `structs:"a14" json:"a14" bson:"a14"`
	A15 interface{} `structs:"a15" json:"a15" bson:"a15"`
}

type LicenseService interface {
	IsActivated() bool
	GenerateLicenseRequest(appkey string, port int) (string, error)
	SetPath(locationPath string)
	Generate(raw_license string, customer string, licenseType string, startDate string, expireDate string, daysAlertBeforeExpire int) (string, error)
	Validate(appkey string) error
	Activate(appkey string) error
}
