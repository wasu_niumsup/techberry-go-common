package facade

import "context"

// Module a plugin that can be initialized
type ServiceContext struct {
	AsyncMode    bool
	TraceContext context.Context
	Context      AppContext
	Properties   Properties
	Config       AppConfig
	DataBus      DataBus
	Connector    Connector
	Handler      Handler
	Service      DataService
	Version      string
}

type DataService interface {
	BindInputRoot(interface{})
	// module
	// function
	// error
	// code
	// message
	// data
	// count
	Output(ResponseContext)
	Context() *ResponseContext
}

type ResponseContext struct {
	Uuid     string
	Module   string
	Function string
	Error    error
	Code     string
	Message  string
	Data     interface{}
	Count    int
}

type Service interface {
	Execute(*ServiceContext) error
}
