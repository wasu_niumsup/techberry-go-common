// Package facade provides provide common libraries.
package facade

import "reflect"

type StructHandler interface {
	// Copy method copies all the exported field values from source `struct` into destination `struct`.
	// The "Name", "Type" and "Kind" is should match to qualify a copy. One exception though;
	// if the destination field type is "interface{}" then "Type" and "Kind" doesn't matter,
	// source value gets copied to that destination field.
	Copy(dst, src interface{}) []error

	// Clone method creates a clone of given `struct` object. As you know go-model does, deep processing.
	// So all field values you get in the result.
	Clone(s interface{}) (interface{}, error)

	// Map method converts all the exported field values from the given `struct`
	// into `map[string]interface{}`. In which the keys of the map are the field names
	// and the values of the map are the associated values of the field.
	Map(s interface{}) (map[string]interface{}, error)

	// Fields method returns the exported struct fields from the given `struct`.
	Fields(s interface{}) ([]reflect.StructField, error)

	// Kind method returns `reflect.Kind` for the given field name from the `struct`.
	Kind(s interface{}, name string) (reflect.Kind, error)

	// Get method returns a field value from `struct` by field name.
	Get(s interface{}, name string) (interface{}, error)

	// Set method sets a value into field on struct by field name.
	Set(s interface{}, name string, value interface{}) error

	// IsZero method returns `true` if all the exported fields in a given `struct`
	// are zero value otherwise `false`. If input is not a struct, method returns `false`.
	IsZero(s interface{}) bool

	// IsZeroInFields method verifies the value for the given list of field names against
	// given struct. Method returns `Field Name` and `true` for the zero value field.
	// Otherwise method returns empty `string` and `false`.
	IsZeroInFields(s interface{}, names ...string) (string, bool)

	// HasZero method returns `true` if any one of the exported fields in a given
	// `struct` is zero value otherwise `false`. If input is not a struct, method
	// returns `false`.
	HasZero(s interface{}) bool

	// Converts struct to Map within a form of map[string]interface{}. For more info refer to Struct types Map() method. It panics if it is not struct.
	ToMap(s interface{}) map[string]interface{}

	// To unmarshal JSON into an struct value, Unmarshal stores one of these in the interface value:
	FromJson(data []byte, s interface{}) error

	// Convert struct into json byte value.
	ToJson(s interface{}) ([]byte, error)

	// Values converts the given s struct's field values to a []interface{}.  A
	// struct tag with the content of "-" ignores the that particular field.
	// Example:
	//
	//   // Field is ignored by this package.
	//   Field int `structs:"-"`
	//
	// A value with the option of "omitnested" stops iterating further if the type
	// is a struct. Example:
	//
	//   // Fields is not processed further by this package.
	//   Field time.Time     `structs:",omitnested"`
	//   Field *http.Request `structs:",omitnested"`
	//
	// A tag value with the option of "omitempty" ignores that particular field and
	// is not added to the values if the field value is empty. Example:
	//
	//   // Field is skipped if empty
	//   Field string `structs:",omitempty"`
	//
	// Note that only exported fields of a struct can be accessed, non exported
	// fields  will be neglected.
	FieldValues(s interface{}) []interface{}

	// Names returns a slice of field names. A struct tag with the content of "-"
	// ignores the checking of that particular field. Example:
	//
	//   // Field is ignored by this package.
	//   Field bool `structs:"-"`
	//
	// It panics if s's kind is not struct.
	FieldNames(s interface{}) []string

	// Name returns the structs's type name within its package. For more info refer
	// to Name() function.
	Name(s interface{}) string

	// IsStruct returns true if the given variable is a struct or a pointer to
	// struct.
	IsStruct(s interface{}) bool

	// PrettyPrint returns formatted data from struct in string format.
	PrettyPrint(s interface{}) string

	InitDefaults(ptr interface{}) error

	CanUpdate(v interface{}) bool
}

// Setter is an interface for setting default values
type Setter interface {
	SetDefaults()
}
