package facade

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type CRUDDocumentStore interface {
	Database() string
	Create(collection string, model interface{}) (string, error)
	CreateMany(collection string, modelList []interface{}) ([]string, error)
	Read(collection string, filter bson.M, model interface{}) (err error)
	ReadMany(collection string, filter bson.M, results *[]interface{}, findOptions *options.FindOptions, binding func(interface{}) interface{}) (err error)
	ReadById(collection string, id string, model interface{}) (err error)
	UpdateById(collection string, id string, model interface{}) int64
	Update(collection string, filter bson.M, update bson.M) int64
	UpdateMany(collection string, filter bson.M, model interface{}) int64
	DeleteById(collection string, id string) bool
	DeleteMany(collection string, filter bson.M) bool
	Upsert(collection string, filter bson.M, model interface{}) (string, error)
	GetDocumentById(collection string, _id string, model interface{}) error
	IsExist(collection string, filter bson.M, model interface{}) (bool, string)
	FindByItemArray(collection string, id string, itemId string, itemKey string) (map[string]interface{}, error)
	Release()
}

type NoSqlHandler interface {
	Debug(debug bool)
	SetAdapter(adapter Adapter)
	SetTargetAdapter(adapter Adapter)
	SetDataModel(dataModel interface{})
	SetInputData(input interface{})
	SetCollection(collection string)
	SetTargetCollection(collection string)
	BindDataModel(task string, m func(_id string, input interface{}, dataModel interface{}) (interface{}, error)) NoSqlHandler
	Filter(task string, m func(_id string, input interface{}, dataModel interface{}) (map[string]interface{}, error)) NoSqlHandler
	Sort(task string, m func(_id string, input interface{}, dataModel interface{}) (interface{}, error)) NoSqlHandler
	PreExecute(task string, m func(adapter Adapter, input interface{}) error) NoSqlHandler
	PostExecute(task string, m func(adapter Adapter, input interface{}, dataModel interface{}, _id string) error) NoSqlHandler
	Create(filter map[string]interface{}) (string, error)
	Update(filter map[string]interface{}) error
	AddItem(_parentId string, parentModel interface{}, path string) error
	Read(filter map[string]interface{}) (string, interface{}, error)
	IsExist(filter map[string]interface{}, model interface{}) (bool, string)
	DeleteItemByMap(_parentId string, _child map[string]interface{}, path string) error
	DeleteItem(_parentId string, _childId string, path string) error
	Delete(_id string) error
	Search(offset int64, limit int64) ([]interface{}, error)
	Move(filter map[string]interface{}) (string, error)
	PrepareUpdateField(changes map[string]interface{}, field string, value interface{}) map[string]interface{}
}
