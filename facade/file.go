package facade

import (
	"io"
	"os"
)

type FileHandler interface {

	// CopyFile copies a file from source to dest and returns
	// an error if any.
	CopyFile(source string, dest string) error

	// CopyDir copies a directory from source to dest and all
	// of its sub-directories. It doesn't stop if it finds an error
	// during the copy. Returns an error if any.
	CopyDir(source string, dest string) error

	// IsDir reports whether the named directory exists.
	IsDir(path string) bool

	// IsFile reports whether the named file or directory exists.
	IsFile(path string) bool

	// IsAbsPath is abs path.
	IsAbsPath(filepath string) bool

	// Mkdir alias of os.Mkdir()
	Mkdir(name string, perm os.FileMode) error

	// MimeType get File Mime Type name. eg "image/png"
	MimeType(path string) (mime string)

	// ReaderMimeType get the io.Reader mimeType
	// Usage:
	// 	file, err := os.Open(filepath)
	// 	if err != nil {
	// 		return
	// 	}
	//	mime := ReaderMimeType(file)
	ReaderMimeType(r io.Reader) (mime string)

	// IsImageFile check file is image file.
	IsImageFile(path string) bool

	// IsZipFile check is zip file.
	IsZipFile(filepath string) bool

	// Unzip a zip archive
	Unzip(archive, target string) (err error)

	// Zip all files from source into target
	Zip(source, target string) error

	// Create directory if not exists.
	CreateDirIfNotExist(dir string) error

	// Check directory or path exists.
	IsDirectoryOrPathExists(path string) bool

	// Remove bad characters from path.
	RemoveBadCharacters(input string, dictionary []string) string

	// Remove special or escapse chracters from file name.
	SanitizeFilename(name string, relativePath bool) string

	// Create temporary file.
	CreateTempFile(dir, prefix string, suffix string) (f *os.File, filename string, err error)

	// Download file from url.
	DownloadFile(filepath string, url string) (string, string, error)

	// Get image dimension width and height.
	GetImageDimension(imagePath string) (int, int)

	// ReadFile reads the file named by filename and returns the contents. A successful call returns err == nil, not err == EOF. Because ReadFile reads the whole file, it does not treat an EOF from Read as an error to be reported.
	ReadFile(filename string) ([]byte, error)

	// ReadJsonFile reads the file named by filename and return the json handler.
	ReadJsonFile(filename string) (JsonHandler, error)

	// Write file by filename and bytes data.
	WriteFile(filename string, data []byte) error

	// List files in directory pattern Ex. *.so, abc_*.so, etc,.
	ListFiles(dir, pattern string) ([]os.FileInfo, error)

	// List files in directory with pattern Ex. /opt/abc/**/*.go etc,.
	ListFilesPattern(dir_with_pattern string) ([]string, error)
}
