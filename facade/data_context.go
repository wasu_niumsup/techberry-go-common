package facade

type DataContext struct {
	Id                 string        `structs:"id" json:"id" bson:"id" mapstructure:"id"`
	Ids                []interface{} `structs:"ids" json:"ids" bson:"ids" mapstructure:"ids"`
	ItemId             string        `structs:"itd" json:"itd" bson:"itd" mapstructure:"itd"`
	BeginDate          string        `structs:"bd" json:"bd" bson:"bd" mapstructure:"bd"`
	EndDate            string        `structs:"ed" json:"ed" bson:"ed" mapstructure:"ed"`
	IndexName          string        `structs:"idx" json:"idx" bson:"idx" mapstructure:"idx"`
	RecId              int64         `structs:"rid" json:"rid" bson:"rid" mapstructure:"rid"`
	Field              string        `structs:"fid" json:"fid" bson:"fid" mapstructure:"fid"`
	Detail             int64         `structs:"d" json:"d" bson:"d" mapstructure:"d"`
	SessionKey         string
	SessionInvalid     bool
	TokenId            string
	Database           string `structs:"db" json:"db" bson:"db" mapstructure:"db"`
	Collection         string
	Username           string `structs:"username" json:"username" bson:"username" mapstructure:"username"`
	Password           string `structs:"passwd" json:"passwd" bson:"passwd" mapstructure:"passwd"`
	Uid                int64
	UserTypeCode       string
	AsyncUniqueId      string
	DocType            string
	ProfilePartnerCode string
	ProfileUserCode    string
	CompanyCode        string `structs:"b1" json:"b1" bson:"b1" mapstructure:"b1"`
	CompanyName        string `structs:"b2" json:"b2" bson:"b2" mapstructure:"b2"`
	CompanyId          int    `structs:"b3" json:"b3" bson:"b3" mapstructure:"b3"`
	From               int    `structs:"b4" json:"b4" bson:"b4" mapstructure:"b4"`
	Size               int    `structs:"b5" json:"b5" bson:"b5" mapstructure:"b5"`
	Query              string `structs:"b6" json:"b6" bson:"b6" mapstructure:"b6"`
	TerminateAfterSize int    `structs:"b7" json:"b7" bson:"b7" mapstructure:"b7"`
	Offset             int    `structs:"b8" json:"b8" bson:"b8" mapstructure:"b8"`
	Limit              int    `structs:"b9" json:"b9" bson:"b9" mapstructure:"b9"`
	CompanyAddress     int    `structs:"b10" json:"b10" bson:"b10" mapstructure:"b10"`
	SortMap            map[string]string
	DataModel          interface{}
	SyncSource         interface{}
	SyncTarget1        interface{}
	SyncTarget2        interface{}
	SyncTarget3        interface{}
	SyncTarget4        interface{}
	Config             SyncConfig         `structs:"config" json:"config" bson:"config" mapstructure:"config"`
	Source             ConnectorContext   `structs:"source" json:"source" bson:"source" mapstructure:"source"`
	Target             []ConnectorContext `structs:"target" json:"target" bson:"target" mapstructure:"target"`
	Plugin             []ConnectorContext `structs:"plugin" json:"plugin" bson:"plugin" mapstructure:"plugin"`
}
