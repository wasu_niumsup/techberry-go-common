package facade

import "github.com/olivere/elastic/v7"

type QueryDSL interface {
	SourceCtx(includeFields []string, excludeFields []string) *elastic.FetchSourceContext
	NewBoolQuery() QueryDSL
	SetBoolQuery(boolQuery *elastic.BoolQuery) QueryDSL
	BoolQuery() *elastic.BoolQuery
	TermsAggregation() *elastic.TermsAggregation
	TermQuery(key string, value interface{}) *elastic.TermQuery
	TermsQuery(key string, value []interface{}) *elastic.TermsQuery
	MustTermByKey(key string, value interface{}) QueryDSL
	MustNotTermByKey(key string, value interface{}) QueryDSL
	MustExists(key string) QueryDSL
	MustNotExists(key string) QueryDSL
	MustMatchQueryCtx(terms []map[string]interface{}) QueryDSL
	ShouldMatchQueryCtx(terms []map[string]interface{}) QueryDSL
	FilterTerms(terms []map[string]interface{}) QueryDSL
	DateMillisRangeCtx(name string, startMillis int64, endMillis int64) QueryDSL

	//SourceCtx(includeFields []string, excludeFields []string) *elk.FetchSourceContext
	//TermByIds(key string, value []interface{}) *elk.TermsQuery
	//TermByKey(key string, value interface{}) *elk.BoolQuery
	//MustMatchQueryCtx(terms []map[string]interface{}) *elk.BoolQuery
	//DateMillisRangeCtx(q *elk.BoolQuery, name string, startMillis int64, endMillis int64) *elk.BoolQuery
}
