package facade

type AppConfig interface {
	InitPluginConfig(path string, filename string)
	AppPath() string
	AppConfigPath() string
	Config() JsonHandler
}
