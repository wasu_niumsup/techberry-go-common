package facade

const (
	BINDVARS_TYPE_POSTGRES = iota
	BINDVARS_TYPE_ORACLE
	BINDVARS_TYPE_MYSQL
)

type StatementHandler interface {
	SetBindVarsType(bindVars int)
	ParameterValue(parameters []interface{}, key string, dataType string, model interface{}) interface{}
	PrepareInsertSQL(tableName string, model interface{}, bindVars int, returningClause string) (string, []interface{})
	PrepareUpdateSQL(tableName string, model interface{}) (string, []interface{})
	PrepareDeleteSQL(tableName string, model interface{}) (string, []interface{})
	PrepareSelectSQL(tableName string, model interface{}) (string, []interface{})
	//PreparesInsertStatement(tableName string, columnList []string) string
	FieldInList(fieldName string, valueList string) string
	FieldFilter(fieldName string, value interface{}) string
	FieldLike(fieldName string, value string) string
	PreparePredicate(condition_fields []string, valueList []string) string
	FieldPredicate(fieldIndex int, fieldName string) string
	PredicateValue(paramPrefix string, fieldIndex int, fieldName string) string
	RemoveSQLInjection(s string) string
	//PrepareOraInsertSQL(tableName string, columnList []string) string
	//PreparePGInsertSQL(tableName string, columnList []string) string
}
