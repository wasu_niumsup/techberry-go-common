package factory

import (
	"techberry-go/common/v2/core"
	"techberry-go/common/v2/facade"
)

// Factory to instantiated the application filtering
func GetAppFilter() facade.AppFilter {
	return core.AppFilterInstance()
}

func GetHandler() facade.Handler {
	return core.NewZxHandler(nil)
}

func GetLogger() facade.LogEvent {
	return core.NewZxZerolog()
}
