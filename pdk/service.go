package pdk

import "techberry-go/common/v2/facade"

type ServicePDK struct {
	Properties facade.Properties
	Context    Context
	Connector  Connector
	Config     Config
	Handler    facade.Handler
	Version    string
}

// Incoming data for a new event.
// TODO: add some relevant data to reduce number of callbacks.
type EventInput struct {
	XTrace      bool
	AsyncMode   bool
	ServiceName string
	Node        string
	Version     string
	Input       interface{}
}

// A callback's response/request.
type EventOutput struct {
	EventId int // event cycle to which this belongs
	State   interface{}
	Data    []byte // carried data
}

type Notification struct {
	Type   int
	Method string
	Params interface{}
}

