package pdk

type Context interface {
	GetResponseMap(isSuccess bool, message string, record_count int, data interface{}, errorMessage string) map[string]interface{}
	ReplyMessage(err error, module string, functionName string, message string, data interface{}, total int) map[string]interface{}
	Reply(err error, module string, functionName string, data interface{}, total int) map[string]interface{}
	BindingModel(fromMap map[string]interface{}, toModel interface{}) error
	MapStruct2Model(fromMap map[string]interface{}, toModel interface{}) error
}
