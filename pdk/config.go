package pdk

import "techberry-go/common/v2/facade"

type Config interface {
	AppPath() string
	AppConfigPath() string
	Config() facade.JsonHandler
}
