package pdk

import "techberry-go/common/v2/facade"

type Connector interface {
	SetDataFactoryHost(host string, port int)
	RpcConnection(host string, port int, timeout int) facade.RpcConnector
	DataFactory() facade.DataFactory
	Elastic(name string) facade.ElasticHandler
	InitElasticConnector(name string, isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) Connector
	Redis(db int) facade.CacheHandler
	GetRedisConnection(host string, port int, db int, poolSize int) facade.CacheHandler
	InitRedisConnector(host string, port int, db int, poolSize int) Connector
	CallMicroserver(socketAddr string, pluginName string, service string, version string, data []byte) ([]byte, error)
}
