package impl

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"strings"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/utils"
	"time"

	"github.com/godror/godror"
	"github.com/mitchellh/mapstructure"
)

type Map map[string]interface{}

type MapImpl struct{}

func NewMapImpl() facade.MapHandler {
	var p facade.MapHandler = &MapImpl{}
	return p
}

func (c *MapImpl) Time(inputMap map[string]interface{}, keyPath string, defaultValue time.Time) time.Time {
	s := c.String(inputMap, keyPath, "")
	timeHandler := NewTimeImpl()
	t, err := timeHandler.ParseDateTime("2006-01-02 15:04:05", s)
	if err != nil {
		return defaultValue
	} else {
		return t
	}
}

func (c *MapImpl) String(inputMap map[string]interface{}, keyPath string, defaultValue string) string {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if utils.AssertTypeOf(value) == "string" {
			return value.(string)
		}
	}
	if defaultValue != "" && len(defaultValue) > 0 {
		return defaultValue
	} else {
		return ""
	}
	/*
		value := inputMap[key]
		if value != nil {
			return inputMap[key].(string)
		} else {
			if defaultValue != "" && len(defaultValue) > 0 {
				return defaultValue
			} else {
				return ""
			}
		}
	*/
}

func (c *MapImpl) Int64(inputMap map[string]interface{}, keyPath string, defaultValue int64) int64 {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if utils.AssertTypeOf(value) == "int64" {
			return value.(int64)
		}
		if utils.AssertTypeOf(value) == "float64" {
			return int64(value.(float64))
		} else if utils.AssertTypeOf(value) == "godror.Number" {
			c, err := strconv.ParseInt(string(value.(godror.Number)), 10, 64)
			if err == nil {
				return c
			}
		}
	}
	return defaultValue

	/*
		value := inputMap[key]
		if value != nil {
			if utils.AssertTypeOf(inputMap[key]) == "int64" {
				return inputMap[key].(int64)
			}
			if utils.AssertTypeOf(inputMap[key]) == "float64" {
				return int64(inputMap[key].(float64))
			} else if utils.AssertTypeOf(inputMap[key]) == "godror.Number" {
				c, err := strconv.ParseInt(string(inputMap[key].(godror.Number)), 10, 64)
				if err == nil {
					return c
				}
			}
		}
		return defaultValue
	*/
}

func (c *MapImpl) Int(inputMap map[string]interface{}, keyPath string, defaultValue int) int {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if utils.AssertTypeOf(value) == "int64" {
			return int(value.(int64))
		} else if utils.AssertTypeOf(value) == "string" {
			i, _ := strconv.Atoi(value.(string))
			return i
		} else if utils.AssertTypeOf(value) == "float64" {
			return int(value.(float64))
		} else if utils.AssertTypeOf(value) == "godror.Number" {
			i, _ := strconv.Atoi(string(value.(godror.Number)))
			return i
		} else {
			return value.(int)
		}
	}
	return defaultValue
	/*
		value := inputMap[key]
		if value != nil {
			if utils.AssertTypeOf(inputMap[key]) == "int64" {
				return int(inputMap[key].(int64))
			} else if utils.AssertTypeOf(inputMap[key]) == "string" {
				i, _ := strconv.Atoi(inputMap[key].(string))
				return i
			} else if utils.AssertTypeOf(inputMap[key]) == "float64" {
				return int(inputMap[key].(float64))
			} else if utils.AssertTypeOf(inputMap[key]) == "godror.Number" {
				i, _ := strconv.Atoi(string(inputMap[key].(godror.Number)))
				return i
			} else {
				return inputMap[key].(int)
			}
		}
		return defaultValue
	*/
}

func (c *MapImpl) Float(inputMap map[string]interface{}, keyPath string, defaultValue float64) float64 {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if utils.AssertTypeOf(value) == "float32" {
			return float64(value.(float32))
		} else if utils.AssertTypeOf(value) == "string" {
			f, _ := strconv.ParseFloat(value.(string), 64)
			return f
		} else if utils.AssertTypeOf(value) == "godror.Number" {
			c, err := strconv.ParseFloat(string(value.(godror.Number)), 64)
			if err == nil {
				return c
			}
		} else {
			return value.(float64)
		}
	}
	return defaultValue
	/*
		value := inputMap[key]
		if value != nil {
			if utils.AssertTypeOf(inputMap[key]) == "float32" {
				return float64(inputMap[key].(float32))
			} else if utils.AssertTypeOf(inputMap[key]) == "string" {
				f, _ := strconv.ParseFloat(inputMap[key].(string), 64)
				return f
			} else if utils.AssertTypeOf(inputMap[key]) == "godror.Number" {
				c, err := strconv.ParseFloat(string(inputMap[key].(godror.Number)), 64)
				if err == nil {
					return c
				}
			} else {
				return inputMap[key].(float64)
			}
		}
		return defaultValue
	*/
}

func (c *MapImpl) Bool(inputMap map[string]interface{}, keyPath string, defaultValue bool) bool {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if utils.AssertTypeOf(value) == "bool" {
			if b, ok := value.(bool); ok {
				return b
			} else {
				return false
			}
		}
	}
	return false
	/*
		value := inputMap[key]
		if value != nil {
			if utils.AssertTypeOf(inputMap[key]) == "bool" {
				if b, ok := inputMap[key].(bool); ok {
					return b
				} else {
					return false
				}
			}
		}
		return false
	*/
}

func (c *MapImpl) GetArray(inputMap map[string]interface{}, keyPath string) []interface{} {
	value := c.getValueFromPath(inputMap, keyPath)
	if value != nil {
		if x, ok := value.([]interface{}); ok {
			return x
		}
	}
	return nil
}

func (c *MapImpl) GetByPath(key string, mp map[string]interface{}) (val interface{}, ok bool) {
	if val, ok := mp[key]; ok {
		return val, true
	}

	// has sub key? eg. "top.sub"
	if !strings.ContainsRune(key, '.') {
		return nil, false
	}

	keys := strings.Split(key, ".")
	topK := keys[0]

	// find top item data based on top key
	var item interface{}
	if item, ok = mp[topK]; !ok {
		return
	}

	for _, k := range keys[1:] {
		switch tData := item.(type) {
		case map[string]string: // is simple map
			item, ok = tData[k]
			if !ok {
				return
			}
		case map[string]interface{}: // is map(decode from toml/json)
			if item, ok = tData[k]; !ok {
				return
			}
		case map[interface{}]interface{}: // is map(decode from yaml)
			if item, ok = tData[k]; !ok {
				return
			}
		default: // error
			ok = false
			return
		}
	}

	return item, true
}

func (c *MapImpl) Keys(mp interface{}) (keys []string) {
	rftVal := reflect.ValueOf(mp)
	if rftVal.Type().Kind() == reflect.Ptr {
		rftVal = rftVal.Elem()
	}

	if rftVal.Kind() != reflect.Map {
		return
	}

	for _, key := range rftVal.MapKeys() {
		keys = append(keys, key.String())
	}
	return
}

func (c *MapImpl) Values(mp interface{}) (values []interface{}) {
	rftTyp := reflect.TypeOf(mp)
	if rftTyp.Kind() == reflect.Ptr {
		rftTyp = rftTyp.Elem()
	}

	if rftTyp.Kind() != reflect.Map {
		return
	}

	rftVal := reflect.ValueOf(mp)
	for _, key := range rftVal.MapKeys() {
		values = append(values, rftVal.MapIndex(key).Interface())
	}
	return
}

func (c *MapImpl) LowerKeys(f interface{}) interface{} {
	switch f := f.(type) {
	case []interface{}:
		for i := range f {
			f[i] = c.LowerKeys(f[i])
		}
		return f
	case map[string]interface{}:
		lf := make(map[string]interface{}, len(f))
		for k, v := range f {
			lf[strings.ToLower(k)] = c.LowerKeys(v)
		}
		return lf
	default:
		return f
	}
}

func (c *MapImpl) ToMap(v interface{}) map[string]interface{} {
	var p []byte
	//    var err := error
	p, err := json.Marshal(v)
	if err == nil {
		jsonMap := make(map[string]interface{})
		err := json.Unmarshal(p, &jsonMap)
		if err == nil {
			return jsonMap
		}
	}
	return nil
}

func (c *MapImpl) FromJson(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (c *MapImpl) ToJson(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (c *MapImpl) ToStruct(data interface{}, result interface{}) error {
	switch f := data.(type) {
	case map[string]interface{}:
		return mapstructure.Decode(f, result)
	case interface{}:
		dataMap := c.ToMap(f)
		return mapstructure.Decode(dataMap, result)
	default:
		return errors.New("Can't bind data to struct model.")
	}
}

func (c *MapImpl) FillStruct(data map[string]interface{}, result interface{}) {
	t := reflect.ValueOf(result).Elem()
	for k, v := range data {
		val := t.FieldByName(k)
		val.Set(reflect.ValueOf(v))
	}
}

func (c *MapImpl) getValueFromPath(m map[string]interface{}, path string) interface{} {
	var obj interface{} = m
	var val interface{} = nil

	parts := strings.Split(path, ".")
	for _, p := range parts {
		if v, ok := obj.(map[string]interface{}); ok {
			obj = v[p]
			val = obj
		} else {
			return nil
		}
	}

	return val
}
