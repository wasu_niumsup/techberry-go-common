package impl

import (
	"bytes"
	"fmt"
	"html/template"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/utils"
	"time"
)

type StatementImpl struct {
	tagPrefix     string
	_bindVarsType int
}

func NewStatementImpl() facade.StatementHandler {
	var p facade.StatementHandler = &StatementImpl{}
	return p
}

func (c *StatementImpl) SetBindVarsType(bindVars int) {
	c._bindVarsType = bindVars
}

func (c *StatementImpl) ParameterValue(parameters []interface{}, key string, dataType string, model interface{}) interface{} {
	tagUtils := new(StructTag) // Instance point to struct tag utility class
	c.tagPrefix = "db"
	value := tagUtils.GetValueByFieldName(key, model)
	switch dataType {
	case "int":
		return value.(int)
	case "int64":
		return value.(int64)
	case "float64":
		return value.(float64)
	case "Time":
		return value.(time.Time)
	case "string":
		return value.(string)
	}
	return nil
}

func (c *StatementImpl) PrepareInsertSQL(tableName string, model interface{}, bindVars int, returningClause string) (string, []interface{}) {
	var binding_parameters []interface{}
	tagUtils := new(StructTag) // Instance point to struct tag utility class
	err, tagMap := tagUtils.ParseStruct("db", model)
	if err == nil {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("INSERT INTO ")
		stmtBuffer.WriteString(tableName)
		stmtBuffer.WriteString("(")
		index := 0
		i := 0
		for key, value := range tagMap {
			if key != "Id" && key != "key" {
				f := value.(FieldInfo)
				if StringUtils.IsNotEmptyString(f.TargetName) && f.TargetType != "skip_insert" {
					if i > 0 {
						stmtBuffer.WriteString(",")
						valueBuffer.WriteString(",")
					}
					stmtBuffer.WriteString(f.TargetName)
					if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
						if f.TargetName == "write_date" || f.TargetName == "create_date" {
							valueBuffer.WriteString("now()")
						} else {
							valueBuffer.WriteString(fmt.Sprintf("$%d", index+1))
							binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, key, f.Type, model))
							index = index + 1
						}
					} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
						if f.TargetName == "write_date" || f.TargetName == "create_date" {
							valueBuffer.WriteString("current_timestamp()")
						} else {
							valueBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
							binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, key, f.Type, model))
							index = index + 1
						}
					}
					i = i + 1
				}
			}
		}
		stmtBuffer.WriteString(")")
		stmtBuffer.WriteString(" VALUES (")
		stmtBuffer.WriteString(valueBuffer.String())
		if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
			stmtBuffer.WriteString(") ")
			stmtBuffer.WriteString(returningClause)
		} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
			stmtBuffer.WriteString(")")
		}
		return stmtBuffer.String(), binding_parameters
	}
	return "", binding_parameters
}

func (c *StatementImpl) PrepareUpdateSQL(tableName string, model interface{}) (string, []interface{}) {
	var binding_parameters []interface{}
	tagUtils := new(StructTag) // Instance point to struct tag utility class
	err, tagMap := tagUtils.ParseStruct("db", model)
	if err == nil {
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("UPDATE ")
		stmtBuffer.WriteString(tableName)
		stmtBuffer.WriteString(" SET ")
		index := 0
		i := 0
		for key, value := range tagMap {
			if key != "Id" && key != "key" {
				f := value.(FieldInfo)
				if StringUtils.IsNotEmptyString(f.TargetName) && f.TargetType != "skip_update" {
					if i > 0 {
						stmtBuffer.WriteString(",")
					}
					stmtBuffer.WriteString(f.TargetName)
					stmtBuffer.WriteString(" = ")
					if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
						if f.TargetName == "write_date" || f.TargetName == "create_date" {
							stmtBuffer.WriteString("now()")
						} else {
							stmtBuffer.WriteString(fmt.Sprintf("$%d", index+1))
							binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, key, f.Type, model))
							index = index + 1
						}
					} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
						if f.TargetName == "write_date" || f.TargetName == "create_date" {
							stmtBuffer.WriteString("current_timestamp()()")
						} else {
							stmtBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
							binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, key, f.Type, model))
							index = index + 1
						}
					}

					i = i + 1
				}
			}
		}
		keyField := tagMap["key"].(*KeyField)
		if len(keyField.Name) > 0 {
			stmtBuffer.WriteString(" WHERE ")
			i := 0
			for _, value := range keyField.Name {
				if i > 0 {
					stmtBuffer.WriteString(" and ")
				}
				f := tagMap[value].(FieldInfo)
				if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
					if StringUtils.IsNotEmptyString(f.TargetName) {
						stmtBuffer.WriteString(f.TargetName)
						stmtBuffer.WriteString(" = ")
						stmtBuffer.WriteString(fmt.Sprintf("$%d", index+1))
						binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, value, f.Type, model))
						index = index + 1
					}
				} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
					if StringUtils.IsNotEmptyString(f.TargetName) {
						stmtBuffer.WriteString(f.TargetName)
						stmtBuffer.WriteString(" = ")
						stmtBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
						binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, value, f.Type, model))
						index = index + 1
					}
				}
				i = i + 1
			}

		}
		return stmtBuffer.String(), binding_parameters
	}
	return "", binding_parameters
}

func (c *StatementImpl) PrepareDeleteSQL(tableName string, model interface{}) (string, []interface{}) {
	var binding_parameters []interface{}
	tagUtils := new(StructTag) // Instance point to struct tag utility class
	err, tagMap := tagUtils.ParseStruct("db", model)
	if err != nil {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("DELETE FROM ")
		stmtBuffer.WriteString(tableName)
		index := 0
		keyField := tagMap["key"].(KeyField)
		if len(keyField.Name) > 0 {
			stmtBuffer.WriteString(" WHERE ")
			for _, value := range keyField.Name {
				if index > 0 {
					stmtBuffer.WriteString(" and ")
				}
				stmtBuffer.WriteString(value)
				stmtBuffer.WriteString(" = ")
				if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
					valueBuffer.WriteString(fmt.Sprintf("$%d", index+1))
				} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
					valueBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
				}
				f := tagMap[value].(FieldInfo)
				binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, value, f.Type, model))
			}
		}
		stmtBuffer.WriteString(valueBuffer.String())
		return stmtBuffer.String(), binding_parameters
	}
	return "", binding_parameters
}

func (c *StatementImpl) PrepareSelectSQL(tableName string, model interface{}) (string, []interface{}) {
	var binding_parameters []interface{}
	tagUtils := new(StructTag) // Instance point to struct tag utility class
	err, tagMap := tagUtils.ParseStruct("db", model)
	if err != nil {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("SELECT * FROM ")
		stmtBuffer.WriteString(tableName)
		index := 0
		keyField := tagMap["key"].(KeyField)
		if len(keyField.Name) > 0 {
			stmtBuffer.WriteString(" WHERE ")
			for _, value := range keyField.Name {
				if index > 0 {
					stmtBuffer.WriteString(" and ")
				}
				stmtBuffer.WriteString(value)
				stmtBuffer.WriteString(" = ")
				if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
					valueBuffer.WriteString(fmt.Sprintf("$%d", index+1))
				} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
					valueBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
				}
				f := tagMap[value].(FieldInfo)
				binding_parameters = append(binding_parameters, c.ParameterValue(binding_parameters, value, f.Type, model))
			}
		}
		stmtBuffer.WriteString(valueBuffer.String())
		return stmtBuffer.String(), binding_parameters
	}
	return "", binding_parameters
}

/*
func (c *StatementImpl) PreparesInsertStatement(tableName string, columnList []string) string {
	if len(columnList) > 0 {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("INSERT INTO ")
		stmtBuffer.WriteString(tableName)
		stmtBuffer.WriteString("(")
		for index, value := range columnList {
			if index > 0 {
				stmtBuffer.WriteString(",")
				valueBuffer.WriteString(",")
			}
			stmtBuffer.WriteString(value)
			if c._bindVarsType == facade.BINDVARS_TYPE_POSTGRES {
				if value == "write_date" || value == "create_date" {
					valueBuffer.WriteString("now()")
				} else {
					valueBuffer.WriteString(fmt.Sprintf("$%d", index+1))
				}
			} else if c._bindVarsType == facade.BINDVARS_TYPE_ORACLE {
				if value == "write_date" || value == "create_date" {
					valueBuffer.WriteString("current_timestamp()")
				} else {
					valueBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
				}
			}
		}
		stmtBuffer.WriteString(")")
		stmtBuffer.WriteString(" VALUES (")
		stmtBuffer.WriteString(valueBuffer.String())
		stmtBuffer.WriteString(") RETURNING id")
		return stmtBuffer.String()
	} else {
		return ""
	}
}
*/

func (c *StatementImpl) FieldInList(fieldName string, valueList string) string {
	return fmt.Sprintf(" %s in (%s) ", fieldName, valueList)
}

func (c *StatementImpl) FieldFilter(fieldName string, value interface{}) string {
	if utils.AssertTypeOf(value) == "int64" {
		value = int(value.(int64))
		return fmt.Sprintf(" %s = %d ", fieldName, value)
	} else if utils.AssertTypeOf(value) == "int" {
		value = value.(int)
		return fmt.Sprintf(" %s = %d ", fieldName, value)
	} else if utils.AssertTypeOf(value) == "string" {
		value = c.RemoveSQLInjection(value.(string))
		return fmt.Sprintf(" %s = '%s' ", fieldName, value)
	} else if utils.AssertTypeOf(value) == "float64" {
		value = value.(float64)
		return fmt.Sprintf(" %s = %.2f ", fieldName, value)
	} else {
		return ""
	}
}

// fieldName ilike $1
func (c *StatementImpl) FieldLike(fieldName string, value string) string {
	tempBuffer := bytes.NewBufferString("")
	left := fieldName
	operator := " ilike "
	tempBuffer.WriteString(left)
	tempBuffer.WriteString(operator)
	tempBuffer.WriteString("'%")
	value = c.RemoveSQLInjection(value)
	tempBuffer.WriteString(value)
	tempBuffer.WriteString("%'")
	return tempBuffer.String()
}

func (c *StatementImpl) PreparePredicate(condition_fields []string, valueList []string) string {
	length := len(valueList)
	buffer := bytes.NewBufferString("")
	tempBuffer := bytes.NewBufferString("")
	if length > 0 {
		for i, value := range valueList {
			value = c.RemoveSQLInjection(value)
			if i > 0 {
				tempBuffer.WriteString(" or ")
			}
			for indx, fieldName := range condition_fields {
				if indx > 0 {
					tempBuffer.WriteString(" or ")
				}
				tempBuffer.WriteString(c.FieldLike(fieldName, value))
			}
		}
		buffer.WriteString(" (")
		buffer.WriteString(tempBuffer.String())
		buffer.WriteString(" )")
	}
	return buffer.String()
}

func (c *StatementImpl) FieldPredicate(fieldIndex int, fieldName string) string {
	tempBuffer := bytes.NewBufferString("")
	if fieldIndex > 1 {
		tempBuffer.WriteString(",")
	}
	tempBuffer.WriteString(fmt.Sprintf(" x_%s = $%d", fieldName, fieldIndex))
	return tempBuffer.String()
}

func (c *StatementImpl) PredicateValue(paramPrefix string, fieldIndex int, fieldName string) string {
	tempBuffer := bytes.NewBufferString("")
	if fieldIndex > 1 {
		tempBuffer.WriteString(",")
	}
	tempBuffer.WriteString(fmt.Sprintf(" %s = %s%d", fieldName, paramPrefix, fieldIndex))
	return tempBuffer.String()
}

func (c *StatementImpl) RemoveSQLInjection(s string) string {
	return template.HTMLEscapeString(s)
}

/*
func (c *StatementImpl) PrepareOraInsertSQL(tableName string, columnList []string) string {
	if len(columnList) > 0 {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("INSERT INTO ")
		stmtBuffer.WriteString(tableName)
		stmtBuffer.WriteString("(")
		for index, value := range columnList {
			if index > 0 {
				stmtBuffer.WriteString(",")
				valueBuffer.WriteString(",")
			}
			stmtBuffer.WriteString(value)
			if value == "write_date" || value == "create_date" {
				valueBuffer.WriteString("now()")
			} else {
				valueBuffer.WriteString(fmt.Sprintf(":param%d", index+1))
			}
		}
		stmtBuffer.WriteString(")")
		stmtBuffer.WriteString(" VALUES (")
		stmtBuffer.WriteString(valueBuffer.String())
		stmtBuffer.WriteString(")")
		return stmtBuffer.String()
	} else {
		return ""
	}
}

func (c *StatementImpl) PreparePGInsertSQL(tableName string, columnList []string) string {
	if len(columnList) > 0 {
		valueBuffer := bytes.NewBufferString("")
		stmtBuffer := bytes.NewBufferString("")
		stmtBuffer.WriteString("INSERT INTO ")
		stmtBuffer.WriteString(tableName)
		stmtBuffer.WriteString("(")
		for index, value := range columnList {
			if index > 0 {
				stmtBuffer.WriteString(",")
				valueBuffer.WriteString(",")
			}
			stmtBuffer.WriteString(value)
			if value == "write_date" || value == "create_date" {
				valueBuffer.WriteString("now()")
			} else {
				valueBuffer.WriteString(fmt.Sprintf("$%d", index+1))
			}
		}
		stmtBuffer.WriteString(")")
		stmtBuffer.WriteString(" VALUES (")
		stmtBuffer.WriteString(valueBuffer.String())
		stmtBuffer.WriteString(") RETURNING id")
		return stmtBuffer.String()
	} else {
		return ""
	}
}

*/
