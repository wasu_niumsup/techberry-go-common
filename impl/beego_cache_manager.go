package impl

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"techberry-go/common/v2/facade"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
)

type BeegoCacheManager struct {
	Cache cache.Cache
}

func NewBeegoCacheManager(cache cache.Cache) facade.RpcCacheConnector {
	var p facade.RpcCacheConnector = &BeegoCacheManager{Cache: cache}
	return p
}

func (self BeegoCacheManager) Save(key string, val interface{}, timeout time.Duration) {
	// Struct to json string
	b, err := json.Marshal(val)
	if err != nil {
		return
	}
	jsonString := string(b)
	raw := base64.RawStdEncoding.EncodeToString([]byte(jsonString))
	err = self.Cache.Put(key, raw, timeout)
	if err != nil {
		beego.Info("Failed to save connector.")
	} else {
		beego.Info("Save connector into cache successfully.")
	}
}

func byteToString(bs []uint8) string {
	b := make([]byte, len(bs))
	for i, v := range bs {
		b[i] = byte(v)
	}
	return string(b)
}

func (self BeegoCacheManager) Load(key string) (interface{}, error) {
	cred := facade.OdooCredential{}
	//connector := RPCConnector{}

	val := self.Cache.Get(key)
	if val != nil {
		var byteValue []uint8 = val.([]uint8)
		str := byteToString(byteValue)
		strDecode, _ := base64.RawStdEncoding.DecodeString(str)
		err := json.Unmarshal([]byte(strDecode), &cred)
		/*
			connector = RPCConnector{
				_host:         cred.Host,
				_port:         cred.Port,
				_timeout:      cred.Timeout,
				_httpClient:   http.DefaultClient,
				CustomHeaders: make(map[string]string),
			}
		*/
		return cred, err
	} else {
		return cred, errors.New("No credential.")
	}
}

func (self BeegoCacheManager) Remove(key string) {
	self.Cache.Delete(key)
}
