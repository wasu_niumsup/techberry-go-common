package impl

import "techberry-go/common/v2/facade"

var (
	StringUtils facade.StringHandler = NewStringImpl()
	MapUtils    facade.MapHandler    = NewMapImpl()
)
