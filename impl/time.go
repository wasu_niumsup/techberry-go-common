package impl

import (
	"log"
	"strconv"
	"techberry-go/common/v2/facade"
	"time"
)

type TimeImpl struct {
	_location string
}

// ISO8601 format to millis instead of to nanos
const RFC3339Millis = "2006-01-02T15:04:05.000Z"

func NewTimeImpl() facade.TimeHandler {
	var p facade.TimeHandler = &TimeImpl{_location: "UTC"}
	return p
}

func (c *TimeImpl) SetLocation(loc string) {
	c._location = loc
}

func (c *TimeImpl) DayDiffFromNow(year int, month int, day int, hour int, min int, sec int) int {
	s := c.DateTimeMillis(year, month, day, hour, min, sec)
	loc, _ := time.LoadLocation(c._location)
	now := time.Now().In(loc)
	diff := now.Sub(s)
	days := int(diff.Hours() / 24)
	return days
}

func (c *TimeImpl) CurrentTimeMillis() int64 {
	loc, _ := time.LoadLocation(c._location)
	now := time.Now().In(loc)
	unixNano := now.UnixNano()
	startMillis := unixNano / int64(time.Millisecond)
	return startMillis
}

func (c *TimeImpl) TimeMillisFromDate(date time.Time) int64 {
	return date.UnixNano() / int64(time.Millisecond)
}

func (c *TimeImpl) MonthIndex(month time.Month) int {
	switch month {
	case time.January:
		return 1
	case time.February:
		return 2
	case time.March:
		return 3
	case time.April:
		return 4
	case time.May:
		return 5
	case time.June:
		return 6
	case time.July:
		return 7
	case time.August:
		return 8
	case time.September:
		return 9
	case time.October:
		return 10
	case time.November:
		return 11
	case time.December:
		return 12
	}
	return 0
}

func (c *TimeImpl) DateTimeMillis(year int, month int, day int, hour int, min int, sec int) time.Time {
	loc, _ := time.LoadLocation(c._location)
	var date time.Time
	switch month {
	case 1:
		date = time.Date(year, time.January, day, hour, min, sec, 0, loc)
	case 2:
		date = time.Date(year, time.February, day, hour, min, sec, 0, loc)
	case 3:
		date = time.Date(year, time.March, day, hour, min, sec, 0, loc)
	case 4:
		date = time.Date(year, time.April, day, hour, min, sec, 0, loc)
	case 5:
		date = time.Date(year, time.May, day, hour, min, sec, 0, loc)
	case 6:
		date = time.Date(year, time.June, day, hour, min, sec, 0, loc)
	case 7:
		date = time.Date(year, time.July, day, hour, min, sec, 0, loc)
	case 8:
		date = time.Date(year, time.August, day, hour, min, sec, 0, loc)
	case 9:
		date = time.Date(year, time.September, day, hour, min, sec, 0, loc)
	case 10:
		date = time.Date(year, time.October, day, hour, min, sec, 0, loc)
	case 11:
		date = time.Date(year, time.November, day, hour, min, sec, 0, loc)
	case 12:
		date = time.Date(year, time.December, day, hour, min, sec, 0, loc)
	}
	return date
}

func (c *TimeImpl) TimeMillis(year int, month int, day int, hour int, min int, sec int) int64 {
	date := c.DateTimeMillis(year, month, day, hour, min, sec)
	return date.UnixNano() / int64(time.Millisecond)
}

func (c *TimeImpl) TimeMillisFromNow(year int, month int, day int) (int64, int64) {
	loc, _ := time.LoadLocation(c._location)
	now := time.Now().In(loc)
	unixNano := now.UnixNano()
	startMillis := unixNano / int64(time.Millisecond)
	expireTime := now.AddDate(year, month, day) // Add expire to a year by default
	unixNano = expireTime.UnixNano()
	expireMillis := unixNano / int64(time.Millisecond)
	return startMillis, expireMillis
}

/*
func (c *TimeImpl) TimeMillis(year int, month int, day int) (int64, int64) {
	loc, _ := time.LoadLocation(c._location)
	now := time.Now().In(loc)
	unixNano := now.UnixNano()
	startMillis := unixNano / int64(time.Millisecond)
	expireTime := now.AddDate(1, 0, 0) // Add expire to a year by default
	unixNano = expireTime.UnixNano()
	expireMillis := unixNano / int64(time.Millisecond)
	return startMillis, expireMillis
}
*/

func (c *TimeImpl) Now() string {
	now := time.Now()
	return now.Format("20060102")
}

func (c *TimeImpl) CurrentDateTime(layout string) time.Time {
	loc, _ := time.LoadLocation(c._location)
	t0 := time.Now().In(loc)
	s := t0.Format(layout)
	t1, err := time.Parse(layout, s)
	if err != nil {
		log.Fatal(err)
	}
	return t1
}

func (c *TimeImpl) ParseDateTime(layout string, value string) (time.Time, error) {
	if StringUtils.IsEmptyString(layout) {
		layout = "2006-01-02" // default format.
	}
	return time.Parse(layout, value)
}

// ParseTimestamp parses a string that represents an ISO8601 time or a unix epoch
func (c *TimeImpl) ParseTimestamp(data string) (time.Time, error) {
	d := time.Now().UTC()
	if data != "now" {
		// fmt.Println("we should try to parse")
		dd, err := time.Parse(RFC3339Millis, data)
		if err != nil {
			dd, err = time.Parse(time.RFC3339, data)
			if err != nil {
				dd, err = time.Parse(time.RFC3339Nano, data)
				if err != nil {
					if data == "" {
						data = "0"
					}
					t, err := strconv.ParseInt(data, 10, 64)
					if err != nil {
						return time.Now(), err
					}
					dd = time.Unix(0, t*int64(time.Millisecond))
				}
			}
		}
		d = dd
	}
	return d.UTC(), nil
}
