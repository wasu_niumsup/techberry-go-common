package impl

import (
	"strconv"
	"techberry-go/common/v2/facade"
)

type QuoteImpl struct{}

func NewQuoteImpl() facade.Quote {
	var p facade.Quote = &QuoteImpl{}
	return p
}

func (c *QuoteImpl) Quote(s string) string {
	return strconv.Quote(s)
}

func (c *QuoteImpl) AppendQuote(dst []byte, s string) []byte {
	return strconv.AppendQuote(dst, s)
}

func (c *QuoteImpl) QuoteToASCII(s string) string {
	return strconv.QuoteToASCII(s)
}

func (c *QuoteImpl) AppendQuoteToASCII(dst []byte, s string) []byte {
	return strconv.AppendQuoteToASCII(dst, s)
}

func (c *QuoteImpl) QuoteToGraphic(s string) string {
	return strconv.QuoteToGraphic(s)
}

func (c *QuoteImpl) AppendQuoteToGraphic(dst []byte, s string) []byte {
	return strconv.AppendQuoteToGraphic(dst, s)
}

func (c *QuoteImpl) QuoteRune(r rune) string {
	return strconv.QuoteRune(r)
}

func (c *QuoteImpl) AppendQuoteRune(dst []byte, r rune) []byte {
	return strconv.AppendQuoteRune(dst, r)
}

func (c *QuoteImpl) QuoteRuneToASCII(r rune) string {
	return strconv.QuoteRuneToASCII(r)
}

func (c *QuoteImpl) AppendQuoteRuneToASCII(dst []byte, r rune) []byte {
	return strconv.AppendQuoteRuneToASCII(dst, r)
}

func (c *QuoteImpl) QuoteRuneToGraphic(r rune) string {
	return strconv.QuoteRuneToGraphic(r)
}

func (c *QuoteImpl) AppendQuoteRuneToGraphic(dst []byte, r rune) []byte {
	return strconv.AppendQuoteRuneToGraphic(dst, r)
}

func (c *QuoteImpl) CanBackquote(s string) bool {
	return strconv.CanBackquote(s)
}

func (c *QuoteImpl) UnquoteChar(s string, quote byte) (value rune, multibyte bool, tail string, err error) {
	return strconv.UnquoteChar(s, quote)
}

func (c *QuoteImpl) Unquote(s string) (string, error) {
	return strconv.Unquote(s)
}

func (c *QuoteImpl) IsPrint(r rune) bool {
	return strconv.IsPrint(r)
}

func (c *QuoteImpl) IsGraphic(r rune) bool {
	return strconv.IsGraphic(r)
}
