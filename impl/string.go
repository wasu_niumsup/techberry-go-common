package impl

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"techberry-go/common/v2/facade"
	"time"

	uuid1 "github.com/google/uuid"
	uuid2 "github.com/satori/go.uuid"
	"github.com/segmentio/ksuid"
)

// Position for padding string
const (
	PosLeft uint8 = iota
	PosRight
)

var (
	stdChars        = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
	stdLength       = 16
	UUIDLen         = 20
	errConvertFail  = errors.New("convert data type is failure")
	errInvalidParam = errors.New("invalid input parameter")

	// some regex for convert string.
	toSnakeReg  = regexp.MustCompile("[A-Z][a-z]")
	toCamelRegs = map[string]*regexp.Regexp{
		" ": regexp.MustCompile(" +[a-zA-Z]"),
		"-": regexp.MustCompile("-+[a-zA-Z]"),
		"_": regexp.MustCompile("_+[a-zA-Z]"),
	}
)

type StringImpl struct{}

func NewStringImpl() facade.StringHandler {
	var p facade.StringHandler = &StringImpl{}
	return p
}

func (c *StringImpl) Trim(s string, cutSet ...string) string {
	if len(cutSet) > 0 && cutSet[0] != "" {
		return strings.Trim(s, cutSet[0])
	}

	return strings.TrimSpace(s)
}

func (c *StringImpl) TrimLeft(s string, cutSet ...string) string {
	if len(cutSet) > 0 {
		return strings.TrimLeft(s, cutSet[0])
	}

	return strings.TrimLeft(s, " ")
}

func (c *StringImpl) TrimRight(s string, cutSet ...string) string {
	if len(cutSet) > 0 {
		return strings.TrimRight(s, cutSet[0])
	}

	return strings.TrimRight(s, " ")
}

func (c *StringImpl) FilterEmail(s string) string {
	s = strings.TrimSpace(s)
	i := strings.LastIndex(s, "@")
	if i == -1 {
		return s
	}

	// According to rfc5321, "The local-part of a mailbox MUST BE treated as case sensitive"
	return s[0:i] + "@" + strings.ToLower(s[i+1:])
}

func (c *StringImpl) Substr(s string, pos, length int) string {
	runes := []rune(s)
	strLen := len(runes)

	// pos is to large
	if pos >= strLen {
		return ""
	}

	l := pos + length
	if l > strLen {
		l = strLen
	}

	return string(runes[pos:l])
}

func (c *StringImpl) Padding(s, pad string, length int, pos uint8) string {
	diff := len(s) - length
	if diff >= 0 { // do not need padding.
		return s
	}

	if pad == "" || pad == " " {
		mark := ""
		if pos == PosRight { // to right
			mark = "-"
		}

		// padding left: "%7s", padding right: "%-7s"
		tpl := fmt.Sprintf("%s%d", mark, length)
		return fmt.Sprintf(`%`+tpl+`s`, s)
	}

	if pos == PosRight { // to right
		return s + c.Repeat(pad, -diff)
	}

	return c.Repeat(pad, -diff) + s
}

// PadLeft a string.
func (c *StringImpl) PadLeft(s, pad string, length int) string {
	return c.Padding(s, pad, length, PosLeft)
}

func (c *StringImpl) PadRight(s, pad string, length int) string {
	return c.Padding(s, pad, length, PosRight)
}

func (c *StringImpl) Repeat(s string, times int) string {
	if times < 2 {
		return s
	}

	var ss []string
	for i := 0; i < times; i++ {
		ss = append(ss, s)
	}

	return strings.Join(ss, "")
}

func (c *StringImpl) RepeatRune(char rune, times int) (chars []rune) {
	for i := 0; i < times; i++ {
		chars = append(chars, char)
	}
	return
}

func (c *StringImpl) Bool(s string, defaultValue bool) bool {
	// return strconv.ParseBool(Trim(s))
	lower := strings.ToLower(s)
	switch lower {
	case "1", "on", "yes", "true":
		return true
	case "0", "off", "no", "false":
		return false
	default:
		return defaultValue
	}
}

func (c *StringImpl) String(s string, defaultValue string) string {
	if StringUtils.IsEmptyString(s) {
		return defaultValue
	} else {
		return s
	}
}

func (c *StringImpl) Float(s string, defaultValue float32) float32 {
	f, err := strconv.ParseFloat(s, 32)
	if err != nil {
		return defaultValue
	} else {
		return float32(f)
	}
}

func (c *StringImpl) Float64(s string, defaultValue float64) float64 {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return defaultValue
	} else {
		return f
	}
}

func (c *StringImpl) Int64(s string, defaultValue int64) int64 {
	f, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return defaultValue
	} else {
		return f
	}
}

func (c *StringImpl) Int(s string, defaultValue int) int {
	f, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return defaultValue
	} else {
		return int(f)
	}
}

func (c *StringImpl) Array(s string, sep ...string) []string {
	return c.Slice(s, sep...)
}

func (c *StringImpl) Slice(s string, sep ...string) []string {
	if len(sep) > 0 {
		return c.Split(s, sep[0])
	}

	return c.Split(s, ",")
}

func (c *StringImpl) Time(s string, layouts ...string) (t time.Time, err error) {
	var layout string
	if len(layouts) > 0 { // custom layout
		layout = layouts[0]
	} else { // auto match layout.
		switch len(s) {
		case 8:
			layout = "20060102"
		case 10:
			layout = "2006-01-02"
		case 13:
			layout = "2006-01-02 15"
		case 16:
			layout = "2006-01-02 15:04"
		case 19:
			layout = "2006-01-02 15:04:05"
		case 20: // time.RFC3339
			layout = "2006-01-02T15:04:05Z07:00"
		}
	}

	if layout == "" {
		err = errInvalidParam
		return
	}

	// has 'T' eg.2006-01-02T15:04:05
	if strings.ContainsRune(s, 'T') {
		layout = strings.Replace(layout, " ", "T", -1)
	}

	// eg: 2006/01/02 15:04:05
	if strings.ContainsRune(s, '/') {
		layout = strings.Replace(layout, "-", "/", -1)
	}

	t, err = time.Parse(layout, s)
	// t, err = time.ParseInLocation(layout, s, time.Local)
	return
}

func (c *StringImpl) RandomBytes(length int) ([]byte, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (c *StringImpl) RandomString(length int) (string, error) {
	b, err := c.RandomBytes(length)
	return base64.URLEncoding.EncodeToString(b), err
}

func (c *StringImpl) SecureRandomString(length int) string {
	if length == 0 {
		return ""
	}
	clen := len(stdChars)
	if clen < 2 || clen > 256 {
		panic("uniuri: wrong charset length for NewLenChars")
	}
	maxrb := 255 - (256 % clen)
	b := make([]byte, length)
	r := make([]byte, length+(length/4)) // storage for random bytes.
	i := 0
	for {
		if _, err := rand.Read(r); err != nil {
			panic("uniuri: error reading random bytes: " + err.Error())
		}
		for _, rb := range r {
			c := int(rb)
			if c > maxrb {
				// Skip this number to avoid modulo bias.
				continue
			}
			b[i] = stdChars[c%clen]
			i++
			if i == length {
				return string(b)
			}
		}
	}
}

func (c *StringImpl) InArray(v interface{}, in interface{}) (ok bool, i int) {
	val := reflect.Indirect(reflect.ValueOf(in))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for ; i < val.Len(); i++ {
			if ok = v == val.Index(i).Interface(); ok {
				return
			}
		}
	}
	return
}

func (c *StringImpl) IsEmptyString(s string) bool {
	if s != "" && len(s) > 0 {
		return false
	} else {
		return true
	}
}

func (c *StringImpl) IsNotEmptyString(s string) bool {
	return !c.IsEmptyString(s)
}

func (c *StringImpl) GetByteArray(s string) []byte {
	return []byte(s)
}

func (c *StringImpl) InArrayString(val string, array []string) (ok bool, i int) {
	for i = range array {
		if ok = array[i] == val; ok {
			return
		}
	}
	return
}

func (c *StringImpl) EqualIgoreCase(s, t string) bool {
	return strings.EqualFold(s, t)
}

func (c *StringImpl) GenerateSortableUid() string {
	uid := ksuid.New().String()
	if c.IsNotEmptyString(uid) {
		return uid
	} else {
		return ""
	}
}

func (c *StringImpl) ReplaceAll(s, old, new string) string {
	return strings.ReplaceAll(s, old, new)
}

func (c *StringImpl) Split(s, sep string) []string {
	return strings.Split(s, sep)
}

func (c *StringImpl) Lower(s string) string {
	return strings.ToLower(s)
}

func (c *StringImpl) Upper(s string) string {
	return strings.ToUpper(s)
}

func (c *StringImpl) UpperWord(s string) string {
	if len(s) == 0 {
		return s
	}

	ss := strings.Split(s, " ")
	ns := make([]string, len(ss))
	for i, word := range ss {
		ns[i] = c.UpperFirst(word)
	}
	return strings.Join(ns, " ")
}

func (c *StringImpl) LowerFirst(s string) string {
	if len(s) == 0 {
		return s
	}

	f := s[0]
	if f >= 'A' && f <= 'Z' {
		return strings.ToLower(string(f)) + s[1:]
	}
	return s
}

func (c *StringImpl) UpperFirst(s string) string {
	if len(s) == 0 {
		return s
	}

	f := s[0]
	if f >= 'a' && f <= 'z' {
		return strings.ToUpper(string(f)) + s[1:]
	}
	return s
}

func (c *StringImpl) UpperTitle(s string) string {
	return strings.Title(s)
}

func (c *StringImpl) Match(s string, pattern string) bool {
	return false
}

func (c *StringImpl) IsStrLenValid(s string, minLength int, maxLength int) bool {
	return false
}

func (c *StringImpl) GenerateUuid(urn bool) string {
	if urn {
		u1, err := uuid1.NewUUID()
		if err != nil {
			return ""
		}
		return u1.URN()
	} else {
		u2, err := uuid2.NewV4()
		if err != nil {
			return ""
		}
		return u2.String()
	}
}

func (c *StringImpl) Snake(s string, sep ...string) string {
	return c.SnakeCase(s, sep...)
}

func (c *StringImpl) SnakeCase(s string, sep ...string) string {
	sepChar := "_"
	if len(sep) > 0 {
		sepChar = sep[0]
	}

	newStr := toSnakeReg.ReplaceAllStringFunc(s, func(s string) string {
		return sepChar + c.LowerFirst(s)
	})

	return strings.TrimLeft(newStr, sepChar)
}

func (c *StringImpl) Camel(s string, sep ...string) string {
	return c.CamelCase(s, sep...)
}

func (c *StringImpl) CamelCase(s string, sep ...string) string {
	sepChar := "_"
	if len(sep) > 0 {
		sepChar = sep[0]
	}

	// Not contains sep char
	if !strings.Contains(s, sepChar) {
		return s
	}

	// Get regexp instance
	rgx, ok := toCamelRegs[sepChar]
	if !ok {
		rgx = regexp.MustCompile(regexp.QuoteMeta(sepChar) + "+[a-zA-Z]")
	}

	return rgx.ReplaceAllStringFunc(s, func(s string) string {
		s = strings.TrimLeft(s, sepChar)
		return c.UpperFirst(s)
	})
}

// Md5 Generate a 32-bit md5 string
func (c *StringImpl) Md5(src interface{}) string {
	return c.GenMd5(src)
}

// GenMd5 Generate a 32-bit md5 string
func (c *StringImpl) GenMd5(src interface{}) string {
	h := md5.New()

	if s, ok := src.(string); ok {
		h.Write([]byte(s))
	} else {
		h.Write([]byte(fmt.Sprint(src)))
	}

	return hex.EncodeToString(h.Sum(nil))
}

// Base64 base64 encode
func (c *StringImpl) Base64(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

// B64Encode base64 encode
func (c *StringImpl) B64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

// URLEncode encode url string.
func (c *StringImpl) URLEncode(s string) string {
	if pos := strings.IndexRune(s, '?'); pos > -1 { // escape query data
		return s[0:pos+1] + url.QueryEscape(s[pos+1:])
	}

	return s
}

// URLDecode decode url string.
func (c *StringImpl) URLDecode(s string) string {
	if pos := strings.IndexRune(s, '?'); pos > -1 { // un-escape query data
		qy, err := url.QueryUnescape(s[pos+1:])
		if err == nil {
			return s[0:pos+1] + qy
		}
	}

	return s
}
