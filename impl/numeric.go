package impl

import (
	"math"
	"strconv"
	"strings"
	"techberry-go/common/v2/facade"
)

type NumericImpl struct{}

func NewNumericImpl() facade.NumericHandler {
	var p facade.NumericHandler = &NumericImpl{}
	return p
}

func (c *NumericImpl) NumDecPlaces(v float64) int {
	s := strconv.FormatFloat(v, 'f', -1, 64)
	i := strings.IndexByte(s, '.')
	if i > -1 {
		return len(s) - i - 1
	}
	return 0
}

func (c *NumericImpl) ToDecimal(num float32, precision int) float32 {
	output := math.Pow(10, float64(precision))
	return float32(float64(c.Round(float64(num)*output)) / output)
}

func (c *NumericImpl) ToDecimal64(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(c.Round(num*output)) / output
}

func (c *NumericImpl) Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func (c *NumericImpl) Int(i int, defaultValue int) int {
	if i == 0 {
		return defaultValue
	} else {
		return i
	}
}

func (c *NumericImpl) Int64(i int64, defaultValue int64) int64 {
	if i == 0 {
		return defaultValue
	} else {
		return i
	}
}

func (c *NumericImpl) ForceInt64(v interface{}, defaultValue int64) int64 {
	switch f := v.(type) {
	case float32:
		return int64(f)
	case float64:
		return int64(f)
	case int:
		return int64(f)
	case int64:
		return f
	default:
		return defaultValue
	}
}

func (c *NumericImpl) ForceInt(v interface{}, defaultValue int) int {
	switch f := v.(type) {
	case float32:
		return int(f)
	case float64:
		return int(f)
	case int64:
		return int(f)
	case int:
		return f
	default:
		return defaultValue
	}
}
