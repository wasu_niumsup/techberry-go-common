package impl

import (
	"errors"
	"reflect"
	"strings"
)

type StructTag struct {
}

type FieldInfo struct {
	Name       string
	Type       string
	TargetName string
	TargetType string
	IsKey      bool
}

type KeyField struct {
	Name []string
}

// Name of the struct tag used in examples
const defaultTagName = "json"

func (c StructTag) ParseStruct(tagName string, s interface{}) (error, map[string]interface{}) {
	if StringUtils.IsEmptyString(tagName) {
		tagName = defaultTagName
	}
	tagMap := make(map[string]interface{})

	// TypeOf returns the reflection Type that represents the dynamic type of variable.
	// If variable is a nil interface value, TypeOf returns nil.
	v := reflect.ValueOf(s)
	i := reflect.Indirect(v)
	t := i.Type()

	//t := reflect.TypeOf(s)
	key := new(KeyField)
	// Iterate over all available fields and read the tag value
	for i := 0; i < t.NumField(); i++ {
		// Get the field, returns https://golang.org/pkg/reflect/#StructField
		field := t.Field(i)

		// Get the field tag value
		tag := field.Tag.Get(tagName)
		err, f := c.extractTagColumn(tag)

		if err != nil {
			return err, nil
		} else {
			if StringUtils.IsNotEmptyString(f.TargetName) {
				f.Type = field.Type.Name()
				f.Name = field.Name
				fieldInfo := FieldInfo{}
				fieldInfo.Type = f.Type
				fieldInfo.IsKey = f.IsKey
				fieldInfo.Name = f.Name
				fieldInfo.TargetName = f.TargetName
				fieldInfo.TargetType = f.TargetType
				tagMap[field.Name] = fieldInfo
				if f.IsKey {
					key.Name = append(key.Name, field.Name) // fix bug : f.TargetName to field.Name
				}
			}
		}
		//fmt.Printf("%d. %v (%v), tag: '%v'\n", i+1, field.Name, field.Type.Name(), tag)
	}
	// Add key fields
	tagMap["key"] = key
	return nil, tagMap
}

func (c StructTag) GetValueByFieldName(fieldName string, s interface{}) interface{} {
	v := reflect.ValueOf(s).Elem()
	return v.FieldByName(fieldName).Interface()
}

// db:"<column>,<datatype>,key"
func (c StructTag) extractTagColumn(tag string) (error, *FieldInfo) {
	args := strings.Split(tag, ",")

	fieldInfo := new(FieldInfo)
	i := len(args)
	if i > 0 {
		if i >= 1 {
			fieldInfo.TargetName = args[0]
		}
		if i >= 2 {
			fieldInfo.TargetType = args[1]
		}
		if i >= 3 && args[2] == "key" {
			fieldInfo.IsKey = true
		} else {
			fieldInfo.IsKey = false
		}
	} else {
		return errors.New("Invalid tag format <column>,<datatype>,key."), nil
	}
	return nil, fieldInfo
}
