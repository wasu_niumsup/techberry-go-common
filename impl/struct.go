package impl

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/utils"
	"time"

	"github.com/fatih/structs"
	model "gopkg.in/jeevatkm/go-model.v1"
)

var (
	errInvalidType = errors.New("not a struct pointer")
)

const (
	fieldName = "default"
)

type StructImpl struct {
}

func NewStructImpl() facade.StructHandler {
	var p facade.StructHandler = &StructImpl{}
	return p
}

func (c *StructImpl) Copy(dst, src interface{}) []error {
	var errs []error
	if src == nil || dst == nil {
		return append(errs, errors.New("Source or Destination is nil"))
	}
	switch f := src.(type) {
	case map[string]interface{}:
		sv := utils.ValueOf(f)
		dv := utils.ValueOf(dst)

		if !utils.IsStruct(sv) || !utils.IsStruct(dv) {
			return append(errs, errors.New("Source or Destination is not a struct"))
		}

		if !utils.IsPtr(dv) {
			return append(errs, errors.New("Destination struct is not a pointer"))
		}

		b, err := json.Marshal(f)
		if err == nil {
			if err := json.Unmarshal(b, dst); err == nil {
				return nil
			}
		}
		return append(errs, errors.New("Copy failed."))
	default:
		return model.Copy(&dst, src)
	}

}

func (c *StructImpl) Clone(s interface{}) (interface{}, error) {
	return model.Clone(s)
}

func (c *StructImpl) Map(s interface{}) (map[string]interface{}, error) {
	return model.Map(s)
}

func (c *StructImpl) Fields(s interface{}) ([]reflect.StructField, error) {
	return model.Fields(s)
}

func (c *StructImpl) Kind(s interface{}, name string) (reflect.Kind, error) {
	return model.Kind(s, name)
}

func (c *StructImpl) Get(s interface{}, name string) (interface{}, error) {
	return model.Get(s, name)
}

func (c *StructImpl) Set(s interface{}, name string, value interface{}) error {
	return model.Set(s, name, value)
}

func (c *StructImpl) IsZero(s interface{}) bool {
	return model.IsZero(s)
}

func (c *StructImpl) IsZeroInFields(s interface{}, names ...string) (string, bool) {
	return model.IsZeroInFields(s, names...)
}

func (c *StructImpl) HasZero(s interface{}) bool {
	return model.HasZero(s)
}

func (c *StructImpl) ToMap(s interface{}) map[string]interface{} {
	return structs.New(s).Map()
}

func (c *StructImpl) FromJson(data []byte, s interface{}) error {
	return json.Unmarshal(data, s)
}

func (c *StructImpl) ToJson(s interface{}) ([]byte, error) {
	return json.Marshal(s)
}

func (c *StructImpl) FieldValues(s interface{}) []interface{} {
	return structs.New(s).Values()
}

func (c *StructImpl) FieldNames(s interface{}) []string {
	return structs.New(s).Names()
}

func (c *StructImpl) Name(s interface{}) string {
	return structs.New(s).Name()
}

func (c *StructImpl) IsStruct(s interface{}) bool {
	return structs.IsStruct(s)
}

func (c *StructImpl) PrettyPrint(s interface{}) string {
	var p []byte
	//    var err := error
	p, err := json.MarshalIndent(s, "", "\t")
	if err != nil {
		return ""
	}
	return string(p)
}

// Struct default values
// Set initializes members in a struct referenced by a pointer.
// Maps and slices are initialized by `make` and other primitive types are set with default values.
// `ptr` should be a struct pointer
func (c *StructImpl) InitDefaults(ptr interface{}) error {
	if reflect.TypeOf(ptr).Kind() != reflect.Ptr {
		return errInvalidType
	}

	v := reflect.ValueOf(ptr).Elem()
	t := v.Type()

	if t.Kind() != reflect.Struct {
		return errInvalidType
	}

	for i := 0; i < t.NumField(); i++ {
		if defaultVal := t.Field(i).Tag.Get(fieldName); defaultVal != "-" {
			if err := c.setField(v.Field(i), defaultVal); err != nil {
				return err
			}
		}
	}
	c.callSetter(ptr)
	return nil
}

func (c *StructImpl) setField(field reflect.Value, defaultVal string) error {
	if !field.CanSet() {
		return nil
	}

	if !c.shouldInitializeField(field, defaultVal) {
		return nil
	}

	if c.isInitialValue(field) {
		switch field.Kind() {
		case reflect.Bool:
			if val, err := strconv.ParseBool(defaultVal); err == nil {
				field.Set(reflect.ValueOf(val).Convert(field.Type()))
			}
		case reflect.Int:
			if val, err := strconv.ParseInt(defaultVal, 0, strconv.IntSize); err == nil {
				field.Set(reflect.ValueOf(int(val)).Convert(field.Type()))
			}
		case reflect.Int8:
			if val, err := strconv.ParseInt(defaultVal, 0, 8); err == nil {
				field.Set(reflect.ValueOf(int8(val)).Convert(field.Type()))
			}
		case reflect.Int16:
			if val, err := strconv.ParseInt(defaultVal, 0, 16); err == nil {
				field.Set(reflect.ValueOf(int16(val)).Convert(field.Type()))
			}
		case reflect.Int32:
			if val, err := strconv.ParseInt(defaultVal, 0, 32); err == nil {
				field.Set(reflect.ValueOf(int32(val)).Convert(field.Type()))
			}
		case reflect.Int64:
			if val, err := time.ParseDuration(defaultVal); err == nil {
				field.Set(reflect.ValueOf(val).Convert(field.Type()))
			} else if val, err := strconv.ParseInt(defaultVal, 0, 64); err == nil {
				field.Set(reflect.ValueOf(val).Convert(field.Type()))
			}
		case reflect.Uint:
			if val, err := strconv.ParseUint(defaultVal, 0, strconv.IntSize); err == nil {
				field.Set(reflect.ValueOf(uint(val)).Convert(field.Type()))
			}
		case reflect.Uint8:
			if val, err := strconv.ParseUint(defaultVal, 0, 8); err == nil {
				field.Set(reflect.ValueOf(uint8(val)).Convert(field.Type()))
			}
		case reflect.Uint16:
			if val, err := strconv.ParseUint(defaultVal, 0, 16); err == nil {
				field.Set(reflect.ValueOf(uint16(val)).Convert(field.Type()))
			}
		case reflect.Uint32:
			if val, err := strconv.ParseUint(defaultVal, 0, 32); err == nil {
				field.Set(reflect.ValueOf(uint32(val)).Convert(field.Type()))
			}
		case reflect.Uint64:
			if val, err := strconv.ParseUint(defaultVal, 0, 64); err == nil {
				field.Set(reflect.ValueOf(val).Convert(field.Type()))
			}
		case reflect.Uintptr:
			if val, err := strconv.ParseUint(defaultVal, 0, strconv.IntSize); err == nil {
				field.Set(reflect.ValueOf(uintptr(val)).Convert(field.Type()))
			}
		case reflect.Float32:
			if val, err := strconv.ParseFloat(defaultVal, 32); err == nil {
				field.Set(reflect.ValueOf(float32(val)).Convert(field.Type()))
			}
		case reflect.Float64:
			if val, err := strconv.ParseFloat(defaultVal, 64); err == nil {
				field.Set(reflect.ValueOf(val).Convert(field.Type()))
			}
		case reflect.String:
			field.Set(reflect.ValueOf(defaultVal).Convert(field.Type()))

		case reflect.Slice:
			ref := reflect.New(field.Type())
			ref.Elem().Set(reflect.MakeSlice(field.Type(), 0, 0))
			if defaultVal != "" && defaultVal != "[]" {
				if err := json.Unmarshal([]byte(defaultVal), ref.Interface()); err != nil {
					return err
				}
			}
			field.Set(ref.Elem().Convert(field.Type()))
		case reflect.Map:
			ref := reflect.New(field.Type())
			ref.Elem().Set(reflect.MakeMap(field.Type()))
			if defaultVal != "" && defaultVal != "{}" {
				if err := json.Unmarshal([]byte(defaultVal), ref.Interface()); err != nil {
					return err
				}
			}
			field.Set(ref.Elem().Convert(field.Type()))
		case reflect.Struct:
			if defaultVal != "" && defaultVal != "{}" {
				if err := json.Unmarshal([]byte(defaultVal), field.Addr().Interface()); err != nil {
					return err
				}
			}
		case reflect.Ptr:
			field.Set(reflect.New(field.Type().Elem()))
		}
	}

	switch field.Kind() {
	case reflect.Ptr:
		c.setField(field.Elem(), defaultVal)
		c.callSetter(field.Interface())
	case reflect.Struct:
		if err := c.InitDefaults(field.Addr().Interface()); err != nil {
			return err
		}
	case reflect.Slice:
		for j := 0; j < field.Len(); j++ {
			if err := c.setField(field.Index(j), defaultVal); err != nil {
				return err
			}
		}
	}

	return nil
}

func (c *StructImpl) isInitialValue(field reflect.Value) bool {
	return reflect.DeepEqual(reflect.Zero(field.Type()).Interface(), field.Interface())
}

func (c *StructImpl) shouldInitializeField(field reflect.Value, tag string) bool {
	switch field.Kind() {
	case reflect.Struct:
		return true
	case reflect.Ptr:
		if !field.IsNil() && field.Elem().Kind() == reflect.Struct {
			return true
		}
	case reflect.Slice:
		return field.Len() > 0 || tag != ""
	}

	return tag != ""
}

// CanUpdate returns true when the given value is an initial value of its type
func (c *StructImpl) CanUpdate(v interface{}) bool {
	return c.isInitialValue(reflect.ValueOf(v))
}

func (c *StructImpl) callSetter(v interface{}) {
	if ds, ok := v.(facade.Setter); ok {
		ds.SetDefaults()
	}
}
