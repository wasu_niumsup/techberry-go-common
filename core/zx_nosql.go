package core

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"

	"github.com/fatih/structs"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type ZxNoSqlImpl struct {
	_database        string
	_collection      string
	ClientConnection *mongo.Client
	_connectionUrl   string
	_logger          facade.LogEvent
}

// Connect establish a connection to database
func NewZxNoSqlImpl(logger facade.LogEvent, connectionURL string, db string) facade.CRUDDocumentStore {
	ctx := context.Background()
	// Initialize a new mongo client with options
	uri := fmt.Sprintf("%s/%s??connect=direct&ssl=false&connectTimeoutMS=100000&authMechanism=SCRAM-SHA-1", connectionURL, db)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		logger.OnDemandTrace("Error : %s", err.Error())
	}
	// Connect the mongo client to the MongoDB server
	err = client.Connect(ctx)
	if err != nil {
		logger.OnDemandTrace("Error : %s", err.Error())
	}
	// Ping MongoDB
	if err = client.Ping(ctx, readpref.Primary()); err != nil {
		logger.OnDemandTrace("Could not ping to mongo db service: %s", err)
	}
	logger.OnDemandTrace("Connected to NoSql.")
	return &ZxNoSqlImpl{
		_connectionUrl:   connectionURL,
		ClientConnection: client,
		_database:        db,
		_logger:          logger,
	}
}

func (c *ZxNoSqlImpl) Database() string {
	return c._database
}

func (c *ZxNoSqlImpl) Release() {
	ctx := context.Background()
	err := c.ClientConnection.Disconnect(ctx)

	if err != nil {
		c._logger.Debug().MsgError(err)
	}
}

func (c *ZxNoSqlImpl) Create(collection string, model interface{}) (string, error) {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)
	result, err := col.InsertOne(ctx, model)

	if err != nil {
		return "", err
	}
	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		return oid.Hex(), nil
	} else {
		return "", errors.New("Can't get object id from inserting document.")
	}
}

func (c *ZxNoSqlImpl) CreateMany(collection string, models []interface{}) ([]string, error) {
	db := c._database
	var stringList []string
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	result, err := col.InsertMany(ctx, models)
	if err != nil {
		return stringList, err
	}
	for _, val := range result.InsertedIDs {
		if oid, ok := val.(primitive.ObjectID); ok {
			stringList = append(stringList, oid.Hex())
		} else {
			return stringList, errors.New("Can't get object id from inserting document.")
		}
	}
	return stringList, nil
}

func (c *ZxNoSqlImpl) Read(collection string, filter bson.M, model interface{}) (err error) {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	err = col.FindOne(ctx, filter).Decode(model)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return err
	}
	/*
		err = c._bindModel(result, &model)
		if err != nil {
			c._mc.Logger().Info("Error : ", err)
			return err
		}
	*/
	return nil
}

func (c *ZxNoSqlImpl) ReadMany(collection string, filter bson.M, results *[]interface{}, findOptions *options.FindOptions, binding func(interface{}) interface{}) (err error) {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := col.Find(ctx, filter, findOptions)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return err
	}

	for cur.Next(ctx) {
		var result bson.M
		err = cur.Decode(&result)
		if err != nil {
			c._logger.Debug().MsgError(err)
			return err
		}
		r := binding(result)
		if r != nil {
			result = r.(bson.M)
		}
		*results = append(*results, result)
		/*
			model := callBack(&result)
			result, err := c._convertToBson(result, model)
			if err == nil {
				*results = append(*results, result)
			}
		*/
	}

	if err = cur.Err(); err != nil {
		c._logger.Debug().MsgError(err)
		return err
	}

	// Close the cursor once finished
	cur.Close(ctx)
	return nil
}

func (c *ZxNoSqlImpl) _bindModel(obj interface{}, model interface{}) error {
	b, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, model)
	if err != nil {
		return err
	}
	return nil
}

func (c *ZxNoSqlImpl) _convertToBson(obj interface{}, model interface{}) (bson.M, error) {
	b, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(b, model)
	if err != nil {
		return nil, err
	}
	return StructUtils.ToMap(model), nil
}

func (c *ZxNoSqlImpl) ReadById(collection string, id string, model interface{}) (err error) {
	db := c._database
	ctx := context.Background()
	objId, _ := primitive.ObjectIDFromHex(id)
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	err = col.FindOne(ctx, bson.M{"_id": objId}).Decode(model)
	return
}

func (c *ZxNoSqlImpl) UpdateById(collection string, id string, model interface{}) int64 {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}

	// set filters and updates
	filter := bson.M{"_id": objId}
	// update document
	result, err := col.ReplaceOne(ctx, filter, model)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return 0
	}
	if result.ModifiedCount > 0 {
		return result.ModifiedCount
	} else {
		return 1
	}
}

func (c *ZxNoSqlImpl) Update(collection string, filter bson.M, update bson.M) int64 {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	// update documents
	result, err := col.UpdateOne(ctx, filter, update)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return 0
	}
	return result.MatchedCount
}

func (c *ZxNoSqlImpl) UpdateMany(collection string, filter bson.M, model interface{}) int64 {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	m := structs.Map(model)
	update := bson.M{"$set": m}

	// update documents
	result, err := col.UpdateMany(ctx, filter, update)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return 0
	}
	return result.MatchedCount
}

func (c *ZxNoSqlImpl) DeleteMany(collection string, filter bson.M) bool {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	// delete documents
	_, err := col.DeleteMany(ctx, filter)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return false
	}
	return true
}

func (c *ZxNoSqlImpl) DeleteById(collection string, id string) bool {
	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}

	// delete document
	_, err1 := col.DeleteOne(ctx, bson.M{"_id": _id})
	if err1 != nil {
		c._logger.Debug().MsgError(err)
		return false
	}
	return true
}

func (c *ZxNoSqlImpl) Upsert(collection string, filter bson.M, model interface{}) (string, error) {
	status, objId := c.IsExist(collection, filter, model)
	c._logger.OnDemandTrace("IsExist = %s, object Id =", status, objId)
	if status {
		c.DeleteById(collection, objId)
		c._logger.OnDemandTrace("Delete existing document.")
		if status == false {
			return "", errors.New("Can't delete object.")
		}
	}
	structHandler := impl.NewStructImpl()
	dataMap := structHandler.ToMap(model)
	delete(dataMap, "id")
	id, err := c.Create(collection, dataMap)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return "", err
	}
	return id, err
}

func (c *ZxNoSqlImpl) IsExist(collection string, filter bson.M, model interface{}) (bool, string) {
	var (
		objId string
		err   error
	)

	//c._mc.Logger().Info(StructUtils.PrettyPrint(model))
	err = c.Read(collection, filter, model)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return false, ""
	} else {
		// Existing document object
		m := MapUtils.ToMap(model)
		if val, ok := m["id"]; ok {
			objId = val.(string)
		} else if val, ok := m["_id"]; ok {
			objId = val.(string)
		} else {
			c._logger.OnDemandTrace("Field (id or _id) not found.")
			return false, ""
		}
	}
	return true, objId
}

func (c *ZxNoSqlImpl) GetDocumentById(collection string, id string, model interface{}) error {
	var (
		err error
	)

	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return err
	}

	filter := bson.M{
		"_id": _id,
	}
	err = c.Read(collection, filter, model)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return err
	} else {
		// Existing document object
		return nil
	}
}

func (c *ZxNoSqlImpl) FindByItemArray(collection string, id string, itemId string, itemKey string) (map[string]interface{}, error) {
	var (
		err       error
		aggregate []interface{}
	)
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return nil, err
	}

	_itemId, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return nil, err
	}

	match_parent := bson.M{
		"$match": bson.M{
			"_id": _id,
		},
	}

	unwind := bson.M{
		"$unwind": "$" + itemKey,
	}

	match_item := bson.M{
		"$match": bson.M{
			itemKey + "._id": _itemId,
		},
	}

	aggregate = append(aggregate, match_parent)
	aggregate = append(aggregate, unwind)
	aggregate = append(aggregate, match_item)

	db := c._database
	ctx := context.Background()
	conn := c.ClientConnection.Database(db)
	col := conn.Collection(collection)

	cursor, err := col.Aggregate(ctx, aggregate)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return nil, err
	} else {
		var document []map[string]interface{}
		if err = cursor.All(ctx, &document); err != nil {
			c._logger.Debug().MsgError(err)
			return nil, err
		} else {
			v := document[0]
			return v, nil
		}
	}
}
