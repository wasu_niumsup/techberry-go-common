package core

import (
	"context"
	"techberry-go/common/v2/facade"

	"go.opencensus.io/trace"
)

type ZxMediationContext struct {
	_logger       facade.Logger
	_traceContext facade.TraceContext
	_data         interface{}
}

func NewZxMediationContext(logger facade.Logger) facade.MediationContext {
	var p facade.MediationContext = &ZxMediationContext{_logger: logger}
	return p
}

func (c *ZxMediationContext) SetTraceContext(tc facade.TraceContext) {
	c._traceContext = tc
}

func (c *ZxMediationContext) Logger() facade.Logger {
	return c._logger
}

func (c *ZxMediationContext) Data() interface{} {
	return c._data
}

func (c *ZxMediationContext) SetData(data interface{}) {
	c._data = data
}

func (c *ZxMediationContext) TraceContext() facade.TraceContext {
	return c._traceContext
}

func (c *ZxMediationContext) RootModel() *facade.TraceModel {
	if c._traceContext != nil {
		return c._traceContext.RootModel()
	}
	return nil
}

func (c *ZxMediationContext) ParentModel() *facade.TraceModel {
	if c._traceContext != nil {
		return c._traceContext.ParentModel()
	}
	return nil
}

func (c *ZxMediationContext) LastModel() *facade.TraceModel {
	if c._traceContext != nil {
		return c._traceContext.LastModel()
	}
	return nil
}

func (c *ZxMediationContext) RootCtx() context.Context {
	if c._traceContext != nil {
		return c._traceContext.RootCtx()
	}
	return nil
}

func (c *ZxMediationContext) ParentCtx() context.Context {
	if c._traceContext != nil {
		return c._traceContext.ParentCtx()
	}
	return nil
}

func (c *ZxMediationContext) LastCtx() context.Context {
	if c._traceContext != nil {
		return c._traceContext.LastCtx()
	}
	return nil
}

func (c *ZxMediationContext) StartSpan(name string, o ...trace.StartOption) facade.Span {
	if c._traceContext != nil {
		return c._traceContext.StartSpan(name, o...)
	}
	return nil
}

func (c *ZxMediationContext) Span(ctx context.Context, name string, o ...trace.StartOption) facade.Span {
	if c._traceContext != nil {
		return c._traceContext.Span(ctx, name, o...)
	}
	return nil
}
