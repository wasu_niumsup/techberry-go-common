package core

import (
	"fmt"
	"techberry-go/common/v2/facade"

	"contrib.go.opencensus.io/exporter/zipkin"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
	_ "github.com/godror/godror"
	_ "github.com/lib/pq"
	openzipkin "github.com/openzipkin/zipkin-go"
	zipkinHTTP "github.com/openzipkin/zipkin-go/reporter/http"
	"go.opencensus.io/trace"
)

type ZxInitializer struct {
	EnableZipkinTrace bool
	EnableSystemCache bool
	BeegoCache        cache.Cache
	_logger           facade.Logger
	_config           facade.JsonHandler
}

func NewZxInitializer(logger facade.Logger, config facade.JsonHandler) ZxInitializer {
	inst := ZxInitializer{_logger: logger, _config: config}
	inst.Init()
	return inst
}

func (c *ZxInitializer) Init() {
	zipkin := c._config.Exists("zipkin")
	zipkin_enable := c._config.Bool(false, "zipkin.enable")
	if zipkin && zipkin_enable {
		c._logger.Debug("Zipkin enabled.")
		service := c._config.String("", "zipkin.service")
		hostport := c._config.String("", "zipkin.hostport")
		c._initZipkin(service, hostport)
		c.EnableZipkinTrace = true
	}
	systemCache := c._config.Exists("system_cache")
	if systemCache {
		c._logger.Debug("System cache enabled.")
		key := c._config.String("", "system_cache.key")
		host := c._config.String("", "system_cache.host")
		port := int(c._config.Int(6379, "system_cache.port"))
		dbnum := int(c._config.Int(9, "system_cache.dbnum"))
		c._initSystemCache(key, host, port, dbnum)
		c.EnableSystemCache = true
	}
}

// Initialize Zipkin
func (c *ZxInitializer) _initZipkin(serviceName string, servicePort string) {
	// ===================================================================
	localEndpoint, err := openzipkin.NewEndpoint(serviceName, servicePort)
	if err != nil {
		beego.Info("Failed to create Zipkin exporter: %v", err)
	} else {
		beego.Info("Initialize zipkin exporter successfully.")
	}
	zipkinSpanHostUrl := "https://trace.partsoto.net/api/v2/spans"
	reporter := zipkinHTTP.NewReporter(zipkinSpanHostUrl)
	exporter := zipkin.NewExporter(reporter, localEndpoint)
	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
}

// Initialize System cache
func (c *ZxInitializer) _initSystemCache(key string, host string, port int, dbnum int) {
	var (
		err error
	)
	config := fmt.Sprintf("{\"key\":\"%s\",\"dbNum\":\"%d\",\"conn\":\"%s:%d\"}", key, dbnum, host, port)
	c.BeegoCache, err = cache.NewCache("redis", config)
	if err != nil {
		beego.Info("Initialize system cache manager failed.")
	} else {
		beego.Info("Initialize system cache manager successfully.")
	}
}
