package ldap

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"techberry-go/common/v2/facade"

	"gopkg.in/ldap.v2"
)

type LDAPClientImpl struct {
	_conn   *ldap.Conn
	_config *facade.LDAPConfig
	_logger facade.LogEvent
}

func NewLdapClient(logger facade.LogEvent) facade.LDAPClient {
	cfg := new(facade.LDAPConfig)
	var p facade.LDAPClient = &LDAPClientImpl{_logger: logger, _config: cfg}
	return p
}

/*
	Attributes         []string
	Base               string
	BindDN             string
	BindPassword       string
	GroupFilter        string // e.g. "(memberUid=%s)"
	Host               string
	ServerName         string
	UserFilter         string // e.g. "(uid=%s)"
	Conn               *ldap.Conn
	Port               int
	InsecureSkipVerify bool
	UseSSL             bool
	SkipTLS            bool
	TrustCertFilePath  string
	ClientCertificates []tls.Certificate // Adding client certificates

*/
func (c *LDAPClientImpl) Config(config *facade.LDAPConfig) {
	c._config = config
}

// Connect connects to the ldap backend.
func (c *LDAPClientImpl) Connect() error {
	if c._conn == nil {
		var l *ldap.Conn
		var err error
		address := fmt.Sprintf("%s:%d", c._config.Host, c._config.Port)
		if !c._config.UseSSL {
			l, err = ldap.Dial("tcp", address)
			if err != nil {
				return err
			}

			// Reconnect with TLS
			if !c._config.SkipTLS {
				err = l.StartTLS(&tls.Config{InsecureSkipVerify: true})
				if err != nil {
					return err
				}
			}
		} else {
			l, err = c._ldapsConnect()
			if err != nil {
				return err
			}
			/*
				config := &tls.Config{
					InsecureSkipVerify: c.InsecureSkipVerify,
					ServerName:         c.ServerName,
				}
				if c.ClientCertificates != nil && len(c.ClientCertificates) > 0 {
					config.Certificates = c.ClientCertificates
				}
				l, err = ldap.DialTLS("tcp", address, config)
				if err != nil {
					return err
				}
			*/
		}

		c._conn = l
	}
	return nil
}

func (c *LDAPClientImpl) _ldapsConnect() (*ldap.Conn, error) {
	rootCA, err := x509.SystemCertPool()
	if err != nil {
		c._logger.OnDemandTrace("Failed to load system cert : %v", err)
		// return nil, err
	}
	if rootCA == nil {
		c._logger.OnDemandTrace("root ca is nil")
		rootCA = x509.NewCertPool()
	}
	certPath := fmt.Sprintf("%s/*.cer", c._config.TrustCertFilePath)
	c._logger.OnDemandTrace("Find trust certificates in path : %s", certPath)
	files, _ := filepath.Glob(certPath)

	if len(files) > 0 {
		for _, file := range files {
			ldapCert, err := ioutil.ReadFile(file)
			if err != nil {
				c._logger.OnDemandTrace("Failed to read ad cert : %s", err)
				continue
			}
			ok := rootCA.AppendCertsFromPEM(ldapCert)
			if !ok {
				c._logger.OnDemandTrace("AD cert of %s is not addeded.", file)
				continue
			} else {
				c._logger.OnDemandTrace("Append certification into store.")
			}
		}
	} else {
		c._logger.OnDemandTrace("Trust certificate files not found.")
	}

	tlsConfig := tls.Config{
		InsecureSkipVerify: c._config.InsecureSkipVerify,
		ServerName:         c._config.ServerName,
		RootCAs:            rootCA,
		Certificates:       nil,
	}
	/*
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
		},
		MinVersion: tls.VersionTLS12,
	*/
	return ldap.DialTLS("tcp", fmt.Sprintf("%s:%d", c._config.Host, c._config.Port), &tlsConfig)
}

// Close closes the ldap backend connection.
func (c *LDAPClientImpl) Close() {
	if c._conn != nil {
		c._conn.Close()
		c._conn = nil
	}
}

// Authenticate authenticates the user against the ldap backend.
func (c *LDAPClientImpl) Authenticate(username, password string) (bool, map[string]string, error) {
	err := c.Connect()
	if err != nil {
		return false, nil, err
	}

	// First bind with a read only user
	if c._config.BindDN != "" && c._config.BindPassword != "" {
		err := c._conn.Bind(c._config.BindDN, c._config.BindPassword)
		if err != nil {
			return false, nil, err
		}
	}

	attributes := append(c._config.Attributes, "dn")
	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
		c._config.Base,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(c._config.UserFilter, username),
		attributes,
		nil,
	)

	sr, err := c._conn.Search(searchRequest)
	if err != nil {
		return false, nil, err
	}

	if len(sr.Entries) < 1 {
		return false, nil, errors.New("User does not exist")
	}

	if len(sr.Entries) > 1 {
		return false, nil, errors.New("Too many entries returned")
	}

	userDN := sr.Entries[0].DN
	user := map[string]string{}
	for _, attr := range c._config.Attributes {
		user[attr] = sr.Entries[0].GetAttributeValue(attr)
	}

	// Bind as the user to verify their password
	err = c._conn.Bind(userDN, password)
	if err != nil {
		return false, user, err
	}

	// Rebind as the read only user for any further queries
	if c._config.BindDN != "" && c._config.BindPassword != "" {
		err = c._conn.Bind(c._config.BindDN, c._config.BindPassword)
		if err != nil {
			return true, user, err
		}
	}

	return true, user, nil
}

// GetGroupsOfUser returns the group for a user.
func (c *LDAPClientImpl) GetGroupsOfUser(username string) ([]string, error) {
	err := c.Connect()
	if err != nil {
		return nil, err
	}

	searchRequest := ldap.NewSearchRequest(
		c._config.Base,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(c._config.GroupFilter, username),
		[]string{"cn"}, // can it be something else than "cn"?
		nil,
	)
	sr, err := c._conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}
	groups := []string{}
	for _, entry := range sr.Entries {
		groups = append(groups, entry.GetAttributeValue("cn"))
	}
	return groups, nil
}
