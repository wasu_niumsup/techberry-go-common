package core

import (
	"bufio"
	"encoding/json"
	"io"
	"os"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/buger/jsonparser"

	"techberry-go/common/v2/facade"
)

type ZxEngine struct {
	zxContext      *ZxContext
	_appController beego.Controller
	_context       *context.Context
	_handler       facade.Handler
	zxConnector    *ZxConnector
	_uploadFiles   []facade.UploadFile

	_rpcConnector      facade.RpcConnector
	_rpcCacheConnector facade.RpcCacheConnector

	_jsonData        facade.JsonHandler
	_jsonRequestData facade.JsonHandler
	_authCred        *facade.AuthCredential
	_elastic         facade.ElasticHandler
	_dataBus         facade.DataBus
	_properties      facade.Properties
	_webSocketMode   bool
	_mc              facade.MediationContext
	_logger          facade.LogEvent
	_appConfig       facade.AppConfig
}

type RequestContext struct {
	_jsonRpc        *facade.JsonRpcRequest
	_jsonHandler    facade.JsonHandler
	_authCredential facade.AuthCredential
}

func NewZxEngine(logger facade.LogEvent, appConfig facade.AppConfig, appData *map[string]interface{}) ZxEngine {
	//trace := NewZxTraceImpl(P_BEEGO)
	//traceContext := NewZxZipkinImpl(trace)
	zxContext := NewZxContext(logger, appData)
	return ZxEngine{
		_handler:   NewZxHandler(logger),
		zxContext:  &zxContext,
		_appConfig: appConfig,
		_logger:    logger,
	}
}

func (c *ZxEngine) AppConfig() facade.AppConfig {
	return c._appConfig
}

func (c *ZxEngine) SetWebSocketMode(mode bool) {
	c._webSocketMode = mode
	c.zxContext.SetWebSocketMode(mode)
}

func (c *ZxEngine) SetController(controller beego.Controller) {
	c._appController = controller
	c.zxContext._appController = controller
}

func (c *ZxEngine) SetContext(context *context.Context) {
	c._context = context
	c.zxContext._context = context
}

func (c *ZxEngine) IsMultipartUpload() bool {
	return c._context.Input.IsUpload()
}

func (c *ZxEngine) DoProcessWSMessage(data []byte) error {
	c._dataBus = NewZxDataBusImpl(c._logger, nil)
	requestCtx := c._getRequestContext()
	if requestCtx != nil {
		authCredential := requestCtx._authCredential
		auth := c._handler.Struct(true).ToMap(authCredential)
		c._dataBus.Set(auth, "auth")
		c._authCred = &authCredential
		c.zxContext._authCred = &authCredential
	}
	jsonData := requestCtx._jsonHandler
	c._jsonData = jsonData
	c.zxContext._jsonData = jsonData
	c._dataBus.Set(jsonData.ToMap(), "request")
	return nil
}

func (c *ZxEngine) DoProcessRequest(tempUploadDir string) error {
	var debugLevel int

	// Initial DataBus stream.
	c._dataBus = NewZxDataBusImpl(c._logger, nil)

	// Validate mulipart upload.
	if c._context.Input.IsUpload() {
		debugLevel = c.Handler().String(true).Int(c._context.Request.PostFormValue("X-TB-Debug"), 0)
	} else {
		debugLevel = c.Handler().String(true).Int(c._context.Input.Header("X-TB-Debug"), 0)
	}
	c._mc.Logger().Debug("Debug header level : ", debugLevel)
	if debugLevel == 4 || debugLevel == 6 || debugLevel == 7 {
		c._mc.Logger().ReloadLevel(debugLevel)
	}

	// Check Input For Upload
	if c._context.Input.IsUpload() {
		attachedPart := c._context.Request.PostFormValue("attachedPart")
		c._logger.Debug().Msgf("Attached part %s", attachedPart)
		if c._handler.String(true).IsNotEmptyString(attachedPart) {
			c._processUpload(tempUploadDir)
		}
		/*
			tokenId := c._context.Request.PostFormValue("token_id")
			credential := c._context.Request.PostFormValue("credential")
			c._logger.Debug().Msgf("Token Id %s", tokenId)
			c._logger.Debug().Msgf("Credential %s", credential)
			if c._handler.String(true).IsEmptyString(tokenId) && c._handler.String(true).IsEmptyString(credential) {
				c.zxContext.Response(400, "Bad Request.")
				return errors.New("Bad Request.")
			}
			authCredential := c._getAuthCredential(credential)
			authCredential.TokenId = tokenId
			authCredential.Credential = credential
		*/
		requestCtx := c._getRequestContext()
		if requestCtx != nil {
			authCredential := requestCtx._authCredential
			auth := c._handler.Struct(true).ToMap(authCredential)
			c._dataBus.Set(auth, "auth")
			c._authCred = &authCredential
			c.zxContext._authCred = &authCredential
		}

		return nil
	} else {
		requestCtx := c._getRequestContext()
		if requestCtx != nil {
			authCredential := requestCtx._authCredential
			auth := c._handler.Struct(true).ToMap(authCredential)
			c._dataBus.Set(auth, "auth")
			c._authCred = &authCredential
			c.zxContext._authCred = &authCredential
		}
		if requestCtx._jsonRpc != nil {
			jsonRpc := requestCtx._jsonRpc
			c._dataBus.Set(true, "jsonrpc")
			c._dataBus.Set(jsonRpc.Method, "method")
			c._dataBus.Set(jsonRpc.Id, "id")
		} else {
			c._dataBus.Set(false, "jsonrpc")
			c._dataBus.Set("", "method")
			c._dataBus.Set(-1, "id")
		}
		jsonData := requestCtx._jsonHandler
		c._jsonData = jsonData
		c.zxContext._jsonData = jsonData
		c._dataBus.Set(jsonData.ToMap(), "request")
		return nil
	}
}

func (c *ZxEngine) _getRequestContext() *RequestContext {
	return c._context.Input.GetData("request_context").(*RequestContext)
}

func (c *ZxEngine) JsonInput() string {
	jsonHandler := c._context.Input.GetData("jsonservice").(facade.JsonHandler)
	b := jsonHandler.GetData()
	if b != nil {
		return string(b)
	}
	return ""
}

func (c *ZxEngine) _processUpload(tempUploadPath string) {
	var (
		fileName   string
		filePartNo int64
	)
	c._logger.Debug().Msgf("Temporary upload directory : %s", tempUploadPath)

	// Parse multipart with maximum memory 20 Mb
	err := c._context.Request.ParseMultipartForm(20 * 1024)
	if err != nil {
		c.zxContext.Response(400, "Multiparts upload are over 20 Mb.")
		return
	}
	attachedPart := c._context.Request.PostFormValue("attachedPart")
	filePartNo = c._handler.String(true).Int64(attachedPart, 1)
	formPart := c._context.Request.MultipartForm
	if len(formPart.File) == 1 {
		handlers := formPart.File["file"]
		fileHandler := handlers[0]
		fileName = fileHandler.Filename
		c._logger.Debug().Msgf("File name : %s", fileName)
		fileDir := c._handler.String(true).GenerateUuid(false)
		c._logger.Debug().Msgf("Generated directory : %s", fileDir)
		err := c._handler.File(true).CreateDirIfNotExist(tempUploadPath + fileDir)
		c._logger.Debug().Msgf("CreateDirIfNotExist error result : %s", err)
		if err != nil {
			c.zxContext.Response(500, "Operating system error to creating directory.")
			return
		}
		filePath := tempUploadPath + fileDir + "/" + fileName
		c._logger.Debug().Msgf("File path : %s", filePath)
		outfile, err := os.Create(filePath)
		c._logger.Debug().Msgf("Create file status : %s", err)
		if err != nil {
			c.zxContext.Response(400, "Invalid upload request.")
			return
		}
		defer outfile.Close()
		file, err := fileHandler.Open()
		reader := bufio.NewReader(file)
		_, err = io.Copy(outfile, reader)
		c._logger.Debug().Msgf("Copy file status : ", err)
		if err != nil {
			c.zxContext.Response(400, "Can't copy file to temp.")
			return
		}
		c._logger.Debug().Msgf("File name : %s, File part : %d, File path : %s", fileName, filePartNo, filePath)
		uploadFile := facade.UploadFile{
			Name:      fileName,
			Part:      filePartNo,
			Directory: fileDir,
			FilePath:  filePath,
		}
		c._uploadFiles = append(c._uploadFiles, uploadFile)
		c.zxContext.SetUploadFiles(c._uploadFiles)
	} else {
		c.zxContext.Response(400, "Don't support more that one upload file part.")
		return
	}
}

func (c *ZxEngine) _isValidDataSection(jsonByteData []byte) bool {
	_, _, _, err := jsonparser.Get(jsonByteData, "data")
	if err != nil {
		return false
	} else {
		return true
	}
}

func (c *ZxEngine) IsValidSession(username string, token_id string) bool {
	cacheCredential, _ := c._rpcCacheConnector.Load(token_id)
	cred := cacheCredential.(facade.OdooCredential)
	if username == cred.User {
		return true
	} else {
		return false
	}
}

func (c *ZxEngine) InvalidateSession(token_id string) {
	c._rpcCacheConnector.Remove(token_id)
}

/*
func (c *ZxEngine) GetJsonMultipart() (facade.JsonHandler, facade.AuthCredential, error) {
	var authCredential = facade.AuthCredential{}
	stream_json, ok := c._appController.GetFiles("json")
	if ok != nil {
		return nil, authCredential, errors.New("No json request part.")
	}
	if len(stream_json) != 1 {
		return nil, authCredential, errors.New("Only one file at a time.")
	}
	if stream_json[0].Filename == "" {
		return nil, authCredential, errors.New("No json stream data.")
	}
	file, err := stream_json[0].Open()
	defer file.Close()
	if err != nil {
		return nil, authCredential, errors.New("Can't read stream data.")
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(file)
	jsonRequest := []byte(buf.String())
	jsonHandler := NewZxJsonImpl(jsonRequest)
	authCredential = c._extractCredential(jsonHandler.GetData())
	return jsonHandler, authCredential, nil
}
*/

func (c *ZxEngine) DoProcessMessage(msgPayload string) error {
	jsonMsgPayload, err := json.Marshal(msgPayload)
	if err != nil {
		return err
	}
	jsonHandler := NewZxJsonImpl(jsonMsgPayload)
	c._jsonRequestData = NewZxJsonImpl(jsonHandler.GetData())
	jsonByte := jsonHandler.GetDataStruct("data")
	c._jsonData = NewZxJsonImpl(jsonByte)
	return nil
}

func (c *ZxEngine) Handler() facade.Handler {
	return c._handler
}

func (c *ZxEngine) ConnectorManager() *ZxConnector {
	return c.zxConnector
}

func (c *ZxEngine) Connector() facade.Connector {
	return c.zxConnector
}

func (c *ZxEngine) SetZxConnector(connector *ZxConnector) {
	c.zxConnector = connector
}

func (c *ZxEngine) Context() facade.AppContext {
	return c.zxContext
}

func (c *ZxEngine) DataBus() facade.DataBus {
	return c._dataBus
}

func (c *ZxEngine) Adapter() facade.Adapter {
	return NewZxAdapter(c._logger)
}
