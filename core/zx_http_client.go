package core

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"techberry-go/common/v2/facade"
	"time"

	"github.com/go-resty/resty/v2"
)

const (
	// MethodGet HTTP method
	MethodGet = "GET"

	// MethodPost HTTP method
	MethodPost = "POST"

	// MethodPut HTTP method
	MethodPut = "PUT"

	// MethodDelete HTTP method
	MethodDelete = "DELETE"

	// MethodPatch HTTP method
	MethodPatch = "PATCH"

	// MethodHead HTTP method
	MethodHead = "HEAD"

	// MethodOptions HTTP method
	MethodOptions = "OPTIONS"
)

type ZxHttpClientImpl struct {
	_requestWrapper facade.Request // Pointer to interface
	_client         *resty.Client
	_host           string
	_hookRequest    []func(facade.HttpClient, facade.Request) error
	_hookResponse   []func(facade.HttpClient, facade.Response) error
	_logger         facade.LogEvent
}

type RequestWrapper struct {
	_request    *resty.Request
	_httpClient *ZxHttpClientImpl
}

type ResponseWrapper struct {
	_response *resty.Response
}

func NewZxHttpClientImpl(logger facade.LogEvent) facade.HttpClient {
	client := resty.New()
	req := client.R()
	requestWrapper := RequestWrapper{_request: req}
	var request facade.Request = &requestWrapper
	httpClient := &ZxHttpClientImpl{_requestWrapper: request,
		_client: client,
		_logger: logger}
	var p facade.HttpClient = httpClient
	requestWrapper._httpClient = httpClient
	return p
}

func (c *ZxHttpClientImpl) SetHostURL(host_url string) facade.HttpClient {
	c._host = host_url
	c._client.SetHostURL(host_url)
	return c
}

func (c *ZxHttpClientImpl) SetHeaders(data map[string]string) facade.HttpClient {
	c._requestWrapper.SetHeaders(data)
	return c
}

func (c *ZxHttpClientImpl) SetHeader(key string, value string) facade.HttpClient {
	c._requestWrapper.SetHeader(key, value)
	return c
}

func (c *ZxHttpClientImpl) _propagateTraceHeader() {
	/*
		if c._mc != nil && c._mc.TraceContext() != nil {
			c._requestWrapper.SetHeader("X-T1-TraceId", c._mc.LastModel().TraceId)
			c._requestWrapper.SetHeader("X-T1-SpanId", c._mc.LastModel().SpanId)
			c._requestWrapper.SetHeader("X-T1-ParentSpanId", c._mc.LastModel().ParentSpanId)
			//c._requestWrapper.SetHeader("X-TB-Sampled", string(c._traceContext.LastModel().Sampled))
			debug := strconv.FormatBool(c._mc.LastModel().Debug)
			c._requestWrapper.SetHeader("X-T1-Debug", debug)
		}
	*/
}

func (c *ZxHttpClientImpl) SetAuthHeader(token string) facade.HttpClient {
	c._requestWrapper.SetAuthToken(token)
	return c
}

func (c *ZxHttpClientImpl) SetBasicAuth(username string, password string) facade.HttpClient {
	c._requestWrapper.SetBasicAuth(username, password)
	return c
}

func (c *ZxHttpClientImpl) SetJsonType() facade.HttpClient {
	c._requestWrapper.SetHeader("Accept", "application/json")
	return c
}

func (c *ZxHttpClientImpl) SetFormType() facade.HttpClient {
	c._requestWrapper.SetHeader("Accept", "application/x-www-form-urlencoded")
	return c
}

func (c *ZxHttpClientImpl) SetPlainTextType() facade.HttpClient {
	c._requestWrapper.SetHeader("Accept", "text/plain; charset=utf-8")
	return c
}

func (c *ZxHttpClientImpl) SetTimeout(timeout time.Duration) facade.HttpClient {
	c._client.SetTimeout(timeout)
	return c
}

func (c *ZxHttpClientImpl) DisableSecurityCheck() facade.HttpClient {
	c._client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	return c
}

func (c *ZxHttpClientImpl) SetBody(body interface{}) facade.HttpClient {
	c._requestWrapper.SetBody(body)
	return c
}

func (c *ZxHttpClientImpl) PostUri(uri string) (facade.Response, error) {
	url := fmt.Sprintf("%s%s", c._host, uri)
	return c._requestWrapper.Post(url)
}

func (c *ZxHttpClientImpl) Post(endpoint string) (facade.Response, error) {
	return c._requestWrapper.Post(endpoint)
}

func (c *ZxHttpClientImpl) GetUri(uri string) (facade.Response, error) {
	url := fmt.Sprintf("%s%s", c._host, uri)
	return c._requestWrapper.Get(url)
}

func (c *ZxHttpClientImpl) Get(endpoint string) (facade.Response, error) {
	return c._requestWrapper.Get(endpoint)
}

func (c *ZxHttpClientImpl) PutUri(uri string) (facade.Response, error) {
	url := fmt.Sprintf("%s%s", c._host, uri)
	return c._requestWrapper.Put(url)
}

func (c *ZxHttpClientImpl) Put(endpoint string) (facade.Response, error) {
	return c._requestWrapper.Put(endpoint)
}

func (c *ZxHttpClientImpl) PatchUri(uri string) (facade.Response, error) {
	url := fmt.Sprintf("%s%s", c._host, uri)
	return c._requestWrapper.Patch(url)
}

func (c *ZxHttpClientImpl) Patch(endpoint string) (facade.Response, error) {
	return c._requestWrapper.Patch(endpoint)
}

func (c *ZxHttpClientImpl) DeleteUri(uri string) (facade.Response, error) {
	url := fmt.Sprintf("%s%s", c._host, uri)
	return c._requestWrapper.Delete(url)
}

func (c *ZxHttpClientImpl) Delete(endpoint string) (facade.Response, error) {
	return c._requestWrapper.Delete(endpoint)
}

func (c *ZxHttpClientImpl) HookRequest(m func(facade.HttpClient, facade.Request) error) facade.HttpClient {
	c._hookRequest = append(c._hookRequest, m)
	return c
}

func (c *ZxHttpClientImpl) HookResponse(m func(facade.HttpClient, facade.Response) error) facade.HttpClient {
	c._hookResponse = append(c._hookResponse, m)
	return c
}

func (c *ResponseWrapper) Body() []byte {
	return c._response.Body()
}

func (c *ResponseWrapper) Status() string {
	return c._response.Status()
}

func (c *ResponseWrapper) StatusCode() int {
	return c._response.StatusCode()
}

func (c *ResponseWrapper) Json() facade.JsonHandler {
	return NewZxJsonImpl(c._response.Body())
}

func (c *ResponseWrapper) Data() interface{} {
	var jsonObject interface{}
	json.Unmarshal(c._response.Body(), &jsonObject)
	return jsonObject
}

func (c *ResponseWrapper) Result() interface{} {
	return c._response.Result()
}

func (c *ResponseWrapper) Error() interface{} {
	return c._response.Error()
}

func (c *ResponseWrapper) Header() http.Header {
	return c._response.Header()
}

func (c *ResponseWrapper) Cookies() []*http.Cookie {
	return c._response.Cookies()
}

func (c *ResponseWrapper) String() string {
	return c._response.String()
}

func (c *ResponseWrapper) Time() time.Duration {
	return c._response.Time()
}

func (c *ResponseWrapper) ReceivedAt() time.Time {
	return c._response.ReceivedAt()
}

func (c *ResponseWrapper) Size() int64 {
	return c._response.Size()
}

func (c *ResponseWrapper) RawBody() io.ReadCloser {
	return c._response.RawBody()
}

func (c *ResponseWrapper) IsSuccess() bool {
	return c._response.IsSuccess()
}

func (c *ResponseWrapper) IsError() bool {
	return c._response.IsError()
}

func (c *RequestWrapper) Context() context.Context {
	return c._request.Context()
}

func (c *RequestWrapper) SetContext(ctx context.Context) facade.Request {
	c._request.SetContext(ctx)
	return c
}

func (c *RequestWrapper) SetHeader(header, value string) facade.Request {
	c._request.Header.Add(header, value)
	return c
}

func (c *RequestWrapper) SetHeaders(headers map[string]string) facade.Request {
	c._request.SetHeaders(headers)
	return c
}

func (c *RequestWrapper) SetQueryParam(param, value string) facade.Request {
	c._request.SetQueryParam(param, value)
	return c
}

func (c *RequestWrapper) SetQueryParams(params map[string]string) facade.Request {
	c._request.SetQueryParams(params)
	return c
}

func (c *RequestWrapper) SetQueryParamsFromValues(params url.Values) facade.Request {
	c._request.SetQueryParamsFromValues(params)
	return c
}

func (c *RequestWrapper) SetQueryString(query string) facade.Request {
	c._request.SetQueryString(query)
	return c
}

func (c *RequestWrapper) SetFormData(data map[string]string) facade.Request {
	c.SetFormData(data)
	return c
}

func (c *RequestWrapper) SetFormDataFromValues(data url.Values) facade.Request {
	c._request.SetFormDataFromValues(data)
	return c
}

func (c *RequestWrapper) SetBody(body interface{}) facade.Request {
	c._request.SetBody(body)
	return c
}

func (c *RequestWrapper) SetResult(res interface{}) facade.Request {
	c._request.SetResult(res)
	return c
}

func (c *RequestWrapper) SetError(err interface{}) facade.Request {
	c.SetError(err)
	return c
}

func (c *RequestWrapper) SetFile(param, filePath string) facade.Request {
	c._request.SetFile(param, filePath)
	return c
}

func (c *RequestWrapper) SetFiles(files map[string]string) facade.Request {
	c._request.SetFiles(files)
	return c
}

func (c *RequestWrapper) SetFileReader(param, fileName string, reader io.Reader) facade.Request {
	c._request.SetFileReader(param, fileName, reader)
	return c
}

func (c *RequestWrapper) SetMultipartField(param, fileName, contentType string, reader io.Reader) facade.Request {
	c._request.SetMultipartField(param, fileName, contentType, reader)
	return c
}

func (c *RequestWrapper) SetContentLength(l bool) facade.Request {
	c._request.SetContentLength(l)
	return c
}

func (c *RequestWrapper) SetBasicAuth(username, password string) facade.Request {
	c._request.SetBasicAuth(username, password)
	return c
}

func (c *RequestWrapper) SetAuthToken(token string) facade.Request {
	c._request.SetAuthToken(token)
	return c
}

func (c *RequestWrapper) SetOutput(file string) facade.Request {
	c._request.SetOutput(file)
	return c
}

func (c *RequestWrapper) SetDoNotParseResponse(parse bool) facade.Request {
	c._request.SetDoNotParseResponse(parse)
	return c
}

func (c *RequestWrapper) SetPathParams(params map[string]string) facade.Request {
	c._request.SetPathParams(params)
	return c
}

func (c *RequestWrapper) ExpectContentType(contentType string) facade.Request {
	c._request.ExpectContentType(contentType)
	return c
}

func (c *RequestWrapper) SetJSONEscapeHTML(b bool) facade.Request {
	c._request.SetJSONEscapeHTML(b)
	return c
}

func (c *RequestWrapper) SetCookie(hc *http.Cookie) facade.Request {
	c._request.SetCookie(hc)
	return c
}

func (c *RequestWrapper) SetCookies(rs []*http.Cookie) facade.Request {
	c._request.SetCookies(rs)
	return c
}

func (c *RequestWrapper) Get(url string) (facade.Response, error) {
	return c.Execute(MethodGet, url)
}

func (c *RequestWrapper) Head(url string) (facade.Response, error) {
	return c.Execute(MethodHead, url)
}

func (c *RequestWrapper) Post(url string) (facade.Response, error) {
	return c.Execute(MethodPost, url)
}

func (c *RequestWrapper) Put(url string) (facade.Response, error) {
	return c.Execute(MethodPut, url)
}

func (c *RequestWrapper) Delete(url string) (facade.Response, error) {
	return c.Execute(MethodDelete, url)
}

func (c *RequestWrapper) Options(url string) (facade.Response, error) {
	return c.Execute(MethodOptions, url)
}

func (c *RequestWrapper) Patch(url string) (facade.Response, error) {
	return c.Execute(MethodPatch, url)
}

func (c *RequestWrapper) Execute(method, url string) (facade.Response, error) {
	// Hook Request.
	for _, f := range c._httpClient._hookRequest {
		if err := f(c._httpClient, c._httpClient._requestWrapper); err != nil {
			return nil, err
		}
	}
	c._httpClient._propagateTraceHeader()
	r, err := c._request.Execute(method, url)
	res := &ResponseWrapper{_response: r}
	// Hook Response.
	for _, f := range c._httpClient._hookResponse {
		if err := f(c._httpClient, res); err != nil {
			return nil, err
		}
	}
	return res, err
}
