package core

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"

	dsl "techberry-go/common/v2/core/elastic/query-dsl"

	elastic "github.com/olivere/elastic/v7"
	"gopkg.in/resty.v1"
)

type ZxElasticImpl struct {
	_isSecure bool
	_host     string
	_port     int
	_client   *elastic.Client
	_debug    bool
	_logger   facade.LogEvent
	_queryDSL facade.QueryDSL
}

/*
{
	"from": 1,
	"size": 1000,
	"sort": [{ "productId": "asc"}, {"name": "asc"}],
	"_source": {
        "excludes": [ "id" ]
	},
	"query": {
    	"bool": {
			"filter": [
    			{
    				"query_string": {
        				"query": "0411154084*"
    				}
    			},
    			{
            		"multi_match": {
            			"type": "best_fields",
            			"query": "toyota",
            			"lenient": true
            		}
        		}
    		]
    	}
	}
}

*/
/*
	{
		"from": 1,
		"size": 1000,
		"sort": [{ "partnerId": "asc"}, {"name": "asc"}],
		"_source": {
	        "excludes": [ "id" ]
		},
		"query" : {
		    "query_string" : {
		      "query" : "11*",
		      "fields"  : ["partnerId", "name", "groupName", "searchField"],
		      "allow_leading_wildcard": true,
		      "analyze_wildcard": true
		    }
		  }
	}
*/
type ESSearch struct {
	TerminateAfter int64               `structs:"terminate_after,omitempty" json:"terminate_after,omitempty" bson:"terminate_after,omitempty" mapstructure:"terminate_after"`
	From           int64               `structs:"from,omitempty" json:"from,omitempty" bson:"from,omitempty" mapstructure:"from"`
	Size           int64               `structs:"size,omitempty" json:"size,omitempty" bson:"size,omitempty" mapstructure:"size"`
	Sort           []map[string]string `structs:"sort,omitempty" json:"sort,omitempty" bson:"sort,omitempty" mapstructure:"sort"`
	Source         struct {
		Includes []string `structs:"includes,omitempty" json:"includes,omitempty" bson:"includes,omitempty" mapstructure:"includes"`
		Excludes []string `structs:"excludes,omitempty" json:"excludes,omitempty" bson:"excludes,omitempty" mapstructure:"excludes"`
	} `structs:"_source,omitempty" json:"_source,omitempty" bson:"_source,omitempty" mapstructure:"_source"`
	Query struct {
		QueryString struct {
			Query                string   `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
			Fields               []string `structs:"fields,omitempty" json:"fields,omitempty" bson:"fields,omitempty" mapstructure:"fields"`
			AllowLeadingWildcard bool     `structs:"allow_leading_wildcard" json:"allow_leading_wildcard" bson:"allow_leading_wildcard,omitempty" mapstructure:"allow_leading_wildcard"`
			AnalyzeWildcard      bool     `structs:"analyze_wildcard" json:"analyze_wildcard" bson:"analyze_wildcard,omitempty" mapstructure:"analyze_wildcard"`
		} `structs:"query_string,omitempty" json:"query_string,omitempty" bson:"query_string,omitempty" mapstructure:"query_string"`
		Term interface{} `structs:"term,omitempty" json:"term,omitempty" bson:"term,omitempty" mapstructure:"term"`
		Bool struct {
			// QueryFilter and/or MultiMatchFilter , BoolFilter
			Filter []interface{} `structs:"filter,omitempty" json:"filter,omitempty" bson:"filter,omitempty" mapstructure:"filter"`
		} `structs:"bool,omitempty" json:"bool,omitempty" bson:"bool,omitempty" mapstructure:"bool"`
	} `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
}

type QueryFilter struct {
	QueryString struct {
		Query string `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
	} `structs:"query_string,omitempty" json:"query_string,omitempty" bson:"query_string,omitempty" mapstructure:"query_string"`
}

type MultiMatchFilter struct {
	MultiMatch struct {
		Type    string `structs:"type,omitempty" json:"type,omitempty" bson:"type,omitempty" mapstructure:"type"`
		Query   string `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
		Lenient bool   `structs:"lenient,omitempty" json:"lenient,omitempty" bson:"lenient,omitempty" mapstructure:"lenient"`
	} `structs:"multi_match,omitempty" json:"multi_match,omitempty" bson:"multi_match,omitempty" mapstructure:"multi_match"`
}

type BoolFilter struct {
	Bool struct {
		// QueryFilter and/or MultiMatchFilter , BoolFilter
		Filter []interface{} `structs:"filter,omitempty" json:"filter,omitempty" bson:"filter,omitempty" mapstructure:"filter"`
	} `structs:"bool,omitempty" json:"bool,omitempty" bson:"bool,omitempty" mapstructure:"bool"`
}

type BoolShould struct {
	Should             []Should `structs:"should,omitempty" json:"should,omitempty" bson:"should,omitempty" mapstructure:"should"`
	MinimumShouldMatch int64    `structs:"minimum_should_match,omitempty" json:"minimum_should_match,omitempty" bson:"minimum_should_match,omitempty" mapstructure:"minimum_should_match"`
}

type Should struct {
	QueryString struct {
		Query string `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
	} `structs:"query_string,omitempty" json:"query_string" bson:"query_string,omitempty" mapstructure:"query_string"`
}
type DisableIndexSettings struct {
	Index struct {
		RefreshInverval  int64 `structs:"refresh_interval,omitempty" json:"refresh_interval,omitempty" bson:"refresh_interval,omitempty" mapstructure:"refresh_interval"`
		NumberOfReplicas int64 `structs:"number_of_replicas,omitempty" json:"number_of_replicas,omitempty" bson:"number_of_replicas,omitempty" mapstructure:"number_of_replicas"`
	} `structs:"index,omitempty" json:"index,omitempty" bson:"index,omitempty" mapstructure:"index"`
}

type EnableIndexSettings struct {
	Index struct {
		RefreshInverval  string `structs:"refresh_interval,omitempty" json:"refresh_interval,omitempty" bson:"refresh_interval,omitempty" mapstructure:"refresh_interval"`
		NumberOfReplicas int64  `structs:"number_of_replicas,omitempty" json:"number_of_replicas,omitempty" bson:"number_of_replicas,omitempty" mapstructure:"number_of_replicas"`
	} `structs:"index,omitempty" json:"index,omitempty" bson:"index,omitempty" mapstructure:"index"`
}

type MaxResultSettings struct {
	Index struct {
		MaxResultWindow int64 `structs:"max_result_window,omitempty" json:"max_result_window,omitempty" bson:"max_result_window,omitempty" mapstructure:"max_result_window"`
	} `structs:"index,omitempty" json:"index,omitempty" bson:"index,omitempty" mapstructure:"index"`
}

/*
		"query": {
	        "bool": {
	            "must": [
	                {"match": {"partnerId" : "1111"}},
	                {"match": {"name": "หจก.พัฒนายนตร์ (สำนักงานใหญ่)"}}
	            ]
	        }
		}
*/
type ESSearchMatch struct {
	Source struct {
		Includes []string `structs:"includes,omitempty" json:"includes,omitempty" bson:"includes,omitempty" mapstructure:"includes"`
		Excludes []string `structs:"excludes,omitempty" json:"excludes,omitempty" bson:"excludes,omitempty" mapstructure:"excludes"`
	} `structs:"_source,omitempty" json:"_source,omitempty" bson:"_source,omitempty" mapstructure:"_source"`
	Query struct {
		Bool struct {
			Must []interface{} `structs:"must,omitempty" json:"must,omitempty" bson:"must,omitempty" mapstructure:"must"`
		} `structs:"bool,omitempty" json:"bool,omitempty" bson:"bool,omitempty" mapstructure:"bool"`
	} `structs:"query,omitempty" json:"query,omitempty" bson:"query,omitempty" mapstructure:"query"`
}

type PainlessScript struct {
	Script struct {
		Language string      `structs:"lang,omitempty" json:"lang,omitempty" bson:"lang,omitempty" mapstructure:"lang"`
		Source   string      `structs:"source,omitempty" json:"source,omitempty" bson:"source,omitempty" mapstructure:"source"`
		Params   interface{} `structs:"params,omitempty" json:"params,omitempty" bson:"params,omitempty" mapstructure:"params"`
	} `structs:"script,omitempty" json:"script,omitempty" bson:"script,omitempty" mapstructure:"script"`
}

func NewZxElasticImpl(logger facade.LogEvent, isSecure bool, host string, port int, app_key string, basicAuthUser string, basicAuthPasswd string) facade.ElasticHandler {
	serverUrl := ""
	if isSecure {
		serverUrl = fmt.Sprintf("https://%s:%d", host, port)
	} else {
		serverUrl = fmt.Sprintf("http://%s:%d", host, port)
	}
	//serverUrl := fmt.Sprintf("http://%s:%d", host, port)
	var (
		err    error
		client *elastic.Client
	)
	if StringUtils.IsNotEmptyString(app_key) {
		logger.OnDemandTrace("ApiKey : %s", app_key)
		h := fmt.Sprintf("ApiKey %s", app_key)
		client, err = elastic.NewClient(elastic.SetURL(serverUrl), elastic.SetHeaders(http.Header{
			"Authorization": []string{h},
		}), elastic.SetHealthcheck(false), elastic.SetSniff(false))
	} else if StringUtils.IsNotEmptyString(basicAuthUser) &&
		StringUtils.IsNotEmptyString(basicAuthPasswd) {
		logger.OnDemandTrace("Auth user : %s, password : %s", basicAuthUser, basicAuthPasswd)
		client, err = elastic.NewClient(elastic.SetURL(serverUrl), elastic.SetBasicAuth(basicAuthUser, basicAuthPasswd))
	} else {
		logger.OnDemandTrace("No security.")
		client, err = elastic.NewClient(elastic.SetURL(serverUrl))
	}
	if err != nil {
		// Handle error
		logger.Debug().MsgError(err)
		logger.OnDemandTrace("Can't connect to server : %s", serverUrl)
		client = nil
	} else {
		// Getting the ES version number is quite common, so there's a shortcut
		//esversion, err := client.ElasticsearchVersion(serverUrl)
		esversion := ElasticVersion(logger, client)
		logger.OnDemandTrace("Elasticsearch version : %s", esversion)
	}
	log.Println(client)
	queryDSL := dsl.NewQueryDSL(logger)
	var p facade.ElasticHandler = &ZxElasticImpl{_isSecure: isSecure, _host: host, _port: port, _client: client, _logger: logger, _queryDSL: queryDSL}

	return p
}

func (c *ZxElasticImpl) Debug(debug bool) {
	c._debug = debug
}

func (c *ZxElasticImpl) Host() string {
	return c._host
}

func (c *ZxElasticImpl) Port() int {
	return c._port
}

func (c *ZxElasticImpl) Client() *elastic.Client {
	return c._client
}

func ElasticVersion(logger facade.LogEvent, client *elastic.Client) string {
	version := ""
	info, err := client.NodesInfo().Do(context.TODO())
	if err != nil {
		logger.Debug().MsgError(err)
	}
	if info == nil {
		logger.OnDemandTrace("Expected nodes info.")
		return ""
	}

	if info.ClusterName == "" {
		logger.OnDemandTrace("Expected cluster name; got : %s", info.ClusterName)
		return ""
	}
	if len(info.Nodes) == 0 {
		logger.OnDemandTrace("Expected some nodes; got : %d", len(info.Nodes))
		return ""
	}
	for _, node := range info.Nodes {
		version = node.Version
		break
	}
	return version
}

func (c *ZxElasticImpl) AdvancedSearch(indexName string, includeFields []string, excludeFields []string, storedFields []string, sort map[string]string, from int64, size int64, query string) (int64, []interface{}) {
	ctx := context.Background()
	if len(includeFields) > 0 || len(excludeFields) > 0 {
		sourceContext := elastic.NewFetchSourceContext(true).Include(includeFields...).Exclude(excludeFields...)
		queryStringQuery := elastic.NewQueryStringQuery(query).AllowLeadingWildcard(true).AnalyzeWildcard(true)
		/*
			src, err := queryStringQuery.Source()
			if err != nil {
				beego.Debug(err)
			}
			data, err := json.Marshal(src)
			if err != nil {
				beego.Debug(err)
			}
			beego.Debug(string(data))
			beego.Debug(queryStringQuery.Source)
		*/
		searchResult, err := c._client.Search(indexName).
			From(int(from)).
			Size(int(size)).
			TerminateAfter(int(size)).
			StoredFields(storedFields...).
			SortMap(sort).
			FetchSourceContext(sourceContext).Query(queryStringQuery).Do(ctx)
		if err != nil {
			// Handle error
		} else {
			hits := searchResult.TotalHits()
			jsonService := NewZxJsonImpl(searchResult.RawData)
			//count := jsonService.GetIntegerValue(0, "hits", "total", "value")
			status, value := jsonService.GetArray("hits.hits")
			if status {
				return hits, value
			}
		}
	}
	return 0, nil
	/*
		searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c.ElasticHost, c.ElasticPort, indexName)

		elastic := ESSearch{}
		elastic.From = from
		elastic.Size = size
		elastic.TerminateAfter = size
		if len(includeFields) > 0 {
			elastic.Source.Includes = includeFields
		}
		if len(excludeFields) > 0 {
			elastic.Source.Excludes = excludeFields
		}
		elastic.Query.QueryString.Query = query
		if fields != nil && len(fields) > 0 {
			elastic.Query.QueryString.Fields = fields
		}
		elastic.Query.QueryString.AllowLeadingWildcard = true
		elastic.Query.QueryString.AnalyzeWildcard = true
		if sort != nil {
			elastic.Sort = sort
		}

		StructUtil := NewStructImpl()
		searchMap := StructUtil.ToMap(elastic)
		MapUtils := NewMapImpl()
		jsonByte, err := MapUtils.ToJson(searchMap)
		beego.Debug(err)
		beego.Debug(string(jsonByte))
		resp, err := resty.R().
			SetHeader("Content-Type", "application/json").
			SetBody(searchMap).
			Post(searchAPI)
		if err == nil {
			if resp.StatusCode() == 200 {
				jsonService := helper.NewJsonService(resp.Body())
				count := jsonService.GetIntegerValue(0, "hits", "total", "value")
				status, value := jsonService.GetArray("hits", "hits")
				if status {
					return count, value
				}
			}
		}
		return 0, nil
	*/
}

func (c *ZxElasticImpl) Search(indexName string, source []string, fields []string, sort []map[string]string, from int64, size int64, query string) (int64, []interface{}) {
	searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c._host, c._port, indexName)

	searchMap := make(map[string]interface{})
	searchMap["size"] = size
	searchMap["from"] = from
	searchMap["_source"] = source
	queryMap := make(map[string]interface{})
	queryStringMap := make(map[string]interface{})
	queryStringMap["fields"] = fields
	queryStringMap["query"] = query
	queryStringMap["allow_leading_wildcard"] = true
	queryStringMap["analyze_wildcard"] = true
	queryMap["query_string"] = queryStringMap
	searchMap["query"] = queryMap
	/*
		sort := []map[string]string{}
		sort = append(sort, map[string]string{
			" ":   "asc",
			"cust_name": "asc",
		})
	*/
	searchMap["sort"] = sort
	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(searchMap).
		Post(searchAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			count := jsonService.Int(0, "hits.total.value")
			status, value := jsonService.GetArray("hits.hits")
			//var responseData map[string]interface{}
			//err := json.Unmarshal(resp.Body(), &responseData)
			if status {
				return count, value
			}
		}
	}
	return 0, nil
}

func (c *ZxElasticImpl) GetDetail(indexName string, includeFields []string, excludeFields []string, fields map[string]interface{}) (interface{}, error) {
	var term []map[string]interface{}

	for key, value := range fields {
		term = c.PrepareTerm(term, key, value)
	}
	v1, err := c.FindDetailMultiTerm(indexName, term)
	if err == nil && len(v1) == 1 {
		return MapUtils.ToMap(v1[0]), nil
	} else {
		return nil, errors.New("Search match criteria > 1.")
	}

	/*
		// ============== Start Span =================
		//span1 := c._mc.StartSpan("elastic getdetail")
		//span1.SetTag("indexName", indexName)
		//span1.SetTag("fields", c._jsonStringFromMap(fields))
		//span1.AddTag()
		// ==========================================
		//defer span1.Finish()
		ctx := context.Background()
		if len(includeFields) > 0 || len(excludeFields) > 0 {
			sourceContext := elastic.NewFetchSourceContext(true).Include(includeFields...).Exclude(excludeFields...)
			src, err := sourceContext.Source()
			if err != nil {
				log.Println(err)
			}
			data, err := json.Marshal(src)
			if err != nil {
				log.Println(err)
			}
			log.Println(string(data))
			boolQuery := elastic.NewBoolQuery()
			src, err = boolQuery.Source()
			if err != nil {
				log.Println(err)
			}
			data, err = json.Marshal(src)
			if err != nil {
				log.Println(err)
			}
			log.Println(string(data))
			searchResult, err := c._client.Search(indexName).
				FetchSourceContext(sourceContext).Query(boolQuery).Do(ctx)
			if err != nil {
				log.Println(err)
				// Handle error
				//span1.SetTag("error", err.Error())
				//span1.AddTag()
			} else {
				hits := searchResult.TotalHits()
				jsonService := NewZxJsonImpl(searchResult.RawData)
				//count := jsonService.GetIntegerValue(0, "hits", "total", "value")
				status, value := jsonService.GetArray("hits.hits")
				//span1.SetTag("hits", hits)
				//span1.AddTag()
				if hits == 1 && status {
					return value
				}
			}
		}
		log.Println("nil")
		return nil
	*/
	/*
		searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c.ElasticHost, c.ElasticPort, indexName)

		elastic := ESSearchMatch{}
		if len(includeFields) > 0 {
			elastic.Source.Includes = includeFields
		}
		if len(excludeFields) > 0 {
			elastic.Source.Excludes = excludeFields
		}
		var must []interface{}
		for key, value := range fields {
			m := make(map[string]interface{})
			m[key] = value
			matchMap := make(map[string]interface{})
			matchMap["match"] = m
			must = append(must, matchMap)
		}
		elastic.Query.Bool.Must = must

		StructUtils := NewStructImpl()
		searchMap := StructUtils.ToMap(elastic)
		MapUtils := NewMapImpl()
		jsonByte, err := MapUtils.ToJson(searchMap)
		beego.Debug(err)
		beego.Debug(string(jsonByte))
		resp, err := resty.R().
			SetHeader("Content-Type", "application/json").
			SetBody(searchMap).
			Post(searchAPI)
		if err == nil {
			if resp.StatusCode() == 200 {
				jsonService := helper.NewJsonService(resp.Body())
				count := jsonService.GetIntegerValue(0, "hits", "total", "value")
				status, value := jsonService.GetArray("hits", "hits")
				beego.Debug("count = ", count, " status = ", status, " value = ", value)
				if count == 1 && status {
					return value
				}
			}
		}
		return nil
	*/
}

func (c *ZxElasticImpl) GetId(indexName string, fields []map[string]interface{}) (string, error) {
	ctx := context.Background()

	mustQuery := c._prepareMustMatchQueryCtx(fields)
	searchResult, err := c._client.Search(indexName).Query(mustQuery).Do(ctx)
	if err != nil {
		return "", err
	} else {
		jsonService := NewZxJsonImpl(searchResult.RawData)
		count := jsonService.Int(0, "hits.total.value")
		status, _ := jsonService.GetArray("hits.hits")
		if count == 1 && status {
			return jsonService.String("", "hits.hits.0._id"), nil
		} else {
			return "", nil
		}
	}
}

func (c *ZxElasticImpl) _prepareMustMatchQueryCtx(terms []map[string]interface{}) *elastic.BoolQuery {
	q := elastic.NewBoolQuery()
	for _, term := range terms {
		for key, value := range term {
			if s, ok := value.(string); ok {
				if strings.Contains(s, "*") {
					q = q.Must(elastic.NewWildcardQuery(key, s))
				} else {
					q = q.Must(elastic.NewTermQuery(key, value))
				}
			} else {
				q = q.Must(elastic.NewTermQuery(key, value))
			}
		}
	}
	return q
}

/*
func (c *ZxElasticImpl) GetDocumentId(indexName string, source []string, fields map[string]interface{}) (bool, string) {
	searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c.ElasticHost, c.ElasticPort, indexName)

	searchMap := make(map[string]interface{})
	if source != nil {
		searchMap["_source"] = source
	}
	searchMap["size"] = 1
	queryMap := make(map[string]interface{})
	queryMap["match"] = fields
	searchMap["query"] = queryMap
	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(searchMap).
		Post(searchAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := helper.NewJsonService(resp.Body())
			count := jsonService.GetIntegerValue(0, "hits", "total", "value")
			status, _ := jsonService.GetArray("hits", "hits")
			if count == 1 && status {
				return true, jsonService.GetStringValue("", "hits", "hits", "[0]", "_id")
			}
		}
	}
	return false, ""
}
*/
/*
func (c *ZxElasticImpl) GetByField(indexName string, source []string, fields map[string]interface{}) []interface{} {
	searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c.ElasticHost, c.ElasticPort, indexName)

	searchMap := make(map[string]interface{})
	if source != nil {
		searchMap["_source"] = source
	}
	searchMap["size"] = 1
	queryMap := make(map[string]interface{})
	queryMap["match"] = fields
	searchMap["query"] = queryMap
	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(searchMap).
		Get(searchAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := helper.NewJsonService(resp.Body())
			//count := jsonService.GetIntegerValue(0, "hits", "total", "value")
			status, value := jsonService.GetArray("hits", "hits")
			if status {
				return value
			}
		}
	}
	return nil
}
*/

func (c *ZxElasticImpl) DeleteById(indexName string, id string) bool {
	ctx := context.Background()
	res, err := c._client.Delete().Index(indexName).Id(id).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return false
	}
	if want, have := "deleted", res.Result; want != have {
		c._logger.OnDemandTrace("Fail to delete. Result = %q", want)
		return false
	} else {
		_, err = c._client.Refresh().Index(indexName).Do(ctx)
		if err != nil {
			c._logger.Debug().MsgError(err)
		}
	}
	return true
}

func (c *ZxElasticImpl) GetDetailById(indexName string, includeFields []string, excludeFields []string, objId string) []interface{} {
	searchAPI := fmt.Sprintf("http://%s:%d/%s/_search", c._host, c._port, indexName)

	elastic := ESSearch{}
	if len(includeFields) > 0 {
		elastic.Source.Includes = includeFields
	}
	if len(excludeFields) > 0 {
		elastic.Source.Excludes = excludeFields
	}
	m := make(map[string]interface{})
	m["_id"] = objId
	elastic.Query.Term = m

	StructUtil := impl.NewStructImpl()
	searchMap := StructUtil.ToMap(elastic)

	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(searchMap).
		Post(searchAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			count := jsonService.Int(0, "hits.total.value")
			status, value := jsonService.GetArray("hits.hits")
			if count == 1 && status {
				return value
			}
		}
	}
	return nil
}

func (c *ZxElasticImpl) Upsert(indexName string, id string, document interface{}) (string, error) {
	ctx := context.Background()

	// Exists document.
	exists, err := c._client.Exists().Index(indexName).Id(id).Do(context.TODO())
	if err != nil {
		c._logger.Debug().MsgError(err)
	}

	if exists {
		c._logger.OnDemandTrace("Document is exist.")
	}

	indexResult, err := c._client.Index().
		Index(indexName).
		Id(id).
		BodyJson(document).
		Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return "", err
	}
	if indexResult == nil {
		c._logger.OnDemandTrace("Result is nil.")
		return "", nil
	} else {
		c._logger.OnDemandTrace("Upsert (%d) successfully with id : %s", indexResult.Status, indexResult.Id)
		return indexResult.Id, nil
	}
}

func (c *ZxElasticImpl) Update(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error) {
	var (
		updateAPI string = ""
		status    bool   = false
	)
	if StringUtils.IsNotEmptyString(_id) {
		updateAPI = fmt.Sprintf("http://%s:%d/%s/_update/%s", c._host, c._port, indexName, _id) // PUT : Update document with _id, , POST /<index_name>/_udpate/<_id> : Update adds new field to the existing document
	} else {
		updateAPI = fmt.Sprintf("http://%s:%d/%s/_update/Zaq1Xsw2Cde3_For_Dummy", c._host, c._port, indexName) // PUT : Update document with dummy _id , POST /<index_name>/_udpate/<_id> : Update adds new field to the existing document
	}

	loc, _ := time.LoadLocation("UTC")
	t := time.Now().In(loc)
	timestamp := t.Format("2006-01-02T15:04:05.000000")
	//timestamp := fmt.Sprintf("%s", time.Now())
	dataMap["@timestamp"] = timestamp
	doc := make(map[string]interface{})
	doc["doc"] = dataMap
	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(doc).
		Post(updateAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			result := jsonService.String("", "result")
			successful := jsonService.Int(0, "_shards.successful")
			if result == "updated" && successful == 1 {
				//docId := jsonService.GetStringValue("", "_id")
				//beego.Debug("Updating existing document successfully for ", docId)
				status = true
			} else if result == "noop" {
				docId := jsonService.String("", "_id")
				c._logger.OnDemandTrace("Noting to update for %s", docId)
				status = true
			} else {
				msg := fmt.Sprintf("Updating existing document failed for %s", _id)
				c._logger.OnDemandTrace(msg)
				err = errors.New(msg)
			}
		} else {
			msg := fmt.Sprintf("Updating existing document exception for %s", _id)
			c._logger.OnDemandTrace(msg)
			err = errors.New(msg)
		}
	} else {
		msg := fmt.Sprintf("Updating into the existing document error : %s", err.Error())
		c._logger.OnDemandTrace(msg)
		err = errors.New(msg)
	}
	return status, err
}

func (c *ZxElasticImpl) ReplaceWithKey(key string, value interface{}, indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error) {
	var (
		term []map[string]interface{}
	)
	term = c.PrepareTerm(term, key, value)
	v1, err := c.FindDetailMultiTerm(indexName, term)
	if err == nil && len(v1) == 1 {
		r := MapUtils.ToMap(v1[0])
		_id := MapUtils.String(r, "_id", "")
		c.DeleteById(indexName, _id)
		return c.Add(indexName, documentType, _id, dataMap)
	} else {
		_id := ""
		return c.Add(indexName, documentType, _id, dataMap)
	}
}

func (c *ZxElasticImpl) Add(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error) {
	ctx := context.Background()
	loc, _ := time.LoadLocation("UTC")
	t := time.Now().In(loc)
	timestamp := t.Format("2006-01-02T15:04:05.000000")
	//timestamp := fmt.Sprintf("%s", time.Now())
	dataMap["@timestamp"] = timestamp
	version := "1"
	dataMap["@version"] = version
	indexResult, err := c._client.Index().Index(indexName).BodyJson(dataMap).Do(ctx)
	if err != nil {
		msg := fmt.Sprintf("Add a document error : %s", err.Error())
		c._logger.OnDemandTrace(msg)
		err = errors.New(msg)
		return false, err
	} else {
		if indexResult != nil {
			c._logger.OnDemandTrace("Add a document successfully for : %s", indexResult.Id)
			return true, nil
		} else {
			c._logger.OnDemandTrace("Add a document exception")
			return false, errors.New("Add a document exception")
		}
	}
}

func (c *ZxElasticImpl) Insert(indexName string, documentType string, _id string, dataMap map[string]interface{}) (bool, error) {
	var (
		insertAPI string = ""
		status    bool   = false
		scheme    string = "http"
	)
	if c._isSecure {
		scheme = "https"
	}
	if StringUtils.IsNotEmptyString(_id) {
		insertAPI = fmt.Sprintf("%s://%s:%d/%s/%s/%s", scheme, c._host, c._port, indexName, documentType, _id) // PUT : Create document with _id, or /<index_name>/_create/<_id> : Create resource to index
	} else {
		insertAPI = fmt.Sprintf("%s://%s:%d/%s/%s/", scheme, c._host, c._port, indexName, documentType) // POST : Create document IDs automatically
	}
	loc, _ := time.LoadLocation("UTC")
	t := time.Now().In(loc)
	timestamp := t.Format("2006-01-02T15:04:05.000000")
	//timestamp := fmt.Sprintf("%s", time.Now())
	dataMap["@timestamp"] = timestamp
	version := "1"
	dataMap["@version"] = version
	var (
		resp *resty.Response
		err  error
	)
	resp, err = resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(dataMap).
		Post(insertAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			result := jsonService.String("", "result")
			if result == "created" {
				docId := jsonService.String("", "_id")
				c._logger.OnDemandTrace("Inserting new document successfully for : %s", docId)
				status = true
			} else {
				msg := fmt.Sprintf("Inserting new document failed for %s", _id)
				c._logger.OnDemandTrace(msg)
				err = errors.New(msg)
			}
		} else if resp.StatusCode() == 201 {
			jsonService := NewZxJsonImpl(resp.Body())
			result := string(jsonService.GetData())
			c._logger.OnDemandTrace(result)
			//beego.Debug("Inserting new document successfully")
			status = true
		} else {
			msg := fmt.Sprintf("Inserting new document exception")
			c._logger.OnDemandTrace(msg)
			err = errors.New(msg)
		}
	} else {
		msg := fmt.Sprintf("Inserting to the new document error : %s", err.Error())
		c._logger.OnDemandTrace(msg)
		err = errors.New(msg)
	}
	return status, err
}

// Disable refresh and replicas for initial loads.
func (c *ZxElasticImpl) DisableIndex(indexName string) {
	ctx := context.Background()

	body := `{
		"index":{
			"refresh_interval":"-1",
			"number_of_replicas": 0
		}
	}`

	// Exists document.
	res, err := c._client.IndexPutSettings().Index(indexName).BodyString(body).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	if res == nil {
		c._logger.OnDemandTrace("Got response error : %v", res)
	}
	if !res.Acknowledged {
		c._logger.OnDemandTrace("Got acknowledged error : %t", res.Acknowledged)
	}
	c._logger.OnDemandTrace("Successfully to disable index : %s", indexName)
}

// Enable refresh and replicas for initial loads.
func (c *ZxElasticImpl) EnableIndex(indexName string) {
	ctx := context.Background()

	body := `{
		"index":{
			"refresh_interval":"60s",
			"number_of_replicas": 1
		}
	}`

	// Exists document.
	res, err := c._client.IndexPutSettings().Index(indexName).BodyString(body).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	if res == nil {
		c._logger.OnDemandTrace("Got response error : %v", res)
	}
	if !res.Acknowledged {
		c._logger.OnDemandTrace("Got acknowledged error : %t", res.Acknowledged)
	}
	c._logger.OnDemandTrace("Successfully to enable index : %s", indexName)
}

// Setting max result for search query.
func (c *ZxElasticImpl) SetMaxResult(indexName string, max int64) {
	ctx := context.Background()

	body := `{
		"index":{
			"max_result_window":%d
		}
	}`
	body = fmt.Sprintf(body, max)

	// Exists document.
	res, err := c._client.IndexPutSettings().Index(indexName).BodyString(body).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	if res == nil {
		c._logger.OnDemandTrace("Got response error : %v", res)
	}
	if !res.Acknowledged {
		c._logger.OnDemandTrace("Got acknowledged error : %t", res.Acknowledged)
	}
	c._logger.OnDemandTrace("Successfully to set max result for index : %s", indexName)
}

func (c *ZxElasticImpl) ScriptUpdate(indexName string, _id string, script string, params map[string]interface{}) (bool, error) {
	var (
		updateAPI string = ""
	)
	if StringUtils.IsNotEmptyString(_id) {
		updateAPI = fmt.Sprintf("http://%s:%d/%s/_update/%s", c._host, c._port, indexName, _id) // POST /<index_name>/_udpate/<_id> : Update adds new field to the existing document
		var (
			resp *resty.Response
			err  error
		)
		painless := PainlessScript{}
		painless.Script.Language = "painless"
		painless.Script.Source = script
		if params != nil {
			painless.Script.Params = params
		}

		StructUtils := impl.NewStructImpl()
		m := StructUtils.ToMap(painless)

		resp, err = resty.R().
			SetHeader("Content-Type", "application/json").
			SetBody(m).
			Post(updateAPI)
		if err == nil {
			if resp.StatusCode() == 200 {
				jsonService := NewZxJsonImpl(resp.Body())
				result := jsonService.String("", "result")
				successful := jsonService.Int(0, "_shards.successful")
				if result == "updated" && successful == 1 {
					//docId := jsonService.GetStringValue("", "_id")
					//beego.Debug("Updating existing document successfully for ", docId)
					return true, nil
				} else {
					return false, errors.New("Fail to update data using painless scripting.")
				}
			} else {
				msg := fmt.Sprintf("Update API failed due to response status code is %d", resp.StatusCode())
				return false, errors.New(msg)
			}
		}
		return false, err
	} else {
		return false, errors.New("Object Id (_id) is required to update.")
	}
}

func (c *ZxElasticImpl) SearchMatch(dataContext *facade.DataContext, excludeFields []string, includeFields []string, sort map[string]string, query elastic.Query) ([]interface{}, int64, error) {
	ctx := context.Background()
	indexName := dataContext.IndexName
	sourceContext := c._queryDSL.SourceCtx(includeFields, excludeFields)
	searchResult, err := c._client.Search(indexName).
		From(dataContext.From).
		Size(dataContext.Size).
		TerminateAfter(dataContext.TerminateAfterSize).
		SortMap(sort).
		FetchSourceContext(sourceContext).Query(query).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	} else {
		//hits := searchResult.TotalHits()
		jsonHandler := NewZxJsonImpl(searchResult.RawData)
		total := jsonHandler.Int(-1, "hits.total.value")
		_, value := jsonHandler.GetArray("hits.hits")
		if total > 0 && value != nil && len(value) > 0 {
			return value, total, nil
		} else {
			c._logger.OnDemandTrace("Search criteria not found.")
			return nil, 0, nil
		}
	}
	return nil, 0, err
}

func (c *ZxElasticImpl) FindDetail(indexName string, key string, value interface{}) ([]interface{}, error) {
	ctx := context.Background()

	excludeFields := []string{"id"}
	includeFields := []string{}

	sourceContext := c._queryDSL.SourceCtx(includeFields, excludeFields)
	//mustQuery := c._queryDSL.NewBoolQuery().MustTermByKey(key, value).BoolQuery()
	termQuery := c._queryDSL.TermQuery(key, value)
	searchResult, err := c._client.Search(indexName).
		FetchSourceContext(sourceContext).Query(termQuery).Do(ctx)
	if err != nil {
		log.Println(err.Error())
	} else {
		//hits := searchResult.TotalHits()
		jsonHandler := NewZxJsonImpl(searchResult.RawData)
		status, value := jsonHandler.GetArray("hits.hits")
		if status {
			return value, nil
		} else {
			return nil, errors.New("Get detail not found.")
		}
	}
	return nil, err
}

func (c *ZxElasticImpl) FindDetailMultiTerm(indexName string, term []map[string]interface{}) ([]interface{}, error) {
	ctx := context.Background()

	excludeFields := []string{"id"}
	includeFields := []string{}

	sourceContext := c._queryDSL.SourceCtx(includeFields, excludeFields)
	//mustQuery := c._queryDSL.NewBoolQuery().MustTermByKey(key, value).BoolQuery()
	//termQuery := c._queryDSL.TermQuery(key, value)
	termQuery := c._queryDSL.NewBoolQuery().MustMatchQueryCtx(term).BoolQuery()
	searchResult, err := c._client.Search(indexName).
		FetchSourceContext(sourceContext).Query(termQuery).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	} else {
		//hits := searchResult.TotalHits()
		jsonHandler := NewZxJsonImpl(searchResult.RawData)
		status, value := jsonHandler.GetArray("hits.hits")
		if status {
			return value, nil
		} else {
			return nil, errors.New("Get detail not found.")
		}
	}
	return nil, err
}

func (c *ZxElasticImpl) PrepareTerm(term []map[string]interface{}, field string, value interface{}) []map[string]interface{} {
	/*
		switch v := value.(type) {
		case string:
			m := make(map[string]interface{})
			m[field] = v
			term = append(term, m)
		case int64:
			m := make(map[string]interface{})
			m[field] = v
			term = append(term, m)
		case float64:
			m := make(map[string]interface{})
			m[field] = v
			term = append(term, m)
		default:
			// errors.New("Value datatype is not supported.")
		}
	*/
	m := make(map[string]interface{})
	m[field] = value
	term = append(term, m)
	return term
}

func (c *ZxElasticImpl) PageToOffset(page int64, limit int64) (int64, int64) {
	var (
		offset_start int64
	)
	if page == 0 {
		return 0, (page + limit - 1)
	}

	offset_start = (page * limit)
	return offset_start, offset_start + limit - 1
}

func (c *ZxElasticImpl) Aggregate(indexName string, term []map[string]interface{}, field string, script string) ([]interface{}, error) {
	ctx := context.Background()

	termQuery := c._queryDSL.NewBoolQuery().MustMatchQueryCtx(term).BoolQuery()
	aggTerms := c._queryDSL.TermsAggregation()
	if StringUtils.IsNotEmptyString(script) {
		s := elastic.NewScript(script)
		aggTerms.Script(s)
	} else {
		aggTerms.Field(field)
	}
	aggTerms.Size(10000) // Maximum aggregatee buckets size
	searchResult, err := c._client.Search(indexName).Size(0).Query(termQuery).Aggregation("count", aggTerms).Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
	} else {
		//hits := searchResult.TotalHits()
		jsonHandler := NewZxJsonImpl(searchResult.RawData)
		status, value := jsonHandler.GetArray("aggregations.count.buckets")
		if status {
			return value, nil
		} else {
			return nil, errors.New("Buckets not found.")
		}
	}
	return nil, err
}

func (c *ZxElasticImpl) UpdateByScript(scriptName string, indexName string, _id string, params map[string]interface{}) error {
	ctx := context.Background()

	script := elastic.NewScriptStored(scriptName).Params(params)
	termQuery := c._queryDSL.TermQuery("_id", _id)
	updateResp, err := c._client.UpdateByQuery().
		Index(indexName).
		Query(termQuery).
		Script(script).
		Do(ctx)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return err
	}
	if updateResp == nil {
		c._logger.OnDemandTrace("Update response is nil.")
	}
	return nil
}

func (c *ZxElasticImpl) _jsonStringFromMap(m map[string]interface{}) string {
	var jsonString string
	mapHandler := impl.NewMapImpl()
	b, err := mapHandler.ToJson(m)
	if err == nil {
		jsonString = string(b)
	} else {
		jsonString = ""
	}
	return jsonString
}

func (c *ZxElasticImpl) PrintSource(src interface{}, err error) {
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	data, err := json.Marshal(src)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	log.Println(string(data))
}

func (c *ZxElasticImpl) UpdateTimestamp(data interface{}) {
	dataMap := data.(map[string]interface{})
	loc, _ := time.LoadLocation("UTC")
	t := time.Now().In(loc)
	timestamp := t.Format("2006-01-02T15:04:05.000000")
	//timestamp := fmt.Sprintf("%s", time.Now())
	dataMap["@timestamp"] = timestamp
}

// ==========================================================
// Obsolete
// ==========================================================

// Disable refresh and replicas for initial loads.
func (c *ZxElasticImpl) DisableIndex_Obsolete(indexName string) {
	settingsAPI := fmt.Sprintf("http://%s:%d/%s/_settings", c._host, c._port, indexName) // PUT : To settings parameters for specific index.

	indexSetting := DisableIndexSettings{}
	indexSetting.Index.RefreshInverval = -1
	indexSetting.Index.NumberOfReplicas = 0

	StructUtils := impl.NewStructImpl()
	m := StructUtils.ToMap(indexSetting)

	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(m).
		Put(settingsAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			acknowledged := jsonService.Bool(false, "acknowledged")
			if acknowledged {
				c._logger.OnDemandTrace("Successfully to disable index : %s", indexName)
			}
		}
	}
}

// Enable refresh and replicas for initial loads.
func (c *ZxElasticImpl) EnableIndex_Obsolete(indexName string) {
	settingsAPI := fmt.Sprintf("http://%s:%d/%s/_settings", c._host, c._port, indexName) // PUT : To settings parameters for specific index.

	indexSetting := EnableIndexSettings{}
	indexSetting.Index.RefreshInverval = "60s"
	indexSetting.Index.NumberOfReplicas = 1

	StructUtils := impl.NewStructImpl()
	m := StructUtils.ToMap(indexSetting)

	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(m).
		Put(settingsAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			acknowledged := jsonService.Bool(false, "acknowledged")
			if acknowledged {
				c._logger.OnDemandTrace("Successfully to enable index : %s", indexName)
			}
		}
	}
}

func (c *ZxElasticImpl) SetMaxResult_Obsolete(indexName string, max int64) {
	settingsAPI := fmt.Sprintf("http://%s:%d/%s/_settings", c._host, c._port, indexName) // PUT : To settings parameters for specific index.

	settings := MaxResultSettings{}
	settings.Index.MaxResultWindow = max

	StructUtils := impl.NewStructImpl()
	m := StructUtils.ToMap(settings)

	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(m).
		Put(settingsAPI)
	if err == nil {
		if resp.StatusCode() == 200 {
			jsonService := NewZxJsonImpl(resp.Body())
			acknowledged := jsonService.Bool(false, "acknowledged")
			if acknowledged {
				c._logger.OnDemandTrace("Successfully to set max result for index : %s", indexName)
			}
		}
	}
}
