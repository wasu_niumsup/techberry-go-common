package core

import (
	"bytes"
	"fmt"
	"os"
	"strconv"
	"techberry-go/common/v2/facade"
	"time"

	"github.com/rs/zerolog"
)

var (
	CallerMarshalFunc = func(file string, line int) string {
		return file + ":" + strconv.Itoa(line)
	}
)

type ZxZerolog struct {
	_outputType       int
	_logger           zerolog.Logger
	_event            *zerolog.Event
	_buffer           *bytes.Buffer
	_kafka            facade.Kafka
	_bootstrapServers string
	_topic            string
	_moduleInfo       *facade.LogModuleInfo
	_x_trace          bool
}

func NewZxZerolog() facade.LogEvent {
	var p facade.LogEvent = &ZxZerolog{}
	return p
}

func (c *ZxZerolog) Kafka(bootstrapServers string, topic string) {
	c._bootstrapServers = bootstrapServers
	c._topic = topic
	c._kafka = NewZxKafkaImpl(c, c._bootstrapServers)
}

func (c *ZxZerolog) SetXTrace(flag bool) {
	c._x_trace = flag
}

func (c *ZxZerolog) SetOutput(outputType int) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMicro

	if outputType == facade.L_CONSOLE {
		c._logger = zerolog.New(os.Stdout).With().CallerWithSkipFrameCount(3).Timestamp().Logger()
		c._outputType = outputType
	} else if outputType == facade.L_KAFKA {
		c._buffer = &bytes.Buffer{}
		c._logger = zerolog.New(c._buffer).With().CallerWithSkipFrameCount(3).Timestamp().Logger()
		c._outputType = outputType
	}
}

func (c *ZxZerolog) SetModuleInfo(info *facade.LogModuleInfo) facade.LogEvent {
	c._moduleInfo = info
	return c
}

func (c *ZxZerolog) Info() facade.LogEvent {
	c._event = c._logger.Info()
	return c
}

func (c *ZxZerolog) Debug() facade.LogEvent {
	c._event = c._logger.Debug()
	return c
}

func (c *ZxZerolog) Error() facade.LogEvent {
	c._event = c._logger.Error()
	return c
}

func (c *ZxZerolog) Trace() facade.LogEvent {
	c._event = c._logger.Trace()
	return c
}

// Str adds the field key with val as a string to the logger context.
func (c *ZxZerolog) Str(key, val string) facade.LogEvent {
	c._event.Str(key, val)
	return c
}

// Strs adds the field key with val as a string to the logger context.
func (c *ZxZerolog) Strs(key string, vals []string) facade.LogEvent {
	c._event.Strs(key, vals)
	return c
}

// Stringer adds the field key with val.String() (or null if val is nil) to the logger context.
func (c *ZxZerolog) Stringer(key string, val fmt.Stringer) facade.LogEvent {
	c._event.Stringer(key, val)
	return c
}

// Bytes adds the field key with val as a []byte to the logger context.
func (c *ZxZerolog) Bytes(key string, val []byte) facade.LogEvent {
	c._event.Bytes(key, val)
	return c
}

// Hex adds the field key with val as a hex string to the logger context.
func (c *ZxZerolog) Hex(key string, val []byte) facade.LogEvent {
	c._event.Hex(key, val)
	return c
}

// RawJSON adds already encoded JSON to context.
//
// No sanity check is performed on b; it must not contain carriage returns and
// be valid JSON.
func (c *ZxZerolog) RawJSON(key string, b []byte) facade.LogEvent {
	c._event.RawJSON(key, b)
	return c
}

func (c *ZxZerolog) AnErr(key string, err error) facade.LogEvent {
	c._event.AnErr(key, err)
	return c
}

// Errs adds the field key with errs as an array of serialized errors to the
// logger context.
func (c *ZxZerolog) Errs(key string, errs []error) facade.LogEvent {
	c._event.Errs(key, errs)
	return c
}

// Err adds the field "error" with serialized err to the logger context.
func (c *ZxZerolog) Err(err error) facade.LogEvent {
	c._event.Err(err)
	return c
}

// Bool adds the field key with val as a bool to the logger context.
func (c *ZxZerolog) Bool(key string, b bool) facade.LogEvent {
	c._event.Bool(key, b)
	return c
}

// Bools adds the field key with val as a []bool to the logger context.
func (c *ZxZerolog) Bools(key string, b []bool) facade.LogEvent {
	c._event.Bools(key, b)
	return c
}

// Int adds the field key with i as a int to the logger context.
func (c *ZxZerolog) Int(key string, i int) facade.LogEvent {
	c._event.Int(key, i)
	return c
}

// Ints adds the field key with i as a []int to the logger context.
func (c *ZxZerolog) Ints(key string, i []int) facade.LogEvent {
	c._event.Ints(key, i)
	return c
}

// Int8 adds the field key with i as a int8 to the logger context.
func (c *ZxZerolog) Int8(key string, i int8) facade.LogEvent {
	c._event.Int8(key, i)
	return c
}

// Ints8 adds the field key with i as a []int8 to the logger context.
func (c *ZxZerolog) Ints8(key string, i []int8) facade.LogEvent {
	c._event.Ints8(key, i)
	return c
}

// Int16 adds the field key with i as a int16 to the logger context.
func (c *ZxZerolog) Int16(key string, i int16) facade.LogEvent {
	c._event.Int16(key, i)
	return c
}

// Ints16 adds the field key with i as a []int16 to the logger context.
func (c *ZxZerolog) Ints16(key string, i []int16) facade.LogEvent {
	c._event.Ints16(key, i)
	return c
}

// Int32 adds the field key with i as a int32 to the logger context.
func (c *ZxZerolog) Int32(key string, i int32) facade.LogEvent {
	c._event.Int32(key, i)
	return c
}

// Ints32 adds the field key with i as a []int32 to the logger context.
func (c *ZxZerolog) Ints32(key string, i []int32) facade.LogEvent {
	c._event.Ints32(key, i)
	return c
}

// Int64 adds the field key with i as a int64 to the logger context.
func (c *ZxZerolog) Int64(key string, i int64) facade.LogEvent {
	c._event.Int64(key, i)
	return c
}

// Ints64 adds the field key with i as a []int64 to the logger context.
func (c *ZxZerolog) Ints64(key string, i []int64) facade.LogEvent {
	c._event.Ints64(key, i)
	return c
}

// Uint adds the field key with i as a uint to the logger context.
func (c *ZxZerolog) Uint(key string, i uint) facade.LogEvent {
	c._event.Uint(key, i)
	return c
}

// Uints adds the field key with i as a []uint to the logger context.
func (c *ZxZerolog) Uints(key string, i []uint) facade.LogEvent {
	c._event.Uints(key, i)
	return c
}

// Uint8 adds the field key with i as a uint8 to the logger context.
func (c *ZxZerolog) Uint8(key string, i uint8) facade.LogEvent {
	c._event.Uint8(key, i)
	return c
}

// Uints8 adds the field key with i as a []uint8 to the logger context.
func (c *ZxZerolog) Uints8(key string, i []uint8) facade.LogEvent {
	c._event.Uints8(key, i)
	return c
}

// Uint16 adds the field key with i as a uint16 to the logger context.
func (c *ZxZerolog) Uint16(key string, i uint16) facade.LogEvent {
	c._event.Uint16(key, i)
	return c
}

// Uints16 adds the field key with i as a []uint16 to the logger context.
func (c *ZxZerolog) Uints16(key string, i []uint16) facade.LogEvent {
	c._event.Uints16(key, i)
	return c
}

// Uint32 adds the field key with i as a uint32 to the logger context.
func (c *ZxZerolog) Uint32(key string, i uint32) facade.LogEvent {
	c._event.Uint32(key, i)
	return c
}

// Uints32 adds the field key with i as a []uint32 to the logger context.
func (c *ZxZerolog) Uints32(key string, i []uint32) facade.LogEvent {
	c._event.Uints32(key, i)
	return c
}

// Uint64 adds the field key with i as a uint64 to the logger context.
func (c *ZxZerolog) Uint64(key string, i uint64) facade.LogEvent {
	c._event.Uint64(key, i)
	return c
}

// Uints64 adds the field key with i as a []uint64 to the logger context.
func (c *ZxZerolog) Uints64(key string, i []uint64) facade.LogEvent {
	c._event.Uints64(key, i)
	return c
}

// Float32 adds the field key with f as a float32 to the logger context.
func (c *ZxZerolog) Float32(key string, f float32) facade.LogEvent {
	c._event.Float32(key, f)
	return c
}

// Floats32 adds the field key with f as a []float32 to the logger context.
func (c *ZxZerolog) Floats32(key string, f []float32) facade.LogEvent {
	c._event.Floats32(key, f)
	return c
}

// Float64 adds the field key with f as a float64 to the logger context.
func (c *ZxZerolog) Float64(key string, f float64) facade.LogEvent {
	c._event.Float64(key, f)
	return c
}

// Floats64 adds the field key with f as a []float64 to the logger context.
func (c *ZxZerolog) Floats64(key string, f []float64) facade.LogEvent {
	c._event.Floats64(key, f)
	return c
}

// Timestamp adds the current local time as UNIX timestamp to the logger context with the "time" key.
// To customize the key name, change zerolog.TimestampFieldName.
//
// NOTE: It won't dedupe the "time" key if the *Context has one already.
func (c *ZxZerolog) Timestamp() facade.LogEvent {
	c._event.Timestamp()
	return c
}

// Time adds the field key with t formated as string using zerolog.TimeFieldFormat.
func (c *ZxZerolog) Time(key string, t time.Time) facade.LogEvent {
	c._event.Time(key, t)
	return c
}

// Times adds the field key with t formated as string using zerolog.TimeFieldFormat.
func (c *ZxZerolog) Times(key string, t []time.Time) facade.LogEvent {
	c._event.Times(key, t)
	return c
}

// Dur adds the fields key with d divided by unit and stored as a float.
func (c *ZxZerolog) Dur(key string, d time.Duration) facade.LogEvent {
	c._event.Dur(key, d)
	return c
}

// Durs adds the fields key with d divided by unit and stored as a float.
func (c *ZxZerolog) Durs(key string, d []time.Duration) facade.LogEvent {
	c._event.Durs(key, d)
	return c
}

// Interface adds the field key with obj marshaled using reflection.
func (c *ZxZerolog) Interface(key string, i interface{}) facade.LogEvent {
	c._event.Interface(key, i)
	return c
}

func (c *ZxZerolog) _getTimestampMillis() int64 {
	now := time.Now()
	nanos := now.UnixNano()
	millis := nanos / 1000000
	return millis
}

func (c *ZxZerolog) Msg(msg string) {
	if c._moduleInfo != nil {
		c.Str("module_title", c._moduleInfo.ModuleTitle)
		c.Str("module_version", c._moduleInfo.ModuleVersion)
		c.Str("module_owner", c._moduleInfo.ModuleOwner)
	}
	//c.Int64("time_millis", c._getTimestampMillis())
	c._event.Msg(msg)
	if c._outputType == facade.L_KAFKA {
		go c.publish()
	}
}

func (c *ZxZerolog) Msgf(format string, v ...interface{}) {
	if c._moduleInfo != nil {
		c.Str("module_title", c._moduleInfo.ModuleTitle)
		c.Str("module_version", c._moduleInfo.ModuleVersion)
		c.Str("module_owner", c._moduleInfo.ModuleOwner)
	}
	//c.Int64("time_millis", c._getTimestampMillis())
	c._event.Msgf(format, v...)
	if c._outputType == facade.L_KAFKA {
		go c.publish()
	}
}

func (c *ZxZerolog) MsgError(err error) {
	if err != nil {
		c.Msgf("Error : %s", err.Error())
	}
}

func (c *ZxZerolog) OnDemandTrace(format string, v ...interface{}) {
	if c._x_trace {
		c._event = c._logger.Trace()
		if c._moduleInfo != nil {
			c.Str("module_title", c._moduleInfo.ModuleTitle)
			c.Str("module_version", c._moduleInfo.ModuleVersion)
			c.Str("module_owner", c._moduleInfo.ModuleOwner)
		}
		//c.Int64("time_millis", c._getTimestampMillis())
		c._event.Msgf(format, v...)
	}
}

func (c *ZxZerolog) publish() {
	c._kafka.Publish(c._topic, c._buffer.Bytes())
	c._buffer.Reset()
}
