package core

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"
)

type ZxConfig struct {
	_appPath          string
	_appConfigPath    string
	_pluginConfigPath string
	_pluginConfig     facade.JsonHandler
	_logger           facade.LogEvent
}

func NewZxConfig(logger facade.LogEvent, path string, module string) pdk.Config {
	config := ZxConfig{_logger: logger}
	config.initPluginConfig(path+"/"+module, "config.json")
	return &config
}

func (c *ZxConfig) initPluginConfig(path string, filename string) {
	var err error
	if StringUtils.IsEmptyString(path) && StringUtils.IsEmptyString(filename) {
		c._logger.OnDemandTrace("Plugin path and filename are empty.")
		return
	}
	c._logger.OnDemandTrace("%s, %s", path, filename)
	c._pluginConfigPath = filepath.Join(path, filename)
	if c._isFileExist(c._pluginConfigPath) {
		c._pluginConfig, err = c._parse(c._pluginConfigPath)
		if err != nil {
			c._logger.OnDemandTrace("Can't find plugin configuration.")
			c._pluginConfig = nil
		} else {
			c._logger.OnDemandTrace("Load plugin configuration successfully.")
		}
	} else {
		c._logger.OnDemandTrace("Can't find plugin configuration.")
	}
}

func (c *ZxConfig) _isFileExist(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func (c *ZxConfig) _parse(filename string) (facade.JsonHandler, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return nil, err
	}
	return NewZxJsonImpl(b), nil
}

func (c *ZxConfig) AppPath() string {
	return c._appPath
}

func (c *ZxConfig) AppConfigPath() string {
	return c._appConfigPath
}

func (c *ZxConfig) Config() facade.JsonHandler {
	return c._pluginConfig
}
