package core

import (
	"techberry-go/common/v2/core/odoo"
	"techberry-go/common/v2/facade"
)

type ZxOdooXmlRpcClientImpl struct {
	_client *odoo.Client
}

func NewZxOdooClientImpl(client *odoo.Client) facade.OdooXmlRpcClient {
	var p facade.OdooXmlRpcClient = &ZxOdooXmlRpcClientImpl{_client: client}
	return p
}

// Create a new model.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#create-records
func (c *ZxOdooXmlRpcClientImpl) Create(model string, values interface{}) (int64, error) {
	return c._client.Create(model, values)
}

// Update existing model row(s).
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#update-records
func (c *ZxOdooXmlRpcClientImpl) Update(model string, ids []int64, values interface{}) error {
	return c._client.Update(model, ids, values)
}

// Delete existing model row(s).
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#delete-records
func (c *ZxOdooXmlRpcClientImpl) Delete(model string, ids []int64) error {
	return c._client.Delete(model, ids)
}

// SearchRead search model records matching with *Criteria and read it.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#search-and-read
func (c *ZxOdooXmlRpcClientImpl) SearchRead(model string, criteria *facade.Criteria, options *facade.Options, elem interface{}) error {
	return c._client.SearchRead(model, criteria, options, elem)
}

// Read model records matching with ids.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#read-records
func (c *ZxOdooXmlRpcClientImpl) Read(model string, ids []int64, options *facade.Options, elem interface{}) error {
	return c._client.Read(model, ids, options, elem)
}

// Count model records matching with *Criteria.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#count-records
func (c *ZxOdooXmlRpcClientImpl) Count(model string, criteria *facade.Criteria, options *facade.Options) (int64, error) {
	return c._client.Count(model, criteria, options)
}

// Search model record ids matching with *Criteria.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#list-records
func (c *ZxOdooXmlRpcClientImpl) Search(model string, criteria *facade.Criteria, options *facade.Options) ([]int64, error) {
	return c._client.Search(model, criteria, options)
}

// FieldsGet inspect model fields.
// https://www.odoo.com/documentation/13.0/webservices/odoo.html#listing-record-fields
func (c *ZxOdooXmlRpcClientImpl) FieldsGet(model string, options *facade.Options) (map[string]interface{}, error) {
	return c._client.FieldsGet(model, options)
}

// ExecuteKw is a RPC function. The lowest library function. It is use for all
// function related to "xmlrpc/2/object" endpoint.
func (c *ZxOdooXmlRpcClientImpl) ExecuteKw(method, model string, args []interface{}, options *facade.Options) (interface{}, error) {
	return c._client.ExecuteKw(method, model, args, options)
}
