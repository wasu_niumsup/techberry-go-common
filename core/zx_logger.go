package core

import (
	"fmt"
	"strings"
	"techberry-go/common/v2/facade"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

type FileLoggerConfig struct {
	_filename string
	_maxlines int
	_maxsize  int
	_daily    bool
	_maxdays  int
	_rotate   bool
	_level    int
}

type ConsoleLoggerConfig struct {
	_level int
}

type ElasticLoggerConfig struct {
	_host  string
	_port  int
	_level int
}

type ZxLogger struct {
	_loggerType         int
	_logger             *logs.BeeLogger
	EnableFileLogger    bool
	EnableElasticLogger bool
	_config             facade.JsonHandler
	_fileLoggerCfg      *FileLoggerConfig
	_consoleLoggerCfg   *ConsoleLoggerConfig
	_elasticLoggerCfg   *ElasticLoggerConfig
}

func NewZxLogger(config facade.JsonHandler) facade.Logger {
	zxLogger := ZxLogger{_config: config}
	zxLogger.init()
	var p facade.Logger = &zxLogger
	return p
}

func (c *ZxLogger) init() {
	status := c._config.Exists("logger.type")
	if status {
		loggerType := c._config.String("", "logger.type")
		if loggerType == "file_logger" {
			c._loggerType = facade.L_FILE
			filename := c._config.String("logger.log", "logger.filename")
			maxlines := int(c._config.Int(1000000, "logger.maxlines"))
			maxsize := int(c._config.Int(1024*1024*10, "logger.maxsize"))
			daily := c._config.Bool(true, "logger.daily")
			maxdays := int(c._config.Int(7, "logger.maxdays"))
			rotate := c._config.Bool(true, "logger.rotate")
			level := int(c._config.Int(6, "logger.level"))
			cfg := new(FileLoggerConfig)
			cfg._filename = filename
			cfg._maxlines = maxlines
			cfg._maxsize = maxsize
			cfg._daily = daily
			cfg._maxdays = maxdays
			cfg._rotate = rotate
			cfg._level = level
			c._fileLoggerCfg = cfg
			c.EnableFileLogger = true
			c.EnableElasticLogger = false
			c._initFileLogger(filename, maxlines, maxsize, daily, maxdays, rotate, level)
		} else if loggerType == "elastic_logger" {
			c._loggerType = facade.L_ELASTIC
			host := c._config.String("", "logger.host")
			port := int(c._config.Int(9200, "logger.port"))
			level := int(c._config.Int(6, "logger.level"))
			cfg := new(ElasticLoggerConfig)
			cfg._host = host
			cfg._port = port
			cfg._level = level
			c._elasticLoggerCfg = cfg
			c.EnableFileLogger = false
			c.EnableElasticLogger = true
			c._initElasticLogger(host, port, level)
		} else if loggerType == "console_logger" {
			level := int(c._config.Int(6, "logger.level"))
			c._loggerType = facade.L_CONSOLE
			cfg := new(ConsoleLoggerConfig)
			cfg._level = level
			c._consoleLoggerCfg = cfg
			c.EnableFileLogger = true
			c.EnableElasticLogger = false
			c._initConsoleLogger(level)
		}
	}
}

func (c *ZxLogger) ReloadLevel(level int) {
	if c._loggerType == facade.L_CONSOLE {
		c._initConsoleLogger(level)
	} else if c._loggerType == facade.L_ELASTIC {
		c._initElasticLogger(c._elasticLoggerCfg._host, c._elasticLoggerCfg._port, level)
	} else if c._loggerType == facade.L_FILE {
		c._initFileLogger(c._fileLoggerCfg._filename, c._fileLoggerCfg._maxlines, c._fileLoggerCfg._maxsize, c._fileLoggerCfg._daily, c._fileLoggerCfg._maxdays, c._fileLoggerCfg._rotate, level)
	}
}

func (c *ZxLogger) Info(v ...interface{}) {
	c.InfoFormat(c._generateFmtStr(len(v)), v...)
}

func (c *ZxLogger) Debug(v ...interface{}) {
	c.DebugFormat(c._generateFmtStr(len(v)), v...)
}

func (c *ZxLogger) InfoFormat(format string, v ...interface{}) {
	if c._logger != nil {
		c._logger.Info(format, v...)
	} else {
		s := fmt.Sprintf(format, v...)
		beego.Info(s)
	}
}

func (c *ZxLogger) DebugFormat(format string, v ...interface{}) {
	if c._logger != nil {
		c._logger.Debug(format, v...)
	} else {
		s := fmt.Sprintf(format, v...)
		beego.Debug(s)
	}
}

func (c *ZxLogger) _generateFmtStr(n int) string {
	return strings.Repeat("%v ", n)
}

// Initialize File Logger Adapter
func (c *ZxLogger) _initFileLogger(filename string, maxlines int, maxsize int, daily bool, maxdays int, rotate bool, level int) {
	config := fmt.Sprintf(`{"filename":"%s","maxlines":%d,"maxsize":%d,"daily":%t,"maxdays":%d,"rotate":%t,"level":%d}`, filename, maxlines, maxsize, daily, maxdays, rotate, level)
	c._logger = logs.NewLogger()
	err := c._logger.SetLogger(logs.AdapterFile, config)
	if err != nil {
		beego.Info("Initialize file logger failed.")
	} else {
		beego.Info("Initialize file logger successfully.")
	}
	c._logger.EnableFuncCallDepth(true)
	c._logger.SetLogFuncCallDepth(4)
	c._logger.Async()
}

// Initialize Console Logger Adapter
func (c *ZxLogger) _initConsoleLogger(level int) {
	config := fmt.Sprintf(`{"level":%d}`, level)
	c._logger = logs.NewLogger()
	err := c._logger.SetLogger(logs.AdapterConsole, config)
	if err != nil {
		beego.Info("Initialize console logger failed.")
	} else {
		beego.Info("Initialize console logger successfully.")
	}
	c._logger.EnableFuncCallDepth(true)
	c._logger.SetLogFuncCallDepth(4)
	c._logger.Async()
}

// Initialize Elastic Logger Adapter
func (c *ZxLogger) _initElasticLogger(host string, port int, level int) {
	config := fmt.Sprintf("{\"dsn\":\"http://%s:%d/\",\"level\":%d}", host, port, level)
	c._logger = logs.NewLogger()
	err := c._logger.SetLogger(logs.AdapterEs, config)
	if err != nil {
		beego.Info("Initialize elastic logger failed.")
	} else {
		beego.Info("Initialize elastic logger successfully.")
	}
	c._logger.EnableFuncCallDepth(true)
	c._logger.SetLogFuncCallDepth(4)
	c._logger.Async()
}
