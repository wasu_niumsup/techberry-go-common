package core

import (
	"encoding/json"
	"fmt"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"
	"techberry-go/common/v2/utils"

	"github.com/fatih/structs"
)

type ZxPdkContext struct {
	_logger facade.LogEvent
}

func NewZxPdkContext(logger facade.LogEvent) pdk.Context {
	return &ZxPdkContext{_logger: logger}
}

//  RPC connector.
func (c *ZxPdkContext) GetRpcConnection(host string, port int, timeout int) facade.RpcConnector {
	return NewRpcConnector(host, port, timeout)
}

func (c *ZxPdkContext) GetResponseMap(isSuccess bool, message string, record_count int, data interface{}, errorMessage string) map[string]interface{} {
	code := ""
	if isSuccess {
		code = "0000I"
	} else {
		code = "0001E"
	}
	responseStruct := c.bindJsonResponse(isSuccess, code, message, record_count, data, errorMessage)
	jsonMap := structs.Map(responseStruct)
	return jsonMap
}

func (c *ZxPdkContext) ReplyMessage(err error, module string, functionName string, message string, data interface{}, total int) map[string]interface{} {
	count := 0
	if data != nil {
		if d, ok := data.([]interface{}); ok {
			count = len(d)
		} else {
			count = 1
		}
	}
	if total > 0 {
		count = total
	}
	if err == nil {
		return c.GetResponseMap(true, fmt.Sprintf("[%s] MSC.%s : %s", module, functionName, message), count, data, "")
	} else {
		return c.GetResponseMap(false, "", 0, nil, fmt.Sprintf("[%s] MSC.%s : %s", module, functionName, message))
	}
}

func (c *ZxPdkContext) Reply(err error, module string, functionName string, data interface{}, total int) map[string]interface{} {
	count := 0
	if data != nil {
		if d, ok := data.([]interface{}); ok {
			count = len(d)
		} else {
			count = 1
		}
	}
	if total > 0 {
		count = total
	}
	if err == nil {
		c._logger.OnDemandTrace("[%s] MSC.%s : Execute successfully.", module, functionName)
		return c.GetResponseMap(true, fmt.Sprintf("[%s] MSC.%s : Execute successfully.", module, functionName), count, data, "")
	} else {
		c._logger.OnDemandTrace("[%s] MSC.%s : Execute  failed.", module, functionName)
		return c.GetResponseMap(false, "", 0, nil, fmt.Sprintf("[%s] MSC.%s : Execute failed.", module, functionName))
	}
}

func (c *ZxPdkContext) BindingModel(fromMap map[string]interface{}, toModel interface{}) error {
	b, _ := json.Marshal(fromMap)
	return json.Unmarshal(b, &toModel)
	//return MapUtils.ToStruct(dataMap, &dataModel)
}

func (c *ZxPdkContext) MapStruct2Model(fromMap map[string]interface{}, toModel interface{}) error {
	return MapUtils.ToStruct(fromMap, &toModel)
}

func (c *ZxPdkContext) bindJsonResponse(success bool, code string, message string, record_count int, data interface{}, errorMessage string) facade.JsonResponse {
	response := facade.JsonResponse{}
	response.Success = success
	response.Code = code
	response.Data = nil
	response.Message = message
	if record_count > 0 {
		response.RecordCount = record_count
	} else {
		response.RecordCount = 0
	}
	if data != nil {
		response.Data = data
	} else {
		switch data.(type) {
		case []interface{}:
			response.Data = utils.Container{}
		default:
			response.Data = c._getBlankJson()
		}
	}
	response.Error = errorMessage
	return response
}

func (c *ZxPdkContext) _getBlankJson() interface{} {
	b := []byte(`{}`)
	var data interface{}
	json.Unmarshal(b, &data)
	return data
}
