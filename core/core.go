package core

import (
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
)

var (
	StringUtils facade.StringHandler = impl.NewStringImpl()
	MapUtils    facade.MapHandler    = impl.NewMapImpl()
	StructUtils facade.StructHandler = impl.NewStructImpl()
	TimeUtils   facade.TimeHandler   = impl.NewTimeImpl()
)
