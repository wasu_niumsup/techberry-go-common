package core

import (
	"encoding/json"
	"errors"
	"net"
	"net/rpc"
	"reflect"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"

	"github.com/ugorji/go/codec"
)

type ZxPdkConnector struct {
	_cachePool              map[int]facade.CacheHandler
	_elasticConnectors      map[string]interface{}
	_dfHost                 string
	_dfPort                 int
	_elasticIsSecure        bool
	_elasticHost            string
	_elasticPort            int
	_elasticApiKey          string
	_elasticBasicAuthUser   string
	_elasticBasicAuthPasswd string
	_redisHost              string
	_redisPort              int
	_redisDB                int
	_redisPoolSize          int
	_logger                 facade.LogEvent
}

func NewZxPdkConnector(logger facade.LogEvent) pdk.Connector {
	return &ZxPdkConnector{
		_cachePool:         make(map[int]facade.CacheHandler),
		_elasticConnectors: make(map[string]interface{}),
		_logger:            logger,
	}
}

func (c *ZxPdkConnector) SetDataFactoryHost(host string, port int) {
	c._dfHost = host
	c._dfPort = port
}

func (c *ZxPdkConnector) RpcConnection(host string, port int, timeout int) facade.RpcConnector {
	return NewRpcConnector(host, port, timeout)
}

func (c *ZxPdkConnector) DataFactory() facade.DataFactory {
	return NewZxDataFactoryImpl(c._logger, c._dfHost, c._dfPort)
}

func (c *ZxPdkConnector) Elastic(name string) facade.ElasticHandler {
	if StringUtils.IsEmptyString(name) {
		name = "default"
	}
	if val, ok := c._elasticConnectors[name]; ok {
		return val.(facade.ElasticHandler)
	} else {
		return nil
	}
}

func (c *ZxPdkConnector) InitElasticConnector(name string, isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) pdk.Connector {
	if StringUtils.IsNotEmptyString(name) {
		c._logger.Debug().Msgf("Initialize elastic to %s:%d", host, port)
		c._elasticIsSecure = isSecure
		c._elasticHost = host
		c._elasticPort = port
		c._elasticApiKey = api_key
		c._elasticBasicAuthUser = basicAuthUser
		c._elasticBasicAuthPasswd = basicAuthPasswd
		c._elasticConnectors[name] = NewZxElasticImpl(c._logger, isSecure, c._elasticHost, c._elasticPort, c._elasticApiKey, c._elasticBasicAuthUser, c._elasticBasicAuthPasswd)
	}
	return c
}

func (c *ZxPdkConnector) Redis(db int) facade.CacheHandler {
	// Return default redis db.
	if db == -1 {
		return c._cachePool[c._redisDB]
	}
	var cache facade.CacheHandler
	if cache, ok := c._cachePool[db]; ok {
		if cache != nil {
			return cache
		}
	}
	cache = NewZxCacheImpl(c._redisHost, c._redisPort, db, c._redisPoolSize)
	c._cachePool[db] = cache
	return cache
}

func (c *ZxPdkConnector) GetRedisConnection(host string, port int, db int, poolSize int) facade.CacheHandler {
	//c._cache = impl.NewCacheImpl(c._mc, host, port, db)
	c._redisHost = host
	c._redisPort = port
	c._redisDB = db
	c._redisPoolSize = poolSize
	return NewZxCacheImpl(host, port, db, poolSize)
}

func (c *ZxPdkConnector) InitRedisConnector(host string, port int, db int, poolSize int) pdk.Connector {
	//c._cache = impl.NewCacheImpl(c._mc, host, port, db)
	c._logger.Debug().Msgf("Initialize redis to %s:%d, dbnum %d, poolSize %d", host, port, db, poolSize)
	c._redisHost = host
	c._redisPort = port
	c._redisDB = db
	c._redisPoolSize = poolSize
	c._cachePool[db] = NewZxCacheImpl(host, port, db, poolSize)
	return c
}

func (c *ZxPdkConnector) CallMicroserver(socketAddr string, pluginName string, service string, version string, data []byte) ([]byte, error) {
	var handle codec.MsgpackHandle
	handle.ReaderBufferSize = 4096
	handle.WriterBufferSize = 4096
	handle.RawToString = true
	handle.MapType = reflect.TypeOf(map[string]interface{}(nil))

	conn, err := net.Dial("unix", socketAddr)
	if err != nil {
		var s []byte
		return s, errors.New("Can't connect to micro server or Micro server is not start.")
	}
	defer conn.Close()

	handshakeMsg := new(pdk.Notification)
	dec := codec.NewDecoder(conn, &handle)
	dec.Decode(&handshakeMsg)

	rpcCodec := codec.MsgpackSpecRpc.ClientCodec(conn, &handle)
	client := rpc.NewClientWithCodec(rpcCodec)

	output := new(pdk.EventOutput)
	input := pdk.EventInput{}
	input.PluginName = pluginName
	input.ServiceName = service
	input.Version = version
	m := make(map[string]interface{})
	err = json.Unmarshal(data, &m)
	if err != nil {
		c._logger.OnDemandTrace("Error in unmarshall.")
		res := []byte("Error in unmarshall.")
		return res, nil
	} else {
		input.Input = m
		output.State = ""
		output.Data = []byte(``)
		client.Call("plugin.ProcessEvent", input, output)
		return output.Data, nil
	}
}
