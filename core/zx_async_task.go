package core

import (
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"
)

type ZxAsyncTask struct {
	_cache facade.CacheHandler
}

func NewZxAsyncTask(cache facade.CacheHandler) facade.AsyncTask {
	task := ZxAsyncTask{}
	task._cache = cache
	var p facade.AsyncTask = &task
	return p
}

func (c *ZxAsyncTask) Start(name string, category string, uuid string) {
	as := facade.EventState{
		Status: "pending",
	}
	structHandler := impl.NewStructImpl()
	b, err := structHandler.ToJson(as)
	if err == nil {
		c._cache.Set(uuid, string(b), time.Duration(48)*time.Hour)
	}
}

func (c *ZxAsyncTask) Fail(uuid string) {
	structHandler := impl.NewStructImpl()
	eventState := &facade.EventState{}
	s, err := c._cache.Get(uuid, false)
	if err == nil {
		structHandler.FromJson([]byte(s), eventState)
		eventState.Status = "fail"
	}
	b, err := structHandler.ToJson(eventState)
	if err == nil {
		c._cache.Set(uuid, string(b), time.Duration(48)*time.Hour)
	}
}

func (c *ZxAsyncTask) Success(uuid string, data interface{}) {
	structHandler := impl.NewStructImpl()
	eventState := &facade.EventState{}
	s, err := c._cache.Get(uuid, false)
	if err == nil {
		structHandler.FromJson([]byte(s), eventState)
		eventState.Status = "success"
		eventState.Data = data
	}
	b, err := structHandler.ToJson(eventState)
	if err == nil {
		c._cache.Set(uuid, string(b), time.Duration(12)*time.Hour)
	}
}

func (c *ZxAsyncTask) GetState(uuid string) (string, error) {
	return c._cache.Get(uuid, false)
}
