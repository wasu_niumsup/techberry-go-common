package core

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"techberry-go/common/v2/facade"
)

type ZxAppConfig struct {
	_appPath string
	// appConfigPath is the path to the config files
	_appConfigPath    string
	_pluginConfigPath string
	_baseConfig       facade.JsonHandler
	_moduleConfig     facade.JsonHandler
	_pluginConfig     facade.JsonHandler
	_logger           facade.LogEvent
}

func NewZxAppConfig(logger facade.LogEvent) facade.AppConfig {
	appConfig := ZxAppConfig{_logger: logger}
	if !appConfig.initModuleConfig() {
		appConfig.initBaseConfig()
	}
	return &appConfig
}

func (c *ZxAppConfig) initBaseConfig() bool {
	var err error
	if c._appPath, err = filepath.Abs(filepath.Dir(os.Args[0])); err != nil {
		c._logger.Debug().Msgf("Error : %v", err)
		return false
	}
	var filename = "apis.json"
	_properties := NewZxProperties()
	runmode := _properties.DefaultString("runmode", "prod")
	if runmode != "" {
		filename = runmode + ".apis.json"
	}
	c._appConfigPath = filepath.Join(c._appPath, "conf", filename)
	c._logger.Debug().Msgf("System running on mode : %s", runmode)
	if c._isFileExist(c._appConfigPath) {
		c._appConfigPath = filepath.Join(c._appPath, "conf", filename)
		if c._isFileExist(c._appConfigPath) {
			c._baseConfig, err = c._parse(c._appConfigPath)
			if err != nil {
				c._logger.OnDemandTrace("Can't find base configuration.")
				c._baseConfig = nil
			} else {
				c._logger.OnDemandTrace("Load base configuration successfully.")
				return true
			}
		}
	} else {
		c._logger.OnDemandTrace("Can't find base configuration.")
	}
	return false
}

func (c *ZxAppConfig) initModuleConfig() bool {
	var err error
	if c._appPath, err = filepath.Abs(filepath.Dir(os.Args[0])); err != nil {
		c._logger.Debug().MsgError(err)
		return false
	}
	var filename = "module.json"
	_properties := NewZxProperties()
	runmode := _properties.DefaultString("runmode", "prod")
	if runmode != "" {
		filename = runmode + ".module.json"
	}
	c._appConfigPath = filepath.Join(c._appPath, "conf", filename)
	if c._isFileExist(c._appConfigPath) {
		c._appConfigPath = filepath.Join(c._appPath, "conf", filename)
		if c._isFileExist(c._appConfigPath) {
			c._moduleConfig, err = c._parse(c._appConfigPath)
			if err != nil {
				c._logger.OnDemandTrace("Can't find module configuration.")
				c._moduleConfig = nil
			} else {
				c._logger.OnDemandTrace("Load module configuration successfully.")
				return true
			}
		}
	} else {
		c._logger.OnDemandTrace("Can't find module configuration.")
	}
	return false
}

func (c *ZxAppConfig) InitPluginConfig(path string, filename string) {
	var err error
	if StringUtils.IsEmptyString(path) && StringUtils.IsEmptyString(filename) {
		c._logger.OnDemandTrace("Plugin path and filename are empty.")
		return
	}
	c._pluginConfigPath = filepath.Join(path, "conf", filename)
	if c._isFileExist(c._pluginConfigPath) {
		c._pluginConfig, err = c._parse(c._pluginConfigPath)
		if err != nil {
			c._logger.OnDemandTrace("Can't find plugin configuration.")
			c._pluginConfig = nil
		} else {
			c._logger.OnDemandTrace("Load plugin configuration successfully.")
		}
	} else {
		c._logger.OnDemandTrace("Can't find plugin configuration.")
	}
}

func (c *ZxAppConfig) _isFileExist(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func (c *ZxAppConfig) _parse(filename string) (facade.JsonHandler, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return nil, err
	}
	return NewZxJsonImpl(b), nil
}

func (c *ZxAppConfig) AppPath() string {
	return c._appPath
}

func (c *ZxAppConfig) AppConfigPath() string {
	return c._appConfigPath
}

func (c *ZxAppConfig) Config() facade.JsonHandler {
	if c._baseConfig != nil {
		if c._moduleConfig != nil {
			return c._moduleConfig
		} else {
			return c._baseConfig
		}
	} else {
		if c._moduleConfig != nil {
			return c._moduleConfig
		}
	}
	return nil
}
