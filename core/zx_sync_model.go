package core

import (
	"errors"
	"io"
	"strconv"
	"techberry-go/common/v2/core/odoo"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"
)

const (
	ClearTarget = iota
	FetchSource
	BindDataModel
	DeleteTarget
	UpsertTarget
	UpdateJournalSource
	UpdateJournalTarget
	HookBeforeUpsert
	HookAfterUpsert
)

const (
	SOURCE_CSV = iota
	SOURCE_DATA_FACTORY
)

type Sequence struct {
	_task               string
	_processTargetIndex int
}

// Sync from Database -> Mongo, Elastic
// Mode = DB_ALL_DOC, DB_NOSQL, DB_ELASTIC, NOSQL_ELASTIC, ELASTIC_NOSQL, CSV_DB
type ZxSyncModel struct {
	_inputData interface{}

	_targetConnectors    []Sequence
	_cleanConnectors     []interface{}
	_syncConfig          *facade.SyncConfig
	_fetchSource         func(facade.Adapter, *facade.SyncConfig, interface{}) (int, []interface{})
	_updateJournalSource []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error
	_bindDataModel       func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}) interface{}
	_clearTarget         []func(facade.Adapter, *facade.SyncConfig, interface{}) error
	_updateJournalTarget []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error
	_hookBeforeTask      []string
	_hookBeforeTarget    []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)
	_hookAfterTask       []string
	_hookAfterTarget     []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)
	_deleteTarget        []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error
	_processTarget       []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)
	//_publishTarget       []func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error
	_adapterSource      facade.Adapter
	_adapterTarget      facade.Adapter
	_adaperPlugin       facade.Adapter
	_debug              bool
	_mc                 facade.MediationContext
	_logger             facade.LogEvent
	_sourceConnectorCtx *facade.ConnectorContext
	_targetConnectorCtx map[string]*facade.ConnectorContext
	_pluginConnectorCtx map[string]*facade.ConnectorContext
	_isElasticAsTarget  bool
}

//var csvModelList []facade.CSVModel

func NewZxSyncModel(logger facade.LogEvent) facade.SyncHandler {
	sync := ZxSyncModel{}
	var p facade.SyncHandler = &sync
	sync._adapterSource = NewZxAdapter(logger)
	sync._adapterTarget = NewZxAdapter(logger)
	config := &facade.SyncConfig{}
	if err := StructUtils.InitDefaults(config); err != nil {
		logger.MsgError(err)
	}
	sync._isElasticAsTarget = false
	sync._syncConfig = config
	sync._logger = logger
	sync._targetConnectorCtx = make(map[string]*facade.ConnectorContext)
	sync._pluginConnectorCtx = make(map[string]*facade.ConnectorContext)
	return p
}

func (c *ZxSyncModel) Debug(debug bool) {
	c._debug = debug
}

func (c *ZxSyncModel) SetConnectors(source facade.ConnectorContext, target []facade.ConnectorContext, plugin []facade.ConnectorContext) {
	switch source.Type {
	case "data-factory":
		c._logger.OnDemandTrace("Input source from Data-Factory.")
		pdk := NewZxPdkConnector(c._logger)
		pdk.SetDataFactoryHost(source.Host, source.Port)
		df := pdk.DataFactory()
		c._cleanConnectors = append(c._cleanConnectors, df)
		c.Source(df)
		c._sourceConnectorCtx = &source
	case "orm":
		c._logger.OnDemandTrace("Input source from ORM.")
		connector := NewZxOrmConnector()
		db := connector.GetOrmConnector(source.DriverName, source.Vendor, source.ConnString, source.MaxIdleConn, source.MaxOpenConn, source.Timeout)
		orm := NewZxOrmImpl(c._logger, db)
		c._cleanConnectors = append(c._cleanConnectors, orm)
		c.Source(orm)
		c._sourceConnectorCtx = &source
	case "elastic":
		c._logger.OnDemandTrace("Input source from Elastic.")
		c.Source(NewZxElasticImpl(c._logger, source.IsSecure, source.Host, source.Port, source.ApiKey, source.BasicAuthUser, source.BasicAuthPassword))
		c._sourceConnectorCtx = &source
	case "mongodb":
		c._logger.OnDemandTrace("Input source from Mongodb.")
		c.Source(NewZxNoSqlImpl(c._logger, source.ConnectionUrl, source.Database))
		c._sourceConnectorCtx = &source
	case "csv":
		c._logger.OnDemandTrace("Input source from CSV.")
		c.Source(NewZxCsvImpl(c._logger, source.CsvInputFile, source.IsFirstRowHeader))
		c._sourceConnectorCtx = &source
	}
	for _, v := range target {
		switch v.Type {
		case "data-factory":
			c._logger.OnDemandTrace("Output target to Data-Factory.")
			pdk := NewZxPdkConnector(c._logger)
			df := pdk.DataFactory()
			c._cleanConnectors = append(c._cleanConnectors, df)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(df)
			c._targetConnectorCtx[v.Id] = &v
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("Data-Factory adapter is skipped.")
			}
		case "orm":
			connector := NewZxOrmConnector()
			db := connector.GetOrmConnector(v.DriverName, v.Vendor, v.ConnString, v.MaxIdleConn, v.MaxOpenConn, v.Timeout)
			orm := NewZxOrmImpl(c._logger, db)
			c._cleanConnectors = append(c._cleanConnectors, orm)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(orm)
			c._targetConnectorCtx[v.Id] = &v
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("ORM connector is skipped.")
			}
		case "elastic":
			elastic := NewZxElasticImpl(c._logger, v.IsSecure, v.Host, v.Port, v.ApiKey, v.BasicAuthUser, v.BasicAuthPassword)
			c._cleanConnectors = append(c._cleanConnectors, elastic)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(elastic)
			c._targetConnectorCtx[v.Id] = &v
			c._isElasticAsTarget = true
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("Elastic connector is skipped.")
			}
		case "mongodb":
			mongodb := NewZxNoSqlImpl(c._logger, v.ConnectionUrl, v.Database)
			c._cleanConnectors = append(c._cleanConnectors, mongodb)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(mongodb)
			c._targetConnectorCtx[v.Id] = &v
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("Mongodb connector is skipped.")
			}
		case "kafka":
			kafka := NewZxKafkaImpl(c._logger, v.BootstrapServers)
			c._cleanConnectors = append(c._cleanConnectors, kafka)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(kafka)
			c._targetConnectorCtx[v.Id] = &v
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("Kafka connector is skipped.")
			}
		case "odoo":
			conn, err := odoo.NewClient(&odoo.ClientConfig{
				Admin:    v.Username,
				Password: v.Password,
				Database: v.Database,
				URL:      v.Url,
			})
			if err != nil {
				c._logger.MsgError(err)
				return
			}
			xmlrpcClient := NewZxOdooClientImpl(conn)
			c._cleanConnectors = append(c._cleanConnectors, xmlrpcClient)
			seq := Sequence{
				_task: v.Id,
			}
			c.Target(xmlrpcClient)
			c._targetConnectorCtx[v.Id] = &v
			if !v.Skip {
				c._targetConnectors = append(c._targetConnectors, seq)
			} else {
				c._logger.OnDemandTrace("Odoo connector is skipped.")
			}
		}
	}
	for _, v := range plugin {
		switch v.Type {
		case "orm":
			c._logger.OnDemandTrace("Enable orm plugin adapter : %s", v.Name)
			connector := NewZxOrmConnector()
			db := connector.GetOrmConnector(v.DriverName, v.Vendor, v.ConnString, v.MaxIdleConn, v.MaxOpenConn, v.Timeout)
			orm := NewZxOrmImpl(c._logger, db)
			c._cleanConnectors = append(c._cleanConnectors, orm)
			c.Plugin(v.Name, orm)
			c._pluginConnectorCtx[v.Id] = &v
		case "elastic":
			c._logger.OnDemandTrace("Enable elastic plugin adapter : %s", v.Name)
			elastic := NewZxElasticImpl(c._logger, v.IsSecure, v.Host, v.Port, v.ApiKey, v.BasicAuthUser, v.BasicAuthPassword)
			c._cleanConnectors = append(c._cleanConnectors, elastic)
			c.Plugin(v.Name, elastic)
			c._pluginConnectorCtx[v.Id] = &v
		}
	}
	// Link plugin to source and target
	c._adapterSource.SetPlugin(c._adapterTarget.Plugin())
}

func (c *ZxSyncModel) _clean() {
	for _, v := range c._cleanConnectors {
		switch t := v.(type) {
		case facade.OrmHandler:
			t.Close()
			c._logger.OnDemandTrace("%s  : persistent resource is close.", t.GetDriverName())
		case facade.CRUDDocumentStore:
			t.Release()
			c._logger.OnDemandTrace("Nosql : document store resource is close.")
		case facade.ElasticHandler:
			t = nil
			c._logger.OnDemandTrace("Elastic : remove pointer to object.")
		case facade.DataFactory:
			t = nil
			c._logger.OnDemandTrace("Data-Factory : remove pointer to object.")

		}
	}
}

func (c *ZxSyncModel) SetInputData(input interface{}) {
	c._inputData = input
}

func (c *ZxSyncModel) SetConfig(config *facade.SyncConfig) {
	if config != nil {
		// Default vfs path.
		if StringUtils.IsEmptyString(config.VfsPath) {
			config.VfsPath = c._syncConfig.VfsPath
		}
		if config.Debug {
			c.Debug(true)
		}
		c._syncConfig = config
	}
}

func (c *ZxSyncModel) GetConfig() *facade.SyncConfig {
	return c._syncConfig
}

func (c *ZxSyncModel) Source(v interface{}) facade.SyncHandler {
	switch t := v.(type) {
	case facade.DataFactory:
		c._adapterSource.SetDataFactory(t)
	case facade.OrmHandler:
		c._adapterSource.SetOrm(t)
	case facade.CRUDDocumentStore:
		c._adapterSource.SetNoSql(t)
	case facade.ElasticHandler:
		c._adapterSource.SetElastic(t)
	case facade.CsvHandler:
		c._adapterSource.SetCsv(t)
	case facade.RpcConnector:
		c._adapterSource.SetRpc(t)
	case facade.OdooXmlRpcClient:
		c._adapterSource.SetOdoo(t)
	}
	return c
}

func (c *ZxSyncModel) Target(v interface{}) facade.SyncHandler {
	switch t := v.(type) {
	case facade.DataFactory:
		c._adapterTarget.SetDataFactory(t)
	case facade.OrmHandler:
		c._adapterTarget.SetOrm(t)
	case facade.CRUDDocumentStore:
		c._adapterTarget.SetNoSql(t)
	case facade.ElasticHandler:
		c._adapterTarget.SetElastic(t)
	case facade.Kafka:
		c._adapterTarget.SetKafka(t)
	case facade.CsvHandler:
		c._adapterTarget.SetCsv(t)
	case facade.RpcConnector:
		c._adapterTarget.SetRpc(t)
	case facade.OdooXmlRpcClient:
		c._adapterTarget.SetOdoo(t)
	}
	return c
}

func (c *ZxSyncModel) Plugin(alias string, v interface{}) facade.SyncHandler {
	switch t := v.(type) {
	case facade.DataFactory:
		c._adapterTarget.Plugin().SetDataFactory(alias, t)
	case facade.OrmHandler:
		c._adapterTarget.Plugin().SetOrm(alias, t)
	case facade.ElasticHandler:
		c._adapterTarget.Plugin().SetElastic(alias, t)
	}
	return c
}

func (c *ZxSyncModel) ClearTarget(m func(facade.Adapter, *facade.SyncConfig, interface{}) error) facade.SyncHandler {
	c._clearTarget = append(c._clearTarget, m)
	return c
}

func (c *ZxSyncModel) FetchSource(m func(facade.Adapter, *facade.SyncConfig, interface{}) (int, []interface{})) facade.SyncHandler {
	c._fetchSource = m
	return c
}

func (c *ZxSyncModel) UpdateJournalSource(m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error) facade.SyncHandler {
	c._updateJournalSource = append(c._updateJournalSource, m)
	return c
}

func (c *ZxSyncModel) BindDataModel(m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}) interface{}) facade.SyncHandler {
	c._bindDataModel = m
	return c
}

func (c *ZxSyncModel) DeleteTarget(task string, m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error) facade.SyncHandler {
	c._deleteTarget = append(c._deleteTarget, m)
	return c
}

func (c *ZxSyncModel) HookBeforeTarget(task string, m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)) facade.SyncHandler {
	c._hookBeforeTarget = append(c._hookBeforeTarget, m)
	return c
}

func (c *ZxSyncModel) HookAfterTarget(task string, m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)) facade.SyncHandler {
	c._hookAfterTarget = append(c._hookAfterTarget, m)
	return c
}

func (c *ZxSyncModel) ProcessTarget(task string, m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) (interface{}, error)) facade.SyncHandler {
	for _, v := range c._targetConnectors {
		if v._task == task {
			v._processTargetIndex = len(c._processTarget) - 1
		}
	}
	c._processTarget = append(c._processTarget, m)
	return c
}

/*
func (c *ZxSyncModel) PublishTarget(m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error) facade.SyncHandler {
	c._publishTarget = append(c._publishTarget, m)
	return c
}
*/

func (c *ZxSyncModel) UpdateJournalTarget(m func(facade.Adapter, *facade.SyncConfig, interface{}, interface{}, interface{}) error) facade.SyncHandler {
	c._updateJournalTarget = append(c._updateJournalTarget, m)
	return c
}

// Sync Data from single source to multiple target
// 1. ClearTarget
// 2. Read CSV file or FetchSource
// 3. Disable elastic indexing.
// 4. Process Data.
func (c *ZxSyncModel) Sync() (error, int, int, int) {
	var (
		countFail       int = 0
		countSuccess    int = 0
		countSkip       int = 0
		inputSourceFlag int = 0
	)
	// ========== Step : Tracking state. ==========
	if c._syncConfig.IsTrackingState {
		c._readTrackingState()
	}

	defer c._timeTrack(time.Now(), "Sync()")

	// ========== Step : Clear target data. ==========
	err_step := c._stepClearTarget()
	if err_step != nil {
		return err_step, countSuccess, countFail, countSkip
	}

	var (
		count          int = 0
		sourceData     []interface{}
		df_source_data []map[string]interface{}
	)
	if c._adapterSource.DataFactory() != nil {
		c._logger.OnDemandTrace("Fetch data from data-factory.")
		// ========== Step : Fetch data from data-factory. ===========
		sc := c._sourceConnectorCtx
		if StringUtils.IsNotEmptyString(sc.DataSource) &&
			StringUtils.IsNotEmptyString(sc.MapperId) {
			if c._syncConfig.CdcMode {
				layout := "2006-01-02 15:04:05"
				lastTimeStr := c._syncConfig.SqlLastTimeValue.Format(layout)
				currentTimeStr := c._syncConfig.CurrentTimeValue.Format(layout)
				sc.Params["last_timestamp"] = lastTimeStr
				sc.Params["current_timestamp"] = currentTimeStr
			}
			df_result, df_err := c._adapterSource.DataFactory().ExecuteQuery(sc.DataSource, sc.MapperId, sc.Params)
			if df_err != nil {
				c._logger.Debug().MsgError(df_err)
				return df_err, countSuccess, countFail, countSkip
			} else {
				if df_result.Hits.Total == 0 {
					return errors.New("No result."), countSuccess, countFail, countSkip
				} else {
					count = int(df_result.Hits.Total)
					df_source_data = df_result.Hits.Data
					inputSourceFlag = SOURCE_DATA_FACTORY
				}
			}
			c._logger.OnDemandTrace("Fetch source count : %d", count)
		}
	} else if c._adapterSource.Csv() != nil {
		// ========== Step : Fetch csv source. ===========
		var err error
		csvHandler := c._adapterSource.Csv()
		if StringUtils.IsNotEmptyString(c._syncConfig.CsvInputFile) {
			err = csvHandler.OpenFile(c._syncConfig.CsvInputFile)
		} else {
			err = csvHandler.Open()
		}
		if err != nil {
			return err, countSuccess, countFail, countSkip
		} else {
			count = 999999 // Maximum line per file.
			inputSourceFlag = SOURCE_CSV
		}
	} else {
		c._logger.OnDemandTrace("Executing fetch source.")
		// ========== Step : Fetch source. ===========
		count, sourceData, err_step = c._stepFetchSource()
		if err_step != nil {
			return err_step, countSuccess, countFail, countSkip
		}
	}

	if count > 0 {
		var (
			bulkSize int           = c._syncConfig.BulkSize
			delay    time.Duration = time.Duration(c._syncConfig.DelayUpdateBulk) * time.Second
			err      error         = nil
			model    interface{}
			elastic  facade.ElasticHandler
		)
		// ========== Step : Disable elastic index ==========
		if c._isElasticAsTarget {
			elastic = c._adapterTarget.Elastic()
			if elastic != nil && c._syncConfig.ElasticAutoDisableIndex {
				c._logger.OnDemandTrace("Disable elastic index.")
				elastic.DisableIndex(c._syncConfig.ElasticIndex)
			}
		}

		startLoop := time.Now()
		if inputSourceFlag == SOURCE_CSV {
			err = nil
			var csvLineData interface{}
			indx := 0
			for {
				csvLineData, err = c._adapterSource.Csv().Read()
				if err != nil {
					if err == io.EOF {
						break
					}
					c._logger.MsgError(err)
					return err, countSuccess, countFail, countSkip
				}
				// Call process target. process line by line
				err = c._process(csvLineData, model, &countSuccess, &countFail, &countSkip)
				if err == nil {
					if c._isElasticAsTarget {
						if bulkSize > 0 && elastic != nil && c._syncConfig.ElasticAutoDisableIndex && (indx+1)%bulkSize == 0 {
							c._logger.OnDemandTrace("Sleep for %d seconds for backend processing.", delay)
							time.Sleep(delay)
						}
					}
				} else if err.Error() == "exception" {
					goto enable_index
				}
				indx = indx + 1
			}
		} else if inputSourceFlag == SOURCE_DATA_FACTORY {
			for indx, val := range df_source_data {
				// Call process target. process line by line
				err = c._process(val, model, &countSuccess, &countFail, &countSkip)
				if err == nil {
					if c._isElasticAsTarget {
						if bulkSize > 0 && elastic != nil && c._syncConfig.ElasticAutoDisableIndex && (indx+1)%bulkSize == 0 {
							c._logger.OnDemandTrace("Sleep for %d seconds for backend processing.", delay)
							time.Sleep(delay)
						}
					}
				} else if err.Error() == "exception" {
					goto enable_index
				}
			}
		} else {
			for indx, value := range sourceData {
				c._logger.OnDemandTrace("index = ", indx)
				dataMap := value.(map[string]interface{})
				// Call process target
				err = c._process(dataMap, model, &countSuccess, &countFail, &countSkip)
				if err == nil {
					if c._isElasticAsTarget {
						if bulkSize > 0 && elastic != nil && c._syncConfig.ElasticAutoDisableIndex && (indx+1)%bulkSize == 0 {
							c._logger.OnDemandTrace("Sleep for %d seconds for backend processing.", delay)
							time.Sleep(delay)
						}
					}
				} else {
					goto enable_index
				}
			}
		}
	enable_index:
		if err == nil {
			if c._syncConfig.IsTrackingState {
				c._saveTrackingState()
			}
		}
		if c._debug {
			elapsed := time.Since(startLoop)
			c._logger.OnDemandTrace("Execute all data from source took : %d", elapsed)
		}
		if elastic != nil && c._syncConfig.ElasticAutoDisableIndex {
			c._logger.OnDemandTrace("Enable elastic index.")
			elastic.EnableIndex(c._syncConfig.ElasticIndex)
			if err != nil {
				return err, countSuccess, countFail, countSkip
			}
		}
		if err != nil && err.Error() == "Force" {
			c._logger.OnDemandTrace("Running task is forced by other process")
		}
	}
	c._debug = true
	c._logger.OnDemandTrace("Success = %d , Fail = %d , Skip = %d", countSuccess, countFail, countSkip)
	c._debug = false
	c._clean()
	return nil, countSuccess, countFail, countSkip
}

func (c *ZxSyncModel) _stepClearTarget() error {
	if len(c._clearTarget) > 0 {
		if c._syncConfig.CdcMode {
			c._logger.OnDemandTrace("Skip to clear target because enabled cdc mode.")
			return nil
		}
	} else {
		return nil
	}
	for _, f_clearTarget := range c._clearTarget {
		start := time.Now()
		err := f_clearTarget(c._adapterTarget, c._syncConfig, c._inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute clear target took : %d", elapsed)
		}
		if err != nil {
			c._logger.MsgError(err)
			if !c._syncConfig.SkippedOnError {
				return err
			} else {
				c._logger.OnDemandTrace("Failed to clear target data.")
			}
		}
	}
	return nil
}

func (c *ZxSyncModel) _stepFetchSource() (int, []interface{}, error) {
	if c._fetchSource == nil {
		return 0, nil, errors.New("No any fetch source implementation.")
	}
	start := time.Now()
	count, sourceData := c._fetchSource(c._adapterSource, c._syncConfig, c._inputData)
	if c._debug {
		elapsed := time.Since(start)
		c._logger.OnDemandTrace("Execute fetch source took : %d", elapsed)
	}
	c._logger.OnDemandTrace("Fetch sync data from source : count %d", count)

	return count, sourceData, nil
}

// Process Data.
// 1. BindDataModel
// 2. HookBeforeTarget
// 3. Loop ProcessTarget
// 3.1 DeleteTarget
// 3.2 ProcessTarget
// 3.3 PublishTarget
// 3.4 UpdateJournalTarget
// 3.5 UpdateJournalSource
// 4. HookAfterTarget
func (c *ZxSyncModel) _process(data interface{}, model interface{}, countSuccess *int, countFail *int, countSkip *int) error {
	var err error = nil

	// ========== Step : Bind data model ==========
	model = c._stepBindDataModel(data, model)

	// ========== Step : Hook before upsert ==========
	if c._hookBeforeTarget != nil && len(c._hookBeforeTarget) > 0 {
		newModel, err := c._stepHookBeforeTarget(data, model)
		if err != nil {
			c._logger.MsgError(err)
			if err.Error() == "Force" {
				goto force_task
			} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
				goto counting
			} else {
				goto exception
			}
		} else {
			if newModel != nil {
				model = newModel
			}
		}
	}

	// Validate process target implementation.
	if c._processTarget == nil || (c._processTarget != nil && len(c._processTarget) == 0) {
		c._logger.OnDemandTrace("No any process target implementation.")
		return errors.New("No any process target implementation.")
	}

	for i, seq := range c._targetConnectors {
		f_indx := seq._processTargetIndex
		err = nil
		// ========== Step : Execute delete target ==========
		if c._deleteTarget != nil && len(c._deleteTarget) > 0 && f_indx < len(c._deleteTarget) {
			c._logger.OnDemandTrace("Execute delete target.")
			start := time.Now()
			f_deleteTarget := c._deleteTarget[f_indx]
			err = f_deleteTarget(c._adapterTarget, c._syncConfig, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute delete target took : %d", elapsed)
			}
			if err != nil {
				c._logger.MsgError(err)
				if err.Error() == "Force" {
					goto force_task
				} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
					goto counting
				} else {
					goto exception
				}
			}
		}

		// ========== Step : Execute upsert target ==========
		c._logger.OnDemandTrace("Execute process target.")
		start := time.Now()
		f_processTarget := c._processTarget[f_indx]
		var newModel1 interface{}
		newModel1, err = f_processTarget(c._adapterTarget, c._syncConfig, c._inputData, data, model)
		if newModel1 != nil {
			model = newModel1
		}
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute process target took : %d", elapsed)
		}
		if err != nil {
			c._logger.MsgError(err)
			if err.Error() == "Force" {
				goto force_task
			} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
				goto counting
			} else {
				goto exception
			}
		}

		// ========== Step : Update journal target ==========
		if c._updateJournalTarget != nil && len(c._updateJournalTarget) > 0 && i < len(c._updateJournalTarget) {
			c._logger.OnDemandTrace("Execute update journal target.")
			f_updateJournalTarget := c._updateJournalTarget[i]
			start := time.Now()
			err = f_updateJournalTarget(c._adapterTarget, c._syncConfig, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute update journal target took : %d", elapsed)
			}
			if err != nil {
				c._logger.MsgError(err)
				if err.Error() == "Force" {
					goto force_task
				} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
					goto counting
				} else {
					goto exception
				}
			}
		}

		// ========== Step : Update journal source ==========
		if c._updateJournalSource != nil && len(c._updateJournalSource) > 0 && i < len(c._updateJournalSource) {
			c._logger.OnDemandTrace("Execute update journal source.")
			f_updateJournalSource := c._updateJournalSource[i]
			start := time.Now()
			err = f_updateJournalSource(c._adapterSource, c._syncConfig, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute update journal source took : %d", elapsed)
			}
			if err != nil {
				c._logger.MsgError(err)
				if err.Error() == "Force" {
					goto force_task
				} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
					goto counting
				} else {
					goto exception
				}
			}
		}
	}
	// ========== Step : Hook before upsert ==========
	if c._hookAfterTarget != nil && len(c._hookAfterTarget) > 0 {
		newModel, err := c._stepHookAfterUpsert(data, model)
		if err != nil {
			c._logger.MsgError(err)
			if err.Error() == "Force" {
				goto force_task
			} else if c._syncConfig.SkippedOnError || err.Error() == "skip" {
				goto counting
			} else {
				goto exception
			}
		} else {
			if newModel != nil {
				model = newModel
			}
		}
	}

counting:
	if err != nil {
		if err.Error() == "skip" {
			*countSkip = *countSkip + 1
		} else {
			*countFail = *countFail + 1
		}
	} else {
		*countSuccess = *countSuccess + 1
	}
	return nil

exception:
	return errors.New("exception")

force_task:
	return errors.New("force in-flight task")
}

func (c *ZxSyncModel) _stepBindDataModel(data interface{}, model interface{}) interface{} {
	if c._bindDataModel != nil {
		start := time.Now()
		model = c._bindDataModel(c._adapterSource, c._syncConfig, c._inputData, data)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute bind data model took : %d", elapsed)
		}
		return model
	}
	return model
}

func (c *ZxSyncModel) _stepHookBeforeTarget(data interface{}, model interface{}) (interface{}, error) {
	for _, f_hookBeforeTarget := range c._hookBeforeTarget {
		start := time.Now()
		newModel, err := f_hookBeforeTarget(c._adapterTarget, c._syncConfig, c._inputData, data, model)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute before process target took : %d", elapsed)
		}
		if err != nil {
			return nil, err
		} else {
			return newModel, nil
		}
	}
	return nil, nil
}

func (c *ZxSyncModel) _stepHookAfterUpsert(data interface{}, model interface{}) (interface{}, error) {
	for _, _hookAfterTarget := range c._hookAfterTarget {
		start := time.Now()
		newModel, err := _hookAfterTarget(c._adapterTarget, c._syncConfig, c._inputData, data, model)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute after after process target took : %d", elapsed)
		}
		if err != nil {
			return nil, err
		} else {
			return newModel, nil
		}
	}
	return nil, nil
}

// Tracking state.
func (c *ZxSyncModel) _readTrackingState() {
	fileHandler := NewZxFileImpl(c._logger)
	filePath := c._syncConfig.VfsPath + "/" + c._syncConfig.ProcessId
	c._logger.OnDemandTrace("Read tracking state from file : %s", filePath)

	if c._syncConfig.UseColumnValue {
		if fileHandler.IsFile(filePath) {
			c._logger.OnDemandTrace("Tracking state file found.")
			b, err := fileHandler.ReadFile(filePath)
			if err != nil {
				stringHandler := impl.NewStringImpl()
				c._syncConfig.SqlLastValue = stringHandler.Int(string(b), 0)
			}
		}
	} else {
		layout := "2006-01-02 15:04:05"
		t := impl.NewTimeImpl()
		t.SetLocation("Asia/Bangkok")
		currentTime := t.CurrentDateTime(layout)
		if fileHandler.IsFile(filePath) {
			c._logger.OnDemandTrace("Tracking state file found.")
			b, err := fileHandler.ReadFile(filePath)
			if err == nil {
				stringHandler := impl.NewStringImpl()
				t, err := time.Parse(layout, stringHandler.Trim(string(b)))
				if err != nil {
					if stringHandler.IsNotEmptyString(c._syncConfig.SqlStartTime) &&
						stringHandler.IsNotEmptyString((c._syncConfig.SqlEndTime)) {
						timeHandler := impl.NewTimeImpl()
						s, err1 := timeHandler.ParseDateTime("", c._syncConfig.SqlStartTime)
						e, err2 := timeHandler.ParseDateTime("", c._syncConfig.SqlStartTime)
						if err1 != nil && err2 != nil {
							c._syncConfig.SqlLastTimeValue = time.Time{}
							c._syncConfig.CurrentTimeValue = currentTime
						} else {
							c._syncConfig.SqlLastTimeValue = s
							c._syncConfig.CurrentTimeValue = e
						}
					} else {
						c._syncConfig.SqlLastTimeValue = time.Time{}
						c._syncConfig.CurrentTimeValue = currentTime
					}
				} else {
					c._syncConfig.SqlLastTimeValue = t
					c._syncConfig.CurrentTimeValue = currentTime
				}
			}
		} else {
			c._logger.OnDemandTrace("Tracking state file not found.")
			c._syncConfig.SqlLastTimeValue = time.Time{}
			c._syncConfig.CurrentTimeValue = currentTime
		}
		lastTimeStr := c._syncConfig.SqlLastTimeValue.Format("2006-01-02 15:04:05")
		currentTimeStr := c._syncConfig.CurrentTimeValue.Format("2006-01-02 15:04:05")
		c._logger.OnDemandTrace("Tracking last time : %s, current time : %s", lastTimeStr, currentTimeStr)
	}

}

func (c *ZxSyncModel) _saveTrackingState() {
	fileHandler := NewZxFileImpl(c._logger)
	filePath := c._syncConfig.VfsPath + "/" + c._syncConfig.ProcessId
	c._logger.OnDemandTrace("Write tracking state to file : %s", filePath)

	if c._syncConfig.UseColumnValue {
		fileHandler.WriteFile(filePath, []byte(strconv.Itoa(c._syncConfig.SqlLastValue)))
	} else {
		layout := "2006-01-02 15:04:05"
		s := c._syncConfig.CurrentTimeValue.Format(layout)
		c._logger.OnDemandTrace("Tracking last time : %v", c._syncConfig.CurrentTimeValue.String())
		fileHandler.WriteFile(filePath, []byte(s))
	}
}

func (c *ZxSyncModel) _timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	c._logger.OnDemandTrace("%s took : %v", name, elapsed.String())
}

/*
func (c *ZxSyncModel) log_format(format string, v ...interface{}) {
	s := fmt.Sprintf(format, v...)
	beego.Debug(s)
}


func (c *ZxSyncModel) debug_format(format string, v ...interface{}) {
	if c._debug {
		s := fmt.Sprintf(format, v...)
		beego.Debug(s)
	}
}

func (c *ZxSyncModel) debug(v ...interface{}) {
	if c._debug {
		beego.Debug(v...)
	}
}
*/

// Database (Source) -> NoSQL and/or Elastic
// JournalDB()
// SourceDB()
// TargetElastic()
// CDC is the source journal
// 1. Clear NoSQL data.
// 2. Clear Elastic data.
// 3. Fetch all CDC data from journal database
// 4. Binding data from source database into model.
// 5. Insert/Update into Elastic.
// 6. Insert/Update into NoSQL.
// 7. Update sync status from journal database (CDC data).
/*
func (c *SyncImpl) SyncType1(fetchJournal func(facade.OrmHandler, interface{}) (int, []interface{}),
	clearNoSQLData func(facade.CRUDDocumentStore, interface{}) error,
	clearElasticData func(facade.ElasticHandler, interface{}) error,
	bindingDataModel func(facade.OrmHandler, interface{}, map[string]interface{}) interface{},
	upsertElastic func(facade.ElasticHandler, string, string, interface{}, interface{}) (bool, error),
	upsertNoSQL func(facade.CRUDDocumentStore, interface{}, interface{}) bool,
	updateJournal func(facade.OrmHandler, interface{}, map[string]interface{}) bool) {

	if c.clearElasticData {
		err := clearElasticData(c.elasticConn, c._inputData)
		if err != nil {
			beego.Debug("Sync target is empty.")
		}
	}

	if c.clearNoSQLData {
		err := clearNoSQLData(c.noSQLDocumentStore, c._inputData)
		if err != nil {
			beego.Debug("Sync target is empty.")
		}
	}
	count, sourceData := fetchJournal(c.journalConn, c._inputData)
	beego.Debug("Fetch sync data from source database : count = ", count)
	if count > 0 {
		var (
			i        int64 = 0
			bulkSize int64 = 0
		)
		if c.elasticBulkSize > 0 {
			bulkSize = c.elasticBulkSize
		} else if c.noSQLBulkSize > 0 {
			bulkSize = c.noSQLBulkSize
		} else {
			bulkSize = 100
		}
		c.elasticConn.DisableIndex(c.elasticIndex)
		beego.Debug("Disable index before start synchronize data into elastic.")
		for _, value := range sourceData {
			dataMap := value.(map[string]interface{})
			model := bindingDataModel(c.sourceConn, c._inputData, dataMap)
			upsertNoSQL(c.noSQLDocumentStore, c._inputData, model)
			upsertElastic(c.elasticConn, c.elasticIndex, c.elasticDocType, c._inputData, model)
			i = i + 1
			if updateJournal(c.journalConn, c._inputData, dataMap) {
				//beego.Debug("Sync [", partnerId, "] successfully.")
			} else {
				beego.Debug("Update stats for sync failed.")
			}
			if i%bulkSize == 0 {
				beego.Debug("Sleep for 30 seconds for backend processing.")
				time.Sleep(10 * time.Second)
			}
		}
		time.Sleep(30 * time.Second)
		c.elasticConn.EnableIndex(c.elasticIndex)
		beego.Debug("Enable index to end synchronize data into elastic.")
	}
	return
}

func (c *SyncImpl) SyncType2(fetchSource func(facade.OrmHandler, interface{}) (int, []interface{}),
	clearTargetData func(facade.OrmHandler, interface{}) error,
	deleteTarget func(facade.OrmHandler, interface{}, map[string]interface{}) bool,
	createTarget func(facade.OrmHandler, interface{}, map[string]interface{}) (int, error),
	deleteJournal func(facade.OrmHandler, interface{}, map[string]interface{}) bool) (int, int) {
	var (
		countFail    int = 0
		countSuccess int = 0
	)

	if c.clearTargetData {
		err := clearTargetData(c.targetConn, c._inputData)
		if err != nil {
			beego.Debug("The target is already empty.")
		}
	}
	count, sourceData := fetchSource(c.sourceConn, c._inputData)
	beego.Debug("Fetch sync data from source database : count = ", count)
	if count > 0 {
		for _, value := range sourceData {
			dataMap := value.(map[string]interface{})
			deleteTarget(c.targetConn, c._inputData, dataMap)
			_, err := createTarget(c.targetConn, c._inputData, dataMap)
			if err != nil {
				countFail = countFail + 1
			} else {
				countSuccess = countSuccess + 1
				// Delete journal from source.
				deleteJournal(c.sourceConn, c._inputData, dataMap)
			}
		}
	}
	beego.Debug("Count insert success = ", countSuccess, ", Count insert fail = ", countFail)

	return countSuccess, countFail
}
*/
// CSV (Source) -> Database
// SourceCSVFileName() or SourceCSVFilePath()
// TargetDB()
/*
func (c *ZxSyncModel) SyncType10(csvColumnModel func() []facade.CSVModel,
	getTargetDBColumn func() (string, []string)) bool {
	if StringUtils.IsNotEmptyString(c.sourceCSVFilePath) {
		files, _ := filepath.Glob("/shared/update_pricelist/*.csv")
		beego.Info(files)
		for _, element := range files {
			tableName, columnList := getTargetDBColumn()
			_, status := c.syncCSV(element, csvColumnModel(), tableName, columnList)
			if status {

			}
		}
	} else if StringUtils.IsNotEmptyString(c.sourceCSVFileName) {
		beego.Debug(c.sourceCSVFileName)
		tableName, columnList := getTargetDBColumn()
		_, status := c.syncCSV(c.sourceCSVFileName, csvColumnModel(), tableName, columnList)
		if status {

		}
	}
	return true
}

func (c *SyncImpl) csvConverting(csvModel facade.CSVModel, value string) interface{} {
	if csvModel.TrimWhiteSpace {
		value = strings.Trim(value, " ")
	}
	if csvModel.ConvertBlank {
		value = strings.Replace(value, " ", "", -1)
	}
	if csvModel.ConvertSingleQuote {
		value = strings.Replace(value, "'", "''", -1)
	}
	if csvModel.ConvertHyphen {
		value = strings.Replace(value, "-", "", -1)
	}
	if csvModel.ConvertComma {
		value = strings.Replace(value, ",", "", -1)
	}
	return value
}

func (c *SyncImpl) syncCSV(fileName string, csvColumnModel []facade.CSVModel, tableName string, columnList []string) (int64, bool) {
	var (
		i      int64 = 0
		status bool  = false
	)
	csvFile, _ := os.Open(fileName)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			beego.Info(error)
		}
		if c.firstRowIsHeader {
			continue
		} else {
			var bindingParameters []interface{}
			for indx, model := range csvColumnModel {
				csvModel := model
				if StringUtils.IsNotEmptyString(csvModel.ColumnType) {
					var value interface{}
					if csvModel.ColumnType == "string" {
						value = c.csvConverting(csvModel, line[csvModel.Index])
						if utils.AssertTypeOf(value) == "string" {
							value = c.RemoveSQLInjectionString(value.(string))
						} else {
							if csvModel.DefaultValue == nil {
								value = ""
							} else {
								value = csvModel.DefaultValue
							}
						}
					} else if csvModel.ColumnType == "int64" {
						value = c.csvConverting(csvModel, line[indx])
						if utils.AssertTypeOf(value) == "int64" {
							value = value.(int64)
						} else {
							if csvModel.DefaultValue == nil {
								value = 0
							} else {
								value = csvModel.DefaultValue
							}
						}
					} else if csvModel.ColumnType == "float64" {
						value = c.csvConverting(csvModel, line[indx])
						if utils.AssertTypeOf(value) == "float64" {
							value = value.(float64)
						} else {
							if csvModel.DefaultValue == nil {
								value = 0.00
							} else {
								value = csvModel.DefaultValue
							}
						}
					} else {
						value = ""
					}
					bindingParameters = append(bindingParameters, value)
				}
			}
			i = i + 1

			StatementUtils := NewStatementImpl()
			pstmt := ""
			if c.targetConn.GetDriverName() == "oracle" {
				pstmt = StatementUtils.PrepareOraInsertSQL(tableName, columnList)
			} else if c.targetConn.GetDriverName() == "postgresql" {
				pstmt = StatementUtils.PreparePGInsertSQL(tableName, columnList)
			} else {
				status = false
				break
			}
			_, err := c.targetConn.Insert(pstmt, bindingParameters...)
			if err != nil {
				beego.Debug(err)
			} else {
				beego.Debug("Success.")
				status = true
			}
		}
	}
	return i, status
}
*/
/*
func (c *ZxSyncModel) RemoveSQLInjectionString(s string) string {
	return template.HTMLEscapeString(s)
}
*/

// Backup
/*
		// ========== Step : Bind data model ==========
		model = c._stepBindDataModel(dataMap)
		// ============================================

		// ========== Step : Hook before upsert ==========
		err = c._stepHookBeforeUpsert(dataMap, model)
		if err != nil {
			return err, countSuccess, countFail, countSkip
		}
		// ===============================================

		err = nil
		if c._upsertTarget == nil || (c._upsertTarget != nil && len(c._upsertTarget) == 0) {
			c.debug("No any upsert target implementation.")
			return errors.New("No any upsert target implementation."), countSuccess, countFail, countSkip
		}
		for i, f_upsertTarget := range c._upsertTarget {
			// ========== Execute Delete Target.
			if c._deleteTarget != nil && len(c._deleteTarget) > 0 {
				c.debug("Execute delete target.")
				f_deleteTarget := c._deleteTarget[i]
				start := time.Now()
				err = f_deleteTarget(c._adapterTarget, c._inputData, dataMap, model)
				if c._debug {
					elapsed := time.Since(start)
					c.debug("Execute delete target took ", elapsed)
				}
				if err != nil {
					c.debug(err)
					if c._syncConfig.SkippedOnError {
						goto counting
					} else {
						goto enable_index
					}
				}
			}
			// ========== Execute Upsert Target.
			c.debug("Execute upsert target.")
			start := time.Now()
			_, err = f_upsertTarget(c._adapterTarget, c._inputData, dataMap, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute upsert target took ", elapsed)
			}
			if err != nil {
				c.debug(err)
				if c._syncConfig.SkippedOnError || err.Error() == "skip" {
					goto counting
				} else {
					goto enable_index
				}
			}
			// ========== Execute Publish Target.
			if c._publishTarget != nil && len(c._publishTarget) > 0 {
				c.debug("Execute publish target.")
				f_publishTarget := c._publishTarget[i]
				start := time.Now()
				err = f_publishTarget(c._adapterTarget, c._inputData, dataMap, model)
				if c._debug {
					elapsed := time.Since(start)
					c.debug("Execute publish target took ", elapsed)
				}
				if err != nil {
					c.debug(err)
					if c._syncConfig.SkippedOnError {
						goto counting
					} else {
						goto enable_index
					}
				}
			}
			// ========== Execute Update Journal Target.
			if c._updateJournalTarget != nil && len(c._updateJournalTarget) > 0 {
				c.debug("Execute update journal target.")
				f_updateJournalTarget := c._updateJournalTarget[i]
				start := time.Now()
				err = f_updateJournalTarget(c._adapterTarget, c._inputData, dataMap, model)
				if c._debug {
					elapsed := time.Since(start)
					c.debug("Execute update journal target took ", elapsed)
				}
				if err != nil {
					c.debug(err)
					if c._syncConfig.SkippedOnError {
						goto counting
					} else {
						goto enable_index
					}
				}
			}
			// ========== Execute Update Journal Source.
			if c._updateJournalSource != nil && len(c._updateJournalSource) > 0 {
				c.debug("Excute update journal source.")
				f_updateJournalSource := c._updateJournalSource[i]
				start := time.Now()
				err = f_updateJournalSource(c._adapterSource, c._inputData, dataMap, model)
				if c._debug {
					elapsed := time.Since(start)
					c.debug("Execute update journal source took ", elapsed)
				}
				if err != nil {
					c.debug(err)
					if c._syncConfig.SkippedOnError {
						goto counting
					} else {
						goto enable_index
					}
				}
			}
		}
		for _, f_hookAfterUpsert := range c._hookAfterUpsert {
			start := time.Now()
			err = f_hookAfterUpsert(c._adapterTarget, c._inputData, dataMap, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute after upsert took ", elapsed)
			}
		}
		i = i + 1
	counting:
		if err != nil {
			if err.Error() == "skip" {
				countSkip = countSkip + 1
			} else {
				countFail = countFail + 1
			}
		} else {
			countSuccess = countSuccess + 1
		}
		if bulkSize > 0 && elastic != nil && c._syncConfig.ElasticAutoDisableIndex && i%bulkSize == 0 {
			c.debug("Sleep for ", delay, " seconds for backend processing.")
			time.Sleep(delay)
		}
*/

/*
func (c *ZxSyncModel) _process(data interface{}, model interface{}, countSuccess *int, countFail *int, countSkip *int) error {
	var err error = nil

	// ========== Step : Bind data model ==========
	model = c._stepBindDataModel(data, model)

	// ========== Step : Hook before upsert ==========
	err = c._stepHookBeforeUpsert(data, model)
	if err != nil {
		return err
	}

	if c._upsertTarget == nil || (c._upsertTarget != nil && len(c._upsertTarget) == 0) {
		c.debug("No any upsert target implementation.")
		return errors.New("No any upsert target implementation.")
	}
	for i, f_upsertTarget := range c._upsertTarget {
		err = nil
		// ========== Step : Execute delete target ==========
		if c._deleteTarget != nil && len(c._deleteTarget) > 0 && i < len(c._deleteTarget) {
			c.debug("Execute delete target.")
			f_deleteTarget := c._deleteTarget[i]
			start := time.Now()
			err = f_deleteTarget(c._adapterTarget, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute delete target took ", elapsed)
			}
			if err != nil {
				c.debug(err)
				if c._syncConfig.SkippedOnError {
					goto counting
				} else {
					goto exception
				}
			}
		}

		// ========== Step : Execute upsert target ==========
		c.debug("Execute upsert target.")
		start := time.Now()
		_, err = f_upsertTarget(c._adapterTarget, c._inputData, data, model)
		if c._debug {
			elapsed := time.Since(start)
			c.debug("Execute upsert target took ", elapsed)
		}
		if err != nil {
			c.debug(err)
			if c._syncConfig.SkippedOnError || err.Error() == "skip" {
				goto counting
			} else {
				goto exception
			}
		}

		// ========== Step : Publish target ==========
		if c._publishTarget != nil && len(c._publishTarget) > 0 && i < len(c._publishTarget) {
			c.debug("Execute publish target.")
			f_publishTarget := c._publishTarget[i]
			start := time.Now()
			err = f_publishTarget(c._adapterTarget, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute publish target took ", elapsed)
			}
			if err != nil {
				c.debug(err)
				if c._syncConfig.SkippedOnError {
					goto counting
				} else {
					goto exception
				}
			}
		}

		// ========== Step : Update journal target ==========
		if c._updateJournalTarget != nil && len(c._updateJournalTarget) > 0 && i < len(c._updateJournalTarget) {
			c.debug("Execute update journal target.")
			f_updateJournalTarget := c._updateJournalTarget[i]
			start := time.Now()
			err = f_updateJournalTarget(c._adapterTarget, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute update journal target took ", elapsed)
			}
			if err != nil {
				c.debug(err)
				if c._syncConfig.SkippedOnError {
					goto counting
				} else {
					goto exception
				}
			}
		}

		// ========== Step : Update journal source ==========
		if c._updateJournalSource != nil && len(c._updateJournalSource) > 0 && i < len(c._updateJournalSource) {
			c.debug("Excute update journal source.")
			f_updateJournalSource := c._updateJournalSource[i]
			start := time.Now()
			err = f_updateJournalSource(c._adapterSource, c._inputData, data, model)
			if c._debug {
				elapsed := time.Since(start)
				c.debug("Execute update journal source took ", elapsed)
			}
			if err != nil {
				c.debug(err)
				if c._syncConfig.SkippedOnError {
					goto counting
				} else {
					goto exception
				}
			}
		}
	}
	// ========== Step : Hook before upsert ==========
	err = c._stepHookAfterUpsert(data, model)
	if err != nil {
		return err
	}

counting:
	if err != nil {
		if err.Error() == "skip" {
			*countSkip = *countSkip + 1
		} else {
			*countFail = *countFail + 1
		}
	} else {
		*countSuccess = *countSuccess + 1
	}
	return nil

exception:
	return errors.New("exception")
}
*/
