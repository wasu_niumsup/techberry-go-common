package core

import (
	"techberry-go/common/v2/facade"

	"github.com/astaxie/beego"
)

type ZxProperties struct {
}

func NewZxProperties() facade.Properties {
	return &ZxProperties{}
}

func (c *ZxProperties) Set(key, val string) error {
	return beego.AppConfig.Set(key, val)
}

func (c *ZxProperties) String(key string) string {
	return beego.AppConfig.String(key)
}

func (c *ZxProperties) Strings(key string) []string {
	return beego.AppConfig.Strings(key)
}

func (c *ZxProperties) Int(key string) (int, error) {
	return beego.AppConfig.Int(key)
}

func (c *ZxProperties) Int64(key string) (int64, error) {
	return beego.AppConfig.Int64(key)
}

func (c *ZxProperties) Bool(key string) (bool, error) {
	return beego.AppConfig.Bool(key)
}

func (c *ZxProperties) Float(key string) (float64, error) {
	return beego.AppConfig.Float(key)
}

func (c *ZxProperties) DefaultString(key string, defaultVal string) string {
	return beego.AppConfig.DefaultString(key, defaultVal)
}

func (c *ZxProperties) DefaultStrings(key string, defaultVal []string) []string {
	return beego.AppConfig.DefaultStrings(key, defaultVal)
}

func (c *ZxProperties) DefaultInt(key string, defaultVal int) int {
	return beego.AppConfig.DefaultInt(key, defaultVal)
}

func (c *ZxProperties) DefaultInt64(key string, defaultVal int64) int64 {
	return beego.AppConfig.DefaultInt64(key, defaultVal)
}

func (c *ZxProperties) DefaultBool(key string, defaultVal bool) bool {
	return beego.AppConfig.DefaultBool(key, defaultVal)
}

func (c *ZxProperties) DefaultFloat(key string, defaultVal float64) float64 {
	return beego.AppConfig.DefaultFloat(key, defaultVal)
}
