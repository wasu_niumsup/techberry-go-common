package core

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	_ "github.com/astaxie/beego/cache/redis"
	_ "github.com/godror/godror"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type ZxOrmConnector struct {
}

func NewZxOrmConnector() ZxOrmConnector {
	return ZxOrmConnector{}
}

func (c *ZxOrmConnector) GetOrmConnector(driverName string, dbType string, connstring string, maxIdleConn int, maxOpenConn int, timeout int) *sqlx.DB {
	var connector *sqlx.DB

	beego.Info(fmt.Sprintf("Initializing %s persistent.", dbType))

	var err error
	// create a normal database connection through database/sql
	connector, err = sqlx.Open(driverName, connstring)

	if err != nil {
		beego.Info(fmt.Sprintf("Initialize persistent failed. Error : %s", err.Error()))
	} else {
		beego.Info("Initialize persistent successfully.")
		// set to reasonable values for production
		connector.SetMaxIdleConns(maxIdleConn)
		connector.SetMaxOpenConns(maxOpenConn)
		connector.SetConnMaxLifetime(time.Minute * time.Duration(timeout))
	}

	return connector
}
