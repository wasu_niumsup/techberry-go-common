package core

import (
	"techberry-go/common/v2/facade"
)

type ZxAdapter struct {
	_csv     facade.CsvHandler
	_elastic facade.ElasticHandler
	_nosql   facade.CRUDDocumentStore
	_orm     facade.OrmHandler
	_kafka   facade.Kafka
	_rpc     facade.RpcConnector
	_odoo    facade.OdooXmlRpcClient
	_df      facade.DataFactory
	_plugin  facade.Plugin
	_logger  facade.LogEvent
}

func NewZxAdapter(logger facade.LogEvent) facade.Adapter {
	plugin := NewZxPlugin()
	var p facade.Adapter = &ZxAdapter{_plugin: plugin, _logger: logger}
	return p
}

func (c *ZxAdapter) SetPlugin(plugin facade.Plugin) {
	c._plugin = plugin
}

func (c *ZxAdapter) Plugin() facade.Plugin {
	return c._plugin
}

func (c *ZxAdapter) SetCsv(csv facade.CsvHandler) {
	c._csv = csv
}

func (c *ZxAdapter) Csv() facade.CsvHandler {
	return c._csv
}

func (c *ZxAdapter) SetElastic(elastic facade.ElasticHandler) {
	c._elastic = elastic
}

func (c *ZxAdapter) Elastic() facade.ElasticHandler {
	return c._elastic
}

func (c *ZxAdapter) SetNoSql(nosql facade.CRUDDocumentStore) {
	c._nosql = nosql
}

func (c *ZxAdapter) NoSql() facade.CRUDDocumentStore {
	return c._nosql
}

func (c *ZxAdapter) SetOrm(orm facade.OrmHandler) {
	c._orm = orm
}

func (c *ZxAdapter) Orm() facade.OrmHandler {
	return c._orm
}

func (c *ZxAdapter) SetKafka(kafka facade.Kafka) {
	c._kafka = kafka
}

func (c *ZxAdapter) Kafka(singleton bool) facade.Kafka {
	// clone object.
	if singleton {
		return c._kafka
	} else {
		servers := c._kafka.BootstrapServers()
		return NewZxKafkaImpl(c._logger, servers)
	}
}

func (c *ZxAdapter) SetRpc(rpc facade.RpcConnector) {
	c._rpc = rpc
}

func (c *ZxAdapter) Rpc() facade.RpcConnector {
	return c._rpc
}

func (c *ZxAdapter) SetOdoo(odoo facade.OdooXmlRpcClient) {
	c._odoo = odoo
}

func (c *ZxAdapter) Odoo() facade.OdooXmlRpcClient {
	return c._odoo
}

func (c *ZxAdapter) SetDataFactory(df facade.DataFactory) {
	c._df = df
}

func (c *ZxAdapter) DataFactory() facade.DataFactory {
	return c._df
}
