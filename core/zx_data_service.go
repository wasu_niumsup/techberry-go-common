package core

import (
	"techberry-go/common/v2/facade"
)

type ZxDataServiceImpl struct {
	_logger      facade.LogEvent
	_respContext *facade.ResponseContext
	_dataBus     facade.DataBus
}

func NewZxDataServiceImpl(logger facade.LogEvent, dataBus facade.DataBus) facade.DataService {
	var p facade.DataService = &ZxDataServiceImpl{_logger: logger, _dataBus: dataBus}
	return p
}

func (c *ZxDataServiceImpl) BindInputRoot(inputRoot interface{}) {
	c._dataBus.RequestBinding(inputRoot)
}

func (c *ZxDataServiceImpl) Output(respContext facade.ResponseContext) {
	c._respContext = &respContext
}

func (c *ZxDataServiceImpl) Context() *facade.ResponseContext {
	return c._respContext
}
