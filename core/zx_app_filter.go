package core

import (
	"encoding/json"

	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/context"
)

type ZxAppFilterImpl struct {
}

func AppFilterInstance() facade.AppFilter {
	var p facade.AppFilter
	p = &ZxAppFilterImpl{}
	return p
}

func (c *ZxAppFilterImpl) JsonFilter() func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		var jsonData map[string]interface{}
		err := json.Unmarshal(ctx.Input.RequestBody, &jsonData)
		if err == nil {
			jsonReq, _ := json.Marshal(jsonData)
			beego.Debug(string(jsonReq))
			jsonHandler := NewZxJsonImpl(jsonReq)
			ctx.Input.SetData("jsonservice", jsonHandler)
			beego.Debug("Injects json to controller.")
		} else {
			if ctx.Input.IsUpload() {
				beego.Debug("Content-type is multipart upload.")
			} else {
				ctx.Output.SetStatus(400)
				ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 400, \"message\": \"Invalid Content Data.\"}"))
				return
			}
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) CredentialFilter() func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		var (
			tokenId    string = ""
			credential string = ""
		)
		var requestCtx = new(RequestContext)

		if ctx.Input.IsUpload() {
			tokenId = ctx.Request.PostFormValue("token_id")
			credential = ctx.Request.PostFormValue("credential")
			beego.Debug("Token Id : ", tokenId)
			beego.Debug("Credential : ", credential)
			if StringUtils.IsEmptyString(tokenId) && StringUtils.IsEmptyString(credential) {
				ctx.Output.SetStatus(400)
				ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 400, \"message\": \"Bad Request.\"}"))
				return
			}
			authCredential := c._getAuthCredential(credential)
			authCredential.TokenId = tokenId
			authCredential.Credential = credential
			requestCtx._authCredential = authCredential
		} else {
			jsonRpc := facade.JsonRpcRequest{}
			err := json.Unmarshal(ctx.Input.RequestBody, &jsonRpc)
			if err != nil {
				ctx.Output.SetStatus(400)
				ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 400, \"message\": \"Invalid Content Data.\"}"))
				return
			}
			var authCredential = facade.AuthCredential{}
			if StringUtils.IsNotEmptyString(jsonRpc.Method) && jsonRpc.Id >= 0 {
				jsonHandler := NewZxJsonImpl(ctx.Input.RequestBody)
				authCredential = c._extractCredential(jsonHandler.GetData())
				// Support only named parameters.
				jsonByte := jsonHandler.GetDataStruct("params")
				jsonParams := NewZxJsonImpl(jsonByte)
				requestCtx._jsonRpc = &jsonRpc
				requestCtx._jsonHandler = jsonParams
				requestCtx._authCredential = authCredential
			} else {
				var jsonData map[string]interface{}
				err := json.Unmarshal(ctx.Input.RequestBody, &jsonData)
				if err == nil {
					jsonRequest, _ := json.Marshal(jsonData)
					jsonHandler := NewZxJsonImpl(jsonRequest)
					// validate data section.
					jsonByte := jsonHandler.GetDataStruct("data")
					var jsonDataSection facade.JsonHandler
					if len(jsonByte) > 0 {
						jsonDataSection = NewZxJsonImpl(jsonByte)
						authCredential = c._extractCredential(jsonDataSection.GetData())
						requestCtx._jsonHandler = jsonDataSection
						requestCtx._authCredential = authCredential
					} else {
						jsonRequest = jsonHandler.GetData()
						authCredential = c._extractCredential(jsonRequest)
						requestCtx._jsonHandler = jsonHandler
						requestCtx._authCredential = authCredential
					}
				}
			}
		}
		ctx.Input.SetData("request_context", requestCtx)
	}
	return filter
}

func (c *ZxAppFilterImpl) VersionFilter(versionList []string) func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		version := ctx.Input.Param(":version")
		if ok, _ := StringUtils.InArrayString(version, versionList); ok {
			beego.Debug("Validate version successfully.")
		} else {
			ctx.Output.SetStatus(400)
			ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 400, \"message\": \"No Service Version.\"}"))
			return
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) BotFilter() func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		var jsonData map[string]interface{}
		err := json.Unmarshal(ctx.Input.RequestBody, &jsonData)
		if err == nil {
			jsonReq, _ := json.Marshal(jsonData)
			beego.Debug(string(jsonReq))
			jsonHandler := NewZxJsonImpl(jsonReq)
			ctx.Input.SetData("jsonservice", jsonHandler)
			beego.Debug("Injects json utils to controller.")
		} else {
			ctx.Output.SetStatus(200)
			ctx.Output.Body(StringUtils.GetByteArray(""))
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) AppKeyCacheFilter(beegoCache cache.Cache) func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		appKey := ctx.Input.Header("AppKey")
		beego.Debug(appKey)
		if StringUtils.IsEmptyString(appKey) || !(beegoCache.IsExist(appKey)) {
			ctx.Output.SetStatus(401)
			ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 401, \"message\": \"Invalid Token or Token is unauthorized or Token expired.\"}"))
			return
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) AppKeyFilter(authKey string) func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		appKey := ctx.Input.Header("AppKey")
		if StringUtils.IsEmptyString(appKey) || (authKey != appKey) {
			ctx.Output.SetStatus(403)
			ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 403, \"error_message\": \"Forbidden to access application.\"}"))
			return
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) LicenseFilter(authKey string) func(ctx *context.Context) {
	var filter = func(ctx *context.Context) {
		if !(ctx.Request.RequestURI == "/lic/requestLicense" ||
			ctx.Request.RequestURI == "/lic/activateLicense") {
			appKey := ctx.Input.Header("AppKey")
			licenseService := NewZxLicense(nil)
			if licenseService.Validate(appKey) != nil {
				ctx.Output.SetStatus(503)
				ctx.Output.Body(StringUtils.GetByteArray("{\"error_code\": 503, \"error_message\": \"License invalid or expire.\"}"))
				return
			}
		}
	}
	return filter
}

func (c *ZxAppFilterImpl) _getAuthCredential(encrypted string) facade.AuthCredential {
	var (
		userName string
		userCode string
		uid      int64
	)
	AesCryptHandler := impl.NewAesCryptoManagerImpl()
	if !StringUtils.IsEmptyString(encrypted) {
		cred_data := AesCryptHandler.SimpleDecrypt(encrypted)
		jsonByte := []byte(cred_data)
		json := NewZxJsonImpl(jsonByte)
		userName = json.String("", "username")
		uid = json.Int(9999, "uid")
		userCode = json.String("", "x_user_code")
	}
	return facade.AuthCredential{
		Uid:      uid,
		UserName: userName,
		UserCode: userCode,
	}
}

func (c *ZxAppFilterImpl) _extractCredential(jsonByte []byte) facade.AuthCredential {
	var (
		token_id               string
		cred                   string
		uid                    int64
		userName               string
		company_id             int64
		currentCompany_id      int64
		currentOwnerCompany_id int64
		offset                 int64
		limit                  int64
		user_code              string
	)
	json := NewZxJsonImpl(jsonByte)
	token_id = json.String("", "token_id")
	cred = json.String("", "credential")
	currentCompany_id = json.Int(1, "company_id")
	currentOwnerCompany_id = json.Int(1, "owner_company_id")
	userName = json.String("", "username")

	AesCryptHandler := impl.NewAesCryptoManagerImpl()
	if StringUtils.IsNotEmptyString(cred) {
		cred_data := AesCryptHandler.SimpleDecrypt(cred)
		//json_data := []byte(cred_data)
		jsonData := NewZxJsonImpl([]byte(cred_data))
		uid = jsonData.Int(9999, "uid")
		userName = jsonData.String("", "username")
		company_id = jsonData.Int(1, "company_id")
		user_code = jsonData.String("", "user_code")
	} else {
		uid = 9999
		company_id = 1
	}
	offset = json.Int(0, "data.offset")
	limit = json.Int(100, "data.limit")
	if offset > 0 {
		offset = (offset - 1) * limit
	}

	return facade.AuthCredential{
		TokenId:               token_id,
		Credential:            cred,
		UserName:              userName,
		Uid:                   uid,
		CompanyId:             company_id,
		CurrentCompanyId:      currentCompany_id,
		CurrentOwnerCompanyId: currentOwnerCompany_id,
		Offset:                offset,
		Limit:                 limit,
		UserCode:              user_code,
	}
}
