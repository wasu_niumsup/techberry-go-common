package core

import (
	"sync"
	"techberry-go/common/v2/core/ldap"
	"techberry-go/common/v2/core/odoo"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"

	"github.com/astaxie/beego/cache"
	"github.com/jmoiron/sqlx"
)

type ZxConnector struct {
	_handler                facade.Handler
	_cachePool              map[int]facade.CacheHandler
	_beegoCache             cache.Cache
	_rpcCache               facade.RpcCacheConnector
	_rpcConnector           facade.RpcConnector
	_noSqlConnUrl           map[string]string
	_elasticConnectors      map[string]interface{}
	_ormConnectors          map[string]interface{}
	_ormIndex               []string
	_mqtt                   map[string]interface{}
	_mqttIndex              []string
	_kafkaBroker            string
	_elasticIsSecure        bool
	_elasticHost            string
	_elasticPort            int
	_elasticApiKey          string
	_elasticBasicAuthUser   string
	_elasticBasicAuthPasswd string
	_debug                  bool
	_logger                 facade.LogEvent
	sync.Mutex
	_redisHost     string
	_redisPort     int
	_redisDB       int
	_redisPoolSize int
	_zxInitilizer  *ZxInitializer
}

func NewZxConnector(logger facade.LogEvent) ZxConnector {
	return ZxConnector{
		_handler:           NewZxHandler(logger),
		_ormConnectors:     make(map[string]interface{}),
		_elasticConnectors: make(map[string]interface{}),
		_noSqlConnUrl:      make(map[string]string),
		_mqtt:              make(map[string]interface{}),
		_cachePool:         make(map[int]facade.CacheHandler),
		_logger:            logger,
	}
}

func (c *ZxConnector) GetOdooXmlRpcConnection(username string, passwd string, db string, url string) facade.OdooXmlRpcClient {
	conn, err := odoo.NewClient(&odoo.ClientConfig{
		Admin:    username,
		Password: passwd,
		Database: db,
		URL:      url,
	})
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	xmlrpcClient := NewZxOdooClientImpl(conn)
	return xmlrpcClient
}

/* Kafka */
func (c *ZxConnector) Kafka() facade.Kafka {
	return NewZxKafkaImpl(c._logger, c._kafkaBroker)
}

func (c *ZxConnector) InitKafka(broker string) facade.Connector {
	c._kafkaBroker = broker
	return c
}

func (c *ZxConnector) GetKafkaConnection(broker string) facade.Kafka {
	return NewZxKafkaImpl(c._logger, broker)
}

/* Elastic */
func (c *ZxConnector) GetElasticConnection(isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) facade.ElasticHandler {
	return NewZxElasticImpl(c._logger, isSecure, c._elasticHost, c._elasticPort, c._elasticApiKey, c._elasticBasicAuthUser, c._elasticBasicAuthPasswd)
}

func (c *ZxConnector) Elastic(name string) facade.ElasticHandler {
	if StringUtils.IsEmptyString(name) {
		name = "default"
	}
	if val, ok := c._elasticConnectors[name]; ok {
		return val.(facade.ElasticHandler)
	} else {
		return nil
	}
}

func (c *ZxConnector) InitElasticConnector(name string, isSecure bool, host string, port int, api_key string, basicAuthUser string, basicAuthPasswd string) facade.Connector {
	if StringUtils.IsNotEmptyString(name) {
		c._logger.OnDemandTrace("Initialize elastic to %s:%d", host, port)
		c._elasticIsSecure = isSecure
		c._elasticHost = host
		c._elasticPort = port
		c._elasticApiKey = api_key
		c._elasticBasicAuthUser = basicAuthUser
		c._elasticBasicAuthPasswd = basicAuthPasswd
		c._elasticConnectors[name] = NewZxElasticImpl(c._logger, isSecure, c._elasticHost, c._elasticPort, c._elasticApiKey, c._elasticBasicAuthUser, c._elasticBasicAuthPasswd)
	}
	return c
}

/* Redis */
func (c *ZxConnector) Redis(db int) facade.CacheHandler {
	// Return default redis db.
	if db == -1 {
		c._logger.OnDemandTrace("Default redis dbnum : %s", db)
		return c._cachePool[c._redisDB]
	}
	var cache facade.CacheHandler
	if cache, ok := c._cachePool[db]; ok {
		if cache != nil {
			return cache
		}
	}
	cache = NewZxCacheImpl(c._redisHost, c._redisPort, db, c._redisPoolSize)
	c._cachePool[db] = cache
	return cache
}

func (c *ZxConnector) GetRedisConnection(host string, port int, db int, poolSize int) facade.CacheHandler {
	//c._cache = impl.NewCacheImpl(c._mc, host, port, db)
	c._redisHost = host
	c._redisPort = port
	c._redisDB = db
	c._redisPoolSize = poolSize
	return NewZxCacheImpl(host, port, db, poolSize)
}

func (c *ZxConnector) InitRedisConnector(host string, port int, db int, poolSize int) facade.Connector {
	//c._cache = impl.NewCacheImpl(c._mc, host, port, db)
	c._logger.OnDemandTrace("Initialize redis to %s:%d, dbnum %d, poolSize %d", host, port, db, poolSize)
	c._redisHost = host
	c._redisPort = port
	c._redisDB = db
	c._redisPoolSize = poolSize
	c._cachePool[db] = NewZxCacheImpl(host, port, db, poolSize)
	return c
}

/* NoSql */
func (c *ZxConnector) InitMongo(name string, url string) facade.Connector {
	if StringUtils.IsNotEmptyString(name) {
		c._logger.OnDemandTrace("Initialize mongodb to %s", name)
		c._noSqlConnUrl[name] = url
	}
	return c
}

func (c *ZxConnector) Mongo(name string, db string) facade.CRUDDocumentStore {
	connection_url := c._noSqlConnUrl[name]
	if StringUtils.IsNotEmptyString(connection_url) {
		return NewZxNoSqlImpl(c._logger, connection_url, db)
	} else {
		return nil
	}
}

func (c *ZxConnector) GetMongoConnection(name string, url string, db string) facade.CRUDDocumentStore {
	if StringUtils.IsNotEmptyString(url) && StringUtils.IsNotEmptyString(db) {
		return NewZxNoSqlImpl(c._logger, url, db)
	} else {
		return nil
	}
}

/* ORM */
func (c *ZxConnector) GetOrmConnection(driverName string, dbType string, connstring string, timeout int, maxIdleConn int, maxOpenConn int) facade.OrmHandler {
	// create a normal database connection through database/sql
	connector := c._createOrmConnector(driverName, dbType, connstring, timeout, maxIdleConn, maxOpenConn)
	return NewZxOrmImpl(c._logger, connector)
}

func (c *ZxConnector) _createOrmConnector(driverName string, dbType string, connstring string, timeout int, maxIdleConn int, maxOpenConn int) *sqlx.DB {
	// create a normal database connection through database/sql
	connector, err := sqlx.Open(driverName, connstring)

	if err != nil {
		c._logger.OnDemandTrace("Initialize %s persistent failed. Error : %s", dbType, err.Error())
	} else {
		c._logger.OnDemandTrace("Initialize %s persistent successfully.", dbType)
		// set to reasonable values for production
		connector.SetMaxIdleConns(maxIdleConn)
		connector.SetMaxOpenConns(maxOpenConn)
		//connector.SetConnMaxLifetime(time.Minute * 60)
		connector.SetConnMaxIdleTime(time.Minute * 60)
	}
	return connector
}

func (c *ZxConnector) InitOrmConnector(key string, driverName string, dbType string, connstring string, timeout int, maxIdleConn int, maxOpenConn int) facade.Connector {
	obj := c._ormConnectors[key]
	var connector *sqlx.DB
	if obj != nil {
		connector = obj.(*sqlx.DB)
		c._logger.OnDemandTrace("Validate %s the connection.", key)
		err := connector.Ping()
		c._logger.Debug().MsgError(err)
		if err == nil {
			return c
		}
	}
	connector = c._createOrmConnector(driverName, dbType, connstring, timeout, maxIdleConn, maxOpenConn)
	c._ormConnectors[key] = NewZxOrmImpl(c._logger, connector)
	return c
}

func (c *ZxConnector) Orm(key string) facade.OrmHandler {
	c._logger.OnDemandTrace("Orm for key : %s", key)
	if c._handler.String(true).IsEmptyString(key) {
		if len(c._ormIndex) > 0 {
			s := c._ormIndex[0]
			return c._ormConnectors[s].(facade.OrmHandler)
		}
	}
	return c._ormConnectors[key].(facade.OrmHandler)
}

/*
func (c *ZxConnector) GetOrmByIndex(index int) facade.OrmHandler {
	if index < len(c._ormIndex) {
		s := c._ormIndex[index]
		return c._ormConnectors[s].(facade.OrmHandler)
	}
	return nil
}
*/

/* MQTT */
func (c *ZxConnector) InitMqttConnector(name string, url string, username string, password string) facade.Connector {
	c.Lock()
	defer c.Unlock()
	clientConfig := facade.MqttConfig{}
	clientConfig.Url = url
	clientConfig.Username = username
	clientConfig.Password = password
	c._mqtt[name] = clientConfig
	c._mqttIndex = append(c._mqttIndex, name)
	return c
}

func (c *ZxConnector) Mqtt(name string, clientId string) facade.MqttClient {
	if c._handler.String(true).IsEmptyString(name) {
		if len(c._mqttIndex) > 0 {
			s := c._mqttIndex[0]
			clientConfig := c._mqtt[s].(facade.MqttConfig)
			if c._handler.String(true).IsEmptyString(clientId) {
				clientId = c._handler.String(true).GenerateUuid(true)
			}
			return NewZxMqttImpl(c._logger, clientId, clientConfig.Url, clientConfig.Username, clientConfig.Password)
		}
	}
	clientConfig := c._mqtt[name].(facade.MqttConfig)
	return NewZxMqttImpl(c._logger, clientId, clientConfig.Url, clientConfig.Username, clientConfig.Password)
}

func (c *ZxConnector) GetMqttConnection(url string, username string, password string, clientId string) facade.MqttClient {
	if c._handler.String(true).IsEmptyString(clientId) {
		clientId = c._handler.String(true).GenerateUuid(true)
	}
	return NewZxMqttImpl(c._logger, clientId, url, username, password)
}

/* HttpClient */
func (c *ZxConnector) HttpClient() facade.HttpClient {
	return NewZxHttpClientImpl(c._logger)
}

// Enable beego as cache manager for RPC caching connector.
func (c *ZxConnector) InitBeegoCache(cache cache.Cache) facade.Connector {
	c._beegoCache = cache
	c._rpcCache = impl.NewBeegoCacheManager(cache)
	return c
}

// Get Beego cache.
func (c *ZxConnector) BeegoCache() cache.Cache {
	return c._beegoCache
}

// Get RPC cache connector.
func (c *ZxConnector) RpcCache() facade.RpcCacheConnector {
	return c._rpcCache
}

func (c *ZxConnector) GetCacheRpc(username string, token_id string) facade.OdooCredential {
	credential, _ := c._rpcCache.Load(token_id)
	return credential.(facade.OdooCredential)
}

//  RPC connector.
func (c *ZxConnector) GetRpcConnection(host string, port int, timeout int) facade.RpcConnector {
	return NewRpcConnector(host, port, timeout)
}

func (c *ZxConnector) Rpc() facade.RpcConnector {
	return c._rpcConnector
}

func (c *ZxConnector) InitRpcConnector(host string, port int, timeout int) facade.Connector {
	c._rpcConnector = NewRpcConnector(host, port, timeout)
	return c
}

func (c *ZxConnector) Csv(csvInputFile string, isFirstRowHeader bool) facade.CsvHandler {
	return NewZxCsvImpl(c._logger, csvInputFile, isFirstRowHeader)
}

// LDAP Client connector.
func (c *ZxConnector) LDAP(config *facade.LDAPConfig) facade.LDAPClient {
	ldapClient := ldap.NewLdapClient(c._logger)
	ldapClient.Config(config)
	return ldapClient
}

func (c *ZxConnector) IsValidSession(username string, token_id string) bool {
	cacheCredential, _ := c._rpcCache.Load(token_id)
	cred := cacheCredential.(facade.OdooCredential)
	if username == cred.User {
		return true
	} else {
		return false
	}
}

func (c *ZxConnector) InvalidateSession(token_id string) {
	c._rpcCache.Remove(token_id)
}
