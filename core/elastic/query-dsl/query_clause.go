package query_dsl

import (
	"strings"
	"techberry-go/common/v2/facade"

	"github.com/olivere/elastic/v7"
)

type QueryDSLImpl struct {
	_boolQuery *elastic.BoolQuery
	_logger    facade.LogEvent
}

func NewQueryDSL(logger facade.LogEvent) facade.QueryDSL {
	var p facade.QueryDSL = &QueryDSLImpl{_logger: logger}
	return p
}

func (c *QueryDSLImpl) SourceCtx(includeFields []string, excludeFields []string) *elastic.FetchSourceContext {
	var (
		fetchSourceCtx *elastic.FetchSourceContext
	)
	if len(includeFields) > 0 || len(excludeFields) > 0 {
		fetchSourceCtx = elastic.NewFetchSourceContext(true)
	} else {
		fetchSourceCtx = elastic.NewFetchSourceContext(false)
	}

	if len(includeFields) > 0 {
		fetchSourceCtx = fetchSourceCtx.Include(includeFields...)
	}
	if len(excludeFields) > 0 {
		fetchSourceCtx = fetchSourceCtx.Exclude(excludeFields...)
	}
	if fetchSourceCtx != nil {
		return fetchSourceCtx
	} else {
		return elastic.NewFetchSourceContext(false)
	}
}

func (c *QueryDSLImpl) NewBoolQuery() facade.QueryDSL {
	c._boolQuery = elastic.NewBoolQuery()
	return c
}

func (c *QueryDSLImpl) SetBoolQuery(boolQuery *elastic.BoolQuery) facade.QueryDSL {
	c._boolQuery = boolQuery
	return c
}

func (c *QueryDSLImpl) BoolQuery() *elastic.BoolQuery {
	return c._boolQuery
}

func (c *QueryDSLImpl) TermsAggregation() *elastic.TermsAggregation {
	return elastic.NewTermsAggregation()
}

func (c *QueryDSLImpl) TermQuery(key string, value interface{}) *elastic.TermQuery {
	return elastic.NewTermQuery(key, value)
}

func (c *QueryDSLImpl) TermsQuery(key string, value []interface{}) *elastic.TermsQuery {
	return elastic.NewTermsQuery(key, value...)
}

func (c *QueryDSLImpl) MustTermByKey(key string, value interface{}) facade.QueryDSL {
	c._boolQuery = c._boolQuery.Must(elastic.NewTermQuery(key, value))
	return c
}

func (c *QueryDSLImpl) MustNotTermByKey(key string, value interface{}) facade.QueryDSL {
	c._boolQuery = c._boolQuery.MustNot(elastic.NewTermQuery(key, value))
	return c
}

func (c *QueryDSLImpl) MustExists(key string) facade.QueryDSL {
	c._boolQuery.Must(elastic.NewExistsQuery(key))
	return c
}

func (c *QueryDSLImpl) MustNotExists(key string) facade.QueryDSL {
	c._boolQuery.MustNot(elastic.NewExistsQuery(key))
	return c
}

func (c *QueryDSLImpl) MustMatchQueryCtx(terms []map[string]interface{}) facade.QueryDSL {
	for _, term := range terms {
		for key, value := range term {
			if s, ok := value.(string); ok {
				if strings.Contains(s, "*") {
					c._boolQuery = c._boolQuery.Must(elastic.NewWildcardQuery(key, s))
				} else {
					c._boolQuery = c._boolQuery.Must(elastic.NewTermQuery(key, value))
				}
			} else {
				c._boolQuery = c._boolQuery.Must(elastic.NewTermQuery(key, value))
			}
		}
	}
	return c
}

func (c *QueryDSLImpl) ShouldMatchQueryCtx(terms []map[string]interface{}) facade.QueryDSL {
	for _, term := range terms {
		for key, value := range term {
			c._boolQuery = c._boolQuery.Should(elastic.NewTermQuery(key, value))
		}
	}
	if len(terms) > 0 {
		c._boolQuery = c._boolQuery.MinimumNumberShouldMatch(1)
	}
	return c
}

/*

	{
		"s_part_no": ["a*", "b*"]
		"name": ["a", "b"]
	}
*/

func (c *QueryDSLImpl) ShouldWildcardCtx(terms map[string]interface{}) facade.QueryDSL {
	for key, value := range terms {
		v_array := value.([]string)
		var query []elastic.Query
		for _, v := range v_array {
			query = append(query, elastic.NewWildcardQuery(key, v))
		}
		c._boolQuery = c._boolQuery.Should(query...)
	}
	return c
}

func (c *QueryDSLImpl) FilterTerms(terms []map[string]interface{}) facade.QueryDSL {
	for _, term := range terms {
		for key, value := range term {
			c._boolQuery = c._boolQuery.Filter(elastic.NewTermQuery(key, value))
		}
	}

	return c
}

func (c *QueryDSLImpl) DateMillisRangeCtx(name string, startMillis int64, endMillis int64) facade.QueryDSL {
	c._boolQuery = c._boolQuery.Must(elastic.NewRangeQuery(name).Gte(startMillis).Lte(endMillis))
	return c
}
