package core

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"techberry-go/common/v2/facade"
	"time"

	"github.com/go-redis/redis/v8"
)

type ZxCacheImpl struct {
	client *redis.Client
	_debug bool
	_ctx   context.Context
}

func NewZxCacheImpl(host string, port int, db int, poolSize int) facade.CacheHandler {
	address := fmt.Sprintf("%s:%d", host, port)
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "", // no password set
		DB:       db, // use default DB
		PoolSize: poolSize,
	})
	ctx := context.Background()

	_, err := client.Ping(ctx).Result()
	if err != nil {
		fmt.Println(err)
	}
	var p facade.CacheHandler = &ZxCacheImpl{client: client, _ctx: ctx}
	return p
}

func (c *ZxCacheImpl) Keys(keyPattern string) ([]string, error) {
	stringSliceCmd := c.client.Keys(c._ctx, keyPattern)
	if stringSliceCmd.Err() != nil {
		return nil, stringSliceCmd.Err()
	} else {
		s, err := stringSliceCmd.Result()
		if err != nil {
			return nil, err
		}
		sort.Strings(s)
		return s, nil
	}
}

func (c *ZxCacheImpl) IsExist(key string) bool {
	_, err := c.client.Exists(c._ctx, key).Result()
	if err != nil {
		return false
	} else {
		return true
	}
}

func (c *ZxCacheImpl) Del(key string) error {
	_, err := c.client.Del(c._ctx, key).Result()
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (c *ZxCacheImpl) Get(key string, autoRemove bool) (string, error) {
	val, err := c.client.Get(c._ctx, key).Result()
	if err == redis.Nil {
		return "", errors.New("Key not found.")
	} else if err != nil {
		return "", err
	} else {
		if autoRemove {
			_, err := c.client.Del(c._ctx, key).Result()
			if err != nil {
				return val, err
			}
		}
		return val, nil
	}
}

func (c *ZxCacheImpl) GetJson(key string, autoRemove bool) (facade.JsonHandler, error) {
	val, err := c.client.Get(c._ctx, key).Result()
	if err == redis.Nil {
		return nil, errors.New("Key not found.")
	} else if err != nil {
		return nil, err
	} else {
		jsonParser := NewZxJsonImpl([]byte(val))
		if autoRemove {
			_, err := c.client.Del(c._ctx, key).Result()
			if err != nil {
				return jsonParser, err
			}
		}
		return jsonParser, nil
	}
}

func (c *ZxCacheImpl) SetJson(key string, value interface{}, expiration time.Duration) error {
	s := ""
	switch f := value.(type) {
	case map[string]interface{}:
		b, err := MapUtils.ToJson(f)
		if err != nil {
			return err
		} else {
			s = string(b)
		}
	case interface{}:
		b, err := StructUtils.ToJson(f)
		if err != nil {
			return err
		} else {
			s = string(b)
		}
	default:
		return errors.New("Can't convert data into string.")
	}
	_, err := c.client.Set(c._ctx, key, s, expiration).Result()
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (c *ZxCacheImpl) Set(key string, value string, expiration time.Duration) error {
	_, err := c.client.Set(c._ctx, key, value, expiration).Result()
	if err != nil {
		return err
	} else {
		return nil
	}
}
