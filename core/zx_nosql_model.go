package core

import (
	"errors"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Create, Read, Update, Delete, Search, Move, Copy

type ZxNoSqlModel struct {
	inputData         interface{}
	_collection       string
	_targetCollection string
	ClientConnection  *mongo.Client
	_connectionUrl    string

	_searchBinding     func(interface{}) interface{}
	_filterTask        string
	_filter            func(string, interface{}, interface{}) (map[string]interface{}, error)
	_sortTask          string
	_sort              func(string, interface{}, interface{}) (interface{}, error)
	_adapter           facade.Adapter
	_targetAdapter     facade.Adapter
	_bindDataModelTask []string
	_bindDataModel     []func(string, interface{}, interface{}) (interface{}, error)
	_preExecuteTask    []string
	_preExecute        []func(facade.Adapter, interface{}) error
	_postExecuteTask   []string
	_postExecute       []func(facade.Adapter, interface{}, interface{}, string) error
	_dataModel         interface{} // Pointer to main data model.
	_debug             bool
	_logger            facade.LogEvent
}

// Connect establish a connection to database
func NewZxNoSqlModel(logger facade.LogEvent, store facade.CRUDDocumentStore) facade.NoSqlHandler {
	nosql := ZxNoSqlModel{}
	var p facade.NoSqlHandler = &nosql
	nosql._adapter = NewZxAdapter(logger)
	nosql._adapter.SetNoSql(store)
	nosql._logger = logger
	return p
}

func NewZxNoSqlFromAdapter(adapter facade.Adapter) facade.NoSqlHandler {
	nosql := ZxNoSqlModel{}
	var p facade.NoSqlHandler = &nosql
	nosql._adapter = adapter
	return p
}

func (c *ZxNoSqlModel) Debug(debug bool) {
	c._debug = debug
}

func (c *ZxNoSqlModel) SetAdapter(adapter facade.Adapter) {
	c._adapter = adapter
}

func (c *ZxNoSqlModel) SetTargetAdapter(adapter facade.Adapter) {
	c._targetAdapter = adapter
}

func (c *ZxNoSqlModel) SetDataModel(model interface{}) {
	c._dataModel = model
}

func (c *ZxNoSqlModel) SetInputData(input interface{}) {
	c.inputData = input
}

func (c *ZxNoSqlModel) SetCollection(collection string) {
	c._collection = collection
}

func (c *ZxNoSqlModel) SetTargetCollection(collection string) {
	c._targetCollection = collection
}

func (c *ZxNoSqlModel) SearchBinding(m func(interface{}) interface{}) facade.NoSqlHandler {
	c._searchBinding = m
	return c
}

func (c *ZxNoSqlModel) Filter(task string, m func(string, interface{}, interface{}) (map[string]interface{}, error)) facade.NoSqlHandler {
	c._filter = m
	c._filterTask = task
	return c
}

func (c *ZxNoSqlModel) Sort(task string, m func(string, interface{}, interface{}) (interface{}, error)) facade.NoSqlHandler {
	c._sort = m
	c._sortTask = task
	return c
}

func (c *ZxNoSqlModel) BindDataModel(task string, m func(string, interface{}, interface{}) (interface{}, error)) facade.NoSqlHandler {
	c._bindDataModel = append(c._bindDataModel, m)
	c._bindDataModelTask = append(c._bindDataModelTask, task)
	return c
}

func (c *ZxNoSqlModel) PreExecute(task string, m func(facade.Adapter, interface{}) error) facade.NoSqlHandler {
	c._preExecute = append(c._preExecute, m)
	c._preExecuteTask = append(c._preExecuteTask, task)
	return c
}

func (c *ZxNoSqlModel) PostExecute(task string, m func(facade.Adapter, interface{}, interface{}, string) error) facade.NoSqlHandler {
	c._postExecute = append(c._postExecute, m)
	c._postExecuteTask = append(c._postExecuteTask, task)
	return c
}

// Pre-Execute
// Binding Data Model
// Post-Execute
func (c *ZxNoSqlModel) Create(filter map[string]interface{}) (string, error) {
	var (
		err      error  = nil
		_id      string = ""
		status   bool   = false
		newModel interface{}
	)
	defer c._timeTrack(time.Now(), "Create()")

	// 1. Pre-Execute
	// Ex. preExecute("Convert Input")
	//     preExecute("Checking Data")
	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-create task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	// Automatically checking object into target. IsExist?
	status, _id = c.IsExist(filter, c._dataModel)
	if status == false {
		// Prepare data into model.
		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_id, c.inputData, c._dataModel)
			if err != nil {
				return "", err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}
		_id, err = c._adapter.NoSql().Create(c._collection, c._dataModel)
		if err != nil {
			c._logger.Debug().MsgError(err)
			return "", err
		}
		c._logger.OnDemandTrace("Object Id = %s is created.", _id)
	} else {
		c._logger.OnDemandTrace("Object Id = %s exist.", _id)
	}

	// Ex. postExecute("Calculate data")
	//     postExecute("Update status")
	for i, f := range c._postExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData, c._dataModel, _id)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute post-create task (%s) took : %d", c._postExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	return _id, nil
}

// Pre-Execute
// Filter
// Binding Data Model
// Post-Execute
func (c *ZxNoSqlModel) Update(filter map[string]interface{}) error {
	var (
		updateFilter map[string]interface{}
		_id          string = ""
		status       bool   = false
		newModel     interface{}
		err          error
	)

	defer c._timeTrack(time.Now(), "Update()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-update task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return err
		}
	}

	status, _id = c.IsExist(filter, c._dataModel)
	if status {
		if c._filter != nil {
			start := time.Now()
			updateFilter, _ = c._filter(_id, c.inputData, c._dataModel)
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute get filter task (%s) took : %d", c._bindDataModelTask, elapsed)
			}
		} else {
			updateFilter = filter
		}

		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_id, c.inputData, c._dataModel)
			if err != nil {
				return err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}
	} else {
		c._logger.OnDemandTrace("No any document found")
		return errors.New("No any document found.")
	}

	switch t := c._dataModel.(type) {
	case map[string]interface{}:
		update := bson.M{"$set": t}
		count := c._adapter.NoSql().Update(c._collection, updateFilter, update)
		if count > 0 {
			c._logger.OnDemandTrace("Update document successfully.")
			for i, f := range c._postExecute {
				start := time.Now()
				err := f(c._adapter, c.inputData, c._dataModel, _id)
				if c._debug {
					elapsed := time.Since(start)
					c._logger.OnDemandTrace("Execute post-create task (%s) took : %d", c._postExecuteTask[i], elapsed)
				}
				if err != nil {
					c._logger.Debug().MsgError(err)
					return err
				}
			}
			return nil
		} else {
			c._logger.OnDemandTrace("Update document failed.")
			return errors.New("Update document failed.")
		}
	default:
		c._logger.OnDemandTrace("Document model is not supported.")
		return errors.New("Document model is not supported.")
	}
}

func (c *ZxNoSqlModel) IsExist(filter map[string]interface{}, model interface{}) (bool, string) {
	defer c._timeTrack(time.Now(), "IsExist()")

	return c._adapter.NoSql().IsExist(c._collection, filter, model)
}

func (c *ZxNoSqlModel) Delete(_id string) error {
	defer c._timeTrack(time.Now(), "Delete()")

	status := c._adapter.NoSql().DeleteById(c._collection, _id)
	if status {
		return nil
	} else {
		return errors.New("Can't delete document.")
	}
}

func (c *ZxNoSqlModel) DeleteItemByMap(_parentId string, _child map[string]interface{}, path string) error {
	defer c._timeTrack(time.Now(), "Delete()")

	objectId, err := primitive.ObjectIDFromHex(_parentId)
	if err != nil {
		return errors.New("Object is not valid.")
	} else {
		filter := bson.M{
			"_id": objectId,
		}
		pullFromArray := bson.M{"$pull": bson.M{path: _child}}
		count := c._adapter.NoSql().Update(c._collection, filter, pullFromArray)
		if count > 0 {
			c._logger.OnDemandTrace("Delete item in document successfully.")
			for i, f := range c._postExecute {
				start := time.Now()
				err := f(c._adapter, c.inputData, c._dataModel, _parentId)
				if c._debug {
					elapsed := time.Since(start)
					c._logger.OnDemandTrace("Execute post-create task (%s) took : ", c._postExecuteTask[i], elapsed)
				}
				if err != nil {
					c._logger.Debug().MsgError(err)
					return err
				}
			}
			return nil
		} else {
			c._logger.OnDemandTrace("Delete item in document failed.")
			return errors.New("Delete item in document failed.")
		}
	}
}

func (c *ZxNoSqlModel) DeleteItem(_parentId string, _childId string, path string) error {
	defer c._timeTrack(time.Now(), "Delete()")

	itemObjectId, err1 := primitive.ObjectIDFromHex(_childId)
	objectId, err2 := primitive.ObjectIDFromHex(_parentId)
	if err1 != nil && err2 != nil {
		return errors.New("Object is not valid.")
	} else {
		filter := bson.M{
			"_id": objectId,
		}
		pullFromArray := bson.M{"$pull": bson.M{path: bson.M{
			"_id": itemObjectId,
		}}}
		count := c._adapter.NoSql().Update(c._collection, filter, pullFromArray)
		if count > 0 {
			c._logger.OnDemandTrace("Delete item in document successfully.")
			for i, f := range c._postExecute {
				start := time.Now()
				err := f(c._adapter, c.inputData, c._dataModel, _parentId)
				if c._debug {
					elapsed := time.Since(start)
					c._logger.OnDemandTrace("Execute post-create task (%s) took : %d", c._postExecuteTask[i], elapsed)
				}
				if err != nil {
					c._logger.Debug().MsgError(err)
					return err
				}
			}
			return nil
		} else {
			c._logger.OnDemandTrace("Delete item in document failed.")
			return errors.New("Delete item in document failed.")
		}
	}
}

// Pre-Execute
// Binding Data Model
// Post-Execute
func (c *ZxNoSqlModel) AddItem(_parentId string, parentModel interface{}, path string) error {
	var (
		model    interface{}
		status   bool = false
		newModel interface{}
		err      error
	)
	defer c._timeTrack(time.Now(), "AppendChildItem()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-append_child_item task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return err
		}
	}

	objectId, err := primitive.ObjectIDFromHex(_parentId)
	if err != nil {
		return err
	}

	filter := bson.M{
		"_id": objectId,
	}

	status, _parentId = c.IsExist(filter, parentModel)
	c._logger.OnDemandTrace("Check existing document status : %t, _parentId = %s", status, _parentId)
	if status {
		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_parentId, c.inputData, c._dataModel)
			if err != nil {
				return err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}

		pushToArray := bson.M{"$push": bson.M{path: c._dataModel}}
		count := c._adapter.NoSql().Update(c._collection, filter, pushToArray)
		c._logger.OnDemandTrace("Append child item document count : %d", count)
		if count > 0 {
			for i, f := range c._postExecute {
				start := time.Now()
				err := f(c._adapter, c.inputData, model, _parentId)
				if c._debug {
					elapsed := time.Since(start)
					c._logger.OnDemandTrace("Execute post-append_child_item task (%s) took : %d", c._postExecuteTask[i], elapsed)
				}
				if err != nil {
					return err
				}
			}
			return nil
		} else {
			c._logger.OnDemandTrace("Append child item document failed.")
			return errors.New("Append child item document failed.")
		}
	} else {
		c._logger.OnDemandTrace("No any document found.")
		return errors.New("No any document found.")
	}
}

// Pre-Execute
// Binding Data Model
// Post-Execute
func (c *ZxNoSqlModel) Read(filter map[string]interface{}) (string, interface{}, error) {
	var (
		_id      string = ""
		status   bool   = false
		newModel interface{}
		err      error
	)
	defer c._timeTrack(time.Now(), "Read()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-read task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return _id, nil, err
		}
	}
	status, _id = c.IsExist(filter, c._dataModel)
	if status {
		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_id, c.inputData, c._dataModel)
			if err != nil {
				return "", nil, err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}
	} else {
		return _id, nil, errors.New("No any document found.")
	}

	for i, f := range c._postExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData, c._dataModel, _id)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute post-read task (%s) took : %d", c._postExecuteTask[i], elapsed)
		}
		if err != nil {
			return _id, nil, err
		}
	}
	return _id, c._dataModel, nil
}

// Pre-Execute
// Filter
// Sort
func (c *ZxNoSqlModel) Search(offset int64, limit int64) ([]interface{}, error) {
	var (
		searchFilter map[string]interface{}
		searchSort   interface{}
		err          error
		modelList    []interface{}
		_id          string
	)

	defer c._timeTrack(time.Now(), "Search()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-read task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return nil, err
		}
	}
	if c._filter != nil {
		start := time.Now()
		searchFilter, err = c._filter(_id, c.inputData, c._dataModel)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute get filter task (%s) took : %d", c._filterTask, elapsed)
		}
		if err != nil {
			return nil, err
		}
	}

	if c._sort != nil {
		start := time.Now()
		searchSort, _ = c._sort(_id, c.inputData, c._dataModel)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute get sort task (%s) took : %d", c._sortTask, elapsed)
		}
		if err != nil {
			return nil, err
		}
	}
	// Default search all. If quoteStatus and quoteNumber set not specified.
	findOptions := options.Find()   // build a `findOptions`
	findOptions.SetSort(searchSort) // reverse order by `when`
	findOptions.SetSkip(offset)     // skip whatever you want, like `offset` clause in mysql
	findOptions.SetLimit(limit)     // like `limit` clause in mysql
	if len(searchFilter) <= 0 {
		searchFilter = nil
	}

	err = c._adapter.NoSql().ReadMany(c._collection, searchFilter, &modelList, findOptions, c._searchBinding)
	if err != nil {
		c._logger.OnDemandTrace("Can't search quote documents, Error : %s", err.Error())
		return modelList, err
	}
	return modelList, nil
}

func (c *ZxNoSqlModel) Move(filter map[string]interface{}) (string, error) {
	var (
		err       error  = nil
		_sourceId string = ""
		_targetId string = ""
		status    bool   = false
		newModel  interface{}
	)
	defer c._timeTrack(time.Now(), "Move()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-create task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	// Automatically checking object into target. IsExist?
	status, _sourceId = c.IsExist(filter, c._dataModel)
	if status {
		// Prepare data into model.
		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_sourceId, c.inputData, c._dataModel)
			if err != nil {
				return "", err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}
		// Create target document.
		_targetId, err = c._targetAdapter.NoSql().Create(c._targetCollection, c._dataModel)
		if err != nil {
			c._logger.Debug().MsgError(err)
			return "", err
		}
		c._logger.OnDemandTrace("Target Id = %s is created.", _targetId)

		// Delete source document.
		status = c._adapter.NoSql().DeleteById(c._collection, _sourceId)
		if err != nil {
			c._logger.Debug().MsgError(err)
			return "", err
		}
		c._logger.OnDemandTrace("Source Id = %s is deleted.", _sourceId)
	} else {
		c._logger.OnDemandTrace("Source Id = %s is not exist.", _sourceId)
	}

	for i, f := range c._postExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData, c._dataModel, _targetId)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute post-create task (%s) took : %d", c._postExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	return _targetId, nil
}

func (c *ZxNoSqlModel) Copy(filter map[string]interface{}) (string, error) {
	var (
		err       error  = nil
		_sourceId string = ""
		_targetId string = ""
		status    bool   = false
		newModel  interface{}
	)
	defer c._timeTrack(time.Now(), "Copy()")

	for i, f := range c._preExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute pre-create task (%s) took : %d", c._preExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	// Automatically checking object into target. IsExist?
	status, _sourceId = c.IsExist(filter, c._dataModel)
	if status {
		// Prepare data into model.
		for i, f := range c._bindDataModel {
			start := time.Now()
			newModel, err = f(_sourceId, c.inputData, c._dataModel)
			if err != nil {
				return "", err
			} else {
				if newModel != nil {
					c._dataModel = newModel
				}
			}
			if c._debug {
				elapsed := time.Since(start)
				c._logger.OnDemandTrace("Execute bind data model task (%s) took : %d", c._bindDataModelTask[i], elapsed)
			}
		}
		// Create target document.
		_targetId, err = c._targetAdapter.NoSql().Create(c._targetCollection, c._dataModel)
		if err != nil {
			c._logger.Debug().MsgError(err)
			return "", err
		}
		c._logger.OnDemandTrace("Target Id = %s is created.", _targetId)
	} else {
		c._logger.OnDemandTrace("Source Id = %s is not exist.", _sourceId)
	}

	for i, f := range c._postExecute {
		start := time.Now()
		err := f(c._adapter, c.inputData, c._dataModel, _targetId)
		if c._debug {
			elapsed := time.Since(start)
			c._logger.OnDemandTrace("Execute post-create task (%s) took : %d", c._postExecuteTask[i], elapsed)
		}
		if err != nil {
			return "", err
		}
	}
	return _targetId, nil
}

func (c *ZxNoSqlModel) PrepareUpdateField(changes map[string]interface{}, field string, value interface{}) map[string]interface{} {
	stringHandler := impl.NewStringImpl()

	switch t := value.(type) {
	case string:
		t = stringHandler.String(t, "")
		if stringHandler.IsNotEmptyString(t) {
			changes[field] = value
		}
	default:
		changes[field] = t
	}
	return changes
}

func (c *ZxNoSqlModel) _timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	c._logger.OnDemandTrace("%s took %d", name, elapsed)
}
