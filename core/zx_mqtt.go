package core

import (
	"crypto/tls"
	"techberry-go/common/v2/facade"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type ZxMqttImpl struct {
	_client        mqtt.Client
	_username      string
	_passwd        string
	_clientId      string
	_url           string
	_publishMode   bool
	_subscribeMode bool
	_logger        facade.LogEvent
}

func NewZxMqttImpl(logger facade.LogEvent, clientId string, url string, username string, password string) facade.MqttClient {
	var p facade.MqttClient = &ZxMqttImpl{
		_url:      url,
		_username: username,
		_passwd:   password,
		_clientId: clientId,
		_logger:   logger,
	}
	return p
}

func (c *ZxMqttImpl) _connectMqtt(opts *mqtt.ClientOptions) mqtt.Client {
	//opts := c._createMqttClientOptions(clientId, url)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		c._logger.Debug().MsgError(err)
	} else {
		if c._publishMode {
			c._logger.OnDemandTrace("Connected to %s from publish mode", c._url)
		} else {
			c._logger.OnDemandTrace("Connected to %s from subscribe mode", c._url)
		}
	}
	return client
}

func (c *ZxMqttImpl) _createMqttClientOptions(clientId string, url string) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(url)
	opts.SetUsername(c._username)
	opts.SetPassword(c._passwd)
	opts.SetClientID(clientId)
	tlsConfig := &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	opts.SetTLSConfig(tlsConfig)
	return opts
}

func (c *ZxMqttImpl) ClientId() string {
	return c._clientId
}

func (c *ZxMqttImpl) Subscribe(topic string, qos byte, callback func(topic string, msgPayload string)) {
	c._subscribeMode = true
	opts := c._createMqttClientOptions(c._clientId, c._url)
	opts.OnConnect = func(client mqtt.Client) {
		if token := client.Subscribe(topic, qos, func(client mqtt.Client, msg mqtt.Message) {
			callback(msg.Topic(), string(msg.Payload()))
			c._logger.OnDemandTrace("* [%s] %s", msg.Topic(), string(msg.Payload()))
			//fmt.Printf("* [%s] %s\n", msg.Topic(), string(msg.Payload()))
		}); token.Wait() && token.Error() != nil {
			c._logger.OnDemandTrace(token.Error().Error())
		}
	}
	client := c._connectMqtt(opts)
	c._client = client
	/*
		c._client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {
			callback(msg.Topic(), string(msg.Payload()))
			//fmt.Printf("* [%s] %s\n", msg.Topic(), string(msg.Payload()))
		})
	*/
}

func (c *ZxMqttImpl) Unsubscribe(topic string) error {
	c._subscribeMode = false
	if token := c._client.Unsubscribe(topic); token.Wait() && token.Error() != nil {
		c._logger.OnDemandTrace(token.Error().Error())
		return token.Error()
	} else {
		return nil
	}
}

func (c *ZxMqttImpl) Publish(topic string, messageId string, qos byte, retained bool, payload interface{}) string {
	if StringUtils.IsEmptyString(topic) {
		uuid := StringUtils.GenerateUuid(false)
		topic = uuid
	}
	c._publishMode = true
	opts := c._createMqttClientOptions(c._clientId, c._url)
	client := c._connectMqtt(opts)
	c._client = client
	token := c._client.Publish(topic, qos, retained, payload)
	if token.Error() != nil {
		c._logger.OnDemandTrace(token.Error().Error())
	}
	return topic
}

func (c *ZxMqttImpl) Disconnect() {
	c._publishMode = false
	c._subscribeMode = false
	c._client.Disconnect(250)
}
