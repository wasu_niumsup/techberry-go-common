package core

import (
	"archive/zip"
	"bytes"
	"fmt"
	"image"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"techberry-go/common/v2/facade"
	"time"
)

var rand_number uint32
var randmu sync.Mutex

var badCharacters = []string{
	"../",
	"<!--",
	"-->",
	"<",
	">",
	"'",
	"\"",
	"&",
	"$",
	"#",
	"{", "}", "[", "]", "=",
	";", "?", "%20", "%22",
	"%3c",   // <
	"%253c", // <
	"%3e",   // >
	"",      // > -- fill in with % 0 e - without spaces in between
	"%28",   // (
	"%29",   // )
	"%2528", // (
	"%26",   // &
	"%24",   // $
	"%3f",   // ?
	"%3b",   // ;
	"%3d",   // =
}

const (
	// sniff Length, use for detect file mime type
	MimeSniffLen = 512
)

var (
	// refer net/http package
	imageMimeTypes = map[string]string{
		"bmp": "image/bmp",
		"gif": "image/gif",
		"ief": "image/ief",
		"jpg": "image/jpeg",
		// "jpe":  "image/jpeg",
		"jpeg": "image/jpeg",
		"png":  "image/png",
		"svg":  "image/svg+xml",
		"ico":  "image/x-icon",
		"webp": "image/webp",
	}
)

type ZxFileImpl struct {
	_logger facade.LogEvent
}

func NewZxFileImpl(logger facade.LogEvent) facade.FileHandler {
	var p facade.FileHandler = &ZxFileImpl{_logger: logger}
	return p
}

func (c *ZxFileImpl) resolve(name string) string {
	// This implementation is based on Dir.Open's code in the standard net/http package.
	if filepath.Separator != '/' && strings.ContainsRune(name, filepath.Separator) ||
		strings.Contains(name, "\x00") {
		return ""
	}

	dir := string(name)
	if dir == "" {
		dir = "."
	}

	return filepath.Join(dir, filepath.FromSlash(c.slashClean(name)))
}

func (c *ZxFileImpl) slashClean(name string) string {
	if name == "" || name[0] != '/' {
		name = "/" + name
	}
	return path.Clean(name)
}

// Gives a number indicating the device driver to be used to access the passed device
func (c *ZxFileImpl) major(device uint64) uint64 {
	return (device >> 8) & 0xfff
}

// Gives a number that serves as a flag to the device driver for the passed device
func (c *ZxFileImpl) minor(device uint64) uint64 {
	return (device & 0xff) | ((device >> 12) & 0xfff00)
}

func (c *ZxFileImpl) mkdev(major int64, minor int64) uint32 {
	return uint32(((minor & 0xfff00) << 12) | ((major & 0xfff) << 8) | (minor & 0xff))
}

func (c *ZxFileImpl) mkdirAllNewAs(path string, mode os.FileMode, ownerUID, ownerGID int) error {
	// make an array containing the original path asked for, plus (for mkAll == true)
	// all path components leading up to the complete path that don't exist before we MkdirAll
	// so that we can chown all of them properly at the end.  If chownExisting is false, we won't
	// chown the full directory path if it exists
	var paths []string
	if _, err := os.Stat(path); err != nil && os.IsNotExist(err) {
		paths = []string{path}
	} else if err == nil {
		// nothing to do; directory path fully exists already
		return nil
	}

	// walk back to "/" looking for directories which do not exist
	// and add them to the paths array for chown after creation
	dirPath := path
	for {
		dirPath = filepath.Dir(dirPath)
		if dirPath == "/" {
			break
		}
		if _, err := os.Stat(dirPath); err != nil && os.IsNotExist(err) {
			paths = append(paths, dirPath)
		}
	}

	if err := os.MkdirAll(path, mode); err != nil && !os.IsExist(err) {
		return err
	}

	// even if it existed, we will chown the requested path + any subpaths that
	// didn't exist when we called MkdirAll
	for _, pathComponent := range paths {
		if err := os.Chown(pathComponent, ownerUID, ownerGID); err != nil {
			return err
		}
	}
	return nil
}

// CopyFile copies a file from source to dest and returns
// an error if any.
func (c *ZxFileImpl) CopyFile(source string, dest string) error {
	si, err := os.Lstat(source)
	if err != nil {
		return err
	}

	st, ok := si.Sys().(*syscall.Stat_t)
	if !ok {
		return fmt.Errorf("could not convert to syscall.Stat_t")
	}

	uid := int(st.Uid)
	gid := int(st.Gid)

	// Handle symlinks
	if si.Mode()&os.ModeSymlink != 0 {
		target, err := os.Readlink(source)
		if err != nil {
			return err
		}
		if err := os.Symlink(target, dest); err != nil {
			return err
		}
	}

	// Handle device files
	if st.Mode&syscall.S_IFMT == syscall.S_IFBLK || st.Mode&syscall.S_IFMT == syscall.S_IFCHR {
		devMajor := int64(c.major(uint64(st.Rdev)))
		devMinor := int64(c.minor(uint64(st.Rdev)))
		mode := uint32(si.Mode() & 07777)
		if st.Mode&syscall.S_IFMT == syscall.S_IFBLK {
			mode |= syscall.S_IFBLK
		}
		if st.Mode&syscall.S_IFMT == syscall.S_IFCHR {
			mode |= syscall.S_IFCHR
		}
		if err := syscall.Mknod(dest, mode, int(c.mkdev(devMajor, devMinor))); err != nil {
			return err
		}
	}

	// Handle regular files
	if si.Mode().IsRegular() {
		sf, err := os.Open(source)
		if err != nil {
			return err
		}
		defer sf.Close()

		df, err := os.Create(dest)
		if err != nil {
			return err
		}
		defer df.Close()

		_, err = io.Copy(df, sf)
		if err != nil {
			return err
		}
	}

	// Chown the file
	if err := os.Lchown(dest, uid, gid); err != nil {
		return err
	}

	// Chmod the file
	if !(si.Mode()&os.ModeSymlink == os.ModeSymlink) {
		if err := os.Chmod(dest, si.Mode()); err != nil {
			return err
		}
	}

	return nil
}

// CopyDir copies a directory from source to dest and all
// of its sub-directories. It doesn't stop if it finds an error
// during the copy. Returns an error if any.
func (c *ZxFileImpl) CopyDir(source string, dest string) error {
	fi, err := os.Stat(source)
	if err != nil {
		return err
	}

	// Get owner.
	st, ok := fi.Sys().(*syscall.Stat_t)
	if !ok {
		return fmt.Errorf("could not convert to syscall.Stat_t")
	}

	// We have to pick an owner here anyway.
	if err := c.mkdirAllNewAs(dest, fi.Mode(), int(st.Uid), int(st.Gid)); err != nil {
		return err
	}

	return filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Get the relative path
		relPath, err := filepath.Rel(source, path)
		if err != nil {
			return nil
		}

		if info.IsDir() {
			// Skip the source directory.
			if path != source {
				// Get the owner.
				st, ok := info.Sys().(*syscall.Stat_t)
				if !ok {
					return fmt.Errorf("could not convert to syscall.Stat_t")
				}

				uid := int(st.Uid)
				gid := int(st.Gid)

				if err := os.Mkdir(filepath.Join(dest, relPath), info.Mode()); err != nil {
					return err
				}

				if err := os.Lchown(filepath.Join(dest, relPath), uid, gid); err != nil {
					return err
				}
			}
			return nil
		}

		return c.CopyFile(path, filepath.Join(dest, relPath))
	})
}

// IsDir reports whether the named directory exists.
func (c *ZxFileImpl) IsDir(path string) bool {
	if path == "" {
		return false
	}

	if fi, err := os.Stat(path); err == nil {
		return fi.IsDir()
	}
	return false
}

// IsFile reports whether the named file or directory exists.
func (c *ZxFileImpl) IsFile(path string) bool {
	if path == "" {
		return false
	}

	if fi, err := os.Stat(path); err == nil {
		return !fi.IsDir()
	}
	return false
}

// IsAbsPath is abs path.
func (c *ZxFileImpl) IsAbsPath(filepath string) bool {
	return path.IsAbs(filepath)
}

// Mkdir alias of os.Mkdir()
func (c *ZxFileImpl) Mkdir(name string, perm os.FileMode) error {
	return os.Mkdir(name, perm)
}

// MimeType get File Mime Type name. eg "image/png"
func (c *ZxFileImpl) MimeType(path string) (mime string) {
	if path == "" {
		return
	}

	file, err := os.Open(path)
	if err != nil {
		return
	}

	return c.ReaderMimeType(file)
}

// ReaderMimeType get the io.Reader mimeType
// Usage:
// 	file, err := os.Open(filepath)
// 	if err != nil {
// 		return
// 	}
//	mime := ReaderMimeType(file)
func (c *ZxFileImpl) ReaderMimeType(r io.Reader) (mime string) {
	var buf [MimeSniffLen]byte
	n, _ := io.ReadFull(r, buf[:])
	if n == 0 {
		return ""
	}

	return http.DetectContentType(buf[:n])
}

// IsImageFile check file is image file.
func (c *ZxFileImpl) IsImageFile(path string) bool {
	mime := c.MimeType(path)
	if mime == "" {
		return false
	}

	for _, imgMime := range imageMimeTypes {
		if imgMime == mime {
			return true
		}
	}
	return false
}

// IsZipFile check is zip file.
// from https://blog.csdn.net/wangshubo1989/article/details/71743374
func (c *ZxFileImpl) IsZipFile(filepath string) bool {
	f, err := os.Open(filepath)
	if err != nil {
		return false
	}
	defer f.Close()

	buf := make([]byte, 4)
	if n, err := f.Read(buf); err != nil || n < 4 {
		return false
	}

	return bytes.Equal(buf, []byte("PK\x03\x04"))
}

// Unzip a zip archive
// from https://blog.csdn.net/wangshubo1989/article/details/71743374
func (c *ZxFileImpl) Unzip(archive, target string) (err error) {
	reader, err := zip.OpenReader(archive)
	if err != nil {
		return
	}

	if err = os.MkdirAll(target, 0755); err != nil {
		return
	}

	for _, file := range reader.File {
		fullPath := filepath.Join(target, file.Name)
		if file.FileInfo().IsDir() {
			err = os.MkdirAll(fullPath, file.Mode())
			if err != nil {
				return err
			}

			continue
		}

		fileReader, err := file.Open()
		if err != nil {
			return err
		}

		targetFile, err := os.OpenFile(fullPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, file.Mode())
		if err != nil {
			fileReader.Close()
			return err
		}

		_, err = io.Copy(targetFile, fileReader)

		// close all
		fileReader.Close()
		targetFile.Close()

		if err != nil {
			return err
		}
	}

	return
}

func (c *ZxFileImpl) Zip(source, target string) error {
	zipfile, err := os.Create(target)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	info, err := os.Stat(source)
	if err != nil {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		if baseDir != "" {
			header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
		}

		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(writer, file)
		return err
	})

	return err
}

func (c *ZxFileImpl) CreateDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *ZxFileImpl) IsDirectoryOrPathExists(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func (c *ZxFileImpl) RemoveBadCharacters(input string, dictionary []string) string {

	temp := input

	for _, badChar := range dictionary {
		temp = strings.Replace(temp, badChar, "", -1)
	}
	return temp
}

func (c *ZxFileImpl) SanitizeFilename(name string, relativePath bool) string {

	// default settings
	var badDictionary []string = badCharacters

	if name == "" {
		return name
	}

	// if relativePath is TRUE, we preserve the path in the filename
	// If FALSE and will cause upper path foldername to merge with filename
	// USE WITH CARE!!!

	if !relativePath {
		// add additional bad characters
		badDictionary = append(badCharacters, "./")
		badDictionary = append(badDictionary, "/")
	}

	// trim(remove)white space
	trimmed := strings.TrimSpace(name)

	// trim(remove) white space in between characters
	trimmed = strings.Replace(trimmed, " ", "", -1)

	// remove bad characters from filename
	trimmed = c.RemoveBadCharacters(trimmed, badDictionary)

	stripped := strings.Replace(trimmed, "\\", "", -1)

	return stripped
}

func (c *ZxFileImpl) reseed() uint32 {
	return uint32(time.Now().UnixNano() + int64(os.Getpid()))
}

func (c *ZxFileImpl) nextSuffix() string {
	randmu.Lock()
	r := rand_number
	if r == 0 {
		r = c.reseed()
	}
	r = r*1664525 + 1013904223 // constants from Numerical Recipes
	rand_number = r
	randmu.Unlock()
	return strconv.Itoa(int(1e9 + r%1e9))[1:]
}

func (c *ZxFileImpl) CreateTempFile(dir, prefix string, suffix string) (f *os.File, filename string, err error) {
	if dir == "" {
		dir = os.TempDir()
	}
	filename = ""
	nconflict := 0
	for i := 0; i < 10000; i++ {
		filename = prefix + c.nextSuffix() + suffix
		name := filepath.Join(dir, filename)
		f, err = os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
		if os.IsExist(err) {
			if nconflict++; nconflict > 10 {
				randmu.Lock()
				rand_number = c.reseed()
				randmu.Unlock()
			}
			continue
		}
		break
	}
	return
}

func (c *ZxFileImpl) DownloadFile(filepath string, url string) (string, string, error) {
	tempFile, filename, err := c.CreateTempFile(filepath, "", ".csv")
	if err != nil {
		return "", "", err
	}

	// Create the file
	out, err := os.Create(tempFile.Name())
	if err != nil {
		return "", "", err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return "", "", err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return "", "", err
	}

	return tempFile.Name(), filename, nil
}

func (c *ZxFileImpl) GetImageDimension(imagePath string) (int, int) {
	file, err := os.Open(imagePath)
	if err == nil {
		image, _, err := image.DecodeConfig(file)
		if err == nil {
			return image.Width, image.Height
		}
	}
	return 0, 0
}

func (c *ZxFileImpl) ReadFile(filename string) ([]byte, error) {
	return ioutil.ReadFile(filename)
}

func (c *ZxFileImpl) ReadJsonFile(filename string) (facade.JsonHandler, error) {
	b, err := c.ReadFile(filename)
	if err != nil {
		if c._logger != nil {
			c._logger.Debug().MsgError(err)
		}
		return nil, err
	}
	return NewZxJsonImpl(b), nil
}

func (c *ZxFileImpl) WriteFile(filename string, data []byte) error {
	return ioutil.WriteFile(filename, data, 0644)
}

func (c *ZxFileImpl) ListFilesPattern(dir_with_pattern string) ([]string, error) {
	fileList := []string{}
	files, err := filepath.Glob(dir_with_pattern)
	if err != nil {
		return fileList, err
	}

	for _, file := range files {
		fileList = append(fileList, file)
	}
	return fileList, nil
}

func (c *ZxFileImpl) ListFiles(dir, pattern string) ([]os.FileInfo, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	filteredFiles := []os.FileInfo{}
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		matched, err := regexp.MatchString(pattern, file.Name())
		if err != nil {
			return nil, err
		}
		if matched {
			filteredFiles = append(filteredFiles, file)
		}
	}
	return filteredFiles, nil
}
