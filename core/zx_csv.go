package core

import (
	"bufio"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"techberry-go/common/v2/facade"
)

type ZxCsvImpl struct {
	_logger           facade.LogEvent
	_mc               facade.MediationContext
	_csvInputFile     string
	_reader           *csv.Reader
	_isFirstRowHeader bool
	_headerLine       []string
}

func NewZxCsvImpl(logger facade.LogEvent, csvInputFile string, isFirstRowHeader bool) facade.CsvHandler {
	var p facade.CsvHandler = &ZxCsvImpl{_logger: logger, _csvInputFile: csvInputFile, _isFirstRowHeader: isFirstRowHeader}
	return p
}

func (c *ZxCsvImpl) OpenFile(csvInputFile string) error {
	c._csvInputFile = csvInputFile
	if StringUtils.IsEmptyString(c._csvInputFile) {
		return errors.New("Can't open file because input file path is empty.")
	}
	csvFile, err := os.Open(csvInputFile)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	c._reader = csv.NewReader(bufio.NewReader(csvFile))
	if c._reader != nil {
		if c._isFirstRowHeader {
			line, err := c.Read()
			if err == nil {
				c._headerLine = line
				return nil
			} else {
				return errors.New("Can't read header line.")
			}
		} else {
			return nil
		}
	} else {
		return errors.New("Can't open reader file buffer.")
	}
}

func (c *ZxCsvImpl) Open() error {
	if StringUtils.IsEmptyString(c._csvInputFile) {
		return errors.New("Can't open file because input file path is empty.")
	}
	csvFile, err := os.Open(c._csvInputFile)
	if err != nil {
		c._logger.Debug().MsgError(err)
	}
	c._reader = csv.NewReader(bufio.NewReader(csvFile))
	if c._reader != nil {
		if c._isFirstRowHeader {
			line, err := c.Read()
			if err == nil {
				c._headerLine = line
				return nil
			} else {
				return errors.New("Can't read header line.")
			}
		} else {
			return nil
		}
	} else {
		return errors.New("Can't open reader file buffer.")
	}
}

func (c *ZxCsvImpl) HeaderLine() []string {
	return c._headerLine
}

func (c *ZxCsvImpl) Read() ([]string, error) {
	line, err := c._reader.Read()
	if err == io.EOF {
		return line, err
	} else if err != nil {
		return nil, err
	} else {
		return line, nil
	}
}

func (c *ZxCsvImpl) _getFieldName(tag, key string, s interface{}) (fieldname string) {
	rt := reflect.TypeOf(s)
	c._mc.Logger().Info(rt)
	if rt.Kind() != reflect.Struct {
		panic("bad type")
	}
	for i := 0; i < rt.NumField(); i++ {
		f := rt.Field(i)
		v := strings.Split(f.Tag.Get(key), ",")[0] // use split to ignore tag "options"
		if v == tag {
			return f.Name
		}
	}
	return ""
}

func (c *ZxCsvImpl) ReadModel(model interface{}) (interface{}, error) {
	line, err := c.Read()
	if err == io.EOF {
		return nil, err
	} else if err != nil {
		return nil, err
	}
	clone, err := StructUtils.Clone(model)
	for i, v := range line {
		if err != nil {
			c._logger.Debug().MsgError(err)
			return nil, err
		}
		s := fmt.Sprintf("s%d", i)
		c._logger.OnDemandTrace(s)
		fieldName := c._getFieldName(s, "csv", clone)
		StructUtils.Set(model, fieldName, v)
	}
	return clone, nil
}
