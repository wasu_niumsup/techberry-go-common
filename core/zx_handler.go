package core

import (
	"encoding/json"
	dsl "techberry-go/common/v2/core/elastic/query-dsl"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
)

type ZxHandler struct {
	_quote              facade.Quote
	_stringHandler      facade.StringHandler
	_mapHandler         facade.MapHandler
	_timeHandler        facade.TimeHandler
	_structHandler      facade.StructHandler
	_structMergeHandler facade.StructMerge
	_fileHandler        facade.FileHandler
	_aesEncryptHandler  facade.AesCryptoHandler
	_statementHandler   facade.StatementHandler
	_numericHandler     facade.NumericHandler
	_httpClient         facade.HttpClient
	_queryDSL           facade.QueryDSL
	_objectId           facade.ObjectID
	_logger             facade.LogEvent
}

func NewZxHandler(logger facade.LogEvent) facade.Handler {
	var p facade.Handler = &ZxHandler{_logger: logger}
	return p
}

func (c *ZxHandler) Quote(singleton bool) facade.Quote {
	if !singleton {
		return impl.NewQuoteImpl()
	} else {
		if c._quote == nil {
			c._quote = impl.NewQuoteImpl()
		}
		return c._quote
	}
}

// Handlers
func (c *ZxHandler) String(singleton bool) facade.StringHandler {
	if !singleton {
		return impl.NewStringImpl()
	} else {
		if c._stringHandler == nil {
			c._stringHandler = impl.NewStringImpl()
		}
		return c._stringHandler
	}
}

func (c *ZxHandler) Map(singleton bool) facade.MapHandler {
	if !singleton {
		return impl.NewMapImpl()
	} else {
		if c._mapHandler == nil {
			c._mapHandler = impl.NewMapImpl()
		}
		return c._mapHandler
	}
}

func (c *ZxHandler) Time(singleton bool) facade.TimeHandler {
	if !singleton {
		return impl.NewTimeImpl()
	} else {
		if c._timeHandler == nil {
			c._timeHandler = impl.NewTimeImpl()
		}
		return c._timeHandler
	}
}

func (c *ZxHandler) Struct(singleton bool) facade.StructHandler {
	if !singleton {
		return impl.NewStructImpl()
	} else {
		if c._structHandler == nil {
			c._structHandler = impl.NewStructImpl()
		}
		return c._structHandler
	}
}

func (c *ZxHandler) StructMerge(singleton bool) facade.StructMerge {
	if !singleton {
		return impl.NewStructMergeImpl()
	} else {
		if c._structMergeHandler == nil {
			c._structMergeHandler = impl.NewStructMergeImpl()
		}
		return c._structMergeHandler
	}
}

func (c *ZxHandler) File(singleton bool) facade.FileHandler {
	if !singleton {
		return NewZxFileImpl(c._logger)
	} else {
		if c._fileHandler == nil {
			c._fileHandler = NewZxFileImpl(c._logger)
		}
		return c._fileHandler
	}
}

func (c *ZxHandler) AesCrypto(singleton bool) facade.AesCryptoHandler {
	if !singleton {
		return impl.NewAesCryptoManagerImpl()
	} else {
		if c._aesEncryptHandler == nil {
			c._aesEncryptHandler = impl.NewAesCryptoManagerImpl()
		}
		return c._aesEncryptHandler
	}
}

func (c *ZxHandler) Json(data []byte) facade.JsonHandler {
	//jsonResponse, err := json.Marshal(v)
	return NewZxJsonImpl(data)
}

func (c *ZxHandler) JsonFromStruct(v interface{}) facade.JsonHandler {
	jsonByte, err := json.Marshal(v)
	if err != nil {
		return nil
	}
	return NewZxJsonImpl(jsonByte)
}

func (c *ZxHandler) Statement(singleton bool) facade.StatementHandler {
	if !singleton {
		return impl.NewStatementImpl()
	} else {
		if c._statementHandler == nil {
			c._statementHandler = impl.NewStatementImpl()
		}
		return c._statementHandler
	}
}

func (c *ZxHandler) Numeric(singleton bool) facade.NumericHandler {
	if !singleton {
		return impl.NewNumericImpl()
	} else {
		if c._numericHandler == nil {
			c._numericHandler = impl.NewNumericImpl()
		}
		return c._numericHandler
	}
}

func (c *ZxHandler) HttpClient(singleton bool) facade.HttpClient {
	if !singleton {
		return NewZxHttpClientImpl(c._logger)
	} else {
		if c._httpClient == nil {
			c._httpClient = NewZxHttpClientImpl(c._logger)
		}
		return c._httpClient
	}
}

func (c *ZxHandler) QueryDSL(singleton bool) facade.QueryDSL {
	if !singleton {
		return dsl.NewQueryDSL(c._logger)
	} else {
		if c._queryDSL == nil {
			c._queryDSL = dsl.NewQueryDSL(c._logger)
		}
		return c._queryDSL
	}
}

func (c *ZxHandler) SyncModel(logger facade.LogEvent) facade.SyncHandler {
	c._logger = logger
	return NewZxSyncModel(c._logger)
}

func (c *ZxHandler) NoSqlModel(store facade.CRUDDocumentStore) facade.NoSqlHandler {
	return NewZxNoSqlModel(c._logger, store)
}

func (c *ZxHandler) NoSqlFromAdapter(adapter facade.Adapter) facade.NoSqlHandler {
	return NewZxNoSqlFromAdapter(adapter)
}

func (c *ZxHandler) AsynTask(cache facade.CacheHandler) facade.AsyncTask {
	return NewZxAsyncTask(cache)
}

func (c *ZxHandler) ObjectID() facade.ObjectID {
	return impl.NewObjectID()
}
