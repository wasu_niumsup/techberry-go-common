package core

import (
	"techberry-go/common/v2/facade"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

type ZxKafkaImpl struct {
	_broker  string
	_headers []kafka.Header
	_logger  facade.LogEvent
}

func NewZxKafkaImpl(logger facade.LogEvent, broker string) facade.Kafka {
	var p facade.Kafka = &ZxKafkaImpl{_broker: broker, _logger: logger}
	return p
}

func (c *ZxKafkaImpl) BootstrapServers() string {
	return c._broker
}

func (c *ZxKafkaImpl) AddHeader(key string, value []byte) {
	h := kafka.Header{
		Key:   key,
		Value: value,
	}
	c._headers = append(c._headers, h)
}

func (c *ZxKafkaImpl) Publish(topic string, message []byte) {
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": c._broker, "message.max.bytes": 15 * 1024 * 1024})

	if err != nil {
		c._logger.OnDemandTrace("Failed to create producer: %s", err.Error())
		return
	}

	c._logger.OnDemandTrace("Created Producer %v", p)
	doneChan := make(chan bool)
	go func() {
		defer close(doneChan)
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				m := ev
				if m.TopicPartition.Error != nil {
					c._logger.OnDemandTrace("Delivery failed: %v", m.TopicPartition.Error)
				} else {
					c._logger.OnDemandTrace("Delivered message to topic %s [%d] at offset %v",
						*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
				}
				return

			default:
				c._logger.OnDemandTrace("Ignored event: %v", ev)
			}
		}
	}()

	p.ProduceChannel() <- &kafka.Message{TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: 0}, Value: message, Headers: c._headers}

	// wait for delivery report goroutine to finish
	_ = <-doneChan

	p.Close()
}

/*
func (c *ZxKafkaImpl) debug_format(format string, v ...interface{}) {
	if c._debug {
		s := fmt.Sprintf(format, v...)
		beego.Debug(s)
	}
}

func (c *ZxKafkaImpl) debug(v ...interface{}) {
	if c._debug {
		beego.Debug(v...)
	}
}

func (c *ZxKafkaImpl) trace(format string, v ...interface{}) {
	if c._debug {
		pc, file, line, ok := runtime.Caller(1)
		if !ok {
			file = "?"
			line = 0
		}

		fn := runtime.FuncForPC(pc)
		var fnName string
		if fn == nil {
			fnName = "?()"
		} else {
			dotName := filepath.Ext(fn.Name())
			fnName = strings.TrimLeft(dotName, ".") + "()"
		}
		var values []interface{}
		values = append(values, filepath.Base(file))
		values = append(values, line)
		values = append(values, fnName)
		if v != nil && len(v) > 0 {
			values = append(values, v...)
		}
		s := fmt.Sprintf("%s:%d %s: "+format, values...)
		beego.Debug(s)
	}
}
*/
