package core

import (
	"errors"
	"fmt"
	"techberry-go/common/v2/facade"
)

type ZxDataFactoryImpl struct {
	_host       string
	_port       int
	_httpClient facade.HttpClient
	_logger     facade.LogEvent
}

func NewZxDataFactoryImpl(logger facade.LogEvent, host string, port int) facade.DataFactory {
	httpClient := NewZxHttpClientImpl(logger)
	df := &ZxDataFactoryImpl{_host: host,
		_port:       port,
		_httpClient: httpClient,
		_logger:     logger}
	var p facade.DataFactory = df
	return p
}

func (c *ZxDataFactoryImpl) SetBasicAuth(username string, password string) facade.DataFactory {
	c._httpClient.SetBasicAuth(username, password)
	return c
}

func (c *ZxDataFactoryImpl) SetApiKey(key string, value string) facade.DataFactory {
	c._httpClient.SetHeader(key, value)
	return c
}

func (c *ZxDataFactoryImpl) SelectOne(group string, mapper_id string, params interface{}) (map[string]interface{}, error) {
	response, err := c.ExecuteQuery(group, mapper_id, params)
	if err == nil {
		if response.Hits.Total == 1 {
			return response.Hits.Data[0], nil
		}
	}
	return nil, err
}

func (c *ZxDataFactoryImpl) SelectMany(group string, mapper_id string, params interface{}) ([]map[string]interface{}, error) {
	response, err := c.ExecuteQuery(group, mapper_id, params)
	if err == nil {
		if response.Hits.Total > 0 {
			return response.Hits.Data, nil
		}
	}
	return nil, err
}

func (c *ZxDataFactoryImpl) ExecuteQuery(group string, id string, data interface{}) (facade.DFQueryResult, error) {
	url := fmt.Sprintf("http://%s:%d/df/%s/query/%s", c._host, c._port, group, id)
	c._logger.OnDemandTrace("url : %s", url)
	response, err := c._httpClient.SetHostURL(url).SetBody(data).Post(url)
	result := facade.DFQueryResult{}
	if err != nil {
		c._logger.Debug().Msgf("Error : %s", err.Error())
		return result, err
	} else {
		if response.IsSuccess() {
			if len(response.Body()) > 0 {
				StructUtils.FromJson(response.Body(), &result)
				return result, nil
			}
		}
		return result, errors.New("Execute call query problem.")
	}
}

func (c *ZxDataFactoryImpl) ExecuteUpdate(group string, action string, id string, data interface{}) (facade.DFResult, error) {
	url := ""
	if action == "create" || action == "update" || action == "delete" {
		url = fmt.Sprintf("http://%s:%d/df/%s/%s/%s", c._host, c._port, group, action, id)
	} else {
		return facade.DFResult{}, errors.New("Action is not supported.")
	}

	response, err := c._httpClient.SetBody(data).Post(url)
	result := facade.DFResult{}
	if err != nil {
		return result, err
	} else {
		if response.IsSuccess() {
			if len(response.Body()) > 0 {
				StructUtils.FromJson(response.Body(), &result)
				return result, nil
			}
		}
		return result, errors.New("Execute call update problem.")
	}
}
