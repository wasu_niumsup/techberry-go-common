package core

import (
	"bytes"
	"encoding/json"
	"strconv"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"

	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

type ZxDataBusImpl struct {
	_data   []byte
	_logger facade.LogEvent
}

func NewZxDataBusImpl(logger facade.LogEvent, data []byte) facade.DataBus {
	var jsonHandler facade.JsonHandler
	if data != nil {
		jsonHandler := NewZxJsonImpl(data)
		jsonHandler.SetBlank("response")
	} else {
		b := []byte(`{}`)
		jsonHandler = NewZxJsonImpl(b)
		jsonHandler.SetBlank("response")
		data = jsonHandler.GetData()
	}
	var p facade.DataBus = &ZxDataBusImpl{_data: data, _logger: logger}
	return p
}

func (c *ZxDataBusImpl) Delete(path string) {
	output, _ := sjson.Delete(string(c._data), path)
	c._data = []byte(output)
}

func (c *ZxDataBusImpl) SetBool(value bool, path string) {
	newValue := strconv.FormatBool(value)
	c.Set(newValue, path)
}

func (c *ZxDataBusImpl) SetInt(value int64, path string) {
	newValue := strconv.FormatInt(value, 10)
	c.Set(newValue, path)
}

func (c *ZxDataBusImpl) SetFloat(value float64, path string) {
	newValue := strconv.FormatFloat(value, 'g', 1, 64)
	c.Set(newValue, path)
}

func (c *ZxDataBusImpl) SetString(value string, path string) {
	newValue := strconv.Quote(value)
	c.Set(newValue, path)
}

func (c *ZxDataBusImpl) Set(value interface{}, path string) {
	output, _ := sjson.SetBytes(c._data, path, value)
	c._data = output
}

func (c *ZxDataBusImpl) SetBlank(path string) {
	c.Set(`{}`, path)
}

func (c *ZxDataBusImpl) SetBlankArray(path string) {
	c.Set(`[]`, path)
}

func (c *ZxDataBusImpl) GetData() []byte {
	return c._data
}

func (c *ZxDataBusImpl) GetDataStruct(path string) []byte {
	var json []byte = c._data
	result := gjson.GetBytes(c._data, path)
	var raw []byte
	if result.Index > 0 {
		raw = json[result.Index : result.Index+len(result.Raw)]
	} else {
		raw = []byte(result.Raw)
	}
	return raw
}

func (c *ZxDataBusImpl) Exists(path string) bool {
	return gjson.Get(string(c._data), path).Exists()
}

func (c *ZxDataBusImpl) Value(path string) interface{} {
	return gjson.Get(string(c._data), path).Value()
}

func (c *ZxDataBusImpl) IsArray(path string) bool {
	return gjson.Get(string(c._data), path).IsArray()
}

func (c *ZxDataBusImpl) IsObject(path string) bool {
	return gjson.Get(string(c._data), path).IsObject()
}

func (c *ZxDataBusImpl) String(defaultValue string, path string) string {
	// Default = ""
	stringHandler := impl.NewStringImpl()
	r := gjson.Get(string(c._data), path).String()
	if stringHandler.IsEmptyString(r) && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxDataBusImpl) Bool(defaultValue bool, path string) bool {
	// Default = false
	r := gjson.Get(string(c._data), path).Bool()
	if r == false && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxDataBusImpl) Int(defaultValue int64, path string) int64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Int()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxDataBusImpl) UInt(defaultValue uint64, path string) uint64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Uint()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxDataBusImpl) Float(defaultValue float64, path string) float64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Float()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxDataBusImpl) GetMany(path ...string) []interface{} {
	results := gjson.GetMany(string(c._data), path...)
	var array []interface{}
	for _, r := range results {
		array = append(array, r)
	}
	return array
}

func (c *ZxDataBusImpl) GetArray(path string) (bool, []interface{}) {
	result := gjson.GetBytes(c._data, path)
	if result.IsArray() {
		return true, result.Value().([]interface{})
	} else {
		return false, nil
	}
}

func (c *ZxDataBusImpl) GetBlankJson() interface{} {
	b := []byte(`{}`)
	var data interface{}
	json.Unmarshal(b, &data)
	return data
}

func (c *ZxDataBusImpl) Request() map[string]interface{} {
	b := c.GetDataStruct("request")
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal(b, &jsonMap)
	if err != nil {
		return nil
	}
	return jsonMap
}

func (c *ZxDataBusImpl) RequestBinding(v interface{}) {
	b := c.GetDataStruct("request")
	c._logger.Debug().Msg(string(b))
	json.Unmarshal(b, v)
}

func (c *ZxDataBusImpl) ToMap() map[string]interface{} {
	m := make(map[string]interface{})
	json.Unmarshal(c._data, &m)
	return m
}

func (c *ZxDataBusImpl) ToJsonString() string {
	v := c.ToMap()
	jsonString, err := json.Marshal(v)
	if err == nil {
		return string(jsonString)
	} else {
		return ""
	}
}

func (c *ZxDataBusImpl) _convertDotPathSyntax(nodes ...string) string {
	jsonPath := bytes.NewBufferString("")
	for indx, key := range nodes {
		if indx > 0 {
			jsonPath.WriteString(".")
		}
		jsonPath.WriteString(key)
	}
	return jsonPath.String()
}
