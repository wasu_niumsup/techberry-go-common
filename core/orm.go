package core

import (
	"errors"
	"fmt"
	"strings"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/utils"

	"github.com/jmoiron/sqlx"
)

type ZxOrmImpl struct {
	_conn   *sqlx.DB
	tx      *sqlx.Tx
	_logger facade.LogEvent
}

func NewZxOrmImpl(logger facade.LogEvent, db *sqlx.DB) facade.OrmHandler {
	var p facade.OrmHandler = &ZxOrmImpl{_logger: logger, _conn: db}
	return p
}

func (c *ZxOrmImpl) _getConnection() *sqlx.DB {
	return c._conn
}

func (c *ZxOrmImpl) typeof(v interface{}) string {
	switch t := v.(type) {
	case int:
		return "string"
	case float64:
		return "float64"
	//... etc
	default:
		_ = t
		return "unknown"
	}
}

func (c *ZxOrmImpl) isMap(x interface{}) bool {
	t := fmt.Sprintf("%T", x)
	return strings.HasPrefix(t, "map[")
}

func (c *ZxOrmImpl) BeginTx() error {
	conn := c._getConnection()
	myTx, err := conn.Beginx()
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return errors.New("Can't start transaction.")
	}
	c.tx = myTx
	return nil
}

func (c *ZxOrmImpl) CommitTx() {
	if c.tx != nil {
		c.tx.Commit()
	}
}

func (c *ZxOrmImpl) RollbackTx() {
	if c.tx != nil {
		c.tx.Rollback()
	}
}

func (c *ZxOrmImpl) NamedInsert(pstmt string, value interface{}) (int, error) {
	conn := c._getConnection()
	row, err := conn.NamedQuery(pstmt, value)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
	}
	if !row.Next() {
		return 0, err
	}
	var id int64
	err = row.Scan(&id)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
	}
	row.Close()
	return int(id), err

	/*
		result, err := c.conn.NamedExec(pstmt, args)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				err = nil
			}
		}
		id, _ := result.LastInsertId()
		//err = c.conn.QueryRowx(pstmt, args...).Scan(&id)
		return int(id), err
	*/
}

func (c *ZxOrmImpl) Insert(pstmt string, values ...interface{}) (int, error) {
	conn := c._getConnection()
	row, err := conn.Query(pstmt, values...)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return 0, err
	}
	if row != nil && !row.Next() {
		return 0, err
	}
	var id int64
	err = row.Scan(&id)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
	}
	row.Close()
	return int(id), err
}

func (c *ZxOrmImpl) NamedInsertTx(pstmt string, value interface{}) (int, error) {
	var id int64
	if c.tx != nil {
		conn := c._getConnection()
		row, err := conn.NamedQuery(pstmt, value)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		if !row.Next() {
			return 0, err
		}
		err = row.Scan(&id)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		return int(id), err
	}
	err := errors.New("No transaction to execute insert.")
	return 0, err
}

func (c *ZxOrmImpl) InsertTx(pstmt string, values ...interface{}) (int, error) {
	var id int64
	if c.tx != nil {
		conn := c._getConnection()
		row, err := conn.Query(pstmt, values...)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		if !row.Next() {
			return 0, err
		}
		err = row.Scan(&id)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		return int(id), err
	}
	err := errors.New("No transaction to execute insert.")
	return int(id), err
}

func (c *ZxOrmImpl) FetchAll(pstmt string, args ...interface{}) (int, []interface{}) {
	var (
		rows *sqlx.Rows
		err  error
	)
	if len(args) == 1 && c.isMap(args[0]) {
		conn := c._getConnection()
		rows, err = conn.NamedQuery(pstmt, args[0])
	} else if len(args) == 0 {
		conn := c._getConnection()
		rows, err = conn.Queryx(pstmt)
	} else {
		conn := c._getConnection()
		rows, err = conn.Queryx(pstmt, args...)
	}
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return 0, nil
	}
	var mapContainer []interface{}
	count := 0
	defer rows.Close()
	for rows.Next() {
		result := make(map[string]interface{})
		err = rows.MapScan(result)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
			return 0, nil
		}
		mapContainer = append(mapContainer, result)
		count += 1
	}
	c._logger.OnDemandTrace("Total fetch count is %d", count)
	return count, mapContainer
}

func (c *ZxOrmImpl) FetchAllIds(pstmt string, args ...interface{}) (int, []interface{}) {
	conn := c._getConnection()
	rows, err := conn.Queryx(pstmt, args...)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return 0, nil
	}
	var mapContainer []interface{}
	count := 0
	defer rows.Close()
	for rows.Next() {
		var (
			id int64
		)
		if err := rows.Scan(&id); err != nil {
			//
			c._logger.OnDemandTrace("Error : %v", err)
		} else {
			mapContainer = append(mapContainer, id)
			count += 1
		}
	}
	return count, mapContainer
}

func (c *ZxOrmImpl) FetchOne(pstmt string, args ...interface{}) (map[string]interface{}, error) {
	result := make(map[string]interface{})
	conn := c._getConnection()
	err := conn.QueryRowx(pstmt, args...).MapScan(result)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return result, err
	} else {
		return result, nil
	}
}

func (c *ZxOrmImpl) FetchId(pstmt string, args ...interface{}) int64 {
	result := make(map[string]interface{})
	conn := c._getConnection()
	err := conn.QueryRowx(pstmt, args...).MapScan(result)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
		return 0
	} else {
		var value int64
		if utils.AssertTypeOf(result["id"]) == "int" {
			value = result["id"].(int64)
		} else {
			value = result["id"].(int64)
		}
		return value
	}
}

// Support only a statement. Not supported for transaction management.
func (c *ZxOrmImpl) Update(pstmt string, args ...interface{}) (err error) {
	conn := c._getConnection()
	_, err = conn.Exec(pstmt, args...)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
	}
	return
}

func (c *ZxOrmImpl) UpdateTx(pstmt string, args ...interface{}) (err error) {
	if c.tx != nil {
		_, err = c.tx.Exec(pstmt, args...)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		return
	}
	err = errors.New("No transaction to execute update.")
	return
}

func (c *ZxOrmImpl) Delete(pstmt string, args ...interface{}) (err error) {
	conn := c._getConnection()
	_, err = conn.Exec(pstmt, args...)
	if err != nil {
		c._logger.OnDemandTrace("Error : %v", err)
	}
	return
}

func (c *ZxOrmImpl) DeleteTx(pstmt string, args ...interface{}) (err error) {
	if c.tx != nil {
		_, err = c.tx.Exec(pstmt, args...)
		if err != nil {
			c._logger.OnDemandTrace("Error : %v", err)
		}
		return
	}
	err = errors.New("No transaction to execute delete.")
	return
}

func (c *ZxOrmImpl) GetDriverName() string {
	conn := c._getConnection()
	if conn.DriverName() == "postgres" {
		return "postgresql"
	} else if conn.DriverName() == "godror" {
		return "oracle"
	} else {
		return "unsupport"
	}
}

func (c *ZxOrmImpl) Close() {
	c._conn.Close()
}
