package core

import (
	"encoding/json"
	"errors"
	"fmt"

	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"techberry-go/common/v2/utils"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/fatih/structs"
	"gopkg.in/resty.v1"
)

type ZxContext struct {
	_handler         facade.Handler
	_appController   beego.Controller
	_context         *context.Context
	_authCred        *facade.AuthCredential
	_elastic         facade.ElasticHandler
	_jsonData        facade.JsonHandler
	_uploadFiles     []facade.UploadFile
	_webSocketMode   bool
	_responseDataMap map[string]interface{}
	_logger          facade.LogEvent
	_appConfig       facade.AppConfig
}

func NewZxContext(logger facade.LogEvent, sharedAppData *map[string]interface{}) ZxContext {
	return ZxContext{
		_authCred: &facade.AuthCredential{},
		_handler:  NewZxHandler(logger),
		_logger:   logger,
	}
}

func (c *ZxContext) SetWebSocketMode(mode bool) {
	c._webSocketMode = mode
}

func (c *ZxContext) GetAuthResponseMap(isSuccess bool, token string, cred string, message string, record_count int, data interface{}, errorMessage string) map[string]interface{} {
	code := ""
	if isSuccess {
		code = "0000I"
	} else {
		code = "0001E"
	}
	responseStruct := c.bindAuthJsonResponse(isSuccess, token, cred, code, message, record_count, data, errorMessage)
	jsonMap := structs.Map(responseStruct)
	return jsonMap
}

func (c *ZxContext) GetResponseMap(isSuccess bool, message string, record_count int, data interface{}, errorMessage string) map[string]interface{} {
	code := ""
	if isSuccess {
		code = "0000I"
	} else {
		code = "0001E"
	}
	responseStruct := c.bindJsonResponse(isSuccess, code, message, record_count, data, errorMessage)
	jsonMap := structs.Map(responseStruct)
	return jsonMap
}

func (c *ZxContext) bindJsonResponse(success bool, code string, message string, record_count int, data interface{}, errorMessage string) facade.JsonResponse {
	response := facade.JsonResponse{}
	response.Success = success
	response.Code = code
	response.Data = nil
	response.Message = message
	if record_count > 0 {
		response.RecordCount = record_count
	} else {
		response.RecordCount = 0
	}
	if data != nil {
		response.Data = data
	} else {
		switch data.(type) {
		case []interface{}:
			response.Data = utils.Container{}
		default:
			response.Data = c._getBlankJson()
		}
	}
	response.Error = errorMessage
	return response
}

func (c *ZxContext) bindAuthJsonResponse(success bool, token string, cred string, code string, message string, record_count int, data interface{}, errorMessage string) facade.AuthJsonResponse {
	response := facade.AuthJsonResponse{}
	response.Success = success
	response.Code = code
	response.Data = nil
	if token != "" && len(token) > 0 {
		response.TokenId = token
	} else {
		response.TokenId = ""
	}
	if cred != "" && len(cred) > 0 {
		response.Credential = cred
	} else {
		response.Credential = ""
	}
	response.Message = message
	if record_count > 0 {
		response.RecordCount = record_count
	} else {
		response.RecordCount = 0
	}
	if data != nil {
		response.Data = data
	} else {
		response.Data = c._getBlankJson()
	}
	response.Error = errorMessage
	return response
}

// ========================================================
// Custom Response
func (c *ZxContext) ReplyCustom(version string, data interface{}, message facade.Message, action interface{}) {
	response := facade.JsonCustomResponse{}
	response.Version = version
	response.Data = data
	response.Message = message
	response.Action = action
	jsonMap := structs.Map(response)
	c.ResponseJson(jsonMap)
}

func (c *ZxContext) ReplyWithMessage(err error, msg string, data interface{}, total int) {
	count := 0
	if data != nil {
		if d, ok := data.([]interface{}); ok {
			count = len(d)
		} else {
			count = 1
		}
	}
	if total > 0 {
		count = total
	}
	c._logger.OnDemandTrace("[Username : %s] : %s", c.AuthCred().UserName, msg)
	if err == nil {
		c.ResponseJson(c.GetResponseMap(true, msg, count, data, ""))
	} else {
		c.ResponseJson(c.GetResponseMap(false, "", 0, nil, msg))
	}
}

func (c *ZxContext) Reply(err error, module string, functionName string, data interface{}, total int) {
	count := 0
	if data != nil {
		if d, ok := data.([]interface{}); ok {
			count = len(d)
		} else {
			count = 1
		}
	}
	if total > 0 {
		count = total
	}
	if err == nil {
		c._logger.OnDemandTrace("[Username : %s] : [%s] MSC.%s : Execute successfully.", c.AuthCred().UserName, module, functionName)
		c.ResponseJson(c.GetResponseMap(true, fmt.Sprintf("[%s] MSC.%s : Execute successfully.", module, functionName), count, data, ""))
	} else {
		c._logger.OnDemandTrace("[Username : %s] : [%s] MSC.%s : Execute  failed.", c.AuthCred().UserName, module, functionName)
		c.ResponseJson(c.GetResponseMap(false, "", 0, nil, fmt.Sprintf("[%s] MSC.%s : Execute failed.", module, functionName)))
	}
}

func (c *ZxContext) GetInputParam(name string) string {
	return c._context.Input.Param(name)
}

func (c *ZxContext) ResponseJsonStatus(status int, json string) {
	c._context.Output.SetStatus(status)
	c._context.Output.Header("Content-Type", "application/json; charset=utf-8")
	stringHandler := impl.NewStringImpl()
	if stringHandler.IsNotEmptyString(json) {
		c._context.Output.Body([]byte(json))
	}
	return
}

func (c *ZxContext) GetResponseDataMap() map[string]interface{} {
	if c._responseDataMap != nil {
		return c._responseDataMap
	} else {
		return make(map[string]interface{})
	}
}

func (c *ZxContext) ResponseJson(res map[string]interface{}) {
	if c._webSocketMode {
		c._responseDataMap = res
	} else {
		c._appController.Data["json"] = res
		c._appController.ServeJSON()
	}
	return
}

func (c *ZxContext) GetJsonDataService() facade.JsonHandler {
	jsonHandler := c._context.Input.GetData("jsonservice").(facade.JsonHandler)
	jsonByte := jsonHandler.GetDataStruct("data")
	return NewZxJsonImpl(jsonByte)
}

func (c *ZxContext) GetErrorMessage(errorCode string, errorDesc string) map[string]interface{} {
	mp := map[string]interface{}{
		"error":             errorCode,
		"error_description": errorDesc,
	}
	return mp
}

func (c *ZxContext) AcceptResponse(data interface{}) {
	c._context.Output.Header("Content-Type", "application/json; charset=utf-8")
	c._context.Output.SetStatus(202)
	c._context.Output.Body([]byte(data.(string)))
}

func (c *ZxContext) Response(status int, data interface{}) {
	response := facade.ErrorResponse{}
	response.ErrorCode = status
	if data != nil {
		response.ErrorMessage = data
	} else {
		response.ErrorMessage = c._getBlankJson()
	}
	jsonMap := structs.Map(response)
	if c._webSocketMode {
		c._responseDataMap = jsonMap
	} else {
		b, err := json.Marshal(jsonMap)
		c._context.Output.Header("Content-Type", "application/json; charset=utf-8")
		if err != nil {
			c._context.Output.SetStatus(500)
			s := fmt.Sprintf("{\"error_code\": 500, \"error_message\": \"Invalide response data.\"}")
			c._context.Output.Body(c._handler.String(false).GetByteArray(s))
		} else {
			c._context.Output.SetStatus(status)
			c._context.Output.Body(b)
		}
	}
}

func (c *ZxContext) ReplyAsync(id string) {
	c._context.Output.Header("Location", id)
	c._context.Output.SetStatus(202)
}

func (c *ZxContext) ReplyMessage(replyUrl string, accessToken string, replyToken string, data []interface{}) {
	message := facade.LineMessage{}
	message.ReplyToken = replyToken
	if data != nil {
		message.MessageData = data
	} else {
		message.MessageData = utils.Container{}
	}
	jsonMap := structs.Map(message)
	//b, err := json.Marshal(jsonMap)
	//beego.Debug(err)
	c._context.Output.Header("User-Agent", "LINE-Smart/1.1")
	resty.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", "Bearer "+accessToken).
		SetHeader("User-Agent", "LINE-Smart/1.1").
		SetBody(jsonMap).
		Post(replyUrl)
	c.Response(200, "")
}

func (c *ZxContext) AuthCred() *facade.AuthCredential {
	return c._authCred
}

func (c *ZxContext) JsonData() facade.JsonHandler {
	return c._jsonData
}

func (c *ZxContext) _getBlankJson() interface{} {
	b := []byte(`{}`)
	var data interface{}
	json.Unmarshal(b, &data)
	return data
}

func (c *ZxContext) SetUploadFiles(uploadFiles []facade.UploadFile) {
	c._uploadFiles = uploadFiles
}

func (c *ZxContext) GetFile(index int) ([]byte, error) {
	if c._uploadFiles != nil && (index > 0) {
		uploadFile := c._uploadFiles[index-1]
		return c._handler.File(true).ReadFile(uploadFile.FilePath)
	} else {
		return nil, errors.New("No file in upload.")
	}
}

func (c *ZxContext) GetFileDescriptor(index int) facade.UploadFile {
	if c._uploadFiles != nil && (index > 0) {
		return c._uploadFiles[index-1]
	} else {
		return facade.UploadFile{}
	}
}
