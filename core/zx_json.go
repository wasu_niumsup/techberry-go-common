package core

import (
	"bytes"
	"encoding/json"
	"errors"
	"strconv"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"

	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

type ZxJsonImpl struct {
	_data []byte
}

func NewZxJsonImpl(data []byte) facade.JsonHandler {
	var p facade.JsonHandler = &ZxJsonImpl{_data: data}
	return p
}

func (c *ZxJsonImpl) IsValidJson() bool {
	var js map[string]interface{}
	return json.Unmarshal(c._data, &js) == nil
}

func (c *ZxJsonImpl) Delete(path string) {
	output, _ := sjson.Delete(string(c._data), path)
	c._data = []byte(output)
}

func (c *ZxJsonImpl) SetBool(value bool, path string) {
	newValue := strconv.FormatBool(value)
	c.Set(newValue, path)
}

func (c *ZxJsonImpl) SetInt(value int64, path string) {
	newValue := strconv.FormatInt(value, 10)
	c.Set(newValue, path)
}

func (c *ZxJsonImpl) SetFloat(value float64, path string) {
	newValue := strconv.FormatFloat(value, 'g', 1, 64)
	c.Set(newValue, path)
}

func (c *ZxJsonImpl) SetString(value string, path string) {
	newValue := strconv.Quote(value)
	c.Set(newValue, path)
}

func (c *ZxJsonImpl) Set(value interface{}, path string) {
	output, _ := sjson.SetBytes(c._data, path, value)
	c._data = output
}

func (c *ZxJsonImpl) SetBlank(path string) {
	c.Set(`{}`, path)
}

func (c *ZxJsonImpl) SetBlankArray(path string) {
	c.Set(`[]`, path)
}

func (c *ZxJsonImpl) GetData() []byte {
	return c._data
}

func (c *ZxJsonImpl) GetDataStruct(path string) []byte {
	var json []byte = c._data
	result := gjson.GetBytes(c._data, path)
	var raw []byte
	if result.Index > 0 {
		raw = json[result.Index : result.Index+len(result.Raw)]
	} else {
		raw = []byte(result.Raw)
	}
	return raw
}

func (c *ZxJsonImpl) Exists(path string) bool {
	return gjson.Get(string(c._data), path).Exists()
}

func (c *ZxJsonImpl) Value(path string) interface{} {
	return gjson.Get(string(c._data), path).Value()
}

func (c *ZxJsonImpl) IsArray(path string) bool {
	return gjson.Get(string(c._data), path).IsArray()
}

func (c *ZxJsonImpl) IsObject(path string) bool {
	return gjson.Get(string(c._data), path).IsObject()
}

func (c *ZxJsonImpl) String(defaultValue string, path string) string {
	// Default = ""
	stringHandler := impl.NewStringImpl()
	r := gjson.Get(string(c._data), path).String()
	if stringHandler.IsEmptyString(r) && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxJsonImpl) Bool(defaultValue bool, path string) bool {
	// Default = false
	r := gjson.Get(string(c._data), path).Bool()
	if r == false && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxJsonImpl) Int(defaultValue int64, path string) int64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Int()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxJsonImpl) UInt(defaultValue uint64, path string) uint64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Uint()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxJsonImpl) Float(defaultValue float64, path string) float64 {
	// Default = 0
	r := gjson.Get(string(c._data), path).Float()
	if r == 0 && r != defaultValue {
		return defaultValue
	} else {
		return r
	}
}

func (c *ZxJsonImpl) GetMany(path ...string) []interface{} {
	results := gjson.GetMany(string(c._data), path...)
	var array []interface{}
	for _, r := range results {
		array = append(array, r)
	}
	return array
}

func (c *ZxJsonImpl) GetArray(path string) (bool, []interface{}) {
	result := gjson.GetBytes(c._data, path)
	if result.IsArray() {
		return true, result.Value().([]interface{})
	} else {
		return false, nil
	}
}

func (c *ZxJsonImpl) GetBlankJson() interface{} {
	b := []byte(`{}`)
	var data interface{}
	json.Unmarshal(b, &data)
	return data
}

func (c *ZxJsonImpl) ToMap() map[string]interface{} {
	m := make(map[string]interface{})
	json.Unmarshal(c._data, &m)
	return m
}

func (c *ZxJsonImpl) _convertPathSyntax(nodes ...string) string {
	jsonPath := bytes.NewBufferString("")
	for indx, key := range nodes {
		if indx > 0 {
			jsonPath.WriteString(".")
		}
		jsonPath.WriteString(key)
	}
	return jsonPath.String()
}

func (c *ZxJsonImpl) DecodingJson() interface{} {
	var m interface{}
	json.Unmarshal(c._data, &m)
	return m
}

func (c *ZxJsonImpl) Binding(v interface{}) error {
	return json.Unmarshal(c._data, v)
}

func (c *ZxJsonImpl) FromMap(inputMap map[string]interface{}) (interface{}, error) {
	var data interface{}
	jsonData, err := json.Marshal(inputMap)
	if err != nil {
		return data, errors.New("Error encoding JSON.")
	}
	json.Unmarshal(jsonData, &data)
	return data, nil
}
