package core

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"techberry-go/common/v2/facade"

	"contrib.go.opencensus.io/exporter/zipkin"
	openzipkin "github.com/openzipkin/zipkin-go"
	zipkinHTTP "github.com/openzipkin/zipkin-go/reporter/http"
	"go.opencensus.io/trace"
)

const (
	TraceIDHeader = "X-TB-TraceId"
	SpanIDHeader  = "X-TB-SpanId"
	SampledHeader = "X-TB-Sampled"
)

type ZxHttpSpanPlugin struct {
	_logger facade.Logger
}

type ZxZipkin struct {
	_root    *facade.TraceModel
	_parent  *facade.TraceModel
	_last    *facade.TraceModel
	_logger  facade.Logger
	_request *http.Request
}

type ZipkinSpanImpl struct {
	_span            *trace.Span
	_logger          facade.Logger
	_traceAttributes []trace.Attribute
}

func newZipkinSpanImpl(logger facade.Logger, span *trace.Span) facade.Span {
	var p facade.Span = &ZipkinSpanImpl{_span: span, _logger: logger}
	return p
}

func NewZxZipkin(logger facade.Logger) ZxZipkin {
	root := &facade.TraceModel{}
	parent := &facade.TraceModel{}
	last := &facade.TraceModel{}

	return ZxZipkin{_root: root, _parent: parent, _last: last, _logger: logger}
}

func (c *ZxZipkin) Initize(serviceName string, serviceHostPort string, spanHost string) {
	localEndpoint, err := openzipkin.NewEndpoint(serviceName, serviceHostPort)
	if err != nil {
		log.Printf("Failed to create Zipkin exporter: %v", err)
	} else {
		log.Print("Initialize zipkin exporter successfully.")
	}
	// spanHost Ex. trace.partsoto.net
	zipkinSpanHostUrl := fmt.Sprintf("https://%s/api/v2/spans", spanHost)
	reporter := zipkinHTTP.NewReporter(zipkinSpanHostUrl)
	exporter := zipkin.NewExporter(reporter, localEndpoint)
	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
}

func (c *ZxZipkin) SetRootContext(ctx context.Context, sc *trace.SpanContext) {
	c._last.Context = ctx
	if sc != nil {
		c._last.SpanId = sc.SpanID.String()
		c._last.TraceId = sc.TraceID.String()
	}
}

func (c *ZxZipkin) RootModel() *facade.TraceModel {
	return c._root
}

func (c *ZxZipkin) ParentModel() *facade.TraceModel {
	return c._parent
}

func (c *ZxZipkin) LastModel() *facade.TraceModel {
	return c._last
}

func (c *ZxZipkin) RootCtx() context.Context {
	return c._root.Context
}

func (c *ZxZipkin) ParentCtx() context.Context {
	return c._parent.Context
}

func (c *ZxZipkin) LastCtx() context.Context {
	return c._last.Context
}

/*ctx, span = trace.StartSpanWithRemoteParent(ctx, name, sc,
  trace.WithSampler(startOpts.Sampler),
  trace.WithSpanKind(trace.SpanKindServer))

*/
func (c *ZxZipkin) StartSpan(name string, o ...trace.StartOption) facade.Span {
	return c.Span(c._last.Context, name, o...)
}

func (c *ZxZipkin) Span(ctx context.Context, name string, o ...trace.StartOption) facade.Span {
	if ctx == nil {
		log.Print("Start span with root.")
		// Create new context background for root
		ctx = context.Background()
		c._root = &facade.TraceModel{
			Context: ctx,
		}
		// Start span for root trace
		traceContext, span := trace.StartSpan(ctx, name, o...)
		// Save span information (TraceId and SpanId) in RootModel.
		c._root.TraceId = span.SpanContext().TraceID.String()
		c._root.SpanId = span.SpanContext().SpanID.String()
		// // Create new span for latest trace model.
		c._last = &facade.TraceModel{
			Context: traceContext,
			TraceId: span.SpanContext().TraceID.String(),
			SpanId:  span.SpanContext().SpanID.String(),
		}
		return newZipkinSpanImpl(c._logger, span)
	} else {
		log.Print("Start span for child.")
		// Move last to parent.
		c._parent = c._last
		traceContext, span := trace.StartSpan(ctx, name, o...)
		// Create new span for latest trace model.
		c._last = &facade.TraceModel{
			Context:      traceContext,
			TraceId:      c._parent.TraceId,
			SpanId:       span.SpanContext().SpanID.String(),
			ParentSpanId: c._parent.ParentSpanId,
			Sampled:      c._parent.Sampled,
		}
		return newZipkinSpanImpl(c._logger, span)
	}
}

func (c *ZipkinSpanImpl) SetTagMap(m map[string]interface{}) facade.Span {
	for k, v := range m {
		c.SetTag(k, v)
	}
	return c
}

func (c *ZipkinSpanImpl) SetTag(key string, value interface{}) facade.Span {
	switch f := value.(type) {
	case string:
		attr := trace.StringAttribute(key, f)
		c._traceAttributes = append(c._traceAttributes, attr)
	case int64:
		attr := trace.Int64Attribute(key, f)
		c._traceAttributes = append(c._traceAttributes, attr)
	case float64:
		attr := trace.Float64Attribute(key, f)
		c._traceAttributes = append(c._traceAttributes, attr)
	default:
		// errors.New("Value datatype is not supported.")
	}
	return c
}

func (c *ZipkinSpanImpl) AddTag() facade.Span {
	c._span.AddAttributes(c._traceAttributes...)
	return c
}

func (c *ZipkinSpanImpl) SetStatus(code int32, message string) facade.Span {
	c._span.SetStatus(trace.Status{
		Code:    code,
		Message: message,
	})
	return c
}

func (c *ZipkinSpanImpl) Finish() {
	c._span.End()
}
