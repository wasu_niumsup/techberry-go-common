package core

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"techberry-go/common/v2/core/lcs"
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/impl"
	"time"
)

type NetworkInfo struct {
	IpAddress         string `structs:"n4" json:"n4" bson:"n4"`
	InterfaceName     string `structs:"n5" json:"n5" bson:"n5"`
	MacAddress        string `structs:"n6" json:"n6" bson:"n6"`
	PhysicalHwAddress string `structs:"n7" json:"n7" bson:"n7"`
}

type LicenseRequest struct {
	NetworkInfo
	Uuid     string `structs:"n0" json:"n0" bson:"n0"`
	Hostname string `structs:"n1" json:"n1" bson:"n1"`
	Port     int    `structs:"n2" json:"n2" bson:"n2"`
	AppKey   string `structs:"n3" json:"n3" bson:"n3"`
}

type LicenseFeature struct {
	Type    string `structs:"f1" json:"f1" bson:"f1"`
	Maximum int    `structs:"f2" json:"f2" bson:"f2"`
	Used    int    `structs:"f3" json:"f3" bson:"f3"`
}

type LicenseInfo struct {
	Customer          string           `structs:"z1" json:"z1" bson:"z1"`
	Type              string           `structs:"z2" json:"z2" bson:"z2"`
	Start             string           `structs:"z3" json:"z3" bson:"z3"`
	Expire            string           `structs:"z4" json:"z4" bson:"z4"`
	DaysAlert         int              `structs:"z5" json:"z5" bson:"z5"`
	Activated         string           `structs:"z6" json:"z6" bson:"z6"`
	LastUsed          string           `structs:"z7" json:"z7" bson:"z7"`
	Hostname          string           `structs:"z8" json:"z8" bson:"z8"`
	Port              int              `structs:"z9" json:"z9" bson:"z9"`
	AppUuid           string           `structs:"z10" json:"z10" bson:"z10"`
	IpAddress         string           `structs:"z11" json:"z11" bson:"z11"`
	MacAddress        string           `structs:"z12" json:"z12" bson:"z12"`
	PhysicalHwAddress string           `structs:"z13" json:"z13" bson:"z13"`
	Uuid              string           `structs:"z14" json:"z14" bson:"z14"`
	Features          []LicenseFeature `structs:"ff" json:"ff" bson:"ff"`
}

type ZxLicense struct {
	_licFeatures     []LicenseFeature
	_path            string
	_licenseLocation string
	_networkInfo     *NetworkInfo
	_isLicenseAlert  bool
	_logger          facade.LogEvent
}

const privateKey = "FD7YCAYBAEFXA22DN5XHIYLJNZSXEAP7QIAACAQBANIHKYQBBIAACAKEAH7YIAAAAAFP7AYFAEBP7BQAAAAP7GP7QIAWCBGN6ORY6Z5WIKFZFG7STNUTRVPAVVJRZVU4QVSUVS35G2ISFMSL6R6XKUD26NC35DCWCCD2PADYNVVLUHOZKTUVFWAT56RVYKXYNHA3KNJHGRAHFLSNYBHN5TJ7C7NXPAD3VEIHXP55HVBGSAOOHGT3N63WAEYQFKQHBOEATFDUI66C7XIJMXQV6GZ73QUHIXY5NIKTBF4CM2VHAU5GTQXCY6EJW7Q44JGH4QYOB2PSZYAA====%"
const publicKey = "ATG7HI4PM63EFC4STPZJW2JY2XQK2UY422OIKZKKZN6TNEJCWJF7I7LVKB5PGRN6RRLBBB5HQB4G22V2DXMVJ2KS3AJ67I24FL4GTQNVGUTTIQDSVZG4ATW6ZU7RPW3XQB52SED3X66T2QTJAHHDTJ5W7N3A===="
const saltKey = "ZXDCFV$%^T*G)BGH"

var (
	LICENSE_FILENAME       = "tb.lic"
	LICENSE_STATE_FILENAME = "tb.ls"
	LICENSE_STATE_LOCATION = "/var/tmp"
)

func NewZxLicense(logger facade.LogEvent) ZxLicense {
	return ZxLicense{
		_logger: logger,
	}
}

func (c *ZxLicense) SetPath(locationPath string) {
	if StringUtils.IsNotEmptyString(locationPath) {
		c._licenseLocation = LICENSE_STATE_LOCATION // Default location
	} else {
		c._licenseLocation = locationPath
	}
}

func (c *ZxLicense) _writeState() error {
	err := c._validateOS()
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (c *ZxLicense) _validateOS() error {
	os := runtime.GOOS
	switch os {
	case "windows":
		return errors.New("Unsupported.")
	case "darwin":
		return errors.New("Unsupported.")
	case "linux":
		return nil
	default:
		return errors.New("Unsupported.")
	}
}

func (c *ZxLicense) _getHostName() string {
	name, err := os.Hostname()
	if err != nil {
		return ""
	}
	return name
}

func (c *ZxLicense) _extractNetworkInfo(ipAddress string) (*NetworkInfo, error) {
	networkInfo := new(NetworkInfo)
	addrs, err := net.InterfaceAddrs()

	if err != nil {
		return nil, errors.New("Generate license request failed.")
	}

	var currentIP, currentNetworkHardwareName string

	for _, address := range addrs {

		// check the address type and if it is not a loopback the display it
		// = GET LOCAL IP ADDRESS
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				if ipnet.IP.String() == ipAddress {
					networkInfo.IpAddress = ipnet.IP.String()
					currentIP = networkInfo.IpAddress
					break
				}
			}
		}
	}
	if StringUtils.IsEmptyString(currentIP) {
		return nil, errors.New("Generate license request failed.")
	}

	interfaces, _ := net.Interfaces()
	for _, interf := range interfaces {
		if addrs, err := interf.Addrs(); err == nil {
			for _, addr := range addrs {
				// only interested in the name with current IP address
				if strings.Contains(addr.String(), currentIP) {
					currentNetworkHardwareName = interf.Name
					break
				}
			}
		}
	}
	if StringUtils.IsNotEmptyString(currentNetworkHardwareName) {
		// extract the hardware information base on the interface name
		// capture above
		netInterface, err := net.InterfaceByName(currentNetworkHardwareName)

		if err != nil {
			return nil, errors.New("Generate license request failed.")
		} else {
			networkInfo.InterfaceName = netInterface.Name
			networkInfo.MacAddress = netInterface.HardwareAddr.String()
		}

		// verify if the MAC address can be parsed properly
		hwAddr, err := net.ParseMAC(networkInfo.MacAddress)

		if err != nil {
			return nil, errors.New("Generate license request failed.")
		} else {
			networkInfo.PhysicalHwAddress = hwAddr.String()
		}
		return networkInfo, nil
	} else {
		return nil, errors.New("Generate license request failed.")
	}
}

func (c *ZxLicense) IsActivated() bool {
	files, _ := filepath.Glob(c._licenseLocation + "/.b962-*")
	if len(files) > 1 {
		return false
	} else if len(files) == 1 {
		return true
	}
	return false
}

func (c *ZxLicense) Validate(app_uuid string) error {
	licInfo, err := c._loadLicense()
	if err != nil {
		return errors.New(fmt.Sprintf("VAL0001 : %s", facade.LIC_VAL0001))
	} else {
		if app_uuid != licInfo.AppUuid {
			return errors.New(fmt.Sprintf("VAL0002 : %s", facade.LIC_VAL0002))
		}
		if status := c._isActivateFileExist(licInfo.Uuid); !status {
			return errors.New(fmt.Sprintf("VAL0003 : %s", facade.LIC_VAL0003))
		}
		if err := c._validateHostCredential(licInfo); err != nil {
			return errors.New(fmt.Sprintf("VAL0004 : %s", facade.LIC_VAL0004))
		}
		if status := c._isLicenseExpire(licInfo); !status {
			return errors.New(fmt.Sprintf("VAL0005 : %s", facade.LIC_VAL0005))
		}
		c._isLicenseAlert = c._validateDaysAlert(licInfo)

		// Update license last used date
	}
	return nil
}

func (c *ZxLicense) _validateHostCredential(licInfo *LicenseInfo) error {
	hostname := c._getHostName()
	if !(hostname == licInfo.Hostname) {
		return errors.New(fmt.Sprintf("VHC0001 : %s", facade.LIC_VHC0001))
	}
	n, err := c._extractNetworkInfo(licInfo.IpAddress)
	if err != nil {
		return errors.New(fmt.Sprintf("VHC0002 : %s", facade.LIC_VHC0002))
	}
	if !(n.MacAddress == licInfo.MacAddress) {
		return errors.New(fmt.Sprintf("VHC0003 : %s", facade.LIC_VHC0003))
	}
	if !(n.PhysicalHwAddress == licInfo.PhysicalHwAddress) {
		return errors.New(fmt.Sprintf("VHC0004 : %s", facade.LIC_VHC0004))
	}
	return nil
}

func (c *ZxLicense) _loadLicense() (*LicenseInfo, error) {
	fileUtils := NewZxFileImpl(c._logger)
	licenseData, err := fileUtils.ReadFile(c._licenseLocation + "tb.lic")
	if err != nil {
		licInfo := LicenseInfo{}
		// unmarshal the document.
		if err := json.Unmarshal(licenseData, &licInfo); err != nil {
			return nil, err
		} else {
			return &licInfo, nil
		}
	} else {
		return nil, err
	}
}

func (c *ZxLicense) _isLicenseExpire(licInfo *LicenseInfo) bool {
	start := licInfo.Start
	expire := licInfo.Expire

	timeUtils := impl.NewTimeImpl()
	s, err := timeUtils.ParseDateTime("2006-01-02 15:04:05", start)
	if err != nil {
		return false
	}
	e, err := timeUtils.ParseDateTime("2006-01-02 15:04:05", expire)
	if err != nil {
		return false
	}
	s_millis := timeUtils.TimeMillisFromDate(s)
	e_millis := timeUtils.TimeMillisFromDate(e)
	current := timeUtils.CurrentTimeMillis()
	if current >= s_millis && current <= e_millis {
		return true
	}

	return false
}

func (c *ZxLicense) _validateDaysAlert(licInfo *LicenseInfo) bool {
	expire := licInfo.Expire

	timeUtils := impl.NewTimeImpl()
	e, err := timeUtils.ParseDateTime("2006-01-02 15:04:05", expire)
	if err != nil {
		return false
	}
	daysDiff := timeUtils.DayDiffFromNow(e.Year(), timeUtils.MonthIndex(e.Month()), e.Day(), e.Hour(), e.Minute(), e.Second())

	if daysDiff <= licInfo.DaysAlert {
		return true
	}

	return false
}

func (c *ZxLicense) _isActivateFileExist(uuid string) bool {
	fileUtils := NewZxFileImpl(c._logger)

	s := fmt.Sprintf("%s/.b962-%s", c._licenseLocation, uuid)

	if fileUtils.IsDirectoryOrPathExists(s) {
		return true
	} else {
		return false
	}
}

func (c *ZxLicense) AddFeature(featureType string, maximum int, used int) {
	f := LicenseFeature{}
	c._licFeatures = append(c._licFeatures, f)
}

/*
	Activate Infomation.
		+ License Credential Information
		+ Company Name
		+ License type : (msa : microservice, server)
		+ License Start Date
		+ License Exire Date
		+ License days alert before expire
		After activated
		+ License Activated Date
		+ Additional (License last used date)
*/
func (c *ZxLicense) Generate(raw_license string, customer string, licenseType string, startDate string, expireDate string, daysAlertBeforeExpire int) (string, error) {
	// Unmarshal the public key.
	publicKey, err := lcs.PublicKeyFromB32String(publicKey)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0001 : %s", facade.LIC_GLI0001))
	}

	// Unmarshal the customer license.
	lic, err := lcs.LicenseFromB32String(raw_license)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0002 : %s", facade.LIC_GLI0002))
	}

	// validate the license signature.
	if ok, err := lic.Verify(publicKey); err != nil {
		return "", errors.New(fmt.Sprintf("GLI0003 : %s", facade.LIC_GLI0001))
	} else if !ok {
		return "", errors.New(fmt.Sprintf("GLI0004 : %s", facade.LIC_GLI0004))
	}

	lr := LicenseRequest{}
	// unmarshal the document.
	if err := json.Unmarshal(lic.Data, &lr); err != nil {
		return "", errors.New(fmt.Sprintf("GLI0005 : %s", facade.LIC_GLI0005))
	}

	li := LicenseInfo{}
	li.Uuid = lr.Uuid
	li.Customer = customer               // Added
	li.Type = licenseType                // Added
	li.Start = startDate                 // Added
	li.Expire = expireDate               // Added
	li.DaysAlert = daysAlertBeforeExpire // Added
	li.Hostname = lr.Hostname
	li.Port = lr.Port
	li.AppUuid = lr.AppKey
	li.IpAddress = lr.IpAddress
	li.MacAddress = lr.MacAddress
	li.PhysicalHwAddress = lr.PhysicalHwAddress
	li.Features = c._licFeatures // Added

	b, err := json.Marshal(li)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0006 : %s", facade.LIC_GLI0006))
	}

	privateKey, err := lcs.PrivateKeyFromB32String(privateKey)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0007 : %s", facade.LIC_GLI0007))
	}
	licenseService, err := lcs.NewLicense(privateKey, b)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0008 : %s", facade.LIC_GLI0007))
	}
	lk, err := licenseService.ToB32String()
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLI0009 : %s", facade.LIC_GLI0008))
	}
	return lk, nil
}

/*
	Activate Infomation.
		+ License Credential Information
		+ Company Name
		+ License type : (msa : microservice, server)
		+ License Start Date
		+ License Exire Date
		+ License days alert before expire
		After activated
		+ License Activated Date
		+ Additional (License last used date)
*/

func (c *ZxLicense) Activate(app_uuid string, license string) error {
	licInfo := LicenseInfo{}
	// unmarshal the document.
	if err := json.Unmarshal([]byte(license), &licInfo); err != nil {
		return err
	}
	if app_uuid != licInfo.AppUuid {
		return errors.New(fmt.Sprintf("ACL0001 : %s", facade.LIC_ACL0001))
	}
	if status := c._isActivateFileExist(licInfo.Uuid); !status {
		return errors.New(fmt.Sprintf("ACL0002 : %s", facade.LIC_ACL0002))
	}
	if err := c._validateHostCredential(&licInfo); err != nil {
		return errors.New(fmt.Sprintf("ACL0003 : %s", facade.LIC_ACL0003))
	}
	if status := c._isLicenseExpire(&licInfo); !status {
		return errors.New(fmt.Sprintf("ACL0004 : %s", facade.LIC_ACL0004))
	}
	c._isLicenseAlert = c._validateDaysAlert(&licInfo)

	t := time.Now()
	activateDate := t.Format("2006-01-02 15:04:05")
	saltKeyByte := []byte(saltKey)
	aes := impl.NewAesCryptoManagerImpl()
	crypt := aes.Encrypt(saltKeyByte, activateDate)
	appUuid := licInfo.AppUuid
	if StringUtils.IsEmptyString(appUuid) {
		s := fmt.Sprintf("%s/.b962-%s", c._licenseLocation, appUuid)
		fileUtils := NewZxFileImpl(c._logger)
		fileUtils.WriteFile(s, []byte(crypt))
		return nil
	} else {
		return errors.New(fmt.Sprintf("ACL0005 : %s", facade.LIC_ACL0005))
	}
}

// License Credential Infomation Required.
/*
	AppKey
	Hostname
	Port
	IP Address
	InterfaceName // Should be by pass.
	MacAddress
	PhysicalHwAddress
*/
func (c *ZxLicense) GenerateLicenseRequest(app_uuid string, ipAddress string, port int) (string, error) {
	lr := LicenseRequest{}
	uuid := StringUtils.GenerateUuid(false)

	hostname := c._getHostName()
	if StringUtils.IsEmptyString(hostname) {
		return "", errors.New(fmt.Sprintf("GLR0001 : %s", facade.LIC_GLR0001))
	}
	lr.Hostname = hostname
	lr.Port = port
	n, err := c._extractNetworkInfo(ipAddress)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLR0002 : %s", facade.LIC_GLR0002))
	}
	lr.NetworkInfo = *n
	/*
		files, _ := filepath.Glob(c._licenseLocation + "/.b962-*")
		if len(files) > 1 {
			return "", errors.New("GLR0003 : Generate license request failed.")
		} else if len(files) == 1 {
			data, err := fileUtils.ReadFile(files[0])
			if err != nil {
				return "", errors.New("GLR0004 : Generate license request failed.")
			} else {
				return string(data), nil
			}
		}
	*/
	lr.Uuid = uuid
	b, err := json.Marshal(lr)
	if err != nil {
		c._logger.Debug().MsgError(err)
		return "", err
	}
	privateKey, err := lcs.PrivateKeyFromB32String(privateKey)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLR0003 : %s", facade.LIC_GLR0003))
	}
	l, err := lcs.NewLicense(privateKey, b)
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLR0004 : %s", facade.LIC_GLR0004))
	}
	lk, err := l.ToB32String()
	if err != nil {
		return "", errors.New(fmt.Sprintf("GLR0005 : %s", facade.LIC_GLR0005))
	}
	return lk, nil

	/*
		s := fmt.Sprintf("%s/.b962-%s", c._licenseLocation, uuid)
		err = fileUtils.WriteFile(s, []byte(lk))
		if err == nil {
			return lk, nil
		} else {
			return "", errors.New("GLR0008 : Generate license request failed.")
		}
	*/
}
