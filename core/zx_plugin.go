package core

import (
	"techberry-go/common/v2/facade"
)

type ZxPlugin struct {
	_orm     map[string]facade.OrmHandler
	_elastic map[string]facade.ElasticHandler
	_df      map[string]facade.DataFactory
}

func NewZxPlugin() facade.Plugin {
	orm := make(map[string]facade.OrmHandler)
	elastic := make(map[string]facade.ElasticHandler)
	var p facade.Plugin = &ZxPlugin{_orm: orm, _elastic: elastic}
	return p
}

func (c *ZxPlugin) SetOrm(alias string, orm facade.OrmHandler) {
	c._orm[alias] = orm
}

func (c *ZxPlugin) Orm(alias string) facade.OrmHandler {
	return c._orm[alias]
}

func (c *ZxPlugin) SetElastic(alias string, elastic facade.ElasticHandler) {
	c._elastic[alias] = elastic
}

func (c *ZxPlugin) Elastic(alias string) facade.ElasticHandler {
	return c._elastic[alias]
}

func (c *ZxPlugin) SetDataFactory(alias string, df facade.DataFactory) {
	c._df[alias] = df
}

func (c *ZxPlugin) DataFactory(alias string) facade.DataFactory {
	return c._df[alias]
}
