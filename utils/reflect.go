package utils

import (
	"errors"
	"fmt"
	"reflect"

	"github.com/astaxie/beego"
)

func IsMethodValid(any interface{}, name string) bool {
	method := reflect.ValueOf(any).MethodByName(name)
	beego.Debug("Method : ", method)
	return method.IsValid()
}

func to_struct_ptr(obj interface{}) interface{} {

	fmt.Println("obj is a", reflect.TypeOf(obj).Name())

	// Create a new instance of the underlying type
	vp := reflect.New(reflect.TypeOf(obj))

	// Should be a *Cat and Cat respectively
	fmt.Println("vp is", vp.Type(), " to a ", vp.Elem().Type())

	vp.Elem().Set(reflect.ValueOf(obj))

	// NOTE: `vp.Elem().Set(reflect.ValueOf(&obj).Elem())` does not work

	// Return a `Cat` pointer to obj -- i.e. &obj.(*Cat)
	return vp.Interface()
}

func test_ptr(ptr interface{}) {
	v := reflect.ValueOf(ptr)
	fmt.Println("ptr is a", v.Type(), "to a", reflect.Indirect(v).Type())
}

func ptr(v reflect.Value) reflect.Value {
	pt := reflect.PtrTo(v.Type()) // create a *T type.
	pv := reflect.New(pt.Elem())  // create a reflect.Value of type *T.
	pv.Elem().Set(v)              // sets pv to point to underlying value of v.
	return pv
}

// Invoke - result, err := Invoke(AnyStructInterface, MethodName, Params...)
func InvokePlugin(c interface{}, name string, args ...interface{}) (interface{}, error) {
	st := reflect.TypeOf(c)
	_, ok := st.MethodByName(name)
	if !ok {
		return reflect.ValueOf(c), errors.New("No method found.")
	}
	method := reflect.ValueOf(c).MethodByName(name)
	methodType := method.Type()
	numIn := methodType.NumIn()
	if numIn > len(args) {
		return reflect.ValueOf(nil), fmt.Errorf("Method %s must have minimum %d params. Have %d", name, numIn, len(args))
	}
	if numIn != len(args) && !methodType.IsVariadic() {
		return reflect.ValueOf(nil), fmt.Errorf("Method %s must have %d params. Have %d", name, numIn, len(args))
	}
	in := make([]reflect.Value, len(args))
	for i := 0; i < len(args); i++ {
		var inType reflect.Type
		if methodType.IsVariadic() && i >= numIn-1 {
			inType = methodType.In(numIn - 1).Elem()
		} else {
			inType = methodType.In(i)
		}
		argValue := reflect.ValueOf(args[i])
		if !argValue.IsValid() {
			return reflect.ValueOf(nil), fmt.Errorf("Method %s. Param[%d] must be %s. Have %s", name, i, inType, argValue.String())
		}
		argType := argValue.Type()
		if argType.ConvertibleTo(inType) {
			in[i] = argValue.Convert(inType)
		} else {
			return reflect.ValueOf(nil), fmt.Errorf("Method %s. Param[%d] must be %s. Have %s", name, i, inType, argType)
		}
	}
	valueReturn := method.Call(in)
	if valueReturn != nil && len(valueReturn) == 2 {
		result := valueReturn[0].Interface()
		result_error := valueReturn[1].Interface()
		if result_error != nil {
			err := result_error.(error)
			return result, err
		} else {
			return result, nil
		}
	} else {
		return nil, nil
	}
}

// Invoke - result, err := Invoke(AnyStructInterface, MethodName, Params...)
func Invoke(c interface{}, name string, args ...interface{}) (reflect.Value, error) {
	st := reflect.TypeOf(c)
	_, ok := st.MethodByName(name)
	if !ok {
		return reflect.ValueOf(c), errors.New("No method found.")
	}
	method := reflect.ValueOf(c).MethodByName(name)
	methodType := method.Type()
	numIn := methodType.NumIn()
	if numIn > len(args) {
		return reflect.ValueOf(nil), fmt.Errorf("Method %s must have minimum %d params. Have %d", name, numIn, len(args))
	}
	if numIn != len(args) && !methodType.IsVariadic() {
		return reflect.ValueOf(nil), fmt.Errorf("Method %s must have %d params. Have %d", name, numIn, len(args))
	}
	in := make([]reflect.Value, len(args))
	for i := 0; i < len(args); i++ {
		var inType reflect.Type
		if methodType.IsVariadic() && i >= numIn-1 {
			inType = methodType.In(numIn - 1).Elem()
		} else {
			inType = methodType.In(i)
		}
		argValue := reflect.ValueOf(args[i])
		if !argValue.IsValid() {
			return reflect.ValueOf(nil), fmt.Errorf("Method %s. Param[%d] must be %s. Have %s", name, i, inType, argValue.String())
		}
		argType := argValue.Type()
		if argType.ConvertibleTo(inType) {
			in[i] = argValue.Convert(inType)
		} else {
			return reflect.ValueOf(nil), fmt.Errorf("Method %s. Param[%d] must be %s. Have %s", name, i, inType, argType)
		}
	}
	valueReturn := method.Call(in)
	if valueReturn != nil && len(valueReturn) > 0 {
		return method.Call(in)[0], nil
	} else {
		return reflect.ValueOf(nil), nil
	}
}

func FieldValue(v interface{}, field string) interface{} {
	r := reflect.ValueOf(v)
	if !r.IsNil() {
		f := reflect.Indirect(r).FieldByName(field)
		return f
	}
	return nil
}

var errFieldNotExists = errors.New("Field does not exists")

func isFieldZero(f reflect.Value) bool {
	// zero value of the given field
	// For example: reflect.Zero(reflect.TypeOf(42)) returns a Value with Kind Int and value 0
	zero := reflect.Zero(f.Type()).Interface()

	return reflect.DeepEqual(f.Interface(), zero)
}

func ModelFields(v reflect.Value) []reflect.StructField {
	v = Indirect(v)
	t := v.Type()

	var fs []reflect.StructField

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)

		// Only exported fields of a struct can be accessed.
		// So, non-exported fields will be ignored
		if f.PkgPath == "" {
			fs = append(fs, f)
		}
	}

	return fs
}

func StructValue(s interface{}) (reflect.Value, error) {
	if s == nil {
		return reflect.Value{}, errors.New("Invalid input <nil>")
	}

	sv := Indirect(ValueOf(s))

	if !IsStruct(sv) {
		return reflect.Value{}, errors.New("Input is not a struct")
	}

	return sv, nil
}

func GetField(sv reflect.Value, name string) (reflect.Value, error) {
	field := sv.FieldByName(name)
	if !field.IsValid() {
		return reflect.Value{}, fmt.Errorf("Field: '%v', does not exists", name)
	}

	return field, nil
}

func ZeroOf(f reflect.Value) reflect.Value {

	// get zero value for type
	ftz := reflect.Zero(f.Type())

	if f.Kind() == reflect.Ptr {
		return ftz
	}

	// if not a pointer then get zero value for interface
	return Indirect(ValueOf(ftz.Interface()))
}

func DeepTypeOf(v reflect.Value) reflect.Type {
	if IsInterface(v) {

		// check zero or not
		if !isFieldZero(v) {
			v = ValueOf(v.Interface())
		}

	}

	return v.Type()
}

func ValueOf(i interface{}) reflect.Value {
	return reflect.ValueOf(i)
}

func Indirect(v reflect.Value) reflect.Value {
	return reflect.Indirect(v)
}

func IsPtr(v reflect.Value) bool {
	return v.Kind() == reflect.Ptr
}

func IsStruct(v reflect.Value) bool {
	if IsInterface(v) {
		v = ValueOf(v.Interface())
	}

	pv := Indirect(v)

	// struct is not yet initialized
	if pv.Kind() == reflect.Invalid {
		return false
	}

	return pv.Kind() == reflect.Struct
}

func IsInterface(v reflect.Value) bool {
	return v.Kind() == reflect.Interface
}

func ExtractType(x interface{}) reflect.Type {
	return reflect.TypeOf(x).Elem()
}
