package utils

import "fmt"

func AssertTypeOf(v interface{}) string {
	return fmt.Sprintf("%T", v)
}
