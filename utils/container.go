package utils

type Container []interface{}

func (c *Container) Put(elem interface{}) {
	*c = append(*c, elem)
}

func (c *Container) Get(index int) interface{} {
	elem := (*c)[index]
	return elem
}
