module techberry-go/common/v2

go 1.15

require (
	contrib.go.opencensus.io/exporter/zipkin v0.1.2
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/units v0.0.0-20201120081800-1786d5ef83d4 // indirect
	github.com/astaxie/beego v1.12.3
	github.com/aws/aws-sdk-go v1.35.36 // indirect
	github.com/buger/jsonparser v1.0.0
	github.com/confluentinc/confluent-kafka-go v1.5.2 // indirect
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/fatih/structs v1.1.0
	github.com/go-redis/redis/v8 v8.4.0
	github.com/go-resty/resty/v2 v2.3.0
	github.com/godror/godror v0.20.9
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0
	github.com/jmoiron/sqlx v1.2.1-0.20200615141059-0794cb1f47ee
	github.com/klauspost/compress v1.11.3 // indirect
	github.com/kolo/xmlrpc v0.0.0-20201022064351-38db28db192b
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.8.0
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/mitchellh/mapstructure v1.4.0
	github.com/nxadm/tail v1.4.5 // indirect
	github.com/olivere/elastic/v7 v7.0.22
	github.com/openzipkin/zipkin-go v0.2.5
	github.com/pkg/errors v0.9.1
	github.com/prometheus/common v0.15.0 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/segmentio/ksuid v1.0.3
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/tidwall/gjson v1.6.3
	github.com/tidwall/match v1.0.2 // indirect
	github.com/tidwall/sjson v1.1.2
	github.com/ugorji/go/codec v1.2.3
	go.mongodb.org/mongo-driver v1.4.3
	go.opencensus.io v0.22.5
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392 // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
	golang.org/x/sys v0.0.0-20201130171929-760e229fe7c5 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/grpc v1.33.2 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.5.2
	gopkg.in/hlandau/passlib.v1 v1.0.10
	gopkg.in/jeevatkm/go-model.v1 v1.1.0
	gopkg.in/ldap.v2 v2.5.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/resty.v1 v1.12.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)

replace (
	github.com/astaxie/beego => ../../astaxie_beego
	github.com/mitchellh/mapstructure => ../../mitchellh_mapstructure
	github.com/olivere/elastic/v7 => ../../olivere_elastic_v7
)
